$("#cat_name").on("blur", function (e) {
    e.preventDefault();
    $('#btnSubmit').on('click');

})

function showPopupAdd(className, data) {
    if (data) {
        fillData(data)
    } else {
        $('#branch_title').text('Thêm mới chi nhánh')
        $('#modal_btn').text('Thêm mới')
    }
    console.log($('#pId').text())
    $('#' + className).modal('show');
}

function showPopupDel(className, data) {
    if (data) {
        $('#branch_name').text(data.name)
        $('#branch_id').text(data.id)
    }
    $('#' + className).modal('show');
}

function fillData(data) {
    if (data.id) {
        $('#pId').text(data.id);
        $('#branch_title').text('Chỉnh sửa chi nhánh')
        $('#modal_btn').text('Sửa')
    }
    $('#name').val(data.name);
    $('#pAddress').val(data.address);
    $('#pTypeStore').val(data.type_store);
    $('#pType').val(data.type);
}


$(document).ready(function () {

    $('#uploadFile').click(function (event) {
        var frm = new FormData();
        frm.append("file", $("#file")[0].files[0]);
        $('input[type=file]').val('');
        $.ajax({
            url: '/TAdmin/danh-muc/import',
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: frm,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                if (res && res.status === true) {
                    $('#exampleModal').modal('hide');
                    notify('ki ki-check icon-nm', 'success', 'Message Success', res.message);
                    setTimeout(function () {
                        window.location.href = "Orders/";
                    }, 3000);

                } else {

                    $('#exampleModal').modal('hide');
                    notify('ki ki-check icon-nm', 'danger', 'Message Success', res.message);
                    setTimeout(function () {
                        window.location.href = "Orders/";
                    }, 3000);

                }

                // $("#btn"+name).css('pointer-events','all');
                // $("#btn"+name).css('opacity', '1');
            },
            error: function (res, status, error) {
                if (res && res.message) {
                    window.location.href = "Orders/";
                    notify('ki ki-close icon-nm', 'danger', 'Error', res.message);

                }
            }
        });
    });

});


function AddBranch() {
    let id = "";
    let data = {
        'name': $('#name').val(),
        'type_store': $('#pTypeStore').val(),
        'type': $('#pType').val(),
        'created_at': $('#created_at').val(),
        'updated_at': $('#created_at').val(),
        'address': $('#pAddress').val(),
        'phone': null,
        'email': null,
        'quantity_staff': null,
        'manager_name': null,
    }
    if ($('#pId').text()) {
        id = $('#pId').text()
        data.id = id
    }

    if (!isEmpty($('#name').val()) || !isEmpty($('#pAddress').val())) {
        $('#error_name').css({ display: "none" });
            $.ajax({
            url: `/TAdmin/chi-nhanh/them-moi`,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data,
            success: function (res) {
                if (res && res.status === true) {
                    $('#btnSubmit').removeAttr('disabled', 'disabled');
                    $.notify(res.message, "success");
                    $('#editCustomer').modal('hide');
                } else {
                    $.notify(res.message, "error");
                    $('#btnSubmit').removeAttr('disabled', 'disabled');
                }
                $('#branches_add').modal('hide');
                location.reload();
            },
        });
    }
    else {
        if (isEmpty($('#name').val())) {
            $('#error_name').css({ display: "block" });
            $('#error_name').text("Vui lòng nhập trường này");
        }
        if (isEmpty($('#pAddress').val())) {
            $('#error_address').css({ display: "block" });
            $('#error_address').text("Vui lòng nhập trường này");
        }
    }
}

function DeleteBranch() {
    let id = $('#branch_id').text();
    $.ajax({
        url: `/TAdmin/chi-nhanh/xoa`,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            'id': id,
        },
        success: function (res) {
            $('#branches_delete').modal('hide');
            location.reload();
        },
    });
}