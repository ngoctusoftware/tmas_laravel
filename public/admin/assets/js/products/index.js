function deleted(id) {
    Swal.fire({
        title: "Bạn chắc chắn muốn xóa sản phẩm này ?",
        icon: "question",
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: "Chấp nhận",
        cancelButtonText: "Hủy!",
        customClass: {
            confirmButton: "btn btn-danger mx-2",
            cancelButton: "btn btn-secondary mx-2",
        },
    }).then(function (result) {
        if (result.value === true) {
            $.ajax({
                url: `/TAdmin/san-pham/deleted/${id}`,
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
                success: function (res) {
                    if (res && res.status == true) {
                        $.notify(res.message, "success");
                        location.reload();
                    } else {
                        $.notify(res.message, "error");
                    }
                },
                error: function (error) {
                    if (error.message) {
                        $.notify(res.message, "error");
                    }
                },
            });
        }
    });
}
function showModalImport() {
    $("#prd_import_excel").modal("show");
}

$(document).ready(function () {
    $("#uploadFile").click(function (event) {
        var frm = new FormData();
        frm.append("file", $("#file")[0].files[0]);
        $("input[type=file]").val("");
        $.ajax({
            url: "/TAdmin/san-pham/upload-by-excel",
            type: "post",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: frm,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                if (res && res.status === true) {
                    $.notify(res.message, "success");
                    $("#prd_import_excel").modal("hide");
                } else {
                    $.notify(res.message, "danger");
                    $("#prd_import_excel").modal("hide");
                }
            },
            error: function (error) {
                if (error) {
                    $.notify(error.message, "success");
                }
            },
        });
    });
});
const table = $("#basic-datatable").DataTable();
$("#searchProduct").on("keyup", function () {
    table.search(this.value).draw();
});
