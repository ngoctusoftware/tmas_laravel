$("#type").select2({
    theme: "bootstrap-5",
    width: $(this).data("width")
        ? $(this).data("width")
        : $(this).hasClass("w-100")
        ? "100%"
        : "style",
    placeholder: $(this).data("placeholder"),
});

$("#is_featured").select2({
    theme: "bootstrap-5",
    width: $(this).data("width")
        ? $(this).data("width")
        : $(this).hasClass("w-100")
        ? "100%"
        : "style",
    placeholder: $(this).data("placeholder"),
});

$("#tags_news").select2({
    theme: "bootstrap-5",
    width: $(this).data("width")
        ? $(this).data("width")
        : $(this).hasClass("w-100")
        ? "100%"
        : "style",
    placeholder: $(this).data("placeholder"),
});

$("#pStatus").select2({
    theme: "bootstrap-5",
    width: $(this).data("width")
        ? $(this).data("width")
        : $(this).hasClass("w-100")
        ? "100%"
        : "style",
    placeholder: $(this).data("placeholder"),
});

function closeModal() {
    $("#post_import").modal("hide");
}

function save(id) {
    let images = "";
    let image = "";
    let dbUrl = "";
    let newUrl = "";
    // Lấy đường dẫn ảnh từ db:
    let dbClassName = document.getElementsByClassName("old_images");
    if (dbClassName.length > 0) {
        dbUrl = getArrayImage(dbClassName, 0, dbClassName.length);
    }
    // Lấy đường dẫn ảnh từ upload
    let newClassName = document.getElementsByClassName("inputUrl");
    if (newClassName.length > 0) {
        newUrl = getArrayImage(
            newClassName,
            dbClassName.length,
            newClassName.length + dbClassName.length
        );
    }
    if (dbUrl.length > 0) {
        images = "{" + dbUrl + "," + newUrl + "}";
    } else {
        images = "{" + newUrl + "}";
    }
    image = getImage(dbClassName);
    if (image === null) {
        image = getImage(newClassName);
    }

    var contents = CKEDITOR.instances["content"].getData();
    var data = {
        name: $("#name").val(),
        code: $("#code").val(),
        slug: $("#slug").val(),
        tag: $("#tag").val(),
        image: image,
        images: images,
        contents: contents,
        description: $("#description").val(),
        info: getDataOption('info'),
        special:  $("#special").val(),
        operation:  $("#operation").val(),
        video:  $("#video").val(),
        price: getDataOption('price'),
        featured: $("#featured").val(),
        warranty: getDataOption('warranty'),
        cat_id: +$("#cat_id").val(),
        status: $("#prdStatus").val(),
        optionName: getDataOption('optionName'),
        deleted: 0,
        meta_title: $("#meta_title").val(),
        meta_keywords: $("#meta_keywords").val(),
        meta_description: $("#meta_description").val(),
        meta_og_url: $("#meta_og_url").val(),
        order: $("#order").val(),
    };
    console.log(data);
    $.ajax({
        url: `../../TAdmin/san-pham/sua-chi-tiet/${id}`,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        data: data,
        success: function (res) {
            if (res.status === true) {
                $.notify(res.message, "success");
            } else {
                $.notify(res.message, "error");
            }
        },
        error: function (err) {
            $.notify(err.message, "success");
        },
    });
}
function getArrayImage(className, index, length) {
    let mUrl = "";
    className.forEach((item) => {
        if (className.length == 1) {
            mUrl = mUrl + `"${index}":"${item.value}"`;
        } else {
            if (index < length - 1) {
                mUrl = mUrl + `"${index}":"${item.value}",`;
            }
            if (index == length - 1) {
                mUrl = mUrl + `"${index}":"${item.value}"`;
            }
        }
        index += 1;
    });

    return mUrl;
}

function getImage(className) {
    let mUrl = "";
    className.forEach((item, index) => {
        if (index == 0) mUrl = `${item.value}`;
    });

    return mUrl;
}

function update() {
    let images = "";
    let image = "";
    let dbUrl = "";
    let newUrl = "";
    // Lấy đường dẫn ảnh từ db:
    let dbClassName = document.getElementsByClassName("old_images");
    if (dbClassName.length > 0) {
        dbUrl = getArrayImage(dbClassName, 0, dbClassName.length);
    }
    // Lấy đường dẫn ảnh từ upload
    let newClassName = document.getElementsByClassName("inputUrl");
    if (newClassName.length > 0) {
        newUrl = getArrayImage(
            newClassName,
            dbClassName.length,
            newClassName.length + dbClassName.length
        );
    }
    if (dbUrl.length > 0) images = "{" + dbUrl + "," + newUrl + "}";
    else images = "{" + newUrl + "}";
    image = getImage(newClassName);

    var contents = CKEDITOR.instances["content"].getData();
    var data = {
        name: $("#name").val(),
        code: $("#code").val(),
        slug: $("#slug").val(),
        tag: $("#tag").val(),
        image: image,
        images: images,
        contents: contents,
        description: $("#description").val(),
        price: getDataOption('price'),
        info: getDataOption('info'),
        special:  $("#special").val(),
        operation:  $("#operation").val(),
        video:  $("#video").val(),
        featured:  $("#featured").val(),
        warranty: getDataOption('warranty'),
        cat_id: +$("#cat_id").val(),
        status: $("#prdStatus").val(),
        optionName: getDataOption('optionName'),
        deleted: 0,
        meta_title: $("#meta_title").val(),
        meta_keywords: $("#meta_keywords").val(),
        meta_description: $("#meta_description").val(),
        meta_og_url: $("#meta_og_url").val(),
        order: $("#order").val(),
    };
    $.ajax({
        url: `../../TAdmin/san-pham/them-san-pham`,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        data: data,
        success: function (res) {
            if (res.status === true) {
                $.notify(res.message, "success");
            } else {
                $.notify(res.message, "error");
            }
        },
        error: function (err) {
            $.notify(err.message, "success");
        },
    });
}

function onSlug(e) {
    if (e.keyCode == 13) {
        $("#slug").val(slugify($("#name").val()));
    }
}
function onGetSlug() {
    $("#slug").val(slugify($("#name").val()));
}

function ShowModal() {
    $("#post_import").modal("show");
    var className = $(".old_images").val();
}
function uploadFile() {
    if ($("#file").val() != "") {
        $("#uploadFile").removeAttr("disabled");
    }
}

function changDeletedStatus(id) {
    let status = $("#prdStatus").val();
    let data = "";

    if (id) {
        if (status != 2) {
            data = {
                deleted: 0,
            };
        } else {
            if (status == 2) {
                data = {
                    deleted: 1,
                };
            }
        }
        $.ajax({
            url: `../../TAdmin/san-pham/chuyen-trang-thai/${id}`,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: data,
            success: function (res) {
                if (res.status === true) {
                    $.notify(res.message, "success");
                } else {
                    $.notify(res.message, "error");
                }
            },
            error: function (err) {
                $.notify(err.message, "success");
            },
        });
    }
}

function onShowModalImportImages() {
    $("#post_import").modal("show");
}
function uploadImage(obj) {
    //Reset input = file
    let bUrl = "{";
    let eUrl = "}";
    let mUrl = "";
    let className = "";
    $("#file").unbind();
    $("#file").click();
    let fd = new FormData();
    $("#file").change(function (e) {
        if (!$("#file").val()) return;
        let files = $("#file")[0].files;
        let i;
        for (i = 0; i < files.length; i++) {
            if (files.length > 0) {
                fd.append("file", files[i]);
                $.ajax({
                    url: "/TAdmin/san-pham/imports",
                    type: "post",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ),
                    },
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function (res) {
                        console.log(res)
                        if (res && res.status === true) {
                            var image = new Image();
                            image.width = 250;
                            image.height = 180;
                            image.title = `../../${public_path ? public_path + "/" : ""}upload/products/${res.filename}`;
                            image.src = `../../${public_path ? public_path + "/" : ""}upload/products/${res.filename}`;
                            image.id = `${res.id}`;
                            image.className = "images";
                            const $div = $(`<div class='col-3 my-2' id='${image.id}'></div>`);
                            const $frame_img = $(`<div class="img_frame_new"></div>`);
                            $div.append($frame_img);
                            $frame_img.append(image);
                            $div.append(`<input type="hidden" value=${image.title} id="input${res.id}" style='width:250px' class='inputUrl pt-2' data-id=${res.id}>`);
                            const closeButton = `<div class="btn_del">
                                <a href="javascript:void(0)" onclick="delImages('../../${public_path ? public_path + "/" : ""}upload/products/${res.filename}', ${res.id}, ${res.id})" class="btnXoaAnh" id='btnXoa_${res.id}' style="z-index:1px;">x</a>
                            </div>`;
                            $(closeButton).appendTo($frame_img);
                            $div.appendTo($("#preview"));
                        } else {
                            alert("File not uploaded");
                        }
                        // $("#newImages").val(bUrl + mUrl + eUrl);
                    },
                });
            }
            // className = document.getElementsByClassName('inputUrl');
            // className.forEach(value => {
            //     return;
            // });
            // let url = getArrayImage(className);
        }
    });
}

function delImages(item, key, id) {
    console.log(item, key, id)
    // let oldImg = $('#dbImage').val();
    Swal.fire({
        title: "Bạn chắc chắn",
        text: "Muốn xóa ảnh sản phẩm này chứ? ",
        imageUrl: item,
        imageWidth: "98%",
        imageHeight: "95%",
        imageAlt: "Custom image",
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Đóng",
        customClass: {
            confirmButton: "btn btn-danger mx-2",
            cancelButton: "btn btn-primary",
        },
    }).then(function (result) {
        if (result.value === true) {
            $(`#${key}`).remove();
            let bUrl = "{";
            let eUrl = "}";
            let mUrl = "";
            let old_images = document.getElementsByClassName("old_images");
            let url = getArrayImage(old_images);
            $(`#imgChange`).val(url);
            let images = $("#imgChange").val();

            $.ajax({
                url: `../../TAdmin/san-pham/xoa-hinh-anh/${id}`,
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    ),
                },
                data: {
                    images: images,
                },
                success: function (res) {
                    if (res && res.status === true) {
                        $.notify(res.message, "success");
                    } else {
                        $.notify(res.message, "danger");
                    }
                },
                error: function (err) {
                    if (err) {
                        $.notify(err.message, "danger");
                    }
                },
            });
        }
    });
}

function generateComponent(number) {
    const componentOption = `
<div class="row py-2 optionRelative" id="optionRelative_${number}">
    <div class="col-sm-4">
        <label class="pr-2">Tên Options ${number}</label><label class="text-danger px-1"> * </label>
        <div class="input-group flex-nowrap">
            <input type="text" class="form-control optionName" placeholder="Nhập tên" name="optionName_${number}" id="optionName_${number}" value="">
        </div>
    </div>
    <div class="col-sm-4">
        <label class="pr-2">Đơn giá ${number}</label><label class="text-danger px-1"> * </label>
        <div class="input-group flex-nowrap">
            <input type="number" class="form-control price" placeholder="Nhập đơn giá" name="price_${number}" id="price_${number}" value="">
            <span class="input-group-text" name="unit">vnđ</span>
        </div>
    </div>
    <div class="col-sm-4">
        <label>Bảo hành ${number}</label><label class="text-danger px-1"> * </label>
        <div class="input-group flex-nowrap">
            <input type="number" class="form-control warranty" placeholder="Nhập số tháng bảo hành" value="" name="warranty_${number}" id="warranty_${number}">
            <span class="input-group-text" name="unit">tháng</span>
        </div>
    </div>
    <div class="col-sm-12">
        <label>Thông số sản phẩm ${number}</label><label class="text-danger px-1"> * </label>
        <div class="input-group flex-nowrap">
            <textarea class="form-control info" placeholder="Nhập thông số sản phẩm" value="" name="info_${number}" id="info_${number}"></textarea>
        </div>
    </div>
    <button class="removeOption" id="removeOption_${number}" onclick="removeOption(${number})" >x</button>
</div>`;
    $("#insert_option").append(componentOption);
}

function addOption() {
    const lastClass = $(".optionName").last().attr("id");
    const count = lastClass.replace(/\D/g, "");
    generateComponent(+count + 1);
}

function getDataOption(className) {
    const inputs = document.querySelectorAll("." + className);
    const values = []; 
    inputs.forEach((input, index) => {
        values.push({[`${className}`]: input.value}); // thêm giá trị của input vào mảng values
    });
    const json = JSON.stringify(values); 
    return json;
}

function removeOption (id) {
    $(`#optionRelative_${id}`).remove()
}

function fillValue() {

}

    let idModel = 25;
    function getListCompany() {
        $.ajax({
            type: "GET",
            url: `/get-list-company`,
        })
            .then((res) => {
                res.res.forEach((element) => {
                    getListModel(element.id,element.name);
                });
                getListModelByProduct(1)
            })
            .catch((err) => {
                console.log("err", err);
            });
    }

    getListCompany();
    
    function getListModel(id, name) {
        $("#select_model").empty();
        $.ajax({
            type: "POST",
            url: `/get-list-model`,
            data: { id },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                    const itemParent = `
                    <h2>${name}</h2>
                    <div id="company_${id}" class="row company_item">
                    </div>
                    <hr>
                    `;
                    $("#model_result").append(itemParent);
                    res.res.forEach((element) => {
                        const item = `<div class="col-2 model_item" id="gr_model_${element.id}">
                            <input onclick="changeStatus(${element.id})" class="form-group check_item_model" type="checkbox" id="model_${element.id}">
                            <label> ${element.name} </label>
                        </div>
                        `;
                        $(`#company_${id}`).append(item);
                    });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    function getListModelByProduct(id) {
        $("#select_model").empty();
        $.ajax({
            type: "POST",
            url: `/get-list-model-by-product`,
            data: { id_product: id },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                console.log(res)
                $('.company_item').each(function(index1, element1) {
                    const id_company = parseInt($(element1).attr('id').replace(/\D/g, ''))
                    $(`#company_${id_company} .model_item`).each(function(index2, element2) {
                        const id_model = parseInt($(element2).attr('id').replace(/\D/g, ''))
                        res.res.forEach(element => {
                           if (element.car_model_id === id_model && element.model_status === 1) {
                                $('#model_' + id_model).prop('checked', true);
                                return false;
                           }
                        });
                    });
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }

    function changeStatus (id) {
        console.log($(`#model_${id}`).is(':checked'))
        changeStatusApi({id_product: 1, id_model: id, status: $(`#model_${id}`).is(':checked') === true ? 1 : 0})
    }

    function changeStatusApi(data) {
        $("#select_model").empty();
        $.ajax({
            type: "POST",
            url: `/change-model-product`,
            data: { id_product: data.id_product, id_model: data.id_model, status: data.status },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                console.log(res)
            })
            .catch((err) => {
                console.log("err", err);
            });
    }