function update(id, type){    
    var data = {
        name: $('#name').val(),
        slug: $('#slug').val(),
        status: $('#cStatus').is(':checked') == true ? 1 : 0,
        deleted: 0,
        created_at: $('#created_at').val(),
        type: type
    }   
    $.ajax({
        url: `/TAdmin/chuyen-muc/chinh-sua/${id}`,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: data,
        success: function (res) {
           if (res.status === true) {
            $.notify(res.message, "success");              
           }
           else {
            $.notify(res.message, "error");  
           }
        },
        error: function(err){
            $.notify(err.message, "success");  
        }
    });
}
function convertSlug(name){    
    $('#slug').val(slugify($('#' + name).val()));
}