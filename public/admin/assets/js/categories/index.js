$("#cat_name").on("blur", function(e) {
    e.preventDefault();    
    $('#btnSubmit').on('click');
    
})

function showPopupImport(className){
    
    $('#' + className).modal('show');
}


$(document).ready(function (){
    
    $('#uploadFile').click(function(event){              
        var frm = new FormData();
        frm.append("file", $("#file")[0].files[0]);    
        $('input[type=file]').val('');
        $.ajax({
            url: '/TAdmin/danh-muc/import',
            type:'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: frm,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,         
            success: function (res) {                      
                 if(res && res.status === true){                       
                    $('#exampleModal').modal('hide');    
                    notify('ki ki-check icon-nm', 'success', 'Message Success', res.message);       
                    setTimeout(function(){
                        window.location.href = "Orders/";
                     }, 3000);
                    
                 }else{     
                    
                    $('#exampleModal').modal('hide');                    
                    notify('ki ki-check icon-nm', 'danger', 'Message Success', res.message); 
                    setTimeout(function(){
                        window.location.href = "Orders/";
                     }, 3000);
                    
                }
                
                // $("#btn"+name).css('pointer-events','all');
                // $("#btn"+name).css('opacity', '1');
            },
            error: function (res, status, error) {
                if (res && res.message) {
                    window.location.href = "Orders/";
                    notify('ki ki-close icon-nm', 'danger', 'Error', res.message);
                    
                }
            }
        });
    });

});

function convertSlug(name){    
    $('#slug').val(slugify($('#' + name).val()));
}

function AddCategories(){
    let type = $('#type').val();    
    switch (type) {
        case '1':
            url = `../../TAdmin/chuyen-muc/them-moi`;
            break;    
        case '2':
            url = `../../TAdmin/nhom-san-pham/them-moi`;
            break;    
        default:
            break;
    }
    let status;
    let deleted;
    if($('#customStatus').is(':checked')==true) status = 1;
    else status = 0;
    if($('#customDeleted').is(':checked')==true) deleted = 1;    
    else deleted = 0;    
    let data = {
        'name': $('#name').val(),
        'slug': $('#slug').val(),
        'created_at': $('#created_at').val(),
        'updated_at': $('#created_at').val(),
        'type': type,
        'status': status,
        'deleted': deleted
    }
    

    if(!isEmpty($('#name').val()) && !isEmpty($('#slug').val()) && !isEmpty($('#created_at').val())){
        
        $('#error_name').css({display: "none"});
        $('#error_slug').css({display: "none"});
        $('#error_created_at').css({display: "none"});
        
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: data,
            success: function (res) {
                if (res && res.status === true) {
                    
                    $('#btnSubmit').removeAttr('disabled', 'disabled');
                    $.notify(res.message, "success");
                    $('#editCustomer').modal('hide');   
                    location.reload();                 
                } else {
                    $.notify(res.message, "error");
                    $('#btnSubmit').removeAttr('disabled', 'disabled');
                }
            },
        });

        
    }
    else {        
        
        if(isEmpty(name)) {
            $('#error_name').css({display: "block"});
            $('#error_name').text("Vui lòng nhập trường này");            
        }
        else {
            $('#error_name').css({display: "none"});
            $('.phancach').css({display: "block"});
        }
        if(isEmpty(slug)) {
            $('#error_slug').css({display: "block"});
            $('#error_slug').text("Vui lòng nhập trường này");                  
        }
        else {
            $('#error_slug').css({display: "none"});
            $('.phancach').css({display: "block"});
        }
        if(isEmpty(created_at)) {
            $('#error_created_at').css({display: "block"});
            $('#error_created_at').text("Vui lòng nhập trường này");       
           
        }
        else {
            $('#error_created_at').css({display: "none"});
            $('.phancach').css({display: "block"});
        }
        $('.phancach').css({display: "none"});     
    }
}

function RemoveCategories(id){
    let type = $('#type').val();
    switch (type) {
        case '2':
            slug = "nhom-san-pham";
            name = 'Nhóm sản phẩm'
            break;
        case '1':
            slug = "chuyen-muc";
            name = 'Chuyên mục'
            break;
        default:
            break;
    }
    Swal.fire({
        title: `Bạn chắc chắn muốn xóa ${name} này`,
        icon: 'question',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: 'Chấp nhận!',
        cancelButtonText: 'Hủy!',
        customClass: {
            confirmButton: 'btn btn-danger mx-2',
            cancelButton: 'btn btn-secondary mx-2'
        }
    }).then(function(result) {
        if (result.value === true) {
            $.ajax({
                url: `/TAdmin/${slug}/xoa-bo/${id}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                success: function(res) {
                    
                    if (res && res.status == true) {
                        $.notify(res.message, "success");   
                        $(`#${id}`).remove();
                    } else {
                        $.notify(res.message, "error");  
                    }
                },
                error: function(error) {
                    if (error.message) {                        
                        $.notify(res.message, "error");  
                    }
                }
            });
        }
    });
}