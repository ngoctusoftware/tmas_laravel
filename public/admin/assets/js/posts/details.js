
$('#type').select2({
    theme: "bootstrap-5",
    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
    placeholder: $(this).data('placeholder'),
});
$('#is_featured').select2({
    theme: "bootstrap-5",
    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
    placeholder: $(this).data('placeholder'),
});
$('#tags_news').select2({
    theme: "bootstrap-5",
    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
    placeholder: $(this).data('placeholder'),
});
$('#pStatus').select2({
    theme: "bootstrap-5",
    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
    placeholder: $(this).data('placeholder'),
});

function closeModal() {
    $('#post_import').modal('hide');
}

function save(id) {
    var content = CKEDITOR.instances['content'].getData()
    var data = {
        name: $('#name').val(),
        slug: $('#slug').val(),
        content: content,
        description: $('#description').val(),
        type: $('#type').val(),
        is_featured: $('#is_featured').val(),
        featured_image: $('#featured_image').val(),
        type_showing: $('#pTypeShowing').val(),
        file: 123,
        meta_og_image: $('#meta_og_image').val(),
        order: $('#order').val(),
        status: $('#pStatus').val(),
        created_by: $('#created_by').val(),
        published_at: $('#published_at').val(),
        created_at: $('#published_at').val(),
        updated_at: $('#published_at').val(),
        meta_title: $('#meta_title').val(),
        meta_keywords: $('#meta_keywords').val(),
        meta_description: $('#meta_description').val(),
        meta_og_url: $('#meta_og_url').val(),
        deleted: $('#deleted').is(':checked') == true ? 1 : 0
    }
    
    console.log(data)
    
    $.ajax({
        url: `/TAdmin/bai-viet/sua-chi-tiet/${id}`,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: data,
        success: function (res) {
            if (res.status === true) {
                $.notify(res.message, "success");
            }
            else {
                $.notify(res.message, "error");
            }
        },
        error: function (err) {
            $.notify(err.message, "success");
        }
    });
}

function update() {

    var content = CKEDITOR.instances['content'].getData()
    var data = {
        name: $('#name').val(),
        slug: $('#slug').val(),
        content: content,
        description: $('#description').val(),
        type: $('#type').val(),
        is_featured: $('#is_featured').val(),
        featured_image: $('#featured_image').val(),
        type_showing: $('#pTypeShowing').val(),
        file: 123,
        meta_og_image: $('#meta_og_image').val(),
        order: $('#order').val(),
        status: $('#pStatus').val(),
        deleted: $('#deleted').is(':checked') == true ? 1 : 0,
        created_by: $('#created_by').val(),
        published_at: $('#published_at').val(),
        created_at: $('#published_at').val(),
        updated_at: $('#published_at').val(),
        meta_title: $('#meta_title').val(),
        meta_keywords: $('#meta_keywords').val(),
        meta_description: $('#meta_description').val(),
        meta_og_url: $('#meta_og_url').val()
    }

    $.ajax({
        url: `../../TAdmin/bai-viet/them-chi-tiet`,
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: data,
        success: function (res) {
            if (res.status === true) {
                $.notify(res.message, "success");

            }
            else {
                $.notify(res.message, "error");
            }
        },
        error: function (err) {
            $.notify(err.message, "success");
        }
    });
}

function onSlug(e) {
    if (e.keyCode == 13) {
        $('#slug').val(slugify($('#name').val()))
    }
}
function onGetSlug() {
    $('#slug').val(slugify($('#name').val()))
}

function ShowModal() {
    $('#post_import').modal('show');
}
function uploadFile() {
    if ($('#file').val() != "") {
        $('#uploadFile').removeAttr('disabled');
    }
}
function uploadImage() {
    var frm = new FormData();
    frm.append("file", $("#file")[0].files[0]);
    $('input[type=file]').val('');
    $.ajax({
        url: '/TAdmin/bai-viet/import',
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: frm,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.status === true) {
                $('#featured_image').val(res.url);
                $('#imgUpload').attr("src", "/" + res.url);
                $.notify(res.message, "success");
            }
            else {
                $.notify(res.message, "error");
            }
        },
        error: function (err) {
            $.notify(err.message, "error");
        }
    });

}
