function deleted(id) {
    Swal.fire({
        title: 'Bạn chắc chắn muốn xóa bài viết này?',
        icon: 'question',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: 'Chấp nhận!',
        cancelButtonText: 'Hủy!',
        customClass: {
            confirmButton: 'btn btn-danger mx-2',
            cancelButton: 'btn btn-secondary mx-2'
        }
    }).then(function(result) {
        if (result.value === true) {
            $.ajax({
                url: `/TAdmin/bai-viet/deleted/${id}`,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                success: function(res) {
                    location.reload()
                    if (res && res.status == true) {
                        $.notify(res.message, "success");                          
                    } else {
                        $.notify(res.message, "error");  
                    }
                },
                error: function(error) {
                    if (error.message) {                        
                        $.notify(res.message, "error");  
                    }
                }
            });
        }
    });
}
function showModalImport(){
    $('#posts_import').modal('show');
}

$(document).ready(function (){
    
    $('#uploadFile').click(function(event){              
        var frm = new FormData();
        frm.append("file", $("#file")[0].files[0]);    
        $('input[type=file]').val('');
        $.ajax({
            url: '/TAdmin/bai-viet/importExcel',
            type:'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            data: frm,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,         
            success: function (res) {                      
                 if(res && res.status === true){                       
                    $('#exampleModal').modal('hide');    
                    notify('ki ki-check icon-nm', 'success', 'Message Success', res.message);       
                    setTimeout(function(){
                        window.location.href = "Orders/";
                     }, 3000);
                    
                 }else{     
                    
                    $('#exampleModal').modal('hide');                    
                    notify('ki ki-check icon-nm', 'danger', 'Message Success', res.message); 
                    setTimeout(function(){
                        window.location.href = "Orders/";
                     }, 3000);
                    
                }
                
                // $("#btn"+name).css('pointer-events','all');
                // $("#btn"+name).css('opacity', '1');
            },
            error: function (res, status, error) {
                if (res && res.message) {
                    window.location.href = "Orders/";
                    notify('ki ki-close icon-nm', 'danger', 'Error', res.message);
                    
                }
            }
        });
    });

});
const table = $("#basic-datatable").DataTable();
$("#searchPost").on("keyup", function () {
    table.search(this.value).draw();
});