$(document).ready(function () {
    function getProductRelated() {
        $.ajax({
            type: "GET",
            url: `/new-related/5`,
        })
            .then((res) => {
                console.log(res);
                let data = res.res;
                data.forEach((item) => {
                    let component = `
                    <div class="card_left_bar_related">
                            <div class="left_bar_related_image">
                                <img src="/${item.featured_image}" alt="${item.name}"/>
                            </div>
                            <div class="left_bar_related_content">
                                <a class="left_bar_related_link" href="/tin-tuc/${item.id}/${item.slug}">${item.name}</a>
                                <p class="left_bar_related_des">${item.description}</p>
                            </div>
                    </div>
                    `;
                    $("#leftside_new_related").append(component);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    getProductRelated();
});
