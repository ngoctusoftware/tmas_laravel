$(document).ready(function () {
    let searchParams = new URLSearchParams(window.location.search);
    let paramId = searchParams.get('loai-san-pham');
    let paramText = searchParams.get('ten-san-pham');
    let queryId = paramId ? paramId : '';
    let queryText = paramText ? paramText : '';
    if (paramId) {
        console.log($('.check_type_product'))
        $('.check_type_product').each(function() {
            console.log(parseInt(paramId))
            if ($(this).val().toString() === paramId) {
                $(this).prop('checked', true);
            }
        })
    }
    if (paramText) {
        $('#search_product').val(paramText)
        $('#search_product_header_desktop').val(paramText)
    }
    getResultSearch(queryId, queryText);

    $('.box_search_product').on('keypress', function (e) {
        if (e.which == 13) {
            getResultSearch('', $('#search_product').val().trim());
        }
    });

    $('.button_search').click(function () {
        getResultSearch('', $('#search_product').val().trim());
    })

    $('.check_type_product').click(function () {
        if ($(this).is(":checked")) {
            getResultSearch($(this).val(), $('#search_product').val().trim());
        } else {
            getResultSearch('', $('#search_product').val().trim());
        }
    })

    $('.box_toggle_btn').click(function () {
        $('.filter_mobile').toggle(200);
    })

    function getResultSearch(queryId, queryText) {
        $('#loading').show();
        $('#result_search_product').empty()
        $.ajax({
            type: "GET",
            url: `/search-product`,
            data: {
                queryId: queryId,
                queryText: queryText,
            }
        }).then(res => {
            let data = res.res;
            if (data.length === 0) {
                let component = `
                    <p class="no_result">Không tìm thấy sản phẩm nào</p>
                `
                $('#result_search_product').append(component);
            } else {
                data.forEach(item => {
                    let component = `
                    <div class="col-sm-12 col-lg-4">
                            <div class="box_image_product">
                                <img class="img_product" src="/${item.image}" alt="${item.slug}">
                            </div>
                            <div class="box_info_product">
                                <a href="${'/san-pham/' + item.id + '/' + item.slug}"><h3 class="title_product text_logo">${item.name}</h3></a>
                                <p class="des_product">${item.description}</p>
                            </div>
                    </div>
                `;
                    $('#result_search_product').append(component);
                });
            }
            $('#loading').hide();
        }).catch(err => {
            $('#loading').hide();
            console.log('err', err)
        })
    }
})