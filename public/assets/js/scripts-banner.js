const HOTSPOTS_CONFIG = [
    
    {
        positions: [
            { imageIndex: 46, xCoord: 268, yCoord: 200 },
            { imageIndex: 47, xCoord: 265, yCoord: 200 },
            { imageIndex: 48, xCoord: 260, yCoord: 200 },
            { imageIndex: 49, xCoord: 255, yCoord: 200 },
            { imageIndex: 1, xCoord: 250, yCoord: 200 },
            { imageIndex: 2, xCoord: 245, yCoord: 200 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/camera360.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cam360",
            description:
                `Camera 360 trên ô tô hỗ trợ người lái xử lý các tình huống khó, hạn chế va chạm khi
                lùi hoặc đỗ xe.
                Thiết kế ô tô thường chỉ có hai gương chiếu hậu có góc nhìn hẹp nên tầm nhìn của
                người lái bị hạn chế và có nhiều điểm mù. Điều này gây khó khăn cho việc kiểm soát đánh
                lái, dễ dẫn đến va chạm với các vật cản hoặc phương tiện di chuyển xung quanh. Camera 360
                có nhiệm vụ hiển thị hình ảnh toàn cảnh xung quanh xe, giúp người lái quan sát được tất cả
                hình ảnh ở các điểm mù, góc khuất mà mắt thường không thể nhìn thấy được thông qua
                gương chiếu hậu.`,
            moreDetailsUrl: "/"
        }
    },
    // {
    //     positions: [
    //         { imageIndex: 1, xCoord: 212, yCoord: 215-40 },
    //         { imageIndex: 2, yCoord: 215, xCoord: 200-40 },
    //         { imageIndex: 3, yCoord: 215, xCoord: 190-40 },
    //         { imageIndex: 4, yCoord: 215, xCoord: 185-40 },
    //         { imageIndex: 5, yCoord: 205, xCoord: 175-40 },
    //         { imageIndex: 6, yCoord: 205, xCoord: 170-40 },
    //     ],
    //     variant: {
    //         images: [
    //             {
    //                 src: "assets/img/banner_product/denotuxa.png",
    //                 alt: "air snorkel"
    //             }
    //         ],
    //         title: "3",
    //         description:
    //             "3",
    //         moreDetailsUrl: "/"
    //     }
    //-40 },
    {
        positions: [
            { imageIndex: 47, xCoord: 265, yCoord: 100 },
            { imageIndex: 48, xCoord: 260, yCoord: 100 },
            { imageIndex: 49, xCoord: 255, yCoord: 100 },
            { imageIndex: 1, xCoord: 250, yCoord: 100 },
            { imageIndex: 2, xCoord: 245, yCoord: 100 },
            { imageIndex: 3, xCoord: 240, yCoord: 100 },
            { imageIndex: 4, xCoord: 235, yCoord: 100 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/canhbaovacham.png",
                    alt: "air snorkel"
                }
            ],
            title: "Hệ thống cảnh báo tiền va chạm Mobileye 630",
            description:
                `Hệ thống cảnh báo tiền va chạm Mobileye 630 giúp giám sát liên tục cung đường phía
                trước xe vào ban ngày, ban đêm và trong các điều kiệu thời tiết để phát hiện phương tiện,
                người đi bộ, người đi xe đạp, xe máy ở khoảng cách lên đến 105m và một góc 40 độ. Hệ
                thống nhận diện những nguy hiểm tiềm tàng và đưa ra những báo động kịp thời nhằm giúp
                tài xế ngăn ngừa hoặc giảm thiểu thiệt hại từ vụ đâm va.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 49, xCoord: 255, yCoord: 160 },
            { imageIndex: 1, xCoord: 250, yCoord: 160 },
            { imageIndex: 2, xCoord: 245, yCoord: 160 },
            { imageIndex: 3, xCoord: 240, yCoord: 160 },
            { imageIndex: 4, xCoord: 235, yCoord: 160 },
            // { imageIndex: 10, xCoord: 150, yCoord: 230-40 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/denotuxa.png",
                    alt: "air snorkel"
                }
            ],
            title: "Đề Nổ Từ Xa Digital CarKey",
            description:
                `Bộ đề nổ từ xa là thiết bị giúp cho việc mở máy, khởi động xe ở khoảng cách xa từ
                10-20m, bật điều hòa, quạt gió, nhằm hạ nhiệt độ và làm mát trong xe trước khi bạn lên xe,
                tính năng này đặc biệt hữu ích khi trời nắng nóng. Đặc biệt với dòng Digital Carkey KTPlus,
                trong trường hợp bạn quên mang chìa khóa, bạn có thể đề nổ xe bằng App trên điện thoại.
                Tính năng mở cửa và khóa cửa thông minh mang lại sự an toàn tuyệt đối cho bạn và gia đình.
                Ngoài ra, sản phẩm dễ dàng sử dụng với phần mềm trên điện thoại thông minh với độ bảo
                mật cao.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 26 - 1, xCoord: 321, yCoord: 198 - 60 },
            { imageIndex: 27 - 1, xCoord: 323, yCoord: 198 - 60 },
            { imageIndex: 28 - 1, xCoord: 325, yCoord: 200 - 60 },
            { imageIndex: 29 - 1, xCoord: 332, yCoord: 200 - 60 },
            { imageIndex: 30 - 1, xCoord: 331, yCoord: 203 - 60 },
            { imageIndex: 31 - 1, xCoord: 330, yCoord: 201 - 60 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/canhbaodiemmu.png",
                    alt: "air snorkel"
                }
            ],
            title: "Hệ thống cảnh báo điểm mù BSM",
            description:
                `Điểm mù trở nên đặc biệt nguy hiểm khi người lái đậu xe vào bãi hoặc rẽ, chuyển làn
                ở các ngã tư. Khi đó, các điểm mù làm người điều khiển xe không thể nhìn thấy những
                phương tiện đang chạy cùng hướng từ phía sau. Đây cũng là nguyên nhân chính gây ra các vụ
                va chạm, tai nạn không mong muốn. Vì thế, lắp hệ thống cảnh báo điểm mù trên ô tô là vô
                cùng cần thiết, giúp người lái dễ dàng quan sát những vị trí bị khuất trong điểm mù.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 20 - 1, xCoord: 164, yCoord: 203 - 60 },
            { imageIndex: 21 - 1, xCoord: 165, yCoord: 202 - 60 },
            { imageIndex: 22 - 1, xCoord: 166, yCoord: 201 - 60 },
            { imageIndex: 23 - 1, xCoord: 168, yCoord: 201 - 60 },
            { imageIndex: 24 - 1, xCoord: 170, yCoord: 201 - 60 },
            { imageIndex: 25 - 1, xCoord: 175, yCoord: 203 - 60 },
            { imageIndex: 26 - 1, xCoord: 180, yCoord: 198 - 60 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/canhbaodiemmu.png",
                    alt: "air snorkel"
                }
            ],
            title: "Hệ thống cảnh báo điểm mù BSM",
            description:
                `Điểm mù trở nên đặc biệt nguy hiểm khi người lái đậu xe vào bãi hoặc rẽ, chuyển làn
                ở các ngã tư. Khi đó, các điểm mù làm người điều khiển xe không thể nhìn thấy những
                phương tiện đang chạy cùng hướng từ phía sau. Đây cũng là nguyên nhân chính gây ra các vụ
                va chạm, tai nạn không mong muốn. Vì thế, lắp hệ thống cảnh báo điểm mù trên ô tô là vô
                cùng cần thiết, giúp người lái dễ dàng quan sát những vị trí bị khuất trong điểm mù.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 12, xCoord: 205, yCoord: 375 - 60 },
            { imageIndex: 13, xCoord: 185, yCoord: 370 - 60 },
            { imageIndex: 14, xCoord: 175, yCoord: 365 - 60 },
            { imageIndex: 15, xCoord: 165, yCoord: 355 - 60 },
            { imageIndex: 16, xCoord: 155, yCoord: 345 - 60 },
            { imageIndex: 17, xCoord: 145, yCoord: 335 - 60 },

            { imageIndex: 33, xCoord: 370, yCoord: 290 - 40 },
            { imageIndex: 34, xCoord: 350, yCoord: 295 - 40 },
            { imageIndex: 35, xCoord: 330, yCoord: 305 - 40 },
            { imageIndex: 36, xCoord: 320, yCoord: 315 - 40 },
            { imageIndex: 37, xCoord: 300, yCoord: 325 - 40 },
            { imageIndex: 38, xCoord: 290, yCoord: 335 - 40 },
            { imageIndex: 39, xCoord: 280, yCoord: 345 - 40 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/apsuatlop.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cảm biến áp suất lốp",
            description:
                `Cảm biến áp suất lốp là một thiết bị điện tử được thiết kế để giám sát áp suất bên
                trong lốp xe, vừa là phụ kiện công nghệ ô tô, cũng là thiết bị đo lường kỹ thuật cần thiết.
                  Tác dụng của cảm biến áp suất lốp là giúp đo lường, kiểm soát nhiệt độ, áp suất bên
                trong lốp xe nhằm đảm bảo an toàn cho xe và người ngồi trong khoang xe.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 12, xCoord: 365, yCoord: 300 },
            { imageIndex: 13, xCoord: 370, yCoord: 292 },
            { imageIndex: 14, xCoord: 375, yCoord: 286 },
            { imageIndex: 15, xCoord: 380, yCoord: 281 },
            { imageIndex: 16, xCoord: 370, yCoord: 277 },
            { imageIndex: 17, xCoord: 360, yCoord: 275 },

            { imageIndex: 33, xCoord: 165, yCoord: 375 - 50 },
            { imageIndex: 34, xCoord: 160, yCoord: 365 - 50 },
            { imageIndex: 35, xCoord: 150, yCoord: 355 - 50 },
            { imageIndex: 36, xCoord: 145, yCoord: 345 - 50 },
            { imageIndex: 37, xCoord: 140, yCoord: 335 - 50 },
            { imageIndex: 38, xCoord: 135, yCoord: 325 - 50 },
            { imageIndex: 39, xCoord: 133, yCoord: 325 - 50 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/apsuatlop.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cảm biến áp suất lốp",
            description:
                `Cảm biến áp suất lốp là một thiết bị điện tử được thiết kế để giám sát áp suất bên
                trong lốp xe, vừa là phụ kiện công nghệ ô tô, cũng là thiết bị đo lường kỹ thuật cần thiết.
                  Tác dụng của cảm biến áp suất lốp là giúp đo lường, kiểm soát nhiệt độ, áp suất bên
                trong lốp xe nhằm đảm bảo an toàn cho xe và người ngồi trong khoang xe.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 9, xCoord: 290, yCoord: 320 - 20 },
            { imageIndex: 10, xCoord: 285, yCoord: 320 - 20 },
            { imageIndex: 11, xCoord: 280, yCoord: 320 - 20 },
            { imageIndex: 12, xCoord: 265, yCoord: 320 - 20 },
            { imageIndex: 13, xCoord: 260, yCoord: 325 - 20 },
            { imageIndex: 14, xCoord: 250, yCoord: 325 - 20 },
            { imageIndex: 15, xCoord: 240, yCoord: 325 - 20 },

            { imageIndex: 34, xCoord: 280, yCoord: 320 - 20 },
            { imageIndex: 35, xCoord: 270, yCoord: 320 - 20 },
            { imageIndex: 36, xCoord: 260, yCoord: 320 - 20 },
            { imageIndex: 37, xCoord: 260, yCoord: 320 - 20 },
            { imageIndex: 38, xCoord: 245, yCoord: 325 - 20 },
            { imageIndex: 39, xCoord: 240, yCoord: 325 - 20 },
            { imageIndex: 40, xCoord: 225, yCoord: 325 - 20 },


        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/bacdandien.png",
                    alt: "air snorkel"
                }
            ],
            title: "Bậc dẫm điện thông minh AOD",
            description:
                `Bậc dẫm điện AOD là loại bậc dẫm cao cấp có thể thu hồi tự động, phù hợp với
                những dòng xe có gầm cao hoặc những dòng xe CUV, SUV.
                Đây là sản phẩm sở hữu công nghệ hiện đại, thông minh và vô cùng tiện dụng với
                trang bị hệ thống cảm biến thông minh. Bậc dẫm điện AOD có thể tự hạ xuống khi mở cửa và
                tự thu vào vào khi xe di chuyển hoặc đóng cửa.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 9, xCoord: 295, yCoord: 240 - 40 },
            { imageIndex: 10, xCoord: 288, yCoord: 240 - 40 },
            { imageIndex: 11, xCoord: 280, yCoord: 245 - 40 },
            { imageIndex: 12, xCoord: 270, yCoord: 235 - 40 },
            { imageIndex: 13, xCoord: 263, yCoord: 235 - 40 },
            { imageIndex: 14, xCoord: 253, yCoord: 235 - 40 },
            { imageIndex: 15, xCoord: 245, yCoord: 235 - 40 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/cuahittudong.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cửa hít tự động Master Door",
            description:
                `Cửa hít hay cửa hít tự động, cửa hít ô tô là một sản phẩm của công nghệ thông minh
                được sản xuất cho các dòng xe ô tô cao cấp. Sản phẩm được dùng để đóng cửa xe một cách
                tự động mà không cần dùng đến lực mạnh. Người dùng chỉ cần tác động một lực nhẹ khi
                đóng cánh cửa, cửa xe sẽ tự động đóng lại một cách nhẹ nhàng, an toàn và dễ dàng. 
                Được thiết kế theo công nghệ thông minh và hiện đại, cửa hít có rất nhiều công dụng
                đối với xe và với người sử dụng.`,
            moreDetailsUrl: "/"
        }
    },
    ,
    {
        positions: [
            { imageIndex: 09, xCoord: 345, yCoord: 240 - 40 },
            { imageIndex: 10, xCoord: 345, yCoord: 240 - 40 },
            { imageIndex: 11, xCoord: 340, yCoord: 245 - 40 },
            { imageIndex: 12, xCoord: 340, yCoord: 235 - 40 },
            { imageIndex: 13, xCoord: 335, yCoord: 235 - 40 },
            { imageIndex: 14, xCoord: 328, yCoord: 235 - 40 },
            { imageIndex: 15, xCoord: 318, yCoord: 235 - 40 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/cuahittudong.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cửa hít tự động Master Door",
            description:
                `Cửa hít hay cửa hít tự động, cửa hít ô tô là một sản phẩm của công nghệ thông minh
                được sản xuất cho các dòng xe ô tô cao cấp. Sản phẩm được dùng để đóng cửa xe một cách
                tự động mà không cần dùng đến lực mạnh. Người dùng chỉ cần tác động một lực nhẹ khi
                đóng cánh cửa, cửa xe sẽ tự động đóng lại một cách nhẹ nhàng, an toàn và dễ dàng. 
                Được thiết kế theo công nghệ thông minh và hiện đại, cửa hít có rất nhiều công dụng
                đối với xe và với người sử dụng.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 24, xCoord: 305 - 25, yCoord: 250 - 40 },
            { imageIndex: 25, xCoord: 273 - 25, yCoord: 250 - 40 },
            { imageIndex: 26, xCoord: 248 - 25, yCoord: 250 - 40 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/copdienthongminh.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cốp điện thông minh Dr. Door/ AOD",
            description:
                `Cốp điện ô tô thông minh là hệ thống đóng mở cốp tự động mà người sử dụng không
                cần dùng lực để nâng và đóng cốp. Phương pháp thực hiện là thay thế ty thủy lực nguyên bản
                thành ty điện và hệ thống điều khiển hoàn toàn tự động. Cốp điện xe ô tô được người dùng
                yêu thích vì có thể tiết kiệm thời gian, sức người và tăng sự đẳng cấp, sang trọng cho xe hơi.`,
            moreDetailsUrl: "/"
        }
    },
    {
        positions: [
            { imageIndex: 24, xCoord: 305 - 25, yCoord: 320 - 40 },
            { imageIndex: 25, xCoord: 273 - 25, yCoord: 320 - 40 },
            { imageIndex: 26, xCoord: 248 - 25, yCoord: 320 - 40 },
        ],
        variant: {
            images: [
                {
                    src: "assets/img/banner_product/copdienthongminh.png",
                    alt: "air snorkel"
                }
            ],
            title: "Cốp điện thông minh Dr. Door/ AOD",
            description:
                `Cốp điện ô tô thông minh là hệ thống đóng mở cốp tự động mà người sử dụng không
                cần dùng lực để nâng và đóng cốp. Phương pháp thực hiện là thay thế ty thủy lực nguyên bản
                thành ty điện và hệ thống điều khiển hoàn toàn tự động. Cốp điện xe ô tô được người dùng
                yêu thích vì có thể tiết kiệm thời gian, sức người và tăng sự đẳng cấp, sang trọng cho xe hơi.`,
            moreDetailsUrl: "/"
        }
    },
];

window.CI360.addHotspots("hotspot-example", HOTSPOTS_CONFIG);

// const container360 = window.CI360._viewers.filter(x => x.id === 'hotspot-example')[0]
// var playing = false
// const playButton = document.querySelector("[data-360-id='play']")
// const stopButton = document.querySelector("[data-360-id='stop']")

// playButton.addEventListener("click", function () {
//     if (playing == true) return
//     playing = true
//     container360.play(150)
// })

// stopButton.addEventListener("click", function () {
//     playing = false
//     container360.stop()
// })

// window.CI360.addHotspots("hotspot-example", HOTSPOTS_CONFIG);

// const container360 = window.CI360._viewers.filter(x => x.id === 'hotspot-example')[0]
// var playing = false
$('.cloudimage-360-modal-more-details').text('Xem chi tiết')
$('.button img').click(function () {
    if ($(window).width() <= 480) {
        $('#hotspot-example').css('top', '-50vh');
    } else {
        $('#hotspot-example').css('top', '-100vh');
        // window.CI360.addHotspots("hotspot-example", HOTSPOTS_CONFIG);
    }
    $('#close_360').toggle()
    $('.button').hide()
    $('.carousel-indicators').hide()
    // $('.hiddenImage').css('bottom', 0)
    // $('#close_select').toggle()
})
$('.banner360').click(function () {
    $('#hotspot-example').css('top', '-100vh');
    $('#close_360').toggle()
})
$('.bannerPano').click(function () {
    handle_MMqLrAhy2oN();
    $('.CloudPano').css('top', '0');
    $('#close_pano').toggle()
})
$('#close_360').click(function () {
    $('.button').show()
    $('#hotspot-example').css('top', '-210vh');
    $('.carousel-indicators').show()
    $('#close_360').hide()
})
$('#close_select').click(function () {
    $('.hiddenImage').css('bottom', '100vh')
    $('#close_select').hide()
})
$('#close_pano').click(function () {
    $('.CloudPano').css('top', '-100vh')
    $('#close_pano').hide()
})