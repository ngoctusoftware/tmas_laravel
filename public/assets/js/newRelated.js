$(document).ready(function () {
    function getNewRelated() {
        $.ajax({
            type: "GET",
            url: `/new-related/3`,
        })
            .then((res) => {
                console.log(res);
                let data = res.res;
                data.forEach((item) => {
                    let component = `
                    <div class="col-lg-4 col-12 space_sm">
                        <div class="card related">
                            <div class="related_image">
                                <img src="/${item.featured_image}" alt="${item.name}"/>
                            </div>
                            <div class="related_content"> 
                                <a class="related_link" href="/tin-tuc/${item.id}/${item.slug}">${item.name}</a>
                            </div>
                        </div>
                    </div>
                    `;
                    $("#new_related").append(component);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    getNewRelated();
});
