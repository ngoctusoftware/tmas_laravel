$(document).ready(function () {
    
});
// $(".card_about").hover(
//     function () {
//         $(this)
//             .find("img")
//             .attr(
//                 "src",
//                 "assets/img/icon_aboutus/" +
//                     $(this).find("img").attr("id") +
//                     "-white.svg"
//             );
//     },
//     function () {
//         $(this)
//             .find("img")
//             .attr(
//                 "src",
//                 "assets/img/icon_aboutus/" +
//                     $(this).find("img").attr("id") +
//                     ".svg"
//             );
//     }
// );
// $(".card_qa").click(function () {
//     $(".answer").hide(300);
//     $(this).find(".answer").show(300);
// });
// $(".cat_id").click(function () {
//     $(".cat_id").removeClass("active");
//     getProduct($(this).attr("id"));
//     drawAnimate($(this).attr("data"));
//     $(this).addClass("active");
// });

//     function getProduct(category) {
//         $("#box_list_product").empty();
//         $.ajax({
//             type: "POST",
//             url: `/product-by-id`,
//             data: { id: category },
//             headers: {
//                 "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
//             },
//         })
//             .then((res) => {
//                 console.log(res);
//                 let data = res.res;
//                 data.forEach((item) => {
//                     const arrayPrice = JSON.parse(item.price);
//                     price = arrayPrice.toLocaleString("vi-VN");
//                     if (typeof arrayPrice === "object" && arrayPrice !== null) {
//                         arrayPrice.sort(function (a, b) {
//                             return a.price - b.price;
//                         });
//                         if (
//                             arrayPrice[0].price !==
//                             arrayPrice[arrayPrice.length - 1].price
//                         ) {
//                             price =
//                                 (+arrayPrice[0].price).toLocaleString("vi-VN") +
//                                 " - " +
//                                 (+arrayPrice[arrayPrice.length - 1]
//                                     .price).toLocaleString("vi-VN");
//                         } else {
//                             price = arrayPrice[0].price.toLocaleString("vi-VN");
//                         }
//                     }
//                     const component = `
//                     <div class="box_product_homepage">
//                         <a href="/san-pham/${item.id}/${item.slug}"">
//                             <div class="card_product">
//                                 <div class="image_box">
//                                     <img class="w-100" src="${item.image}" alt="${item.name}">
//                                 </div>
//                                 <div class="product_content">
//                                     <div class="name_product">
//                                         <p>${item.name}</p>
//                                     </div>
//                                     <div class="price">
//                                         <p>${price}</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </a>
//                     </div>`;
//                     $("#box_list_product").append(component);
//                 });
//                 if (APP_ENV === "local") {
//                     replaceIMG();
//                 } else {
//                     loadImgErr();
//                 }
//                 if (
//                     $("#box_list_product")[0].scrollWidth >
//                     $("#box_list_product").innerWidth()
//                 ) {
//                     $(".list_product").append(
//                         `<div id='next_box_product' onclick="scrollNext('500')"></div><div onclick="scrollNext('-500')" id='prev_box_product'></div>`
//                     );
//                 } else {
//                     $("#next_box_product").remove();
//                     $("#prev_box_product").remove();
//                 }
//             })
//             .catch((err) => {
//                 console.log("err", err);
//             });
//     }
//     getProduct(+$(".item_category p").attr("id"));

//     function drawAnimate(slug) {
//         const canvas = document.getElementById("draw_car");
//         new rive.Rive({
//             src: "assets/riv/car.riv",
//             canvas: canvas,
//             autoplay: true,
//             stateMachines: slug,
//         });
//     }

//     $("#select_cat").click(function () {
//         $(".modal_custom").fadeIn(200);
//     });

//     $(".item_category").click(function () {
//         $(".modal_custom").fadeOut(200);
//         $("#selected_cat").text($(this).text());
//     });
// });
// function scrollNext(number) {
//     $("#box_list_product").animate({ scrollLeft: `+=${+number}` }, 500);
// }
// $("html, body").css("overflow", "hidden");
// $(".main").onepage_scroll({
//     sectionContainer: ".section",
//     animationTime: 800,
//     beforeMove: function (index) {
//         document.removeEventListener("touchmove", touchMoveHandler, {
//             passive: false,
//         });
//     },
//     afterMove: function (index) {
//         if (index === 3) {
//             drawAnimate('start');
//         }
//         document.addEventListener("touchmove", touchMoveHandler, {
//             passive: false,
//         });
//     },
// });
// drawAnimate('start');

// function touchMoveHandler(event) {
//     event.preventDefault();
//     // Thực hiện các hành động khác
// }

// $(".onepage-pagination li a").each(function (index) {
//     $(this).attr(
//         "href",
//         window.location.href + "#" + $(this).attr("data-index")
//     );
// });

// $("#slick_partner").slick({
//     infinite: true,
//     draggable: false,
//     dots: true,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//     autoplay: true,
//     dots: false,
//     autoplaySpeed: 3000,
//     cssEase: "linear",
//     prevArrow: null,
//     nextArrow: null,
//     responsive: [
//         {
//             breakpoint: 1025,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 1,
//                 autoplaySpeed: 2000,
//             },
//         },
//         {
//             breakpoint: 481,
//             settings: {
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//             },
//         },
//     ],
// });

// $(".slick_slider_branch").slick({
//     infinite: true,
//     draggable: true,
//     slidesToShow: 12,
//     slidesToScroll: 1,
//     autoplay: true,
//     dots: false,
//     autoplaySpeed: 3000,
//     cssEase: "linear",
//     prevArrow: null,
//     nextArrow: null,
//     responsive: [
//         {
//             breakpoint: 1025,
//             settings: {
//                 slidesToShow: 6,
//                 slidesToScroll: 1,
//                 autoplaySpeed: 2000,
//             },
//         },
//         {
//             breakpoint: 481,
//             settings: {
//                 slidesToShow: 4,
//                 slidesToScroll: 1,
//             },
//         },
//     ],
// });
// $("#slick_banner_home").slick({
//     infinite: true,
//     draggable: false,
//     dots: true,
//     slidesToShow: 1,
//     slidesToScroll: 1,
//     autoplay: true,
//     autoplaySpeed: 8000,
//     cssEase: "linear",
//     prevArrow: null,
//     nextArrow: null,
// });

// $(".item_category").click(function () {
//     const selected = "#" + $(this).attr("id").replaceAll("-", "");
//     console.log(selected);
//     $(selected).show(300);
//     $(".info_cat").show(300);
//     $(selected);
// });

// function setIntervalTitle(listTitle) {
//     var idx = 0;
//     var len = listTitle.length;
//     function doNext() {
//         var entry = listTitle[idx];
//         $("#auto_load").fadeOut(500, function () {
//             $("#auto_load").empty();
//         });
//         $("#auto_load").fadeIn(500, function () {
//             $("#auto_load").append(entry);
//         });
//         idx++;
//         if (idx < len) {
//         } else {
//             idx = 0;
//         }
//         setTimeout(doNext, 8000);
//     }
//     doNext();
// }

// function getAutoLoad() {
//     $.ajax({
//         type: "GET",
//         url: `/autoload-new`,
//     })
//         .then((res) => {
//             console.log(res);
//             let data = res.res;
//             let array = [];
//             data.forEach((item) => {
//                 let component = `
//                     <a class="link_auto" href="/tin-tuc/${item.id}/${item.slug}">${item.name}</a>
//                     <div class="box_img_auto">
//                         <img src="/${item.featured_image}" alt="${item.name}"/>
//                     </div>
//                 `;
//                 array.push(component);
//             });
//             setIntervalTitle(array);
//         })
//         .catch((err) => {
//             console.log("err", err);
//         });
// }
// getAutoLoad();
