$(document).ready(function () {
    function getProductRelated() {
        $.ajax({
            type: "GET",
            url: `/product-related/5`,
        })
            .then((res) => {
                console.log(res);
                let data = res.res;
                data.forEach((item) => {
                    let component = `
                    <div class="card_left_bar_related">
                            <div class="left_bar_related_image">
                                <img src="/${item.image}" alt="${item.name}"/>
                            </div>
                            <div class="left_bar_related_content">
                                <a class="left_bar_related_link" href="/san-pham/${item.id}/${item.slug}">${item.name}</a>
                                <p class="left_bar_related_des">${item.description}</p>
                            </div>
                    </div>
                    `;
                    $("#leftside_product_related").append(component);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    getProductRelated();
});
