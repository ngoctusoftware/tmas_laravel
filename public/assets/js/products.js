$(document).ready(function () {
    let queryString = window.location.search;
    const currentUrl = window.location.href;
    let params = {};
    if (queryString) {
        queryString = queryString.substring(1);
        let paramArray = queryString.split("&");
        for (let i = 0; i < paramArray.length; i++) {
            let param = paramArray[i].split("=");
            params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, " "));
        }
    }

    let catSlug = params["danh-muc"];
    if (catSlug) {
        $(".cat_id").each(function (index, element) {
            if (catSlug === $(element).attr("data")) {
                $(".cat_id").removeClass("active");
                getProduct($(element).attr("id"));
                $(element).addClass("active");
            }
        });
        $('#description_for_cat').append(catSlug)

    } else {
        getProduct(0);
    }

    $(".cat_id").click(function () {
        $('#description_for_cat').empty()
        $(".cat_id").removeClass("active");
        getProduct($(this).attr("id"));
        $(this).addClass("active");
        let newDanhMucId = $(this).attr("data");
        let newUrl = "/san-pham";
        const slug = $(this).attr("data");
        let temp = {}
        contentDesCat.forEach(element => {
            if (slug === element.slug) {
                temp = element
            }
        });
        console.log(temp.contentAttention)
        $('#description_for_cat').append(temp.contentAttention)
        if (newDanhMucId) {
            let newUrl = catSlug
                ? currentUrl.replace(
                      /([?&]danh-muc=)[^&]+/,
                      "$1" + newDanhMucId
                  )
                : currentUrl +
                  (currentUrl.indexOf("?") !== -1 ? "&" : "?") +
                  "danh-muc=" +
                  newDanhMucId;
            window.history.replaceState(null, null, newUrl);
        } else {
            window.history.replaceState(null, null, newUrl);
        }
    });

    $("#category_select").change(function () {
        $('#description_for_cat').empty()
        const selectedValue = $(this).val();
        console.log(selectedValue);
        getProduct(selectedValue);
        let newDanhMucId = $(this).attr("data");
        let newUrl = "/san-pham";
        if (newDanhMucId) {
            let newUrl = catSlug
                ? currentUrl.replace(
                      /([?&]danh-muc=)[^&]+/,
                      "$1" + newDanhMucId
                  )
                : currentUrl +
                  (currentUrl.indexOf("?") !== -1 ? "&" : "?") +
                  "danh-muc=" +
                  newDanhMucId;
            window.history.replaceState(null, null, newUrl);
        } else {
            window.history.replaceState(null, null, newUrl);
        }
    });

    function getProduct(category) {
        $(".box_product").empty();
        $.ajax({
            type: "POST",
            url: `/product-by-id`,
            data: { id: category },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                let data = res.res;
                const box = `
                <div class="row" id="box_list_product">
                    </div>`;
                $(".box_product").append(box);
                data.forEach((item) => {
                    const arrayPrice = JSON.parse(item.price);
                    price = arrayPrice.toLocaleString("vi-VN");
                    if (typeof arrayPrice === "object" && arrayPrice !== null) {
                        arrayPrice.sort(function (a, b) {
                            return a.price - b.price;
                        });
                        if (
                            arrayPrice[0].price !==
                            arrayPrice[arrayPrice.length - 1].price
                        ) {
                            price =
                                (+arrayPrice[0].price).toLocaleString("vi-VN") +
                                " - " +
                                (+arrayPrice[arrayPrice.length - 1]
                                    .price).toLocaleString("vi-VN");
                        } else {
                            price = (+arrayPrice[0].price).toLocaleString(
                                "vi-VN"
                            );
                        }
                    }
                    const component = `
                    <div class="col-md-4 col-12 space_sm">
                        <a href="/san-pham/${item.id}/${item.slug}"">
                            <div class="card_product">
                                <div class="image_box">
                                    <img class="w-100" src="${item.image}" alt="${item.name}">
                                </div>
                                <div class="product_content">
                                    <div class="name_product">
                                        <p>${item.name}</p>
                                    </div>
                                    <div class="price">
                                        <p>${price} đ</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>`;
                    $("#box_list_product").append(component);
                });
                if (APP_ENV === "local") {
                    replaceIMG();
                } else {
                    loadImgErr();
                }
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
});
