function loadImgErr() {
    $("img").on("error", function () {
        $(this).attr("src", "assets/img/err.svg").css("object-fit", "contain");
    });
}

function replaceIMG() {
    $("img").on("error", function () {
        if ($(this).attr("src").includes("/public")) {
            const newSrc = $(this).attr("src").replace("/public", "");
            $(this).attr("src", newSrc);
        } else {
            // loadImgErr()
        }
    });
}

$(document).ready(function () {
    if (APP_ENV === "local") {
        replaceIMG();
    } else {
        loadImgErr();
    }
    $("#button_search").click(function () {
        $(".search_form").show(300);
        if (window.location.pathname === "/") {
            $(".main").onepage_scroll({
                sectionContainer: null,
            });
        } else {
            $("body").css("overflow", "hidden");
            $("html").css("overflow", "hidden");
        }
    });
    $("#close_search").click(function () {
        $(".search_form").hide(300);
        if (window.location.pathname === "/") {
            $(".main").onepage_scroll({
                sectionContainer: ".section",
            });
        } else {
            $("body").css("overflow-x", "hidden").css("overflow-y", "auto");
            $("html").css("overflow-x", "hidden").css("overflow-y", "auto");
        }
    });

    $("#menu_mobile").click(function () {
        $(".list_menu").show(300);
    });

    $("#close_menu").click(function () {
        $(".list_menu").hide(300);
    });

    $("#search_input").on("input", function () {
        $("#result_search").empty();
        if ($("#search_input").val().length > 2) {
            getSearch($("#search_input").val());
        }
    });

    function getSearch(data) {
        $.ajax({
            url: `/search-text-product`,
            type: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: { textSearch: data },
            success: function (res) {
                console.log(res.data);
                let component = [];
                res.data.forEach((element) => {
                    const item = `
                        <div class="col-12 col-lg-12 card_search_result_product">
                            <div class="image_result_product">
                                <img src="${element.image}" alt="${
                        element.name
                    }" />
                            </div>
                            <div class="box_content_result_product">
                                <div class="name_result_product">
                                    <a href="/san-pham/${element.id}/${
                        element.slug
                    }"><h3> ${element.name} </h3></a>
                                </div>
                                <div class="des_result_product">
                                    <p>Mô tả: <span>${
                                        element.description
                                    }</span> </p>
                                </div>
                                <div class="price_result_product">
                                    <p>Giá niêm yết: <span>${(+element.price).toLocaleString(
                                        "vi-VN"
                                    )}</span> VND</p>
                                </div>
                            </div>
                        </div>
                    `;
                    component.push(item);
                });
                $("#result_search").append(component);
            },
            error: function (error) {},
        });
    }
    $('.tag_item').click(function() {
        $("#result_search").empty();
        getSearch($(this).text())
    })
// breadcrum
    let urlPath = [
        {
            path: "gioi-thieu",
            name: "Giới thiệu",
        },
        {
            path: "san-pham",
            name: "Sản Phẩm",
        },
        {
            path: "tin-tuc",
            name: "Tin tức",
        },
        {
            path: "lien-he",
            name: "Liên hệ",
        },
        {
            path: "tuyen-dung",
            name: "Tuyển dụng",
        },
        {
            path: "bao-hanh-dien-tu",
            name: "Bảo hành điện tử",
        },
        {
            path: "thu-vien-hinh-nen",
            name: "Thư viện hình nền",
        },
    ];

    urlPath &&
        urlPath.forEach((item) => {
            if (location.pathname.split("/")[1] === item.path) {
                $("#page_child_1").text(item.name);
                $("#link_page_child_1").attr("href", "/" + item.path);
            }
            if (location.pathname.split("/")[2]) {
                $("#page_icon_2").show();
                if (location.pathname.split("/")[1] === "san-pham") {
                    $("#page_child_2").text("Chi tiết sản phẩm");
                }
                if (location.pathname.split("/")[1] === "tin-tuc") {
                    $("#page_child_2").text("Chi tiết bài viết");
                }
                if (location.pathname.split("/")[1] === "tuyen-dung") {
                    $("#page_child_2").text("Chi tiết tuyển dụng");
                }
            }
        });
    
});
