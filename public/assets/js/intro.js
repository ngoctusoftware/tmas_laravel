let slideTimer;
const indexPageToolTip = [
    "info",
    "tam-nhin-su-menh",
    "nhung-con-so-an-tuong",
    "giai-thuong",
    "linh-vuc",
    "lich-su-hinh-thanh-va-phat-trien",
    "doi-ngu-va-con-nguoi",
    "footer",
];
const indexPage = [
    "Thông tin về TMAS",
    "Tầm nhìn & Sứ mệnh",
    "Những con số ấn tượng",
    "Giải thưởng",
    "Lĩnh vực hoạt động",
    "Lịch sử hình thành và phát triển",
    "Footer",
];
$(document).ready(function () {
    $("#fullpage").fullpage({
        scrollingSpeed: 800,
        autoScrolling: true,
        loopHorizontal: true,
        fitToSection: true,
        loopBottom: true,
        fitToSectionDelay: 50,
        anchors: indexPageToolTip,
        verticalCentered: false,
        slidesNavigation: true,
        slidesNavPosition: "bottom",
        navigation: true,
        navigationPosition: "right",
        navigationTooltips: indexPage,
        responsiveWidth: 100,
        controlArrows: false,
        afterLoad: function (origin, destination, direction) {
            if (destination === 1) {
                $(".box_view_image img").animate({ top: 0 }, 500);
            }
            if (destination === 2) {
                $("#vision").animate({ marginTop: 0 }, 1000);
                $("#mission").delay(200).animate({ marginTop: 0 }, 1000);
                $("#value").delay(400).animate({ marginTop: 0 }, 1000);
            }
            if (destination === 3) {
                $("#dai-ly").animate({ marginTop: 0 }, 1000);
                $("#tinh-thanh").delay(200).animate({ marginTop: 0 }, 1000);
                $("#khach-hang").delay(400).animate({ marginTop: 0 }, 1000);
            }
            if (destination === 4) {
                $(".box_image_cup img").animate({ marginTop: '120px' }, 1000);
            }
        },
        onLeave: function (index, nextIndex, direction) {
            $(".box_view_image img").animate({ top: "100%" }, 400);
            $(".card_mission").animate({ marginTop: "100%" }, 400);
        },
    });
});
