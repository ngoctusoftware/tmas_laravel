$(document).ready(function () {
    $(".type_new_item").click(function () {
        $(".type_new_item").removeClass("active");
        $(this).addClass("active");
        getNewsByType($(this).text());
    });

    function getNewsByType(type_new) {
        $(".box_new_filter").empty();
        $.ajax({
            type: "POST",
            url: `/new-by-type`,
            data: { type: type_new },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                let data = res.res;
                console.log(res.res);
                if (data.length) {
                    data.forEach((item) => {
                        const component = `
                        <div class="card_card_post_filter_component space_md">
                            <a href="/tin-tuc/${item.id}/${item.slug}"">
                                <div class="card_post_filter">
                                    <div class="image_box">
                                        <img class="w-100" src="../../${item.featured_image}" alt="${item.name}">
                                    </div>
                                    <div class="card_post_filter_content">
                                        <div class="name_post">
                                            <p>${item.name}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                       `;
                        $(".box_new_filter").append(component);
                    });
                } else {
                    $(".box_new_filter").append(
                        `<p class="text-center"> Chưa có bài viết cho mục này </p>`
                    );
                }

                if (APP_ENV === "local") {
                    replaceIMG();
                } else {
                    loadImgErr();
                }
            })
            .catch((err) => {
                console.log("err", err);
            });
    }

    function resetPagination() {
        const href = window.location.pathname;
        $(".pagination_post li a").attr(
            "href",
            href + $(".pagination_post li a").attr("href")
        );
    }

    getNewsByType("Sự kiện");

    $(".body_content_detail img").each(function () {
        const altText = $(this).attr("alt");
        console.log( altText)

        if (altText) {
            $(this).after(
                '<div class="image-description">' + altText + "</div>"
            );
        }
    });
});
