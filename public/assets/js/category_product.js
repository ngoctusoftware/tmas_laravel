$(document).ready(function () {

    let data_detail = {
        url: '',
        content: ''
    }

    $('.col-custom').click(function () {
        if ($(this).attr('id') === 'cam-bien') {
            data_detail = {
                url: 'assets/img/type_product/detail_cambien.png',
                content: 'TMAS Việt Nam nắm trong tay nhiều giải pháp công nghệ tiên tiến nhất được ứng dụng trong việc đo lường và cung cấp các cảnh báo, gợi ý hành động thông minh với độ tin cậy cao và chính xác tuyệt đối. '
            }
        }
        if ($(this).attr('id') === 'an-toan') {
            data_detail = {
                url: 'assets/img/type_product/detail_antoan.png',
                content: 'Được thúc đẩy bởi một kỳ vọng về tương lai an toàn cho tất cả mọi người khi tham gia giao thông, và dẫn dắt bởi công nghệ. Các sản phẩm của chúng tôi giúp cho tài xế tránh khỏi những nguy hiểm không lường trước được khi lái xe và bù đắp những thiếu sót rất quan trọng có thể dẫn tới những sai lầm trong quá trình vận hành xe.'
            }
        }
        if ($(this).attr('id') === 'toan-canh') {
            data_detail = {
                url: 'assets/img/type_product/detail_toancanh.png',
                content: 'Chúng tôi nhận thấy một vài góc quan sát trên xe là không đủ, tầm nhìn toàn cảnh phải trở thành một chuẩn mực trên xe hơi. Tiện dụng, an toàn và thông minh đã giúp cho những sản phẩm của TMAS Việt Nam dễ dàng tiếp cận thị trường đại chúng và tạo dựng vị thế riêng cho mình. '
            }
        }
        if ($(this).attr('id') === 'man-hinh') {
            data_detail = {
                url: 'assets/img/type_product/detail_manhinh.png',
                content: 'Với vai trò tiên phong trong việc tái định hình trải nghiệm giải trí và tiện ích trang bị trên xe. Mỗi sản phẩm mang thương hiệu SafeView của TMAS Việt Nam luôn sở hữu những tính năng vượt trội và mang đến trải nghiệm khác biệt.'
            }
        }
        if ($(this).attr('id') === 'tu-dong') {
            data_detail = {
                url: 'assets/img/type_product/detail_tudong.png',
                content: 'Ở lĩnh vực công nghệ hỗ trợ tự động trên xe hơi, TMAS Việt Nam hiện đang nắm giữ vị thế dẫn đầu. Không ngừng cải tiến và chưa khi nào hài lòng, mỗi sản phẩm đều hướng tới mục đích đem tới sự tiện dụng, gia tăng độ chính xác và luôn đảm bảo an toàn khi vận hành.'
            }
        }
        $('.image_des img').attr('src', data_detail.url)
        $('#content_type_product').text(data_detail.content)
        $('.fill_content').fadeIn()
    })

    if ($(window).width() > 480) {
        $('.fill_content').click(function () {
            $(this).fadeOut()
        })
    } else {
        $('.fill_content').click(function () {
            $(this).fadeOut()
        })
    }

})