if ($(window).width() > 768) {
    require([
        "esri/Map",
        "esri/views/MapView",
        "esri/layers/FeatureLayer"
    ], function (Map, MapView, FeatureLayer) {

        var map = new Map({
            basemap: "topo-vector",
            autoResize: false
        });

        var view = new MapView({
            container: "viewDiv",
            map: map,
            center: [108, 16],
            zoom: 6,
            autoResize: false
        });
        if ($(window).width() < 768) {
            view.on("mouse-wheel", function (event) {
                event.stopPropagation();
            });
            view.on("mouse-drag", function (event) {
                event.stopPropagation();
            });
        }
        view.on("mouse-wheel", function (event) {
            event.stopPropagation();
        });
        view.on("mouse-drag", function (event) {
            event.stopPropagation();
        });

        // // Trailheads feature layer (points)
        // var trailheadsLayer = new FeatureLayer({
        //     url: "https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Trailheads/FeatureServer/0"
        // });

        // map.add(trailheadsLayer);

        // // Trails feature layer (lines)
        // var trailsLayer = new FeatureLayer({
        //     url: "https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Trails/FeatureServer/0"
        // });

        // map.add(trailsLayer, 0);

        // // Parks and open spaces (polygons)
        // var parksLayer = new FeatureLayer({
        //     url: "https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Parks_and_Open_Space/FeatureServer/0"
        // });
        // map.add(parksLayer, 0);
    });
}
