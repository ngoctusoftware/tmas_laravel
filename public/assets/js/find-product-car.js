$(document).ready(function () {
    let idModel = 25;
    let companyName = 'Toyota';
    let modelName='Wigo';
    function getListCompany() {
        $.ajax({
            type: "GET",
            url: `/get-list-company`,
        })
            .then((res) => {
                res.res.forEach((element) => {
                    const item = `<option value="${element.id}" class="option_company">${element.name}</option>`;
                    $("#select_company").append(item);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    getListCompany();
    $("#select_company").change(function () {
        var selectedValue = $(this).val();
        getListModel(selectedValue);
        companyName = $('#select_company option:selected').text()
    });
    $("#select_model").change(function () {
        idModel = $(this).val();
        modelName=$('#select_model option:selected').text()
    });
    function getListModel(id) {
        $("#select_model").empty();
        $.ajax({
            type: "POST",
            url: `/get-list-model`,
            data: { id },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                res.res.forEach((element) => {
                    const item = `<option value="${element.id}" class="option_model">${element.name}</option>`;
                    $("#select_model").append(item);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    getListModel(1);
    $("#button_search_product_by_car").click(function () {
        $('#modal_result_label').text(`Các sản phẩm của TMAS có thể lắp trên xe ${companyName} ${modelName}`)
        $("#result_product").empty();
        const id  = $("#select_company").val();
        console.log(id, 'id')
        $.ajax({
            type: "POST",
            url: `/get-list-product-by-model`,
            data: { id_model: idModel },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                let data = res.res;
                const box = `
                <div class="row" id="box_list_product_by_model">
                    </div>`;
                $("#result_product").append(box);
                data.forEach((item) => {
                    const arrayPrice = JSON.parse(item.price);
                    price = arrayPrice.toLocaleString("vi-VN");
                    if (typeof arrayPrice === "object" && arrayPrice !== null) {
                        arrayPrice.sort(function (a, b) {
                            return a.price - b.price;
                        });
                        if (
                            arrayPrice[0].price !==
                            arrayPrice[arrayPrice.length - 1].price
                        ) {
                            price =
                                (+arrayPrice[0].price).toLocaleString("vi-VN") +
                                " - " +
                                (+arrayPrice[arrayPrice.length - 1]
                                    .price).toLocaleString("vi-VN");
                        } else {
                            price = (+arrayPrice[0].price).toLocaleString(
                                "vi-VN"
                            );
                        }
                    }
                    const component = `
                    <div class="col-md-4 col-12 space_sm">
                        <a href="/san-pham/${item.id}/${item.slug}"">
                            <div class="card_product">
                                <div class="image_box">
                                    <img class="w-100" src="${item.image}" alt="${item.name}">
                                </div>
                                <div class="product_content">
                                    <div class="name_product">
                                        <p>${item.name}</p>
                                    </div>
                                    <div class="price">
                                        <p>${price} đ</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>`;
                    $("#box_list_product_by_model").append(component);
                });
                if (APP_ENV === "local") {
                    replaceIMG();
                } else {
                    loadImgErr();
                }
            })
            .catch((err) => {
                console.log("err", err);
            });
    });
});
