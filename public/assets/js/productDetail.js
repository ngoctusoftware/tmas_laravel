let slideTimer;
const slug = [
    // "banner-san-pham",
    "anh-san-pham",
    "thong-so",
    "san-pham-noi-bat",
    "footer",
];
const nameHome = [
    // "Banner",
    "Ảnh sản phẩm",
    "Thông số sản phẩm",
    "Sản phẩm nổi bật",
    "Footer",
];

$(document).ready(function () {
    $("#fullpage").fullpage({
        scrollingSpeed: 800,
        autoScrolling: true,
        loopHorizontal: true,
        fitToSection: true,
        loopBottom: true,
        fitToSectionDelay: 50,
        anchors: slug,
        verticalCentered: false,
        slidesNavigation: true,
        slidesNavPosition: "bottom",
        navigation: true,
        navigationPosition: "right",
        navigationTooltips: nameHome,
        responsiveWidth: 100,
        controlArrows: false,
    });
});
let dataModel;
let dataModelProd;
function openModel(id) {
    $(".pop_model_select_body").empty();
    dataModelProd.forEach((item) => {
        if (+id === +item.car_company_id && +item.model_status === 1) {
            const itemModel = `<p class="item_model">${item.car_model_name}</p>`;
            $(".pop_model_select_body").append(itemModel);
        }
    });
    $(".pop_model_select").show(200);
}

$(".close").click(function () {
    $(".pop_model_select").hide(200);
});

$(document).ready(function () {
    $(".main_image img").attr(
        "src",
        $(".slide-item.active").find("img").attr("src")
    );
    $(".slide-item").click(function () {
        $(".slide-item").removeClass("active");
        $(this).addClass("active");
        const urlImage = $(this).find("img").attr("src");
        $(".main_image img").attr("src", urlImage);
    });
    $(".option").click(function () {
        $(".option").removeClass("active");
        $(".price").removeClass("active");
        $(".warranty").removeClass("active");

        $(this).addClass("active");
        const className = $(this).attr("class");
        const index = className.replace(/\D/g, "");
        $(`.price_${index}`).addClass("active");
        $(`.warranty_${index}`).addClass("active");
    });

    $("#phan-loai").change(function () {
        $(".price").removeClass("active");
        $(".warranty").removeClass("active");

        const selectedValue = $(this).val();
        const index = selectedValue.replace(/\D/g, "");
        console.log(selectedValue);
        $(`.price_${index}`).addClass("active");
        $(`.warranty_${index}`).addClass("active");
    });

    function getListModelByProduct(id) {
        $("#select_model").empty();
        $.ajax({
            type: "POST",
            url: `/get-list-model-by-product`,
            data: { id_product: id },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        })
            .then((res) => {
                console.log(res.res);
                dataModelProd = res.res;
            })
            .catch((err) => {
                console.log("err", err);
            });
    }

    const productId = window.location.pathname.split("/")[2];
    getListCompany();

    getListModelByProduct(productId);

    function getListCompany() {
        $.ajax({
            type: "GET",
            url: `/get-list-company`,
        })
            .then((res) => {
                dataModel = res.res;
                res.res.forEach((element) => {
                    const item = `<div class="col-lg-3 col-6 box_item_company"><p id="company_${element.id}" class="option_company" onclick="openModel(${element.id})">${element.name}</p></div>`;
                    $(".car_model_product").append(item);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
});
