let slideTimer;
const slug = [
    "banner",
    "about-TMAS",
    "TMAS-product",
    "news",
    "contact",
    "partner",
    "footer",
];
const nameHome = [
    "Banner",
    "Về chúng tôi",
    "Sản phẩm của TMAS",
    "Tin tức",
    "Liên hệ",
    "Đối tác",
    "Footer",
];

$(document).ready(function () {
    $("#fullpage").fullpage({
        scrollingSpeed: 800,
        autoScrolling: true,
        loopHorizontal: true,
        fitToSection: true,
        loopBottom: true,
        fitToSectionDelay: 50,
        anchors: slug,
        verticalCentered: false,
        slidesNavigation: true,
        slidesNavPosition: "bottom",
        navigation: true,
        navigationPosition: "right",
        navigationTooltips: nameHome,
        responsiveWidth: 100,
        controlArrows: false,
        afterLoad: function (origin, destination, direction) {
            console.log("afterLoad: ", origin, destination, direction);
            if (destination === 1) {
                $(".title_banner").animate({ top: "0" }, 500);
                $(".description_banner").delay(200).animate({ top: 0 }, 500);
                $(".box_view_image img").delay(400).animate({ top: 0 }, 800);
            }
        },
        onSlideLeave: function (section, origin, destination, direction) {
            $(".title_banner").animate({ top: "100%" }, 100);
            $(".description_banner").animate({ top: "100%" }, 100);
            $(".box_view_image img").animate({ top: "100%" }, 100);
        },
        afterSlideLoad: function (section, origin, destination, direction) {
            if (origin === 1) {
                $(".title_banner").animate({ top: "0" }, 500);
                $(".description_banner").delay(200).animate({ top: "0" }, 500);
                $(".box_view_image img").delay(400).animate({ top: 0 });
            }
        },
        afterRender: function () {
            // $(".section").each(function () {
            //     $(this).find(".slider").slick({
            //         arrows: false,
            //         dots: true,
            //         infinite: false,
            //         autoplay: true,
            //         autoplaySpeed: 10000,
            //         slidesToShow: 1,
            //         slidesToScroll: 1,
            //     });
            // });
        },
    });
});
