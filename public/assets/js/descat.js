const contentDesCat = [
    {
        slug: 'ho-tro-tu-dong',
        contentAttention: 'Bạn có bao giờ gặp tình huống khi cần phải mở hoặc đóng cốp xe nhưng tay bận không thể làm điều đó? Các tính năng hỗ trợ tự động đang trở thành một xu hướng phổ biến để giải quyết vấn đề này.',
        contentInterest: 'Điều gì khiến các tính năng hỗ trợ tự động trở nên hấp dẫn đối với người sử dụng ô tô? Những tính năng này giúp tăng tính tiện dụng và giảm thời gian cần thiết để mở hoặc đóng cốp xe',
        contentDesire: 'Bạn muốn sở hữu một chiếc ô tô được trang bị các tính năng hỗ trợ tự động để có thể mở và đóng cốp xe một cách dễ dàng và nhanh chóng trong những tình huống khẩn cấp hay khi tay bận?',
        contentAction: 'Hãy trang bị cho chiếc ô tô của bạn các tính năng hỗ trợ tự động để có trải nghiệm lái xe an toàn và tiện lợi hơn bao giờ hết.',
    },
    {
        slug: 'man-hinh-android',
        contentAttention: 'Với màn hình Android, bạn sẽ có trải nghiệm giải trí tuyệt vời và tính năng thông minh tuyệt đỉnh trên màn hình lớn.',
        contentInterest: 'Màn hình Android hỗ trợ các ứng dụng phổ biến như Netflix, YouTube, Facebook và các ứng dụng giải trí khác. Bạn có thể trải nghiệm âm thanh chất lượng cao và hình ảnh sắc nét từ màn hình lớn. Với các tính năng thông minh, bạn có thể kết nối với các thiết bị thông minh khác và điều khiển các thiết bị đó trực tiếp từ màn hình của mình.',
        contentDesire: 'Với màn hình Android, bạn sẽ có trải nghiệm giải trí tuyệt vời và tính năng thông minh tuyệt đỉnh trên màn hình lớn. Bạn sẽ không còn cảm thấy bị giới hạn bởi màn hình nhỏ trên điện thoại hoặc máy tính bảng.',
        contentAction: 'Hãy sắm ngay một màn hình Android để trải nghiệm giải trí và tính năng thông minh tuyệt vời trên màn hình lớn.',
    },
    {
        slug: 'camera',
        contentAttention: 'Bạn có biết rằng việc lắp đặt camera trên ô tô là một trong những cách để tăng tính an toàn và giảm nguy cơ tai nạn giao thông?',
        contentInterest: 'Camera trên ô tô giúp người lái quan sát được mọi góc cạnh xung quanh xe, giúp tránh được các va chạm và tai nạn không mong muốn.',
        contentDesire: 'Bạn muốn sở hữu một chiếc ô tô được trang bị camera để có thể quan sát xung quanh xe một cách dễ dàng và giảm nguy cơ tai nạn?',
        contentAction: 'Hãy lắp đặt camera trên ô tô của bạn để có trải nghiệm lái xe an toàn và thoải mái hơn.',
    },
    {
        slug: 'cam-bien-an-toan',
        contentAttention: 'Bạn có biết rằng cảm biến an toàn trên ô tô là một trong những cách để giảm nguy cơ tai nạn giao thông?',
        contentInterest: 'Các cảm biến an toàn trên ô tô giúp phát hiện và cảnh báo nguy hiểm, giúp tránh được các va chạm và tai nạn không mong muốn.',
        contentDesire: 'Bạn muốn sở hữu một chiếc ô tô được trang bị các cảm biến an toàn để có thể lái xe an toàn và giảm nguy cơ tai nạn?',
        contentAction: 'Hãy trang bị cho chiếc ô tô của bạn các cảm biến an toàn để có trải nghiệm lái xe an toàn và thoải mái hơn.',
    },
    {
        slug: 'android-box',
        contentAttention: 'Bạn có biết rằng Android Box trên ô tô giúp giải trí và kết nốivới thế giới bên ngoài trong khi lái xe?',
        contentInterest: 'Android Box trên ô tô cho phép người lái và hành khách trải nghiệm đa phương tiện trên màn hình lớn trong xe, giúp giải trí và giảm stress khi lái xe.',
        contentDesire: 'Bạn muốn sở hữu một chiếc ô tô được trang bị Android Box để có thể trải nghiệm đa phương tiện và kết nối với thế giới bên ngoài khi lái xe?',
        contentAction: 'Hãy trang bị cho chiếc ô tô của bạn Android Box để có trải nghiệm lái xe thú vị và kết nối với thế giới bên ngoài một cách tiện lợi hơn.',
    },
    {
        slug: 'cam-bien-ap-suat-lop',
        contentAttention: 'Bạn có biết rằng áp suất lốp thấp có thể gây nguy hiểm và làm tăng nguy cơ tai nạn giao thông?',
        contentInterest: 'Cảm biến áp suất lốp giúp theo dõi áp suất lốp và cảnh báo người lái khi có vấn đề với áp suất lốp, giúp giảm nguy cơ tai nạn và tiết kiệm nhiên liệu.',
        contentDesire: 'Bạn muốn sở hữu một chiếc ô tô được trang bị cảm biến áp suất lốp để có thể theo dõi áp suất lốp một cách dễ dàng và giảm nguy cơ tai nạn?',
        contentAction: 'Hãy trang bị cho chiếc ô tô của bạn cảm biến áp suất lốp để có trải nghiệm lái xe an toàn hơn và tiết kiệm nhiên liệu hơn.',
    },
    {
        slug: 'de-no-tu-xa',
        contentAttention: 'Bạn có biết rằng tính năng đề nổ từ xa giúp người sử dụng ô tô tiết kiệm thời gian và tiện lợi hơn khi sử dụng xe?',
        contentInterest: 'Tính năng đề nổ từ xa cho phép người lái khởi động động cơ mà không cần phải ngồi trong xe, giúp tiết kiệm thời gian và dễ dàng hơn trong các tình huống khẩn cấp.',
        contentDesire: 'Bạn muốn sở hữu một chiếc ô tô được trang bị tính năng đề nổ từ xa để có thể khởi động động cơ một cách dễ dàng và tiện lợi hơn?',
        contentAction: 'Hãy trang bị cho chiếc ô tô của bạn tính năng đề nổ từ xa để có trải nghiệm lái xe tiện lợi và dễ dàng hơn.',
    },
    
]
