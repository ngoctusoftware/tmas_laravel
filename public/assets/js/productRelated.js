$(document).ready(function () {
    function getProductRelated() {
        $.ajax({
            type: "GET",
            url: `/product-related/3`,
        })
            .then((res) => {
                console.log(res);
                let data = res.res;
                data.forEach((item) => {
                    let component = `
                    <div class="col-lg-4 col-12">
                        <div class="card related">
                            <div class="related_image">
                                <img src="/${item.image}" alt="${item.name}"/>
                            </div>
                            <div class="related_content"> 
                                <a class="related_link" href="/san-pham/${item.id}/${item.slug}">${item.name}</a>
                            </div>
                        </div>
                    </div>
                    `;
                    $("#product_related").append(component);
                });
            })
            .catch((err) => {
                console.log("err", err);
            });
    }
    getProductRelated();
});
