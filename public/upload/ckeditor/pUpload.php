<?php
if(isset($_FILES['upload']['name']))
{
  $url = '';
  $src = '';
  $file = $_FILES['upload']['tmp_name'];
  $file_name = $_FILES['upload']['name'];
  $file_name_array = explode(".", $file_name);
  $extension = end($file_name_array);
  $new_image_name = rand() . '.' . $extension;
  chmod('upload', 0777);
  $allowed_extension = array("jpg", "gif", "png", "webp");
  if(getenv('APP_ENV') == 'production') {
  $url = '../public/posts/';
  $src = '../../../public/upload/posts/' . $new_image_name;  
  }
  else {
  $url = '../posts/';
  $src = '../../../upload/posts/' . $new_image_name;  
  }
  if(in_array($extension, $allowed_extension))
  {  
      move_uploaded_file($file, $url . $new_image_name);  
      $function_number = $_GET['CKEditorFuncNum'];
      echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$src', '$message');</script>";
  }
}
