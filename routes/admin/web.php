<?php

use App\Http\Controllers\Admin\BranchesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[DashboardController::class,'index'])->middleware('auth')->name('admin.home');
Route::group(['prefix' => 'Dashboard'], function () {
    Route::get('/',[DashboardController::class,'index'])->middleware('auth')->name('admin.dashboard');
    
});

Route::group(['prefix' => 'nhom-san-pham'], function () {    
    Route::get('/',[CategoriesController::class,'index'])->middleware('auth')->name('admin.categories');        
    Route::get('/file-mau',[CategoriesController::class,'DownloadTemplate'])->middleware('auth')->name('admin.categories.template');        
    Route::post('/import',[CategoriesController::class,'Import'])->middleware('auth')->name('admin.categories.import');        
    Route::get('/export',[CategoriesController::class,'export'])->middleware('auth')->name('admin.categories.export');        
    Route::post('/them-moi',[CategoriesController::class,'store'])->middleware('auth')->name('admin.categories.add');        
    Route::get('/chinh-sua/{id}',[CategoriesController::class,'edit'])->middleware('auth')->name('admin.categories.chinhsua');        
    Route::post('/xoa-bo/{id}',[CategoriesController::class,'remove'])->middleware('auth');
});
Route::group(['prefix' => 'chuyen-muc'], function () {
    Route::get('/',[CategoriesController::class,'home'])->middleware('auth')->name('admin.dashboard');
    Route::post('/them-moi',[CategoriesController::class,'store'])->middleware('auth')->name('admin.categories.new');
    Route::get('/chinh-sua/{id}',[CategoriesController::class,'view'])->middleware('auth')->name('admin.categories.edit');
    Route::post('/chinh-sua/{id}',[CategoriesController::class,'save'])->middleware('auth')->name('admin.categories.update');
    Route::post('/xoa-bo/{id}',[CategoriesController::class,'remove'])->middleware('auth');
    
});
Route::group(['prefix' => 'bai-viet'], function () {
    Route::get('/',[PostsController::class,'index'])->middleware('auth')->name('admin.posts');
    Route::get('/them-chi-tiet',[PostsController::class,'create'])->middleware('auth')->name('admin.posts.add');
    Route::post('/them-chi-tiet',[PostsController::class,'createPostController'])->middleware('auth')->name('admin.posts.update');
    Route::get('/sua-chi-tiet/{id}',[PostsController::class,'getProductByIdController'])->middleware('auth')->name('admin.posts.edit');
    Route::post('/sua-chi-tiet/{id}',[PostsController::class,'updateProductController'])->middleware('auth')->name('admin.posts.save');    
    Route::post('/import',[PostsController::class,'Import'])->middleware('auth')->name('admin.posts.import');
    Route::Post('/deleted/{id}',[PostsController::class,'Deleted'])->middleware('auth')->name('admin.posts.deleted');
    Route::post('/importExcel',[PostsController::class,'ImportExcel'])->middleware('auth')->name('admin.posts.import');
    Route::post('/upload',[PostsController::class,'upload'])->middleware('auth')->name('admin.posts.upload');
});

Route::group(['prefix' => 'san-pham'], function () {
    Route::get('/',[ProductsController::class,'index'])->middleware('auth')->name('admin.products');
    Route::get('/them-chi-tiet',[ProductsController::class,'create'])->middleware('auth')->name('admin.products.add');
    Route::post('/them-san-pham',[ProductsController::class,'update'])->middleware('auth')->name('admin.products.update');
    Route::get('/sua-chi-tiet/{id}',[ProductsController::class,'edit'])->middleware('auth')->name('admin.products.edit');
    Route::post('/sua-chi-tiet/{id}',[ProductsController::class,'save'])->middleware('auth')->name('admin.products.save');
    Route::post('/deleted/{id}',[ProductsController::class,'deleted'])->middleware('auth')->name('admin.products.deleted');
    Route::post('/xoa-hinh-anh/{id}',[ProductsController::class,'ImagesDelete'] )->name('admin.products.imgDelete');
    Route::post('/chuyen-trang-thai/{id}',[ProductsController::class,'changeStatusDeleted'])->middleware('auth')->name('admin.products.changeStatus');
    Route::post('/imports',[ProductsController::class,'imports'] )->name('admin.products.imports');
    Route::post('/upload-by-excel', [ProductsController::class,'ProductsImportExcel']) -> name('admin.products.ProductsImportExcel');
    Route::get('/export-by-excel', [ProductsController::class,'ProductsExport']) -> name('admin.products.export');
});


Route::group(['prefix' => 'chi-nhanh'], function () {
    Route::get('/',[BranchesController::class,'index'])->middleware('auth')->name('admin.branches');     
    Route::post('/them-moi',[BranchesController::class,'save'])->middleware('auth')->name('admin.branches.add');        
    Route::post('/xoa',[BranchesController::class,'delete'])->middleware('auth')->name('admin.branches.delete');      
});

Route::group(['prefix' => 'lien-he'], function () {
    Route::get('/',[ContactController::class,'index'])->middleware('auth')->name('admin.contact.index');
});


