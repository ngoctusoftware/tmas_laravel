<?php

use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Pages\BranchController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Pages\NewsController;
use App\Http\Controllers\Pages\ContactController;
use App\Http\Controllers\Pages\GuideController;
use App\Http\Controllers\Pages\IntroController;
use App\Http\Controllers\Pages\ProductsController;
use App\Http\Controllers\Pages\HomeController;
use App\Http\Controllers\Pages\TalentController;
use App\Http\Controllers\SitemapXmlController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/sitemap.xml', [SitemapXmlController::class, 'index']);

Route::get('/', [HomeController::class, 'index'], function () {
    return view('pages.contents.home.main');
});
Auth::routes();

Route::get('/search-product', [ProductsController::class, 'searchProduct']) -> name('search-product');
Route::get('/download-android', [HomeController::class, 'downloadAndroid']) -> name('downloadAndroid');
Route::get('/download-ios', [HomeController::class, 'downloadIos']) -> name('downloadIos');
Route::post('/search-text-product', [ProductsController::class, 'searchTextProduct']) -> name('search-text-product');
Route::get('/autoload-new', [HomeController::class, 'autoload']) -> name('autoload');
Route::get('/new-related/{number}', [HomeController::class, 'newRelated']) -> name('newRelated');
Route::get('/product-related/{number}', [HomeController::class, 'productRelated']) -> name('productRelated');
Route::post('/product-by-id', [ProductsController::class, 'productBySlug']) -> name('productBySlug');
Route::post('/new-by-type', [NewsController::class, 'newByType']) -> name('newByType');

Route::get('/get-branch', [HomeController::class, 'getBranch']) -> name('branch');
Route::get('/get-customers', [HomeController::class, 'getCustomer']) -> name('customer');
Route::get('/huong-dan-dai-ly', [GuideController::class, 'index']) -> name('guide');
Route::get('/bao-hanh-dien-tu', [GuideController::class, 'warranty']) -> name('warranty');
Route::get('/hinh-nen', [GuideController::class, 'library']) -> name('library');
//for mobile
Route::get('/post-mobile', [NewsController::class, 'postMobile']) -> name('postMobile');
Route::get('/detail-post-mobile/{id}', [NewsController::class, 'detailMobile']) -> name('detailMobile');
// end
//search product by car
Route::get('/get-list-company', [ProductsController::class, 'getListCompany']) -> name('productBySlug');
Route::post('/get-list-model', [ProductsController::class, 'getListModel']) -> name('productBySlug');
Route::post('/get-list-product-by-model', [ProductsController::class, 'getListProductByModel']) -> name('getListProductByModel');
Route::post('/get-list-model-by-product', [ProductsController::class, 'getListModelByProduct']) -> name('getListModelByProduct');
Route::post('/change-model-product', [ProductsController::class, 'changeStatus']) -> name('changeStatus');
// end
Route::group(['prefix' => 'lien-he'], function () {
    Route::get('/',[ContactController::class,'index'])->name('pages.contact.index');
    Route::post('/save', [ContactController::class, 'Save']) -> name('pages.contact.save');
});

Route::group(['prefix' => 'tin-tuc'], function () {
    Route::get('/',[NewsController::class,'index'])->name('pages.news.index');
    Route::get('/{id}/{slug?}', [NewsController::class, 'detail']) -> name('pages.news.detail');
});

Route::group(['prefix' => 'tuyen-dung'], function () {
    Route::get('/',[TalentController::class,'index'])->name('pages.talent.index');
    Route::get('/{id}/{slug?}', [TalentController::class, 'detail']) -> name('pages.talent.detail');
});

Route::group(['prefix' => 'san-pham'], function () {
    Route::get('/',[ProductsController::class,'index'])->name('pages.products.index');
    Route::get('/{id}/{slug?}', [ProductsController::class, 'detail']) -> name('pages.products.detail');
});

Route::group(['prefix' => 'gioi-thieu'], function () {
    Route::get('/',[IntroController::class,'index'])->name('pages.intro.index');
});

Route::group(['prefix' => 'dai-ly'], function () {
    Route::get('/',[BranchController::class,'index'])->name('pages.branch.branch');
});

Route::fallback(function () {
    return response()->view('errors.404', [], 404);
});