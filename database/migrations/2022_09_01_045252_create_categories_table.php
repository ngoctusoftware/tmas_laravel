<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',100);
                $table->string('slug',50)->unique();             
                $table->bigInteger('type');             
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->timestamps();
            });
        }
       
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',100);
                $table->string('slug',50)->unique();             
                $table->bigInteger('type');             
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->timestamps();
            });
        }
    }
};
