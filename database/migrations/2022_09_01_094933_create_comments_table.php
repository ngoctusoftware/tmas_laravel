<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('comments')) {  
            Schema::create('comments', function (Blueprint $table) {
                $table->bigIncrements('id');   
                //Sản phẩm
                $table->string('prd_id');
                $table->string('name');
                $table->string('code');
                $table->string('image');            
                //Thông tin khách hàng
                $table->bigInteger('cus_id');
                $table->string('cus_name',100);            
                //Thông tin comment
                $table->string('content',100);            
                $table->timestamps();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('comments')) {  
            Schema::create('comments', function (Blueprint $table) {
                $table->bigIncrements('id');   
                //Sản phẩm
                $table->string('prd_id');
                $table->string('name');
                $table->string('code');
                $table->string('image');            
                //Thông tin khách hàng
                $table->bigInteger('cus_id');
                $table->string('cus_name',100);            
                //Thông tin comment
                $table->string('content',100);            
                $table->timestamps();
            });
        }
    }
};
