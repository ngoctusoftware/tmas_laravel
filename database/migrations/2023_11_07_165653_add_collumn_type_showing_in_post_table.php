<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('posts')) {            
            if (!Schema::hasColumn('posts', 'type_showing')) {                 
                 Schema::table('posts', function(Blueprint $table) {
                    $table->integer("type_showing")->after('type');                      
                 });
            }
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('posts')) {            
            if (!Schema::hasColumn('posts', 'type_showing')) {                 
                 Schema::table('posts', function(Blueprint $table) {
                    $table->integer("type_showing")->after('type');                      
                 });
            }
            
        }
    }
};
