<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('send_message_info')) {  
            Schema::create('send_message_info', function (Blueprint $table) {
                $table->bigIncrements('id'); 
                $table->bigInteger('user_id');
                $table->bigInteger('nav_id');                                     
                $table->timestamps();
            });                
        }
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
};
