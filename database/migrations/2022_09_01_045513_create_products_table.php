<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products')) {   
            Schema::create('products', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('code')->unique();
                $table->string('slug')->unique();
                $table->string('information');
                $table->string('description'); //Mô tả
                $table->string('image');
                $table->text('tag');
                $table->decimal('price',18);
                $table->tinyInteger('featured'); //Nội bật
                $table->string('news',20); //mới ? %            
                $table->string('accessories',200); //phụ kiện
                $table->string('promotion'); //Khuyến mại
                $table->integer('warranty'); //bảo hành
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->bigInteger('cat_id')->unsigned(); // Cái này ở bảng categories với type == 2
                $table->foreign('cat_id')->references('id')->on('categories')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
