<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('send_message_info')) {  
            Schema::create('send_message_info', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('cus_name',150);
                $table->string('cus_phone',13);
                $table->string('cus_email',150);    
                $table->string('cus_address');    
                $table->string('subject',250); 
                $table->text('content');        
                $table->timestamps();
            });                      
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('send_message_info')) {  
            Schema::create('send_message_info', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('cus_name',150);
                $table->string('cus_phone',13);
                $table->string('cus_email',150);    
                $table->string('cus_address');    
                $table->string('subject',250); 
                $table->text('content');        
                $table->timestamps();
            });                      
        }
    }
};
