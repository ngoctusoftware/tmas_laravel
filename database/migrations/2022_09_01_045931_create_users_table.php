<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {  
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('address', 255)->nullable();
                $table->string('password');
                $table->string('full_name',100);
                $table->string('email', 255)->nullable();
                $table->string('phone');
                $table->tinyInteger('status');
                $table->tinyInteger('deleted') ;
                $table->tinyInteger('per_id')->unsigned();            
                $table->timestamps();
            });           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('users')) {  
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('address', 255)->nullable();
                $table->string('password');
                $table->string('full_name',100);
                $table->string('email', 255)->nullable();
                $table->string('phone');
                $table->tinyInteger('status');
                $table->tinyInteger('deleted') ;
                $table->tinyInteger('per_id')->unsigned();            
                $table->timestamps();
            });           
        }
    }
};
