<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cards')) {  
            Schema::create('cards', function (Blueprint $table) {
                $table->bigIncrements('id');   
                //Sản phẩm
                $table->string('name');
                $table->string('code');
                $table->string('image');
                $table->decimal('price',18); //Giá
                $table->integer('quantity')->unsigned(); //số lượng            
                $table->decimal('total',18);// Giá *  số lương --> Total
                $table->tinyInteger('state');
                //Thông tin khách hàng
                $table->string('cus_name',100);
                $table->string('cus_address',100);
                $table->string('cus_email',100);
                $table->string('cus_phone',100);
                $table->bigInteger('cus_id');
                $table->timestamps();
            });           
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
};
