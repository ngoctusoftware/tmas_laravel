<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('branch')) {  
            Schema::create('branch', function (Blueprint $table) {
                $table->bigIncrements('id');               
                $table->string('name');
                $table->string('address'); 
                $table->string('email')->nullable();   
                $table->int('type'); // Khu vuc 
                $table->string('phone')->nullable();   
                $table->string('quantity_staff')->nullable(); 
                $table->string('manager_name')->nullable();       
                $table->timestamps();
            });          
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('branch')) {  
            Schema::create('branch', function (Blueprint $table) {
                $table->bigIncrements('id');               
                $table->string('name');
                $table->string('address'); 
                $table->string('email')->nullable();   
                $table->int('type'); // Khu vuc 
                $table->string('phone')->nullable();   
                $table->string('quantity_staff')->nullable(); 
                $table->string('manager_name')->nullable();       
                $table->timestamps();
            });          
        }
    }
};
