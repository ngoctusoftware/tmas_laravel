<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('customer')) {   
            Schema::create('customer', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('username');
                $table->string('password');    
                $table->string('email',150); 
                $table->string('phone',250);
                $table->string('ip_address');
                $table->tinyInteger('status');
                $table->tinyInteger('deleted') ;
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('customer')) {   
            Schema::create('customer', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('username');
                $table->string('password');    
                $table->string('email',150); 
                $table->string('phone',250);
                $table->string('ip_address');
                $table->tinyInteger('status');
                $table->tinyInteger('deleted') ;
                $table->timestamps();
            });
        }
    }
};
