<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('promotions')) {  
            Schema::create('promotions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('content');                         
                $table->string('url',250) ;
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->bigInteger('prd_id')->unsigned();
                $table->foreign('prd_id')->references('id')->on('products')->onDelete('cascade');
                $table->timestamps();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('promotions')) {  
            Schema::create('promotions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('content');                         
                $table->string('url',250) ;
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->bigInteger('prd_id')->unsigned();
                $table->foreign('prd_id')->references('id')->on('products')->onDelete('cascade');
                $table->timestamps();
            });
        }
    }
};
