<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('admin_navigations')) {
            Schema::create('admin_navigations', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',100);
                $table->string('slug',50)->unique(); 
                $table->integer('parent');    
                $table->string('icon',250); 
                $table->string('url',250) ;
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('admin_navigations')) {
            Schema::create('admin_navigations', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',100);
                $table->string('slug',50)->unique(); 
                $table->integer('parent');    
                $table->string('icon',250); 
                $table->string('url',250) ;
                $table->tinyInteger('status') ;
                $table->tinyInteger('deleted') ;
                $table->timestamps();
            });
        }
    }
};
