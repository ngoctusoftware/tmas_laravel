<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminNavigationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_navigations')->delete();
        DB::table('admin_navigations')->insert(
        	[       		
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Báo cáo', 'slug'=>'quan-ly', 'parent' => 1 , 'icon'=>'uil-chart-bar' , 'url'=>'/TAdmin/bao-cao', 'status' => 1, 'deleted' => 0 ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Danh mục', 'slug'=>'danh-muc', 'parent' => 2 , 'icon'=>'uil-window-grid' , 'url'=>'/TAdmin/danh-muc','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],
                    ['name' => 'Sản phẩm', 'slug'=>'san-pham', 'parent' => 2 , 'icon'=>'uil-truck' , 'url'=>'/TAdmin/san-pham','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                                                    
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Khách hàng', 'slug'=>'khach-hang', 'parent' => 3 , 'icon'=>'uil-users-alt' , 'url'=>'/TAdmin/khach-hang','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                                
                    ['name' => 'Khuyễn mãi', 'slug'=>'khuyen-mai', 'parent' => 3 , 'icon'=>'uil-gift' , 'url'=>'/TAdmin/khuyen-mai','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Đơn hàng', 'slug'=>'ao-nu', 'parent' => 3 , 'icon'=>'uil-shopping-cart-alt' , 'url'=>'/TAdmin/don-hang','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],
                    ['name' => 'Giỏ hàng', 'slug'=>'quan-nu', 'parent' => 3 , 'icon'=>'uil-cart' , 'url'=>'/TAdmin/gio-hang','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Người dùng', 'slug'=>'nguoi_dung', 'parent' => 4 , 'icon'=>'uil-user' , 'url'=>'/TAdmin/nguoi-dung','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],
                    ['name' => 'Phân quền', 'slug'=>'phan-quyen', 'parent' => 4 , 'icon'=>'uil-lock-open-alt' , 'url'=>'/TAdmin/phan-quyen','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],
                    ['name' => 'Vai trò', 'slug'=>'vai-tro', 'parent' => 4 , 'icon'=>'uil-align-justify' , 'url'=>'/TAdmin/vai-tro','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],
        		//------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Chuyên mục', 'slug'=>'chuyen-muc', 'parent' => 5 , 'icon'=>'uil-web-section-alt' , 'url'=>'/TAdmin/chuyen-muc','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],  
                    ['name' => 'Bài viết', 'slug'=>'bai-viet', 'parent' => 5 , 'icon'=>'uil-receipt-alt' , 'url'=>'/TAdmin/bai-viet','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],  
                    ['name' => 'Tin tức', 'slug'=>'tin-tuc', 'parent' => 5 , 'icon'=>'uil-newspaper' , 'url'=>'/TAdmin/tin-tuc','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],  
                    ['name' => 'Chi nhánh', 'slug'=>'chi-nhanh', 'parent' => 5 , 'icon'=>'uil-shop' , 'url'=>'/TAdmin/chi-nhanh','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],  
                    //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Cài đặt', 'slug'=>'cai-dat', 'parent' => 6 , 'icon'=>'uil-bright' , 'url'=>'/TAdmin/cai-dat','status' => 1, 'deleted' => 0  ,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],  
	        ]   
	    );

    }
}
