<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('permissions')->delete();
        DB::table('permissions')->insert(
        	[       		
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Administrator', 'description'=>'Quản trị viên','created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'User', 'description'=>'Người dùng','created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Guest', 'description'=>'Khách','created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s") ],                
	        ]   
	    );
    }
}
