<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert(
        	[       		
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Đối tác', 'slug'=>'doi-tac', 'type' => 1, 'status' => 1, 'deleted' => 0,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Tin tức', 'slug'=>'tin-tuc', 'type' => 1, 'status' => 1, 'deleted' => 0,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Chính sách bảo hành', 'slug'=>'chinh-sach-bao-hanh', 'type' => 1, 'status' => 1, 'deleted' => 0,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Tính năng sản phẩm', 'slug'=>'tinh-nang-san-pham', 'type' => 1, 'status' => 1, 'deleted' => 0,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                //------------------------------------------------------------------------------------------------------------------------
                    ['name' => 'Dòng sản phẩm công nghệ cảm biến tự động obd', 'slug'=>'dong-san-pham-cong-nghe-cam-bien-tu-dong-obd', 'type'=>2,'status' => 1, 'deleted' => 0,'created_at'=> date('Y-m-d h:i:s'),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Dòng sản phẩm công nghệ hỗ trợ lái xe an toàn', 'slug'=>'dong-san-pham-cong-nghe-ho-tro-lai-xe-an-toan', 'type'=>2,'status' => 1, 'deleted' => 0,'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s") ],                
                    ['name' => 'Dòng sản phẩm công nghệ hỗ trợ quan sát toàn cảnh camera 360', 'slug' => 'dong-san-pham-cong-nghe-ho-tro-quan-sat-toan-canh-camera-360', 'type'=>2,'status' => 1, 'deleted' => 0 ,'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s")],                
                    ['name' => 'Dòng sản phẩm công nghệ màn hình ô tô thông minh', 'slug'=>'dong-san-pham-cong-nghe-man-hinh-o-to-thong-minh', 'type'=>2,'status' => 1, 'deleted' => 0 ,'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s")],                
                    ['name' => 'Dòng sản phẩm công nghệ hỗ trợ tự động', 'slug'=>'dong-san-pham-cong-nghe-ho-tro-tu-dong', 'type'=>2,'status' => 1, 'deleted' => 0 ,'created_at'=> date("Y-m-d H:i:s"),'updated_at'=> date("Y-m-d H:i:s")],                
                
	        ]   
	    );


    }
}
