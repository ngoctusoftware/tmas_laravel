<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert(
        	[       		
                //------------------------------------------------------------------------------------------------------------------------
                    [
                        'full_name' => 'Quản trị viên', 
                        'email'=>'admin@gmail.com',                                               
                        'password'=>'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                        'phone'=>'0123456789',
                        'status' => 1, 
                        'deleted' => 0, 
                        'per_id' => 1,
                        'created_at'=> date("Y-m-d H:i:s"),
                        'updated_at'=> date("Y-m-d H:i:s") 
                    ],
                //------------------------------------------------------------------------------------------------------------------------
                    [
                        'full_name' => 'Nhân viên', 
                        'email'=>'nhanvien@gmail.com',                                               
                        'password'=>'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                        'phone'=>'0987654321',
                        'status' => 1, 
                        'deleted' => 0, 
                        'per_id' => 1,
                        'created_at'=> date("Y-m-d H:i:s"),
                        'updated_at'=> date("Y-m-d H:i:s") 
                    ],
                    
	        ]   
	    );
    }
}
