<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        DB::table('products')->insert(
        	[       		
                //------------------------------------------------------------------------------------------------------------------------
                    [
                        'name'=>'Màn hình 14 android thế hệ mới',
                        'code'=>'MHA0001',
                        'slug'=>'man-hinh-14-android-the-he-moi',
                        'tag'=>"[xe thông minh camera cảm biến]",
                        'images'=>"['upload/products/001.jpg','upload/products/002.jpg','upload/products/003.jpg']",
                        'contents'=>'Màn hình 14 android thế hệ mới',
                        'description'=>'Màn hình 14 android thế hệ mới',
                        'price'=>1000000,
                        'news'=>'100%',
                        'featured'=>1,
                        'accessories'=>'Sách hướng dẫn chìa khóa dự phòng lốp phụ',
                        'promotion'=>'"Không có khuyến mãi gì"',
                        'warranty'=>12,
                        'cat_id'=>90,
                        'status'=>1,
                        'deleted'=>0,
                        'meta_title'=>'Màn hình 14 android thế hệ mới',
                        'meta_keywords'=>'Màn hình 14 android thế hệ mới',
                        'meta_description'=>'Màn hình 14 android thế hệ mới',
                        'meta_og_url'=>'Màn hình 14 android thế hệ mới',
                        'order'=> 1,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                    ] 
	        ]   
	    );
    }
}
