<?php
    return [
        'admin' => [
            // '1' => 'Quản lý Phân tích',
            '2' => 'Quản lý Sản Phẩm',
            // '3' => 'Quản lý Khách hàng',
            // '4' => 'Quản lý Tài khoản',
            '5' => 'Quản lý Thông tin',
            '6' => 'Danh sách liên hệ'
        ],
        'pages' => [
            'ward' => [
                '1' => 'Miền Bắc',
                '2' => 'Miền Trung',
                '3' => 'Miền Nam',
            ]
        ]
    ];
?>