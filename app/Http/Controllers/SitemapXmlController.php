<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class SitemapXmlController extends Controller
{
    public function index()
    {
        $arrayMenu = array('', 'gioi-thieu', 'san-pham', 'tin-tuc', 'lien-he');
        $siteMapPost = DB::table('posts')->select('id', 'slug', 'created_at')->where('deleted', 0)->where('status', 1)->orderBy('created_at', 'DESC')->get();
        $siteMapProduct = DB::table('products')->select('id', 'slug', 'created_at')->where('deleted', 0)->where('status', 1)->orderBy('created_at', 'DESC')->get();
        return response()->view('pages.contents.sitemap', [
            'arrayMenu' => $arrayMenu,
            'siteMapPost' => $siteMapPost,
            'siteMapProduct' => $siteMapProduct,
        ])->header('Content-Type', 'text/xml');
    }
}
