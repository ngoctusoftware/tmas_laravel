<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;
class ContactController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact){
        $this->contact = $contact;
    }

    public function index(){
        return view('pages.contents.contact.index');
    }

    public function Save(Request $request){
        $model = $this->contact->store($request);
        return redirect() -> back();
     
    }
}
