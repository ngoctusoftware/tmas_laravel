<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;

class ProductsController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact)
    {
        $this->contact = $contact;
    }

    public function index()
    {
        $listCategory = DB::table('categories')->where('type', '=', 2)->orderBy('created_at', 'DESC')->get();
        return view('pages.contents.products.index', compact(
            'listCategory',
        ));
    }

    public function detail($id)
    {
        $detail = DB::table('products')->where('id', '=', $id)->get();
        $detailProducts = $detail[0];
        $listProductsRelated = DB::table('products')->get();
        // $listPostRelated = DB::table('posts')->limit(10)->orderBy('created_at', 'DESC')->get();
        return view('pages.contents.products.detailProducts', compact(
            'detailProducts',
            'listProductsRelated'
        ));
    }

    public function searchProduct(Request $request)
    {
        $dataRequest = $request->all();
        $queryId = isset($dataRequest['queryId']) ? $dataRequest['queryId'] : 0;
        $queryText = isset($dataRequest['queryText']) ? $dataRequest['queryText'] : '';
        $result_search = DB::table('products')
            ->where(function ($query) use ($queryId) {
                if ($queryId != 0)
                    $query->where('cat_id', '=', (int)$queryId);
            })
            ->where('name', 'like', '%' . $queryText . '%')
            ->get();
        $count = count($result_search->toArray());;
        return response()->json(['res' => $result_search, 'count' => $count]);
    }

    public function searchTextProduct(Request $request)
    {
        $dataRequest = $request->all();
        $queryText = isset($dataRequest['textSearch']) ? $dataRequest['textSearch'] : 0;
        $result_search = DB::table('products')->where('name', 'like', '%' . $queryText . '%')->get();
        return response()->json(['data' => $result_search]);
    }

    public function productBySlug(Request $request)
    {
        $dataRequest = $request->all();
        $result = [];
        if ($dataRequest['id'] == "0") {
            $result_search = DB::table('products')->where('status', '=', 1)->get();
        } else {
            $result_search = DB::table('products')->where('status', '=', 1)->where('cat_id', '=', (int)$dataRequest['id'])->get();
        }
        return response()->json(['res' => $result_search]);
    }

    public function getListCompany()
    {
        $result = DB::table('car_company')->get();
        return response()->json(['res' => $result]);
    }

    public function getListModel(Request $request)
    {
        $dataRequest = $request->all();
        $result = DB::table('car_model')->where('parent', '=', (int)$dataRequest['id'])->get();
        return response()->json(['res' => $result]);
    }

    public function getListProductByModel(Request $request)
    {
        $dataRequest = $request->all();
        $result = DB::table('products')
            ->join('product_model', 'products.id', '=', 'product_model.id_product')
            ->where('product_model.id_model', '=', (int)$dataRequest['id_model'])
            ->where('product_model.status', '=', 1)
            ->get();
        return response()->json(['res' => $result]);
    }
    public function getListModelByProduct(Request $request)
    {
        $dataRequest = $request->all();
        $result = DB::table('product_model')
            ->join('car_model', 'car_model.id', '=', 'product_model.id_model')
            ->join('car_company', 'car_company.id', '=', 'car_model.parent')
            ->select('car_model.id AS car_model_id', 'car_model.name AS car_model_name', 'car_company.id AS car_company_id', 'car_company.name AS car_company_name', 'product_model.status AS model_status')
            ->where('product_model.id_product', '=', (int)$dataRequest['id_product'])
            ->get();
        return response()->json(['res' => $result]);
    }

    public function changeStatus(Request $request)
    {
        $dataRequest = $request->all();
        $result = DB::table('product_model')
            ->where('id_model', '=', (int)$dataRequest['id_model'])
            ->where('id_product', '=', (int)$dataRequest['id_product'])
            ->update(['status' => (int)$dataRequest['status']]);
        return response()->json(['res' => $result]);
    }
}
