<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;

class TalentController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact)
    {
        $this->contact = $contact;
    }

    public function index()
    {
        $listPost = DB::table('posts')
            ->where('type_showing', 4)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->orderBy('created_at', 'DESC')
            ->paginate(3);
        return view('pages.contents.talent.index', compact(
            
            'listPost'
        ));
    }

    public function detail($id)
    {
        $detailPost = DB::table('posts')->where('id', '=', $id)->get();
        $dataDetail = $detailPost[0];
        $listPostRelated = DB::table('posts')
            ->where('type_showing', 4)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('pages.contents.talent.detailTalent', compact(
            'dataDetail',
            'listPostRelated'
        ));
    }
}
