<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;

class GuideController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact)
    {
        $this->contact = $contact;
    }

    public function index()
    {
        $listProductsRelated = DB::table('products')->limit(4)->get();
        return view('pages.contents.policy.guide.index', compact(
            'listProductsRelated'
        ));
    }
    public function warranty()
    {
        $data="";
        return view('pages.contents.policy.guide.warranty', compact(
            'data'
        ));
    }
    public function library()
    {
        $data="";
        return view('pages.contents.library.index', compact(
            'data'
        ));
    }
}
