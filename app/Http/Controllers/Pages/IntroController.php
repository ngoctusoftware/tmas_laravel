<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;
class IntroController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact){
        $this->contact = $contact;
    }

    public function index(){
        $title = "TMAS là thương hiệu tiên phong tại Việt Nam phân phối các sản phẩm hỗ trợ lái xe an toàn từ các thương hiệu hàng đầu Thế giới";
        $content = "Sau hơn 3 năm hoạt động, đến nay Thiên Minh Autosafety hoạt động với 02 Showroom chính tại Hà Nội và HCM và mạng lưới phát triển kênh phân phối toàn quốc, tại khắp 63 Tỉnh, thành phố, 02 Showroom chính tại Hà Nội và HCM. Bộ máy vận hành với đội ngũ nhân viên có trình độ chuyên môn, kỹ thuật viên tay nghề cao, được đào tạo bài bản, chuyên nghiệp.";
        $image = "https://thienminh-autosafety.com/wp-content/uploads/2021/06/1-1.png";
        return view('pages.contents.intro.index',compact(
            'title', 'content', 'image'));
    }
}
