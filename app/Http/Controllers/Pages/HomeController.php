<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;

class HomeController extends Controller
{
    protected $contact;
    public function __construct(Contract $contact)
    {
        $this->contact = $contact;
    }

    public function index()
    {
        $dataNewsRelated = DB::table('posts')
            ->where([
                ['order', '>', 1],
                ['is_featured', '=', 1],
                ['deleted', '=', 0],
                ['status', '=', 1]
            ])
            ->where(function ($query) {
                $query->where('type_showing', '=', 2)
                    ->orWhere('type_showing', '=', 3);
            })
            ->orderBy('order')
            ->limit(3)
            ->get();
        $dataNewsFeature = DB::table('posts')
            ->where('order', '=', 1)
            ->limit(1)
            ->get();

        $listCategory = DB::table('categories')->where('type', '=', 2)->orderBy('created_at', 'DESC')->get();
            $dataNews = [
            'dataNewsRelated' => $dataNewsRelated,
            'dataNewsFeature' => $dataNewsFeature
        ];
        return view('pages.contents.home.main', compact(
            'dataNews',
            'listCategory',
        ));
    }
    public function autoload()
    {
        $dataNews = DB::table('posts')
            ->where('is_featured', '=', 1)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->where('type_showing', '=', 1)
            ->orwhere('type_showing', '=', 3)
            ->orderBy('order', 'DESC')
            ->limit(10)
            ->get();
        return response()->json(['res' => $dataNews]);
    }

    public function newRelated($number)
    {
        $dataNews = DB::table('posts')
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->orderBy('order', 'DESC')
            ->limit($number)
            ->get();
        return response()->json(['res' => $dataNews]);
    }
    public function productRelated($number)
    {
        $dataProduct = DB::table('products')
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->orderBy('order', 'DESC')
            ->limit($number)
            ->get();
        return response()->json(['res' => $dataProduct]);
    }
    public function getBranch()
    {
        $dataBranch = DB::table('branch')->select('id', 'name', 'type', 'address', 'type_store')->get();
        return response()->json(['data' => $dataBranch]);
    }

    public function getCustomer()
    {
        $dataBranch = DB::table('customers')->select('id', 'name', 'type', 'address', 'type_store')->get();
        return response()->json(['data' => $dataBranch]);
    }

    public function downloadAndroid()
    {
        $url = 'https://play.google.com/store/apps/details?id=com.rambo.tmasapp&hl=vi';
        return view('pages.contents.downloadApp.android', compact('url'));
    }
    public function downloadIos()
    {
        $url = 'https://apps.apple.com/vn/app/tmas/id1629831494?l=vi';
        return view('pages.contents.downloadApp.ios', compact('url'));
    }
}
