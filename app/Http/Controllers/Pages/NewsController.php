<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;

class NewsController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact)
    {
        $this->contact = $contact;
    }

    public function index()
    {
        $listPost = DB::table('posts')
            ->where('type_showing', '!=', 4)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->where('type', 'Sự kiện')
            ->orderBy('created_at', 'DESC')
            ->paginate(11);
        $listPostRelated = DB::table('posts')
            ->where('type_showing', '!=', 4)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->where('type', 'Công nghệ')
            ->limit(10)
            ->orderBy('created_at', 'DESC')
            ->get();
        $listProductRelated = DB::table('products')
            ->limit(10)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('pages.contents.news.index', compact(
            'listPost',
            'listPostRelated',
            'listProductRelated'
        ));
    }

    public function detail($id)
    {
        $detailPost = DB::table('posts')->where('id', '=', $id)->get();
        $dataDetail = $detailPost[0];
        $listPostRelated = DB::table('posts')
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->where('type', 'Công nghệ')
            ->limit(10)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('pages.contents.news.detailNews', compact(
            'dataDetail',
            'listPostRelated'
        ));
    }

    public function postMobile()
    {
        $listPost = DB::table('posts')
            ->where('type_showing', '!=', 4)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->where('type', 'Sự kiện')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        return response()->json(['res' => $listPost]);
    }

    public function detailMobile($id)
    {
        $detailPost = DB::table('posts')->where('id', '=', $id)->get();
        return response()->json(['res' => $detailPost]);
    }

    public function newByType(Request $request)
    {
        $dataRequest = $request->all();
        $listPost = DB::table('posts')
            ->where('type', $dataRequest['type'])
            ->where('type_showing', '!=', 4)
            ->where('deleted', '=', 0)
            ->where('status', '=', 1)
            ->orderBy('created_at', 'DESC')
            ->get();
        return response()->json(['res' => $listPost]);
    }
}
