<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pages\Contract;
class BranchController extends Controller
{
    //
    // private $contact;
    protected $contact;
    public function __construct(Contract $contact){
        $this->contact = $contact;
    }

    public function index(){
        $listBranch = DB::table('branch')->get();
        return view('pages.contents.branch.branch',compact('listBranch'));
    }
}
