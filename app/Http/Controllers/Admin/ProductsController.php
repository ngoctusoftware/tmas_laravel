<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Products;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helpers;
use App\Imports\ProductImports;
use App\Exports\ProductExport;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;

class ProductsController extends Controller
{
    protected $products;
    public function __construct(Products $products)
    {
        $this->products = $products;
    }
    public function index(Request $request)
    {
        $data['prd_model'] = $this->products->index($request);
        $data['title'] = 'Danh sách sản phẩm';
        return view('admin.contents.products.index', $data);
    }

    public function create()
    {
        $data['title'] = "Thêm sản phẩm";
        $data['categories'] = $this->products->getCategories();
        $data['model'] = [];
        return view('admin.contents.products.details', $data);
    }

    public function update(Request $request)
    {
        $input = $request->only([
            'name', 'code', 'slug', 'tag', 'images', 'contents', 'description','info','special','operation','video', 'price', 'featured','optionName', 'warranty', 'cat_id', 'status', 'deleted', 'meta_title', 'meta_keywords', 'meta_description', 'meta_og_url', 'order'
        ]);

        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        $validator = Validator::make($input, [
            'name' => 'required',
            'code' => 'required',
            'slug' => 'required',
            'tag' => 'required',
            'images' => 'required',
            'contents' => 'required',
            'info' => 'required',
            'special' => 'required',
            'operation' => 'required',
            'video' => 'required',
            'description' => 'required',
            'price' => 'required',
            'featured' => 'required',
            'warranty' => 'required',
            'cat_id' => 'required',
            'optionName' => 'required',
            'status' => 'required',
            'deleted' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'meta_og_url' => 'required',
            'order' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }
        $add_model = $this->products->prdSave($request, null);
        if ($add_model == true) {
            return response()->json([
                'status' => true,
                'message' => 'Sản phẩm đã được thêm mới thành công'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Sản phẩm thêm thất bại'
            ]);
        }
    }

    public function edit($id)
    {
        $data['categories'] = $this->products->getCategories();
        $data['model'] = $this->products->edit($id);
        $data['images'] = json_decode($data['model']['images']);
        $data['title'] = "Chỉnh sửa sản phẩm";
        return view('admin.contents.products.details', $data);
    }

    public function save(Request $request, $id)
    {
        $input = $request->only([
            'name', 'code', 'slug', 'tag', 'images', 'contents','info','special','operation','video', 'description', 'price', 'featured', 'warranty','optionName', 'cat_id', 'status', 'deleted', 'meta_title', 'meta_keywords', 'meta_description', 'meta_og_url', 'order'
        ]);
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        $validator = Validator::make($input, [
            'name' => 'required',
            'code' => 'required',
            'slug' => 'required',
            'tag' => 'required',
            'info' => 'required',
            'special' => 'required',
            'operation' => 'required',
            'video' => 'required',
            'images' => 'required',
            'contents' => 'required',
            'description' => 'required',
            'price' => 'required',
            'featured' => 'required',
            'warranty' => 'required',
            'optionName' => 'required',
            'cat_id' => 'required',
            'status' => 'required',
            'deleted' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'meta_og_url' => 'required',
            'order' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }

        $upModel = $this->products->prdSave($request, $id);
        if ($upModel >= 1) {
            return response()->json([
                'status' => true,
                'message' => 'Sản phẩm đã được cập nhật thành công'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Sản phẩm đã được cập nhật thất bại'
            ]);
        }
    }

    public function Deleted($id)
    {
        $input = [
            'deleted' => 1,
            'status'  => 2,
            'updated_at' => Date('Y-m-d H:i:s')
        ];
        $deleted = Products::where('id', $id)->update($input);
        if ($deleted > 0) {
            return response()->json([
                'status' => true,
                'message' => 'Sản phẩm đã được xóa thành công'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu xóa không thành công'
            ]);
        }
    }
    public function changeStatusDeleted(Request $request, $id)
    {
        $input = $request->only(['status', 'deleted']);
        $status = Products::where('id', $id)->update($input);
        return $status;
    }
    public function import(Request $request)
    {

        // Lấy tên file ảnh
        $filename = $_FILES['file']['name'];
        //Lấy kích thước của file ảnh
        $fileSize = $_FILES['file']['size'];
        //Kiểm tra kích thước có lớn hơn 2MB không
        if ($fileSize > 2097152) {
            return response()->json([
                'status' => false,
                'message' => "Vui lòng kiểm tra dung lượng ảnh (>2MB)"
            ]);
        }

        $temp = explode(".", $_FILES["file"]["name"]);
        $d = new DateTime();
        $id = $d->getTimestamp();
        $newfilename = $id . '.' . end($temp);
        // $location = "upload/" . $newfilename;                 
        $location = env('APP_ENV') == 'production' ? 'public/upload/products/' . $newfilename : "upload/products/" . $newfilename;
        $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
        $imageFileType = strtolower($imageFileType);

        /* Valid extensions */
        $valid_extensions = array("jpg", "jpeg", "png", "gif");
        $response = 0;
        /* Check file extension */
        if (in_array(strtolower($imageFileType), $valid_extensions)) {
            /* Upload file */
            if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
                $response = $location;
            }
            return response()->json([
                'status' => true,
                'message' => "Tải ảnh thành công",
                'url' => $location
            ]);
        } else {
            return response()->json([
                'status' => false,
                'response' => "File upload không phải file ảnh"
            ]);
        }
    }

    public function imports(Request $request)
    {
        /* Getting file name */

        $filename = $_FILES['file']['name'];
        $temp = explode(".", $_FILES["file"]["name"]);
        $id = rand();
        $newfilename = $id . '.' . end($temp);
        $location = env('APP_ENV') == "production" ? 'public/upload/products/' . $newfilename : "upload/products/" . $newfilename;
        $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
        $imageFileType = strtolower($imageFileType);

        /* Valid extensions */
        $valid_extensions = array("jpg", "jpeg", "png");
        $response = [];
        /* Check file extension */
        if (in_array(strtolower($imageFileType), $valid_extensions)) {
            /* Upload file */
            if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
                array_push($response, $location);
            }

            return response()->json([
                'status' => true,
                'filename' => $newfilename,
                'id' => $id
            ]);
        } else {
            return response()->json([
                'status' => false,
                'response' => $response
            ]);
        }

        // echo $response;
    }

    public function ImagesDelete(Request $request, $id)
    {

        $images = $request->only('images');
        $model = Products::where('id', $id)->update($images);
        if ($model >= 1) {
            return [
                'status' => true,
                'message' => 'Ảnh sản phẩm được xóa thành công'
            ];
        } else {
            return [
                'status' => true,
                'message' => 'Ảnh sản phẩm xóa không thành công'
            ];
        }
    }
    public function ProductsImportExcel(Request $request)
    {
        Excel::import(new ProductImports, request()->file('file'));
        return response()->json([
            'status' => true,
            'message' => 'Tải file thành công'
        ]);
    }
    public function ProductsExport(Request $request)
    {
        return Excel::download(new ProductExport, 'ProductsExport.xlsx'); //download file export
    }
}
