<?php

namespace App\Http\Controllers\Admin;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Categories;
use App\Exports\CategoriesTemplateExport;
use App\Exports\CategoriesDataExport;
use App\Imports\CategoriesImport;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    private $categories;
    
    public function __construct(Categories $categories, Excel $excel)
    {
        $this->excel = $excel;
        $this->categories = $categories;
    }

    public function index(Request $request){  
        $data['type'] = 2;
        $data['cat_model'] =$this->categories->index($request,  $data['type']);
        $data['cat_name_old'] = $request->cat_name;       
        $data['title'] = 'Nhóm sản phẩm'; 
        $data['slug'] = 'nhom-san-pham'; 
        return view('admin.contents.categories.index', $data);
    }
    public function home(Request $request){
        $data['type'] = 1;
        $data['slug'] = 'chuyen-muc'; 
        $data['cat_model'] =$this->categories->index($request,  $data['type']);
        $data['cat_name_old'] = $request->cat_name;       
        $data['title'] = 'Chuyên mục';                
        return view('admin.contents.categories.index', $data);
    }
    public function DownloadTemplate(Request $request){
        ob_end_clean(); // this
        ob_start(); // and this
        $query =  $this->excel->download(new CategoriesTemplateExport, 'template.xlsx');
        return  $query;
    }
    public function Import(){
        $this->excel->import(new CategoriesImport,request()->file('file'));        
        return back();
    }
    public function Export(Request $request) {      
        
        ob_end_clean(); // this
        ob_start(); // and this
        if(isset($request->search['cat_name']) && $request->search['cat_name']!=""){            
            // return $this->excel->download(new CategoriesDataExport($request->search), 'ExportExcel.xlsx');
            return Excel::download(new CategoriesDataExport($request->search), 'ExportExcel.xlsx');
        }
        else {            
            $search = [];
            return Excel::download(new CategoriesDataExport($search), 'ExportExcel.xlsx');
            // $query =  $this->excel->download(new CategoriesDataExport($search), 'ExportExcel.xlsx');
            // return $query;
        }
    }

    public function save(Request $request, $id){              
        $input = $request->only(['name','slug','created_at','status','deleted', 'updated_at', 'type']);        
        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required'
        ]);        
        if ($validator->fails()) {
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }

        $cat_model = $this->categories->cUpdate($request, $id);

        if($cat_model==true){
            return response() -> json([
                'status' => true,
                'message' => 'Danh mục cập nhật thành công'
            ]);
        }
        else{
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu cập nhật thất bại'
            ]); 
        }
    }

    public function store(Request $request){  
        $input = $request->only(['name','slug','created_at','status','deleted', 'updated_at', 'type']);        
        $input['updated_at'] = Date('Y-m-d H:i:s');           
        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required'
        ]);
        
        if ($validator->fails()) {
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }


        
        $cat_model = $this->categories->store($request);        
        if($cat_model == true){
            return response() -> json([
                'status' => true,
                'message' => 'Dữ liệu được thêm mới thành công'
            ]);
        }
        else{
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu cập nhật thất bại'
            ]); 
        }
    }

    public function edit ($id) {        
        $data['model'] = $this->categories->edit($id);
        $data['title'] = "Chỉnh sửa nhóm sản phẩm";
        $data['name'] = "nhóm sản phẩm";
        $data['type'] = 2;
        return view('admin.contents.categories.details', $data);
    }
    public function view ($id) {        
        $data['model'] = $this->categories->edit($id);
        $data['title'] = "Chỉnh sửa chuyên mục";
        $data['name'] = "chuyên mục";
        $data['type'] = 1;
        return view('admin.contents.categories.details', $data);
    }

    public function remove($id){
        
        $cat_model = $this->categories->remove($id);
        if($cat_model >= 1) {
            return response() -> json([
                'status' => true,
                'message' => 'Chuyên mục đã được xóa thành công'
            ]);
        }
        else {
            return response() -> json([
                'status' => false,
                'message' => 'Chuyên mục đã được xóa thất bại'
            ]);
        }
    }
}
