<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Posts;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helpers;
use DateTime;

class PostsController extends Controller
{
    private $posts;
    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    public function index(Request $request)
    {
        $data['post_model'] = $this->posts->index($request);
        $data['title'] = 'Danh sách bài đăng';
        return view('admin.contents.posts.index', $data);
    }


    public function getProductByIdController($id)
    {
        if (isset($id)) {
            $data['model'] = $this->posts->getProductByIdModel($id);
        }
        $data['title'] = "Chi tiết bài viết";
        return view('admin.contents.posts.details', $data);
    }

    public function updateProductController(Request $request, $id)
    {
        $input = $request->only([
            'name', 
            'slug', 
            'content', 
            'description', 
            'type', 
            'type_showing', 
            'is_featured', 
            'featured_image', 
            'meta_og_image', 
            'order', 
            'status', 
            'created_by', 
            'published_at', 
            'created_at', 
            'meta_title', 
            'meta_keywords', 
            'meta_description', 
            'meta_og_url'
        ]);

        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'description' => 'required',
            'type' => 'required',
            'is_featured' => 'required',
            'featured_image' => 'required',
            'type_showing' => 'required',
            'status' => 'required',
            'created_by' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'meta_og_url' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }
        $model = $this->posts->UpdateProductModel($request, $id);

        if ($model) {
            return [
                'status' => true,
                'message' => "Cập nhật thành công"
            ];
        } else {
            return [
                'status' => false,
                'message' => "Cập nhật thất bại"
            ];
        }
    }

    public function create()
    {
        $data['title'] = "Chi tiết bài viết";
        $data['model'] = [];
        return view('admin.contents.posts.details', $data);
    }

    public function createPostController(Request $request)
    {
       
        $input = $request->only([
            'name',
            'slug',
            'content',
            'description',
            'type',
            'type_showing',
            'is_featured',
            'featured_image',
            'meta_og_image',
            'order',
            'status',
            'created_by',
            'published_at',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'meta_og_url',
        ]);

        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');

        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'description' => 'required',
            'type' => 'required',
            'is_featured' => 'required',
            'type_showing' => 'required',
            'featured_image' => 'required',
            'status' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'meta_og_url' => 'required'
        ]);
        // dd($validator->fails());

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }
        //Update
        $model = $this->posts->createPostModel($request);
        return response()->json([
            'status' => true,
            'message' => 'Bài viết đã được thêm mới'
        ]);
    }

    //Image import
    public function Import(Request $request)
    {

        // Lấy tên file ảnh
        $filename = $_FILES['file']['name'];
        //Lấy kích thước của file ảnh
        $fileSize = $_FILES['file']['size'];

        //Kiểm tra kích thước có lớn hơn 2MB không
        if ($fileSize > 2097152) {
            return response()->json([
                'status' => false,
                'message' => "Vui lòng kiểm tra dung lượng ảnh (>2MB)"
            ]);
        }

        $temp = explode(".", $_FILES["file"]["name"]);
        $d = new DateTime();
        $id = $d->getTimestamp();
        $newfilename = $id . '.' . end($temp);
        // $location = "upload/" . $newfilename;                 
        $location = env('APP_ENV') == 'production' ? 'public/upload/posts/' . $newfilename : "upload/posts/" . $newfilename;
        $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
        $imageFileType = strtolower($imageFileType);

        /* Valid extensions */
        $valid_extensions = array("jpg", "jpeg", "png", "gif");
        $response = 0;
        /* Check file extension */
        if (in_array(strtolower($imageFileType), $valid_extensions)) {
            /* Upload file */
            if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
                $response = $location;
            }
            return response()->json([
                'status' => true,
                'message' => "Tải ảnh thành công",
                'url' => $location
            ]);
        } else {
            return response()->json([
                'status' => false,
                'response' => "File upload không phải file ảnh"
            ]);
        }
    }
    //Export import
    public function ImportExcel(Request $request)
    {

        // Lấy tên file ảnh
        $filename = $_FILES['file']['name'];
        //Lấy kích thước của file ảnh
        $fileSize = $_FILES['file']['size'];

        //Kiểm tra kích thước có lớn hơn 2MB không
        if ($fileSize > 2097152) {
            return response()->json([
                'status' => false,
                'message' => "Vui lòng kiểm tra dung lượng ảnh (>2MB)"
            ]);
        }

        $temp = explode(".", $_FILES["file"]["name"]);
        $d = new DateTime();
        $id = $d->getTimestamp();
        $newfilename = $id . '.' . end($temp);
        // $location = "upload/" . $newfilename;                 
        $location = env('APP_ENV') == 'production' ? 'public/upload/' . $newfilename : "upload/" . $newfilename;
        $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
        $imageFileType = strtolower($imageFileType);

        /* Valid extensions */
        $valid_extensions = array("xlsx", "xls");
        $response = 0;
        /* Check file extension */
        if (in_array(strtolower($imageFileType), $valid_extensions)) {
            /* Upload file */
            if (move_uploaded_file($_FILES['file']['tmp_name'], $location)) {
                $response = $location;
            }
            return response()->json([
                'status' => true,
                'message' => "Tải ảnh thành công",
                'url' => $location
            ]);
        } else {
            return response()->json([
                'status' => false,
                'response' => "File upload không phải file excel"
            ]);
        }
    }

    public function Deleted($id)
    {
        $deleted = Posts::where('id', $id)->delete();
        if ($deleted > 0) {
            return response()->json([
                'status' => true,
                'message' => 'Dữ liệu đã xóa thành công'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu xóa không thành công'
            ]);
        }
    }
}
