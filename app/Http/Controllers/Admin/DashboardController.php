<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\AdminNavigation;
class DashboardController extends Controller
{
    private $navitions;
    public function __construct(AdminNavigation $navitions){
        $this->navitions = $navitions;
    }
    public function index(){                  
        return view('admin.contents.dashboard.index');
    }
}
