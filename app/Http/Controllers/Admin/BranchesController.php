<?php

namespace App\Http\Controllers\Admin;

use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Branches;
// use App\Exports\BranchesTemplateExport;
// use App\Exports\BranchesDataExport;
// use App\Imports\BranchesImport;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class BranchesController extends Controller
{
    private $branches;

    public function __construct(Branches $branches, Excel $excel)
    {
        $this->excel = $excel;
        $this->branches = $branches;
    }

    public function index(Request $request)
    {
        $data['branch_model'] = $this->branches->index($request);
        $data['title'] = 'Chi nhánh';
        return view('admin.contents.branches.index', $data);
    }

    public function save(Request $request)
    {
        $input = $request->only(['id', 'name', 'phone', 'email', 'type', 'type_store', 'address', 'quantity_staff', 'manager_name', 'created_at', 'updated_at']);

        $validator = Validator::make($input, [
            'name' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }

        $branch_model = $this->branches->BranchesUpdate($input);

        if ($branch_model == true) {
            return response()->json([
                'status' => true,
                'message' => 'Chi nhánh đã được thêm mới'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Dữ liệu chưa được thêm mới'
            ]);
        }
    }

    public function delete(Request $request)
    {
        $input = $request->only(['id']);
        $branch_model = $this->branches->BranchesDelete($input);
        if ($branch_model == true) {
            return response()->json([
                'status' => true,
                'message' => 'Chi nhánh đã được xoá'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => $branch_model
            ]);
        }
    }
}
