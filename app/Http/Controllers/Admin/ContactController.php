<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index(){
        $data['contact'] = DB::table('send_message_info')->orderBy('id', 'DESC')->get();
        return view('admin.contents.contact.index', compact(
            'data',
        ));
    }
}
