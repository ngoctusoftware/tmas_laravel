<?php

namespace App\Exports;

use App\Models\Admin\Categories;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
$i = 1;
class CategoriesDataExport implements FromCollection,WithHeadings,WithMapping
{
    
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(array $search) {
        
    	$this->search = $search;        
    }

    public function collection()
    {
        $cat_model = new Categories;
        if(isset($this->seach['cat_name']) && $this->seach['cat_name']!="") $cat_model = $cat_model -> where('name',$this->seach['cat_name']);
        $cat_model = $cat_model -> select([ 'id', 'name', 'slug','status', 'deleted', 'created_at', 'updated_at']);
        $cat_model = $cat_model -> where('deleted',0)->orderBy('name','DESC')->get();
        
        return $cat_model;
    }
        
    public function headings(): array {
        return [
            'STT',
            'Tên danh mục' ,        
            'Tên định danh',
            'Trạng thái',
            'Ngày tạo'
        ];
    }
    public function map($categories):array{
        GLOBAL $i;
        $i++;
        $cat_name = $categories ->  name;
        $slug = $categories ->  slug;
        $status = $categories ->  status;
        $created_at = $categories ->  created_at;
        $data = [
            $i,
            $cat_name,
            $slug,
            $status,
            $created_at
        ];        
        return $data;
    }
}
