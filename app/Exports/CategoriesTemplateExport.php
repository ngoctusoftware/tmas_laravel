<?php

namespace App\Exports;

use App\Models\Admin\Categories;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
class CategoriesTemplateExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Categories::all();
    }
    public function headings(): array {
       
        return [      
            'Tên danh mục',
            'Slug'
        ];  
    }
    //Giá trị
    public function map($orders): array {
        return [        
            
         ];
    }
}
