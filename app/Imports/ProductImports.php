<?php

namespace App\Imports;

use App\Models\Admin\Products;
use App\Models\Admin\Categories;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Helpers\Helpers;
use Date;
class ProductImports implements ToModel,WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    //Load từ dòng thứ 2 bỏ qua header

    public function startRow(): int
    {
        return 2;
    }
    public function model(array $row)
    {

        $cat_name = $row[2];        
        $cat_model = Categories::select(['id','name'])
                              ->  where('name','Like','%'. $cat_name .'%')
                              -> where('type',2)
                                ->first();
        $cat_id = $cat_model->id;
        switch ($row[3]) {
            case 'Còn hàng':
                $status = 1;
                break;
            case 'Hết hàng':
                $status = 0;
                break;            
            case 'Đã xóa';
                $status = 2;
            default:
                $status = 1;
                break;
        }

        switch ($row[3]) {
            case 'Có':
                $featured = 1;
                break;
            case 'Không':
                $featured = 0;
                break;                        
            default:
                $featured = 0;
                break;
        }
        

        $slug = slugify($row[1]);
        if(isSlugExist($slug,'products') == true) return;

        
        $data = [
            'code'=>$row[0],
            'name'=>$row[1],
            'cat_id'=>$cat_id,
            'status'=>$status,
            'tag'=>$row[4],
            'description'=>$row[5],
            'contents'=>$row[6],            
            'price'=>$row[7],
            'warranty'=>$row[8],
            'news'=>$row[9],
            'featured'=>$featured,
            'promotion'=>$row[11],
            'accessories'=>$row[12],
            'meta_title'=>$row[13],
            'meta_keywords'=>$row[14],
            'meta_og_url'=>$row[15],
            'meta_description'=>$row[16],
            'order'=>$row[17],
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
            'slug'=> $slug,
            'deleted'=> 0
        ];              
        return new Products($data);
    }
}
