<?php

namespace App\Imports;
use App\Models\Admin\Categories;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Validation\Rule;
use App\Helpers\Helpers;
class CategoriesImport implements ToModel
{
    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    function slugify1($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);
    
        // transliterate
        // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
    
        // trim
        $text = trim($text, $divider);
    
        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);
    
        // lowercase
        $text = strtolower($text);
    
        if (empty($text)) {
            return 'n-a';
        }
    
        return $text;
    }

    public function model(array $row)
    {
        
        $data = [
            'name'    =>  $row[0],            
            'slug'    => slugify($row[0]),             
            'status'  => 1,
            'deleted' => 0,
            'created_at' => date('Y-m-d H:i:s')
        ];                
        
        return new Categories($data);
    }
   
}
