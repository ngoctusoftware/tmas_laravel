<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Branches extends Model
{
    use HasFactory;
    protected $table = 'branch';
    protected $fillable = [
        'id', 'name', 'phone', 'email', 'type', 'type_store', 'address', 'quantity_staff', 'manager_name', 'created_at', 'updated_at'
    ];
    protected $primaryKey = null;
    public $incrementing = false;

    public function index(Request $request)
    {
        $branch_model = new Branches();
        $branch_model = $branch_model->select(['id', 'name', 'type', 'type_store', 'address']);
        $branch_model = $branch_model->orderby('type','asc')->get();
        return $branch_model;
    }

    public function BranchesUpdate($data)
    {
        if (!isset($data['id'])) {
            $branch_model = new Branches();
            $branch_model = Branches::insert($data);
            return $branch_model;
        } else {
            $branch_model = new Branches();
            $branch_model = $branch_model->where('id', $data['id'])->update($data);
            return $branch_model;
        }
    }

    public function BranchesDelete($data)
    {
        $branch_model = new Branches();
        $branch_model = $branch_model->where('id', $data['id'])->delete();
        return $data['id'];
    }
}
