<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Admin\Categories;
class Products extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['id','name','code','slug','tag','image','images','contents','description','info','special','operation','video','optionName','price','featured','warranty','cat_id','status','deleted','meta_title','meta_keywords','meta_description','meta_og_url','order','created_at','updated_at'];
    public function index(Request $request) {
        $model = new Products; 
        if(isset($request->name) && $request->name!=""){      
            $model = $model ->where('name','LIKE', '%'.$request->name.'%') ; 
        }        
        $model = $model -> select(['id', 'name', 'code', 'slug', 'tag', 'contents', 'description','info','special','operation','video','optionName', 'image','images', 'price', 'featured', 'warranty', 'cat_id', 'status', 'deleted', 'meta_title', 'meta_keywords', 'meta_description', 'meta_og_url', 'created_at', 'updated_at']);
        $model = $model ->orderBy('created_at','asc') -> get();        
        return $model;        
    }
    public function edit($id){   
        $model = Products::where('id',$id) -> first();  
        return $model;
    }
    public function prdSave(Request $request, $id){
        $input = $request -> only([
            'name','code','slug','tag','image','images','contents','description','info','special','operation','video','price','optionName','featured','warranty','cat_id','status','deleted','meta_title','meta_keywords','meta_description','meta_og_url','order'
        ]);
        if(!isset($id)) $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');        
        $validator = Validator::make($input, [
            'name'=>'required',
            'code'=>'required',
            'slug'=>'required',
            'tag'=>'required',            
            'contents'=>'required',
            'info' => 'required',
            'special' => 'required',
            'operation' => 'required',
            'video' => 'required',
            'description'=>'required',
            'price'=>'required',
            'featured'=>'required',
            'optionName'=>'required',
            'warranty'=>'required',
            'cat_id'=>'required',
            'status'=>'required',
            'deleted'=>'required',
            'meta_title'=>'required',
            'meta_keywords'=>'required',
            'meta_description'=>'required',
            'meta_og_url'=>'required',
            'order'=> 'required',                    
        ]);
      
        if ($validator->fails()) {            
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }

        if (isset($id)) {
            $model = Products::where('id' , $id) -> update($input);
        }
        else {
            $model = Products::insert($input);
        }

        
        return $model;
    }

    public function prdUpdate(Request $request){
        
        $input = $request -> only([
            'name','code','slug','tag','image','images','contents','description','info','special','operation','video', 'optionName','price','featured','warranty','cat_id','status','deleted','meta_title','meta_keywords','meta_description','meta_og_url','order'
        ]);
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');        
        $validator = Validator::make($input, [
            'name'=>'required',
            'code'=>'required',
            'slug'=>'required',
            'tag'=>'required',            
            'contents'=>'required',
            'info' => 'required',
            'special' => 'required',
            'operation' => 'required',
            'video' => 'required',
            'optionName'=>'required',
            'description'=>'required',
            'price'=>'required',
            'featured'=>'required',
            'warranty'=>'required',
            'cat_id'=>'required',
            'status'=>'required',
            'deleted'=>'required',
            'meta_title'=>'required',
            'meta_keywords'=>'required',
            'meta_description'=>'required',
            'meta_og_url'=>'required',
            'order'=> 'required',                    
        ]);
      
        if ($validator->fails()) {            
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }
        
        $model = Products::insert($input);
        return $model;
    }

    public function getCategories(){
        $model = Categories::select(['id','name','slug']) -> where('type',2)  ->  get();
        return $model;
    }

    
}
