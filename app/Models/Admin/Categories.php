<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class Categories extends Model
{
    use HasFactory;
    protected $table = 'categories';
    protected $fillable = [
        'id', 'name', 'slug','status', 'deleted', 'created_at', 'updated_at'
    ];
    public function index(Request $request,$type){     
         
        $cat_model = new Categories();
        if(isset($request->cat_name) && $request->cat_name!=""){
            $cat_model = $cat_model ->where('name','LIKE', '%'.$request->cat_name.'%') ; 
        }
        $cat_model = $cat_model -> select (['id', 'name', 'slug','status', 'deleted', 'created_at', 'updated_at']);        
        $cat_model = $cat_model -> where('status',1);
        $cat_model = $cat_model -> where('type',$type);
        $cat_model = $cat_model -> where('deleted',0)->orderBy('name','DESC')->get();
        return $cat_model;
    }

    //Thêm mới
    public function store(Request $request){     
        
        $input = $request->only(['name','slug','created_at','status','deleted', 'updated_at', 'type']);        
        $input['updated_at'] = Date('Y-m-d H:i:s');
        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required'
        ]);
        
        if ($validator->fails()) {
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }

        if(isSlugExist($input['slug'], 'categories')) {
            return response() -> json([
                'status' => false,
                'message' => 'Slug đã tồn tại'
            ]);
        } 
        $cat_model = Categories::insert($input);        
        return $cat_model;      
    }

    public function cUpdate(Request $request, $id){ 
        
        $input = $request->only(['name','slug','created_at','status','deleted', 'updated_at', 'type']);        
        $input['updated_at'] = Date('Y-m-d H:i:s');
        $validator = Validator::make($input, [
            'name' => 'required',
            'slug' => 'required'
        ]);
        if ($validator->fails()) {
            return response() -> json([
                'status' => false,
                'message' => 'Dữ liệu nhập chưa đầy đủ'
            ]);
        }
        if(isSlugExist($input['slug'], 'categories')) {
            return response() -> json([
                'status' => false,
                'message' => 'Slug đã tồn tại'
            ]);
        } 
        
        $cUpdate  = Categories::where('id',$id)->update($input);
        return $cUpdate;    
    }


    public function isSlugExist($slug){
        if($slug != ""){
            $model = Categories::where('slug',$slug) -> get();
            if(count($model)>0) return true;
            else return false;
        }
    }
    public function edit($id) {
        $model = Categories::find($id);
        return $model;
    }

    public function remove($id){
        $model = Categories::where('id',$id) -> delete();
        return $model;
    }
    
}
