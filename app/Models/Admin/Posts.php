<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Posts extends Model
{
    use HasFactory;
    protected $table = 'posts';
    protected $fillable = ['id', 'name', 'slug', 'deleted', 'content', 'description', 'type', 'is_featured', 'type_showing', 'featured_image', 'file', 'meta_og_image', 'order', 'status', 'created_by', 'published_at', 'created_at', 'meta_title', 'meta_keywords', 'meta_description', 'meta_og_url'];
    public function index(Request $request)
    {
        $model = new Posts;

        if (isset($request->name) && $request->name != "") {

            $model = $model->where('name', 'LIKE', '%' . $request->name . '%');
        }
        // $model = $model ->where('type','news') ; 
        $model = $model->select(['id', 'name', 'slug', 'content', 'description', 'type', 'is_featured', 'type_showing', 'featured_image', 'file', 'meta_og_image', 'order', 'status', 'created_by', 'published_at', 'created_at', 'meta_title', 'meta_keywords', 'meta_description', 'meta_og_url', 'deleted']);
        $model = $model->orderBy('created_at', 'asc')->get();

        return $model;
    }
    public function getProductByIdModel($id)
    {
        $model = Posts::where('id', $id)->first();
        return $model;
    }
    public function UpdateProductModel(Request $request, $id)
    {
        $input = $request->only([
            'name',
            'slug',
            'content',
            'description',
            'type',
            'is_featured',
            'featured_image',
            'type_showing',
            'meta_og_image',
            'order',
            'status',
            'created_by',
            'published_at',
            'created_at',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'meta_og_url', 
        ]);
        $model = Posts::where('id', $id)->update($input);
        return $model;
    }

    public function createPostModel(Request $request)
    {
        $input = $request->only([
            'name',
            'slug',
            'content',
            'description',
            'type',
            'type_showing',
            'is_featured',
            'featured_image',
            'meta_og_image',
            'order',
            'status',
            'deleted',
            'created_by',
            'published_at',
            'created_at',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'meta_og_url',
        ]);
      
        $model = Posts::insert($input);
        return $model;
    }
}
