<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminNavigation extends Model
{
    use HasFactory;
    protected $table = 'admin_navigations';
    protected $primary_key = 'id';
    protected $connection = 'mysql';
    protected $filter = 
        [
            'id', 'name', 'slug', 'parent', 'created_at', 'updated_at','icon','url'
        ];
    
    public function index(){
        $model = AdminNavigation::select('id', 'name', 'slug', 'parent', 'created_at', 'updated_at','icon','url')->get();                
        
        return $model;
    }
}
