<?php

namespace App\Models\Pages;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class Contract extends Model
{
    use HasFactory;
    protected $table = 'send_message_info';
    public function create(Request $request){

    }
    public function store(Request $request){
        $input = $request->only(['cus_name', 'cus_phone','cus_address', 'content']);
        $input['created_at'] = now();

        // $validator = Valid::make($input, [
        //     'cus_name'     => 'required',
        //     'cus_phone'    => 'required',
        //     'cus_address'  => 'required', 
        //     'content'      => 'required',
        // ]);
        
        // if ($validator->fails()) { 
        //     return;
        // }
        // else {
            $model = Contract::insert($input);
            return $model; 
        // }

             
    }
}
