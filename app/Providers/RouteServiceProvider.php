<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/TAdmin';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();      
    }
 
    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */    

    // Khai báo router
    public function map(){
        $this->mapApiRoutes();
        $this->mapWebRoutes();
        $this->mapQuantriRoutes();
    }

    
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }
    
    protected function mapQuantriRoutes()
    {
        Route::prefix('TAdmin')
            ->middleware('web')
            ->namespace('App\Http\Controllers')
            ->group(base_path('routes/admin/web.php'));
    }
}
