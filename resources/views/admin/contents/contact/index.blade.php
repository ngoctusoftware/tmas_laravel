@extends('admin.layouts.layout')

@section('title', 'Danh mục liên hệ')
@section('content')
    <div class="container-fluid">
        @include('admin.contents.title')
        <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('admin.contents.contact.components.table', ['contact' => $data['contact']])
                </div> 
            </div> 
        </div>
    </div> 
    @include('admin.contents.contact.components.modal_view_detail')
    </div>

@endsection
@section('scripts')
    <script src='assets/js/contact/index.js'></script>
@endsection
<!--end::script-->