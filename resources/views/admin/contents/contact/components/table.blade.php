<div class="row pt-4">
    <div class="col-sm-12">
        <div class="tab-pane show active" id="basic-datatable-preview">
            <table id="branch_datatables" class="table dt-responsive nowrap w-100 border-color-secondary">
                <thead>
                    <tr style="font-size: 16px; vertical-align: middle">
                        <th class="text-dark text-center">STT</th>
                        <th class="text-dark text-left">Tên khách hàng</th>
                        <th class="text-dark text-left">SĐT</th>
                        <th class="text-dark text-center">Địa chỉ</th>
                        <th class="text-dark text-center">Nội dung</th>
                        <th class="text-dark text-center">Thao tác</th>
                    </tr>
                </thead>
                <tbody style="font-size: 15px; vertical-align: middle;">
                    @foreach ($contact as $key => $item)
                    <tr class="align-item-center py-2">
                        <td class="text-dark text-center">{{$key + 1}}</td>
                        <td class="text-dark ">{{$item->cus_name}}</td>
                        <td class="text-dark ">{{$item->cus_phone}}</td>
                        <td class="text-dark ">{{$item->cus_address}}</td>
                        <td class="text-dark ">{{$item->content}}</td>
                        <td class="text-center">                            
                            <a onclick="showModal({{json_encode($item)}})">Xem chi tiết</a>
                        </td>   
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function showModal (item) {
        $('#contact_detail').modal('show');
        $('#cus_name').text(item.cus_name)
        $('#cus_address').text(item.cus_address)
        $('#cus_phone').text(item.cus_phone)
        $('#content').text(item.content)
    }
</script>