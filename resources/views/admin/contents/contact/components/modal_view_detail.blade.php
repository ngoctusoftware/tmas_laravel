<div class="modal fade" id="contact_detail" tabindex="-1" role="dialog" aria-labelledby="branch_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header px-0 mx-0">
                <h4 class="modal-title px-2 py-1 text-dark" id="branch_title"></h4>
            </div>
            <div class="modal-body">
                <div class="row show_info">
                    <div class="">
                        <p><span>Tên khách hàng: </span> <span id="cus_name"></span></p>
                    </div>
                    <div class="">
                        <p><span>Địa chỉ: </span> <span id="cus_address"></span></p>
                    </div>
                    <div class="">
                        <p><span>SĐT: </span> <span id="cus_phone"></span></p>
                    </div>
                    <div class="">
                        <p><span>Nội dung: </span> <span id="content"></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="text-align: end">
                        <button type="button" class="btn btn-danger" name="btnHuy" id="btnHuy" onclick="$('#contact_detail').modal('hide')">
                            <i class="uil uil-cancel" style="font-size: 18px"></i>
                            Hủy bỏ
                        </button>
                    </div>
                </div>
                <!-- Content -->
            </div>
        </div>
    </div>
</div>