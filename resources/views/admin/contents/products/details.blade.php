@extends('admin.layouts.layout')

@section('title', 'Chi tiết bài viết')

@section('content')


@include('admin.contents.title')
<div class="container-fluid group-product">
    <div class="row">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h3 style="color: #000; text-transform: uppercase"><i class="uil uil-file-alt" style="font-size:30px;"></i>
                        {{ isset($title) ? $title : '' }}
                    </h3>
                </div>
                <div class="card-body">
                {{ isset($model['id']) ? $model['id'] : '' }}      
                              <div class="container-fluid ">
                        <!-- Name, Slug, code-->
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Tên sản phẩm</label><label class="text-danger px-1"> *</label>
                                    <input type="text" onkeydown="onSlug(event)" onblur="onGetSlug()" value="{{ isset($model['name']) ? $model['name'] : '' }}" name="name" id="name" placeholder="Nhập tên" required class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Slug</label><label class="text-danger px-1"> *</label>
                                    <input type="text" value="{{ isset($model['slug']) ? $model['slug'] : '' }}" name="slug" id="slug" placeholder="Nhập slug" required class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Mã sản phẩm</label><label class="text-danger px-1"> *</label>
                                    <input type="text" value="{{ isset($model['code']) ? $model['code'] : '' }}" name="code" id="code" required placeholder="Nhập mã sản phẩm" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- Nhóm sản phẩm, Trạng thái, tag -->
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Nhóm sản phẩm</label><label class="text-danger px-1"> *</label>
                                    <select name="cat_id" id="cat_id" class="form-control select2">
                                        <option>--- Chọn nhóm sản phẩm ---</option>
                                        @if (isset($categories))
                                        @foreach ($categories as $item)
                                        <option class="text-dark" {{isset($model['cat_id']) ? ($model['cat_id'] == $item['id'] ? 'selected' : '') : ''}} value="{{ isset($item['id']) ? $item['id'] : '' }}">
                                            {{ isset($item['name']) ? $item['name'] : '' }}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Trạng thái</label><label class="text-danger px-1"> *</label>
                                    <select name="prdStatus" id="prdStatus" class="form-control" onchange="changDeletedStatus({{isset($model['id']) ? $model['id'] : null}})">
                                        <option value="1" selected>Còn hàng</option>
                                        <option value="0">Hết hàng</option>
                                        <option value="2">Đã xóa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Tag</label><label class="text-danger px-1"> *</label>
                                    <input type="text" class="form-control" name="tag" id="tag" placeholder="Nhập tag" value=" {{ isset($model['tag']) ? $model['tag'] : '' }}">
                                </div>
                            </div>
                        </div>
                        <!-- Descrition -->
                        <div class="row py-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Video hướng dẫn</label><label class="text-danger px-1"> *</label>
                                    <input type="text" class="form-control" name="video" id="video" placeholder="Nhập url video" value=" {{ isset($model['video']) ? $model['video'] : '' }}">
                                </div>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Mô tả</label><label class="text-danger px-1"> *</label>
                                    <textarea required name="description" id="description" cols="30" rows="5" class="form-control">{{ isset($model['description']) ? $model['description'] : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Nguyên lý hoạt động</label><label class="text-danger px-1"> *</label>
                                    <textarea required name="operation" id="operation" cols="30" rows="5" class="form-control">{{ isset($model['operation']) ? $model['operation'] : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Đặc điểm nổi bật</label><label class="text-danger px-1"> *</label>
                                    <textarea required name="special" id="special" cols="30" rows="5" class="form-control">{{ isset($model['special']) ? $model['special'] : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                       
                        <!-- Content -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Content</label><label class="text-danger px-1"> *</label>
                                    <div>
                                        <textarea name="content" required id="content" class="form-control">{!! isset($model['contents']) ? $model['contents'] : '' !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-sm-3">
                                <div class="form-group featured">
                                    <label>Sản phẩm đặc sắc</label><label class="text-danger px-1"> *</label>
                                    <select name="featured" id="featured" class="form-control">
                                        <option value="1" selected>
                                            Có
                                        </option>
                                        <option value="0">
                                            Không
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- Đơn giá, Bảo hành, Phụ kiện -->
                        <div id="insert_option">
                            <div class="row py-2">
                                <div class="col-sm-4">
                                    <label class="pr-2">Tên Options</label><label class="text-danger px-1"> * </label>
                                    <div class="input-group flex-nowrap">
                                        <input type="text" class="form-control optionName" placeholder="Nhập tên" name="optionName_1" id="optionName_1" value="">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="pr-2">Đơn giá </label><label class="text-danger px-1"> * </label>
                                    <div class="input-group flex-nowrap">
                                        <input type="number" class="form-control price" placeholder="Nhập đơn giá" name="price_1" id="price_1" value="">
                                        <span class="input-group-text" name="unit">vnđ</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Bảo hành </label><label class="text-danger px-1"> * </label>
                                    <div class="input-group flex-nowrap">
                                        <input type="number" class="form-control warranty" placeholder="Nhập số tháng bảo hành" value="" name="warranty_1" id="warranty_1">
                                        <span class="input-group-text" name="unit">tháng</span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label>Thông số sản phẩm 1</label><label class="text-danger px-1"> * </label>
                                    <div class="input-group flex-nowrap">
                                        <textarea class="form-control info" placeholder="Nhập thông số sản phẩm" value="" name="info_1" id="info_1"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Đặc sắc, Khuyến mãi, Phụ kiện -->
                        <div class="row py-2">
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-primary" onclick="addOption()">Thêm options</button>
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="row pt-2">
                            <div class="col-sm-12" style="text-align: end">
                                @if (isset($model['id']))
                                <button type="button" class="btn btn-primary" onclick="save('{{ $model['id'] }}')">
                                    Cập nhật
                                </button>
                                @else
                                <button type="button" class="btn btn-primary" onclick="update()">
                                    Thêm mới
                                </button>
                                @endif
                                <a href="{{ route('admin.products') }}" class="btn btn-secondary">
                                    Quay lại
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <!------------------ Upload ảnh sản phẩm --------------------->
            <div class="card">
                <div class="card-header">
                    <h3 class="text-dark">Ảnh sản phẩm</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 align-seft-middle">
                            <div class="custom-file">
                                @include('admin.contents.products.components.pSlider', ['images' => isset($model['images']) ? $model['images'] : null])
                            </div>
                        </div>
                        <div class="col-lg-12 pt-2" style="text-align: end">
                            <button type="buton" onclick="onShowModalImportImages()" style="font-size: 16px;" data-bs-toggle="modal" data-bs-target="#post_import" class="btn btn-secondary py-1 px-3" data-control="#input_import" id="uploadFile">Tải ảnh</button>
                        </div>
                    </div>
                </div>
            </div>
            <!------------------ Meta --------------------->
            <div class="card py-2">
                <div class="card-header">
                    <h3 class="text-dark">Meta</h3>
                </div>
                <div class="card-body py-0">
                    <!-- Meta Title, Meta Keywords, Order -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Meta Title</label><label class="text-danger px-1"> *</label>
                                <input type="text" value="{{ isset($model['meta_title']) ? $model['meta_title'] : '' }}" required name="meta_title" id="meta_title" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Meta Keywords </label><label class="text-danger px-1"> *</label>
                                <input type="text" value="{{ isset($model['meta_keywords']) ? $model['meta_keywords'] : '' }}" required name="meta_keywords" id="meta_keywords" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Order </label>
                                <input type="number" value="{{ isset($model['order']) ? $model['order'] : '' }}" name="order" id="order" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- Meta_description ,meta_og_image -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Meta Description</label><label class="text-danger px-1"> *</label>
                                <input type="text" value="{{ isset($model['meta_description']) ? $model['meta_description'] : '' }}" name="meta_description" required id="meta_description" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- Meta_og_url -->
                    <div class="row py-2">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Meta Open Graph URL</label><label class="text-danger px-1"> *</label>
                                <input type="text" value="{{ isset($model['meta_og_url']) ? $model['meta_og_url'] : '' }}" name="meta_og_url" id="meta_og_url" required class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card py-2">
                <div class="card-header d-flex" style="justify-content: space-between">
                    <h3 class="text-dark">Dòng xe lắp đặt sản phẩm</h3>
                    <button class="btn btn-primary" id="openModal" data-bs-toggle="modal" data-bs-target="#modal_result">Chỉnh sửa</button>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_result" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header px-0 mx-0">
                <h5 class="modal-title px-2" id="exampleModalLabel">Chỉnh sửa thêm xe</h5>
            </div>
            <div class="modal-body">
                <!-- Content -->
                <div class="card-body py-0">
                    <div class="row space_lg" id="model_result">

                    </div>
                </div>
                <!-- Content -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng cửa sổ</button>
            </div>
        </div>
    </div>
</div>

@include('admin.contents.products.components.modal_upload_img', ['images' => isset($model['images']) ? $model['images'] : null])

@endsection
@section('scripts')
<script src="http://cdn.ckeditor.com/4.8.0/standard-all/ckeditor.js"></script>
<script>
    if (APP_ENV == 'production') str = '../../../public/upload/ckeditor/prdUpload.php';
    else str = '../../../upload/ckeditor/prdUpload.php';
    CKEDITOR.replace('content', {
        height: 300,
        filebrowserUploadUrl: str,
    });
    $('#prdStatus,#cat_id,#featured').select2({
        theme: "bootstrap-5",
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        placeholder: $(this).data('placeholder'),
    });
    $(document).ready(function() {
        const id = <?php echo isset($model['id']) ? $model['id'] : 0; ?>;
        if (id !== 0) {
            const optionName = <?php echo isset($model['optionName']) ? $model['optionName'] : 0; ?>;
            const price = <?php echo isset($model['price']) ? $model['price'] : 0; ?>;
            const warranty = <?php echo isset($model['warranty']) ? $model['warranty'] : 0; ?>;
            console.log(optionName)
            console.log(price)
            console.log(warranty)
            optionName.forEach((item, index) => {
                if (index !== 0) {
                    addOption(index + 1);
                }
                // if (index === 0) {
                $(`#optionName_${index+1}`).val(optionName[index].optionName)
                $(`#price_${index+1}`).val(price[index].price)
                $(`#warranty_${index+1}`).val(warranty[index].warranty)
                // }

            })
        }

    })
</script>
<script src="{{env('APP_ENV') == "production" ? asset('public/admin/assets/js/products/details.js') : asset('/admin/assets/js/products/details.js')}}"></script>
@endsection
<!--end::script-->

<style>
    .carousel-control-next-icon,
    .carousel-control-prev-icon {
        background-color: #000;
    }

    .group-product {
        height: calc(100% - 225px);
        overflow-y: scroll;
    }

    .optionRelative {
        position: relative;
    }

    .removeOption {
        width: 40px !important;
        border-radius: 4px;
        border: 0;
        position: absolute;
        top: 0;
        right: 0;
        color: red;
    }

    .card_model {
        border: 1px solid;
        padding: 12px;
        border-radius: 8px;
        justify-content: space-between;
    }
</style>