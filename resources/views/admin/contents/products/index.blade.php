@extends('admin.layouts.layout')

@section('title', 'Danh sách bài đăng')

@section('content')
<div class="container-fluid">
    <!--------------------Title-------------------->
    @include('admin.contents.title')

    <!--------------------Filter-------------------->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('admin.contents.products.components.filter')
                    <!--------------------Table-------------------->
                    @include('admin.contents.products.components.table')
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.contents.products.components.modal_import_excel')
@endsection
@section('scripts')
<script src="{{env('APP_ENV') == "production" ? asset('public/admin/assets/js/products/index.js') : asset('/admin/assets/js/products/index.js')}}"></script>
@endsection
<!--end::script-->