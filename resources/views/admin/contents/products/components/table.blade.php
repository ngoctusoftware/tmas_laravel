<div class="row pt-4">
    <div class="col-sm-12">
        <div class="tab-pane show active" id="post_table">
            <table id="basic-datatable" class="table dt-responsive nowrap w-100 border-color-secondary">
                <thead>
                    <tr style="font-size: 16px; vertical-align: middle">
                        <th class="text-dark text-center">STT</th>
                        <th class="text-dark text-center">Mã sản phẩm</th>
                        <th class="text-dark text-left">Tên sản phẩm</th>
                        <th class="text-dark text-center">Ảnh sản phẩm</th>
                        <th class="text-dark text-center">Đơn giá</th>
                        <th class="text-dark text-center">Tình trạng</th>
                        <th class="text-dark text-center">Ngày tạo</th>
                        <th class="text-dark text-center">Xử lý</th>
                    </tr>
                </thead>
                <tbody style="font-size: 15px; vertical-align: middle;">
                    @if (isset($prd_model))
                    @foreach ($prd_model as $key => $item)

                    <tr class="align-item-center py-2 rowProduct">
                        <td class="text-dark text-center">{{$key + 1}}</td>
                        <td class="text-dark text-center">{{isset($item['code']) ? $item['code'] : '' }}</td>
                        <td class="text-dark" id="nameProduct"><span style="font-size: 18px;">{!! isset($item['name']) ? $item['name'] : '' !!}</span></td>
                        <td class="text-dark text-center">
                            <img src="{{isset($item['image']) && $item['image'] ? asset(env('PUBLIC_PATH') . $item['image'])  :  asset(env('PUBLIC_PATH')) .'upload/no-thumbnail.jpg'}}" width="200px" height="200px" style="border-radius:5px" />
                        </td>
                        <td class="text-dark text-center">{{isset($item['price']) ? $item['price'] : ''}} đ</td>
                        <td class="text-dark text-center">
                            @if (isset($item['status']))
                            @switch($item['status'])
                            @case(0)
                            <span class="text-info">Hết hàng</span>
                            @break
                            @case(1)
                            <span class="text-success">Còn hàng</span>
                            @break
                            @case(2)
                            @if (isset($item['deleted']) && $item['deleted']==1)
                            <span class="text-danger">Đã xóa</span>
                            @endif
                            @break
                            @endswitch
                            @endif
                        </td>
                        <td class="text-dark text-center">{{$item['created_at']}}</td>
                        </td>
                        <td class="text-center">
                            <a target="_blank" href="{{route('admin.products.edit',[$item['id']])}}"><i class="uil uil-edit text-info" style="font-size: 22px"></i></a>
                            <a href="javascript:void(0);" onclick="deleted({{$item['id']}})">
                                <i class="uil-trash-alt text-danger text-lg" style="font-size: 22px"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div> <!-- end preview-->
    </div>
</div>