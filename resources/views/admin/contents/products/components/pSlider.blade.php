<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner" role="listbox">
        @php
        if (isset($images)) {
        $str = $images;
        $position = strlen($images) - 2;
        if ($images[strlen($images) - 2] == ',') {
        $str = substr($str, 0, $position) . substr($str, $position + 1);
        }
        $images = json_decode($str);
        }

        @endphp
        @if (isset($images))
        @foreach ($images as $key => $item)
        <div class="carousel-item {{$key == 0 ? "active" : ''}}">
            <img class="d-block img-fluid" src="{{isset($item) ? env('APP_ENV') == 'local' ? asset(env('PUBLIC_PATH') . str_replace('/public', '', $item)) : asset(env('PUBLIC_PATH') . $item) : asset(env('PUBLIC_PATH') .'upload/no-thumbnail.jpg')}}" alt="{{$key}} slide" style="width: 600px !important; height: 400px !important;">
        </div>
        @endforeach
        @endif
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </a>
</div>