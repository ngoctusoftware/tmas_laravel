<div class="row pt-2">
    <div class="col-sm-3">
        <input type="text" name="searchProduct" id="searchProduct" class="form-control">
    </div>

    <div class="col-sm-6">
        <a href="{{route('admin.products.add')}}" class="btn btn-secondary" style="font-size: 15px" target="_blank">
            Thêm mới
        </a>
    </div>
    <div class="col-sm-3" style="text-align: end">
        <button class="btn btn-info" data-toggle="modal" data-target="#posts_import" onclick="showModalImport()" type="button">
            <i class="uil uil-download-alt" style="font-size: 15px"></i>
            <span>Import</span>
        </button>
        <a class="btn btn-info" name="btnExport" id="btnExport" type="button" href="{{route('admin.products.export')}}">
            <i class="uil-upload-alt" style="font-size: 15px"></i>
            <span>Export</span>
        </a>
    </div>
</div>
