<div class="modal fade" id="post_import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header px-3 mx-0">
                <button data-control="#file" id="btnUploadFile" onclick="uploadImage($(this))" class="btn btn-secondary ">Upload Files</button>
                <input type="hidden" name="full_url" id="full_url" class="form-control">
                <input class="file" type="file" id="file" name="file" class="form-control" multiple style="display:none" accept="image/*">
            </div>
            <div class="modal-body">
                @php
                if (isset($model['id']) && isset($images)) {
                $str = $images;
                $position = strlen($images) - 2;
                if ($images[strlen($images) - 2] == ',') {
                $str = substr($str, 0, $position) . substr($str, $position + 1);
                }
                $images = json_decode($str);
                }
                @endphp
                @if (isset($model['id']) && isset($images))
                <div class="row">
                    <label class="px-2 f-w-bold" style="font-weight: bold">Ảnh trong Album</label>
                    @foreach ( $images as $key => $item)
                    <div class="col-sm-3" id={{$key}}>
                        <div class="img_frame">
                            <div class="btn_del">
                                <a href="javascript:void(0)" onclick="delImages('{{$item}}', {{$key}}, {{$model['id']}})" class="btnXoaAnh" id='btnXoa_{{$key}}' style="z-index:1px;">x</a>
                            </div>
                            <img class="img-fluid py-1" src="{{isset($item) ?  env('APP_ENV') == 'local' ? asset(env('PUBLIC_PATH') . str_replace('/public', '', $item)) :asset(env('PUBLIC_PATH') . $item) : asset(env('PUBLIC_PATH') .'upload/no-thumbnail.jpg')}}" alt="{{$model['name']}} tmas" style="border-radius:5px; width:280px; height:200px; z-index:0px;">
                        </div>

                        <input type="hidden" class="old_images" value="{{isset($item) ? $item : 'upload/no-thumbnail.jpg'}}" style="width:100%">
                    </div>
                    @endforeach
                </div>
                <hr class="">
                @endif
                <label class="px-0 py-0 f-w-bold" style="font-weight: bold">Ảnh Upload mới</label>
                <div class="row" id="preview"></div>
            </div>
        </div>
    </div>
</div>
<style>
    #uploadFile {
        font-size: 18px;
    }

    .img_frame, .img_frame_new {
        position: relative;
    }

    .btn_del {
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>
<script>
</script>