@extends('admin.layouts.layout')

@section('title', 'Chi tiết bài viết')

@section('content')


@include('admin.contents.title')
<div class="card">
    <div class="card-header">
        <h3 style="color: #000; text-transform: uppercase"><i class="uil uil-file-alt" style="font-size:30px;"></i> {{ $title }}</h3>
    </div>
    <div class="card-body">
        <div class="container-fluid">
            <!-- Name, Slug, Author Name Alias-->
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tên bài viết</label><label class="text-danger"> (*)</label>
                        <input type="text" onkeydown="onSlug(event)" onblur="onGetSlug()" value="{{isset($model['name']) ? $model['name'] :''}}" name="name" id="name" placeholder="Nhập tên" required class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Slug</label><label class="text-danger"> (*)</label>
                        <input type="text" value="{{isset($model['slug']) ? $model['slug'] :''}}" name="slug" id="slug" placeholder="Nhập slug" required class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Người tạo</label>
                        <input type="text" value="{{isset($model['created_by']) ? $model['created_by'] :Auth::user()->full_name}}" name="created_by" id="created_by" disabled placeholder="Nhập Author Name Alias" class="form-control">
                    </div>
                </div>
            </div>
            <!-- Descrition -->
            <div class="row py-2">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Mô tả</label><label class="text-danger"> (*)</label>
                        <textarea required name="description" id="description" cols="30" rows="5" class="form-control">{{isset($model['description']) ? $model['description'] :''}}</textarea>
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Content</label><label class="text-danger"> (*)</label>
                        <div>
                            <textarea name="content" required id="content" class="form-control ">{!!isset($model['content']) ? $model['content'] :''!!}</textarea>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Featured Image -->
            <div class="row py-2">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Hình ảnh nổi bật (Kích thước: <= 2MB)</label><label class="text-danger"> (*)</label>
                                <div class="row px-2">
                                    <input type="text" value="{{isset($model['featured_image'])  ? $model['featured_image'] :''}}" name="featured_image" id="featured_image" placeholder="Nhập Featured Image" class="form-control col-10" style="height:43px; width:90.2%">
                                    <button type="button" onclick="ShowModal()" class="btn btn-secondary text-dark px-0" id="btnBrown" data-toggle="modal" data-target="#post_import">Mở file</button>
                                </div>
                                <input type="file" value="{{isset($model['featured_image'])  ? $model['featured_image'] :''}}" name="featured_image1" id="featured_image1" placeholder="Nhập Featured Image" class="form-control" style="display:none">
                    </div>
                </div>
                @include('admin.contents.posts.components.modal_upload_img')

            </div>
            <!-- Type, Feautured -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Chọn kiểu viết</label><label class="text-danger"> (*)</label>
                        <select class="form-select" name="type" id="type" data-placeholder="Chọn type ...">
                            <option value="" disabled> --- Select ---</option>
                            <option value="Công nghệ" {{isset($model['type']) && $model['type'] == 'Công nghệ' ? 'selected' : '' }}>Công nghệ</option>
                            <option value="Sự kiện" {{isset($model['type']) && $model['type'] == 'Sự kiện' ? 'selected' : '' }}>Sự kiện</option>
                            <option value="Cẩm nang" {{isset($model['type']) && $model['type'] == 'Cẩm nang' ? 'selected' : '' }}>Cẩm nang</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Bài viết nổi bật</label><label class="text-danger"> (*)</label>
                        <select name="is_featured" id="is_featured" class="form-control">
                            <option value="" selected disabled> --- Select ---</option>
                            <option value="1" {{isset($model['is_featured'])&& $model['is_featured'] == 1 ? 'selected' : '' }}>Có</option>
                            <option value="0" {{isset($model['is_featured']) && $model['is_featured'] == 0 ? 'selected' : '' }}>Không</option>
                        </select>
                    </div>
                </div>
            </div>
            <!-- Status, Published At -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Trạng thái</label><label class="text-danger"> (*)</label>
                        <select name="pStatus" id="pStatus" class="form-control">
                            <option value="1" {{isset($model['is_featured']) && $model['is_featured'] == 1 ? 'selected' : '' }}>Hiển thị</option>
                            <option value="0" {{isset($model['is_featured']) && $model['is_featured'] == 0 ? 'selected' : '' }}>Ẩn bài viết</option>
                            <option value="2" {{isset($model['is_featured']) && $model['is_featured'] == 2 ? 'selected' : '' }}>Bài nháp</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nơi hiển thị</label><label class="text-danger"> (*)</label>
                        <select name="pTypeShowing" id="pTypeShowing" class="form-control">
                            <option value="2" {{isset($model['type_showing']) && $model['type_showing'] == 2 ? 'selected' : '' }}>Tin tức</option>
                            <option value="1" {{isset($model['type_showing']) && $model['type_showing'] == 1 ? 'selected' : '' }}>Banner</option>
                            <option value="3" {{isset($model['type_showing']) && $model['type_showing'] == 3 ? 'selected' : '' }}>Hỗn hợp</option>
                            <option value="4" {{isset($model['type_showing']) && $model['type_showing'] == 4 ? 'selected' : '' }}>Tuyển dụng</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group" id='datetimepicker1'>
                        <label>Xuất bản lúc</label>
                        <input type="datetime-local" class="form-control date" id="published_at" value="{{isset($model['published_at']) ?date('Y-m-d h:m:s', strtotime($model['published_at'])) : '' }}">
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-group text-center">
                        <label class="form-check-label col-12" for="customCheck2">Trạng thái xóa</label>
                        <input type="checkbox" class="form-check-input" id="deleted" name="deleted" {{isset($model['deleted']) && $model['deleted'] == 1 ? 'checked' : ''}} style="width:23px; height:23px; border:1px solid #ccc">
                    </div>
                </div>
            </div>
            <!-- Meta Title, Meta Keywords, Order -->
            <div class="row py-2">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Meta Title</label><label class="text-danger"> (*)</label>
                        <input type="text" value="{{isset($model['meta_title']) ? $model['meta_title'] : ''}}" required name="meta_title" id="meta_title" class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Meta Keywords </label><label class="text-danger"> (*)</label>
                        <input type="text" value="{{isset($model['meta_keywords']) ? $model['meta_keywords'] : ''}}" required name="meta_keywords" id="meta_keywords" class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thứ tự sắp xếp </label>
                        <input type="number" value="{{isset($model['order']) ? $model['order'] : ''}}" name="order" id="order" class="form-control">
                    </div>
                </div>
            </div>
            <!-- meta_description ,meta_og_image -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Meta Description</label><label class="text-danger"> (*)</label>
                        <input type="text" value="{{isset($model['meta_description']) ? $model['meta_description'] : ''}}" name="meta_description" required id="meta_description" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Meta Open Graph Image</label>
                        <input type="text" value="{{isset($model['meta_og_image']) ? $model['meta_og_image'] : ''}}" name="meta_og_image" id="meta_og_image" required class="form-control">
                    </div>
                </div>
            </div>
            <!-- meta_og_url -->
            <div class="row py-2">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Meta Open Graph URL</label><label class="text-danger"> (*)</label>
                        <input type="text" value="{{isset($model['meta_og_url']) ? $model['meta_og_url'] : ''}}" name="meta_og_url" id="meta_og_url" required class="form-control">
                    </div>
                </div>
            </div>
            <!--  button -->
            <div class="row">
                <div class="col-sm-12" style="text-align: end">
                    @if (isset($model['id']))
                    <button type="button" class="btn btn-primary" onclick="save('{{$model['id']}}')">
                        Update
                    </button>
                    @else
                    <button type="button" class="btn btn-primary" onclick="update()">
                        Thêm bài viết
                    </button>
                    @endif

                    <a href="{{route('admin.posts')}}" class="btn btn-secondary">
                        Quay lại
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

<script src="{{env('APP_ENV') == "production" ? asset('public/admin/assets/js/posts/details.js') : asset('/admin/assets/js/posts/details.js')}}"></script>
<script>
    let str = '';
    if (APP_ENV == 'production') str = '../../../public/upload/ckeditor/pUpload.php';
    else str = '../../../upload/ckeditor/pUpload.php';
    CKEDITOR.replace('content', {
        height: 300,
        filebrowserUploadUrl: str,
    });
</script>

@endsection
<!--end::script-->