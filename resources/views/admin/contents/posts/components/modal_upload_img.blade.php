<div class="modal fade" id="post_import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header px-0 mx-0">
                <h5 class="modal-title px-2" id="exampleModalLabel">Upload ảnh Featured Image</h5>
            </div>
            <div class="modal-body">
                <!-- Content -->
                <div class="row">
                    {{-- <div class="col-lg-2"></div> --}}
                    <div class="col-lg-12 align-seft-middle">
                        <div class="custom-file">
                            <base href="{{ env('APP_ENV') == 'production' ? asset(''). 'public/' : asset('') }}">
                            <img id="imgUpload" src="{{ env('APP_ENV') == 'production' ? asset('public/upload/no-thumbnail.jpg') : asset('upload/no-thumbnail.jpg') }}" width="467rem" class="px-0 rounded"/>
                        </div>
                        <div class="custom-file py-2">
                            <input type="file" class="form-control custom-file-input" name="file" id="file" onchange="uploadFile()">
                        </div>                        
                    </div>
                    <div class="col-lg-12 " style="text-align: end">
                        <button type="buton" disabled onclick="uploadImage()" class="btn btn-secondary py-1 px-2" data-control="#input_import" id="uploadFile">Tải ảnh</button>    
                        <button type="buton"  class="btn btn-secondary py-1 px-2" data-toggle="modal" data-target="#post_import" onclick="closeModal()"  id="Close" >Đóng</button>                            
                    </div>    
                </div>    
                <!-- Content -->
            </div>
    
        </div>
    </div>
    </div>
    