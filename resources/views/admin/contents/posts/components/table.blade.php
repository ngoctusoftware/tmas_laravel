<div class="row pt-4">
    <div class="col-sm-12">
        <div class="tab-pane show active" id="post_table">
            <table id="basic-datatable" class="table dt-responsive nowrap w-100 border-color-secondary">
                <thead>
                    <tr style="font-size: 16px; vertical-align: middle">
                        <th class="text-dark text-center">STT</th>
                        <th class="text-dark text-left">Tên</th>
                        <th class="text-dark text-center">Loại bài viết</th>
                        <th class="text-dark text-center">Loại hiển thị</th>
                        <th class="text-dark text-center">Thời gian</th>
                        <th class="text-dark text-center">Trạng thái</th>
                        <th class="text-dark text-center">Xử lý</th>
                    </tr>
                </thead>
                <tbody style="font-size: 15px; vertical-align: middle;">
                    @if (isset($post_model))
                    @foreach ($post_model as $key => $item)
                    <tr class="align-item-center py-2">
                        <td class="text-dark text-center">{{$key + 1}}</td>
                        <td class="text-dark "><span style="font-size: 18px;">{!! isset($item['name']) ? substr($item['name'], 0, 30).'...' : '' !!}</span></td>
                        <td class="text-dark text-center">{{$item['type']}}</td>
                        <td class="text-dark text-center">
                            @switch((string)$item['type_showing'])
                            @case('2')
                            <span class="text-primary">Tin tức</span>
                            @break
                            @case('1')
                            <span class="text-danger">Banner</span>
                            @break
                            @case('3')
                            <span class="text-success">Hỗn hợp</span>
                            @break
                            @case('4')
                            <span class="text-success">Tuyển dụng</span>
                            @endswitch
                        </td>
                        <td class="text-dark text-center">
                            <p data-toggle="tooltip" data-placement="top" title="{{$item['created_at']}}">
                                {{date("d/m/Y",strtotime($item['created_at']))}}
                            </p>
                        </td>
                        </td>
                        <td class="text-dark text-center">
                            @switch($item['status'])
                            @case('2')
                            <span class="text-danger">Nháp</span>
                            @break
                            @case('0')
                            <span class="text-warning">Ẩn</span>
                            @break
                            @case('1')
                            <span class="text-success">Hiển thị</span>
                            @endswitch
                        </td>
                        <td class="text-center">
                            <a target="_blank" href="{{route('admin.posts.edit',[$item['id']])}}"><i class="uil uil-edit text-info" style="font-size: 22px"></i></a>
                            <a href="javascript:void(0);" onclick="deleted({{$item['id']}})">
                                <i class="uil-trash-alt text-danger text-lg" style="font-size: 22px"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>