<div class="row pt-4">
    <div class="col-sm-12">
        <div class="tab-pane show active" id="basic-datatable-preview">

            <table id="branch_datatables" class="table dt-responsive nowrap w-100 border-color-secondary">
                <thead>
                    <tr style="font-size: 16px; vertical-align: middle">
                        <th class="text-dark text-center">STT</th>
                        {{-- <th class="text-dark text-center">ID</th> --}}
                        <th class="text-dark text-left">Tên cửa hàng</th>
                        <th class="text-dark text-left">Địa chỉ</th>
                        <th class="text-dark text-left">Loại</th>
                        <th class="text-dark text-center">Vùng miền</th>
                        <th class="text-dark text-center">Ngày tạo</th>
                        <th class="text-dark text-center">Xử lý</th>
                    </tr>
                </thead>
                <tbody style="font-size: 15px; vertical-align: middle;">
                    @foreach ($branch_model as $key => $item)
                    <tr class="align-item-center py-2">
                        <td class="text-dark text-center">{{$key + 1}}</td>
                        {{-- <td class="text-dark ">{{$item['id']}}</td> --}}
                        <td class="text-dark ">{{isset($item['name']) ? $item['name'] : ""}}</td>
                        <td class="text-dark ">{{$item['address']}}</td>
                        <td class="text-dark text-left">{{isset($item['type_store']) && $item['type_store'] === 1 ? "Đại lý" : "TTNT"}}</td>
                        <td class="text-dark text-center">
                            @if (isset($item['type']) && $item['type']==1)
                            <span class="text-primary">Miền Bắc</span>
                            @elseif (isset($item['type']) && $item['type']==2)
                            <span class="text-secondary"> Miền Trung</span>
                            @else
                            <span class="text-danger"> Miền Nam</span>
                            @endif
                        </td>
                        <td class="text-dark text-center">{{$item['created_at']}}</td>
                        <td class="text-center">
                            <a data-toggle="modal" data-target="#branches_edit" onclick="showPopupAdd('branches_add', {{$item}})" type="button"><i class="uil uil-edit text-secondary" style="font-size: 22px"></i></a>
                            <a data-toggle="modal" data-target="#branches_del" onclick="showPopupDel('branches_del', {{$item}})" type="button"><i class="uil-trash-alt text-danger text-lg" style="font-size: 22px"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div> <!-- end preview-->
    </div>
</div>