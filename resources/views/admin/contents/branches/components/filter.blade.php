@php
    
    if(isset($_GET['cat_name'])) {
       $cat_name_old =  $_GET['cat_name'];
       echo "<input type='hidden' value='$cat_name_old' class='form-control' name='cat_name_old' id='cat_name_old'>" ;
    }
@endphp

<form action="/TAdmin/danh-muc" method="GET">
@csrf
<div class="row pt-2">
    <div class="col-sm-3">
        <input type="text" name = "cat_name" id="cat_name" class="form-control" 
        @if (isset($cat_name) && $cat_name!="")
            value = "{{$cat_name_old}}"
        @else
            placeholder="Tìm kiếm..." 
        @endif
        >     
    </div>
    
    <div class="col-sm-6">
        <button type="submit" class="btn btn-secondary" id="btnSubmit">
            <i class="uil uil-search pr-3" style="font-size: 15px"></i>
            Tìm kiếm
        </button>      
        <button class="btn btn-secondary"  data-toggle="modal" data-target="#branches_add" onclick="showPopupAdd('branches_add', '')" type="button">
            <i class="uil-plus" style="font-size: 15px"></i>
            <span>Thêm mới</span>
        </button>   
    </div>   
    <div class="col-sm-3" style="text-align: end">
        <!-- <button class="btn btn-info" data-toggle="modal" data-target="#branches_import" onclick="showPopupImport('branches_import')" type="button">
            <i class="uil uil-download-alt" style="font-size: 15px"></i>
            <span>Import</span>
        </button>  
        <a class="btn btn-info" name="btnExport" id="btnExport" type="button" href="{{route('admin.categories.export')}}" >
            <i class="uil-upload-alt" style="font-size: 15px"></i>
            <span>Export</span>
        </a>      -->
    </div>    
</div>

</form>
