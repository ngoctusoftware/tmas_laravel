<div class="modal fade" id="categories_import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header px-0 mx-0">
            <h5 class="modal-title px-2" id="exampleModalLabel">Tải danh mục từ file excel</h5>
        </div>
        <div class="modal-body">
            <!-- Content -->
            <div class="row">
                {{-- <div class="col-lg-2"></div> --}}
                <div class="col-lg-12 align-seft-middle">
                    <div class="custom-file ">
                        <input type="file" class="form-control custom-file-input" name="file" id="file">
                        @if (session('message'))
                            <div>  <p class="text-danger my-2">{{ session('message') }}</p>  </div>
                        @endif
                        
                    </div>
                </div>
                <div class="col-lg-12 py-2" style="text-align: end">
                    <button type="buton" class="btn btn-secondary py-1 px-2" data-control="#input_import" 
                        id="uploadFile">Upload file
                    </button>
                    {{-- <a href="{{route('admin.categories.template')}}" class="btn btn-success ">Download Template</a> --}}
                </div>

            </div>

            <!-- Content -->
        </div>

    </div>
</div>
</div>
