<div class="modal fade" id="branches_add" tabindex="-1" role="dialog" aria-labelledby="branch_title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header px-0 mx-0">
                <h4 class="modal-title px-2 py-1 text-dark" id="branch_title"></h4>
            </div>
            <div class="modal-body">
                <h2 id="pId"></h2>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="">
                            <label for="name" class="form-label">Tên chi nhánh</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <br class="phancach">
                        <span class="py-2" id="error_name" style="color:red;font-size:14px"></span>
                    </div>
                    <div class="col-sm-6">
                        <div class="">
                            <label for="pAddress" class="form-label">Địa chỉ</label>
                            <input type="text" name="pAddress" id="pAddress" class="form-control">
                        </div>
                        <br class="phancach">
                        <span class="py-2" id="error_address" style="color:red;font-size:14px"></span>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Loại chi nhánh</label><label class="text-danger"> (*)</label>
                                <select name="pTypeStore" id="pTypeStore" class="form-control">
                                    <option value="1">Đại lý</option>
                                    <option value="2">TTNT</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Miền</label><label class="text-danger"> (*)</label>
                                <select name="pType" id="pType" class="form-control">
                                    <option value="1">Miền Bắc</option>
                                    <option value="2">Miền Trung</option>
                                    <option value="3">Miền Nam</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class=" position-relative" id="datepicker1">
                            <label class="form-label">Ngày tạo</label>
                            <input type="text" class="form-control" data-provide="datepicker" data-date-container="#datepicker1" name="created_at" id="created_at" value="{{Date('Y-m-d H:i:s')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="text-align: end">
                        <button type="button" class="btn btn-secondary" name="btnSubmit" id="btnSubmit" onclick="AddBranch()">
                            <i class="uil uil-plus" style="font-size: 18px"></i>
                            <span id="modal_btn"></span>
                        </button>
                        <button type="button" class="btn btn-danger" name="btnHuy" id="btnHuy" onclick="$('#branches_add').modal('hide'); location.reload()">
                            <i class="uil uil-cancel" style="font-size: 18px"></i>
                            Hủy bỏ
                        </button>
                    </div>
                </div>
                <!-- Content -->
            </div>
        </div>
    </div>
</div>