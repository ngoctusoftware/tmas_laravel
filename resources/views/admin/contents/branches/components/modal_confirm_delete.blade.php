<div class="modal fade" id="branches_del" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header px-0 mx-0">
                <h4 class="modal-title px-2 py-1 text-dark" id="exampleModalLabel">Xoá chi nhánh</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Bạn có thực sự muốn xoá chi nhánh</h3>
                        <h3 id="branch_name"> </h3>
                        <h3 id="branch_id"> </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="text-align: end">
                        <button type="button" class="btn btn-danger" name="btnSubmit" id="btnSubmit" onclick="DeleteBranch()">
                            <i class="uil uil-trash" style="font-size: 18px"></i>
                            Xoá
                        </button>
                        <button type="button" class="btn btn-danger" name="btnHuy" id="btnHuy" onclick="$('#branches_del').modal('hide'); location.reload()">
                            <i class="uil uil-cancel" style="font-size: 18px"></i>
                            Hủy bỏ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>