@extends('admin.layouts.layout')

@section('title', 'Danh mục sản phẩm')

@section('content')
    <div class="container-fluid">
        <!--------------------Title-------------------->
        @include('admin.contents.title')
        <!--------------------Filter-------------------->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @include('admin.contents.branches.components.filter')
                        <!--------------------Table-------------------->
                        @include('admin.contents.branches.components.table')
                    </div>
                </div>
            </div>
        </div>
        <!-- @include('admin.contents.branches.components.modal_import') -->
        @include('admin.contents.branches.components.modal_addnew')
        @include('admin.contents.branches.components.modal_confirm_delete')
    </div>

@endsection
@section('scripts')
    <script src='assets/js/branches/index.js'></script>
    {{-- <script>
        $(document).ready( function () {
            $('#branch_datatables').DataTable();
            $('#branch_datatables_filter').hide();
            $('.dataTables_length').hide();
            
        } );
      
    </script> --}}
@endsection
<!--end::script-->
