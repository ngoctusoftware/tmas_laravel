{{-- Đường dẫn --}}                
<div class="row mx-0">
    <div class="col-12 px-0 py-0">
        <div class="page-title-box mx-0">
            <div class="page-title-right px-2">
                <ol class="breadcrumb py-3 my-0">
                    <li class="breadcrumb-item">
                      <a href="/">
                          <i class='uil uil-globe' style="font-size: 20px; color:#000"></i>
                      </a></li>
                      <li class="breadcrumb-item">
                      <a href="/TAdmin">
                          <i class='uil uil-home-alt' style="font-size: 20px; color:#000"></i>
                      </a>
                    </li>
                    <li class="breadcrumb-item active text-dark" style="font-size: 18px;color:#000">
                        <a href="{{isset($link) ?  $link : '#'}}">
                            {{isset($title) && $title!="" ? __($title) : ''}}
                        </a>
                        
                    </li>
                </ol>
            </div>
            
            {{-- <h3 class="page-title f-w-bold px-0 mx-0" style="font-size:1.5rem;color:#000" >{{__($title)}}</h3> --}}
        </div>
    </div>
</div>