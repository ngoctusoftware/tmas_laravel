@extends('admin.layouts.layout')

@section('title', 'Chuyên mục')

@section('content')
    <!-- Content -->
    <div class="container" >
        <div class="row">
            @include('admin.contents.title')
            <div class="card mx-4" style="width:1200px">
                <div class="card-header">
                    <h3 style="color: #000; text-transform: uppercase"><i class="uil uil-file-alt" style="font-size:30px;"></i>
                        {{ isset($title) ? $title : '' }}
                    </h3>
                    <input type="hidden" name="type" id="type" value="{{isset($type) ? $type : ''}}">
                </div>
                <div class="card-body">
                   
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="form-label">Tên {{$name}}</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{isset($model['name']) ? $model['name'] : '' }}" onblur="convertSlug('name')">
                                </div>
                                <br class="phancach">
                                <span class="py-2" id="error_name" style="color:red;font-size:14px"></span>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="slug" class="form-label">Slug</label>
                                    <input disabled type="text" name="slug" id="slug" class="form-control" value="{{isset($model['slug']) ? $model['slug'] : '' }}">
                                </div>
                                <br class="phancach">
                                <span class="py-2" id="error_slug" style="color:red;font-size:16px"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 pt-1">
                                <div class="form-check form-checkbox-info mb-2">
                                    <input type="checkbox" class="form-check-input" id="cStatus" name="status" {{isset($model['status']) && $model['status'] == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label py-0 my-0">Trạng thái</label>
                                </div>                                
                            </div>
                            <div class="col-sm-6">
                                <div class=" position-relative" id="datepicker1">
                                    <label class="form-label">Ngày tạo</label>
                                    <input type="text" class="form-control" data-provide="datepicker"
                                        data-date-container="#datepicker1" name="created_at" id="created_at"
                                        value="{{ isset($model['created_at']) ? $model['created_at'] : Date('Y-m-d H:i:s') }}">
                                </div>
                                <span class="py-2"id="error_created_at" style="display:none; color:red;font-size:14px"></span>
                            </div>
                        </div>
                        <div class="row pt-3">
                            <div class="col-sm-12" style="text-align: end">
                                <button type="button" class="btn btn-secondary" name="btnSubmit" id="btnSubmit"
                                    onclick="update({{$model['id']}})">
                                    <i class="uil uil-edit" style="font-size: 18px"></i>
                                    Lưu lại
                                </button>                             
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>

   


    <!-- Content -->

@endsection
@section('scripts')
    <script src='assets/js/categories/details.js'></script>
@endsection
<!--end::script-->
