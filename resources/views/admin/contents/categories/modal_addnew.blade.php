
    <div class="modal fade" id="categories_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header px-0 mx-0">
                    <h4 class="modal-title px-2 py-1 text-dark" id="exampleModalLabel">Thêm mới {{ isset($title) ? $title : 'Thêm mới' }}</h4>
                    <input type="hidden" name="type" id="type" value="{{isset($type) ? $type : ''}}">
                </div>
                <div class="modal-body">
                    <!-- Content -->
                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="">
                                <label for="name" class="form-label">Tên danh mục</label>
                                <input type="text" name="name" id="name" class="form-control" onblur="convertSlug('name')">
                            </div>  
                            <br class="phancach">
                            <span class="py-2" id="error_name" style="color:red;font-size:14px"></span>
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <label for="slug" class="form-label">Slug</label>
                                <input disabled type="text" name="slug" id="slug" class="form-control">
                            </div> 
                             <br class="phancach">
                            <span  class="py-2" id="error_slug" style="color:red;font-size:16px"></span>                          
                        </div>
                    </div>      
                    <div class="row">                       
                        <div class="col-sm-6 pt-1">
                            <div class="form-check form-checkbox-info mb-2">
                                <input type="checkbox" class="form-check-input" id="customStatus" name="status" checked>
                                <label class="form-check-label" for="customStatus">Trạng thái hiển thị</label>
                            </div> 
                            <div class="form-check form-checkbox-info mb-2">
                                <input type="checkbox" class="form-check-input" id="customDeleted" name="deleted">
                                <label class="form-check-label" for="customDeleted">Trạng thái xóa</label>
                            </div>                 
                        </div>
                        <div class="col-sm-6">
                            <div class=" position-relative" id="datepicker1">
                                <label class="form-label">Ngày tạo</label>
                                <input type="text" class="form-control" data-provide="datepicker" data-date-container="#datepicker1" 
                                       name="created_at" id = "created_at" value="{{Date('Y-m-d H:i:s')}}">                                
                            </div>   
                            <span  class="py-2"id="error_created_at" style="display:none; color:red;font-size:14px"></span>                          
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-sm-12" style="text-align: end">
                            <button type="button" class="btn btn-secondary" name="btnSubmit" id="btnSubmit" onclick="AddCategories()">
                                 <i class="uil uil-plus" style="font-size: 18px"></i>
                                 Thêm mới
                            </button>
                           
                            <button type="button" class="btn btn-danger" name="btnHuy" id="btnHuy" onclick="$('#categories_add').modal('hide'); location.reload()">
                                <i class="uil uil-cancel" style="font-size: 18px"></i>
                                Hủy bỏ
                           </button>
                        </div>
                    </div>
                    <!-- Content -->
                </div>
        
            </div>
        </div>
    </div>
    
