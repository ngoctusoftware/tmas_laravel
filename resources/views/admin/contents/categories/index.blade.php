@extends('admin.layouts.layout')

@section('title', 'Danh mục sản phẩm')

@section('content')

    <div class="container-fluid">
        <!--------------------Title-------------------->
        {{-- @include('admin.contents.categories.components.title') --}}
        @include('admin.contents.title')
        <!--------------------Filter-------------------->
        <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    
                    @include('admin.contents.categories.components.filter')
                    <!--------------------Table-------------------->
                    @include('admin.contents.categories.components.table')
                </div> 
            </div> 
        </div>
    </div> 
    
    @include('admin.contents.categories.components.modal_import')
    @include('admin.contents.categories.modal_addnew')
    </div>

@endsection
@section('scripts')
    <script src='assets/js/categories/index.js'></script>

@endsection
<!--end::script-->