<div class="row pt-4">
    <div class="col-sm-12">
        <div class="tab-pane show active" id="basic-datatable-preview">
            
            <table id="basic-datatable" class="table dt-responsive nowrap w-100 border-color-secondary">
                <thead>
                    <tr style="font-size: 16px; vertical-align: middle">
                        <th class="text-dark text-center">STT</th>
                        <th class="text-dark text-left">Tên danh mục</th>
                        <th class="text-dark text-left" width="20%">Tên định danh</th>                                                
                        <th class="text-dark text-center">Trạng thái</th>                        
                        <th class="text-dark text-center" >Ngày tạo</th>
                        <th class="text-dark text-center">Xử lý</th>
                    </tr>
                </thead>
                <tbody style="font-size: 15px; vertical-align: middle;">                    
                    @foreach ($cat_model as $key => $item)
                    <tr class="align-item-center py-2" id="{{$item['id']}}" >
                        <td class="text-dark text-center">{{$key + 1}}</td>
                        <td class="text-dark ">{{isset($item['name']) ? $item['name'] : ""}}</td>
                        <td class="text-dark text-left">{{isset($item['slug']) ? $item['slug'] : ""}}</td>
                        <td class="text-dark text-center">
                          @if (isset($item['status']) && $item['status']==1)
                              <span class="text-primary">Kích hoạt</span>
                          @else
                          <span  class="text-danger"> Ngừng kích hoạt</span>
                          @endif
                        </td>                        
                        <td class="text-dark text-center">{{$item['created_at']}}</td>   
                        <td class="text-center">                            
                            <a href="/TAdmin/{{$slug}}/chinh-sua/{{$item['id']}}"><i class="uil uil-edit text-secondary" style="font-size: 22px"></i></a>
                            <a href="javascript:void(0);" onclick="RemoveCategories({{$item['id']}})" id="cat_del" data-name="{{$item['name']}}"><i class="uil-trash-alt text-danger text-lg" style="font-size: 22px"></i></a>                            
                        </td>   
                    </tr>
                    @endforeach
                   
        
                </tbody>
            </table>
        </div> <!-- end preview-->
    </div>
</div>