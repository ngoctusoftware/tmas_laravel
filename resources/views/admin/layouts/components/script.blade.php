 <!-- bundle -->



 <script src="assets/js/jquery-3.1.1.min.js"></script>
 <script src="assets/js/vendor.min.js"></script>
 <script src="assets/js/app.min.js"></script>

 <!-- third party js -->
 <script src="assets/js/vendor/apexcharts.min.js"></script>
 <script src="assets/js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
 <script src="assets/js/vendor/jquery-jvectormap-world-mill-en.js"></script>
 <!-- third party js ends -->
 {{-- <script src="assets/js/vendor/dataTables.select.min.js"></script> --}}

 <!-- datatable .js -->
 {{-- <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> --}}
 <script src="assets/js/vendor/jquery.dataTables.min.js"></script>
 <script src="assets/js/vendor/dataTables.bootstrap5.js"></script>
 <script src="assets/js/vendor/dataTables.responsive.min.js"></script>


 <script src="assets/js/helpers.js"></script>
 <script src="assets/js/notify.js"></script>
 <script src="assets/js/notify.min.js"></script>

 <!-- validator -->
 <script src="https://unpkg.com/validator@latest/validator.min.js"></script>

 <!-- tinyMCE -->

 <script src="http://cdn.ckeditor.com/4.8.0/standard-all/ckeditor.js"></script>
 <!-- <script src="assets/js/ckeditor.js"></script> -->
 <!-- select2 -->
 {{-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script> --}}
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js" integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.33/sweetalert2.all.js" integrity="sha512-rWLB2C36twDMqRNNXk5CCDGmMpqjGDao4cXJXkXDKf6F6jef9d7FYCKGAoRO3urNLe09W79JbjT78jTthVZ2MA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

 <script>
     let APP_ENV = "{{env('APP_ENV')}}";
     $(document).ready(function() {
         $('#branch_datatables, #basic-datatable').DataTable();
         $('#branch_datatables_filter').hide();
         $('.dataTables_length').hide();
     });
 </script>

 @yield('scripts')