<!-- ========== Left Sidebar Start ========== -->
<div class="leftside-menu">    
    <!-- LOGO -->
    <a href="index.html" class="logo text-center logo-light">
        <span class="logo-lg">                        
            <img src="assets/images/logo-white.svg" alt="tmasvn" height="50px" width="200px">                        
        </span>
        <span class="logo-sm">
            <img src="assets/images/logo-white.svg" alt="tmasvn" height="16">
        </span>
    </a>
    <hr>
    <!-- LOGO -->
    <a href="index.html" class="logo text-center logo-dark">
        <span class="logo-lg">
            <img src="assets/images/logo-white.svg" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="assets/images/logo-white.svg" alt="" height="16">
        </span>
    </a>
    <div class="h-100" id="leftside-menu-container" data-simplebar="">
        <!--- Sidemenu -->
            <ul class="side-nav">                        
                @foreach ($title as $key => $value)
                    <li class="side-nav-title side-nav-item sidebar_title" >
                        <span class="text-white">{{$value}}</span>
                        @foreach ($admin_navigation as $item)
                            @if ($item['parent']  ==  $key && $item['status'] == 1)
                            <li class="side-nav-item text-secondary">                                    
                                <a href="{{$item['url']}}" class="side-nav-link">
                                    <i class="{{$item['icon']}}"></i>                                
                                    <span style="color:#ccc"> {{$item['name']}} </span>
                                </a>                          
                            </li>
                            @endif                            
                        @endforeach
                    </li>    
                @endforeach
            </ul>
        <!-- End Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->