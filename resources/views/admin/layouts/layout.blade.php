@php
    use App\Models\Admin\AdminNavigation;
    $admin_navigation = AdminNavigation::all();   
    $title = config('navigation.admin'); 
@endphp
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
        <title>@yield('title') | Tmasvn Admin</title>        
        <base href="{{ env('APP_ENV') == 'production' ? asset(''). 'public/admin/' : asset('').'admin/'  }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description">
        <meta content="Coderthemes" name="author">
        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.svg">

        <!-- third party css -->
        <link href="assets/css/vendor/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css">
        <!-- third party css end -->

        <!-- App css -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" id="light-style">
        <link href="assets/css/app-dark.min.css" rel="stylesheet" type="text/css" id="dark-style">        
        <link href="assets/css/vendor/responsive.bootstrap5.css" rel="stylesheet" type="text/css">
        <link href="assets/css/vendor/buttons.bootstrap5.css" rel="stylesheet" type="text/css">
        <link href="assets/css/vendor/select.bootstrap5.css" rel="stylesheet" type="text/css">

        {{-- datatable --}}       

        <link href="assets/css/vendor/dataTables.bootstrap5.css" rel="stylesheet" type="text/css">



        <!-- select cdn -->        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
        <!-- Or for RTL support -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.rtl.min.css" />
        <script>
            let public_path = "{{env('PUBLIC_PATH')}}";
        </script>
    </head>
    
    <body class="loading" data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
        <!-- Begin page -->
        <div class="wrapper">
            @include('admin.layouts.components.navigation')
            <div class="content-page">
                <div class="content">
                    @include('admin.layouts.components.headers')
                    @yield('content')  
                </div> 
                <!-- content -->
                @include('admin.layouts.components.footer')
            </div>         
        
        @include('admin.layouts.components.script')
    </body>     
</html>