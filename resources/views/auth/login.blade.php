<!DOCTYPE html>

    <html lang="en">

    <head>

        <meta charset="utf-8">

        <title>Đăng nhập hệ thống</title>        

        <base href="{{  env('APP_ENV') == 'production' ? asset('').'public/admin/' : asset('').'admin/' }}">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description">

        <meta content="Coderthemes" name="author">

        <!-- App favicon -->

        <link rel="shortcut icon" href="assets/images/favicon.ico">



        <!-- third party css -->

        <link href="assets/css/vendor/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css">

        <!-- third party css end -->



        <!-- App css -->

        <link href="assets/css/style.css" rel="stylesheet" type="text/css">

        <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css">

        <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" id="light-style">

        <link href="assets/css/app-dark.min.css" rel="stylesheet" type="text/css" id="dark-style">



    </head>

    

    <body style="background-image:url('assets/images/banner.jpg')">

        <div class="container" style="margin:15% !important">

            <div class="row justify-content-center">

                <div class="col-md-8">

                    <div class="card">

                        <div class="card-header text-white">{{ __('ĐĂNG NHẬP HỆ THỐNG') }}</div>

                        <div class="card-body">

                            <form method="POST" action="{{ route('login') }}">

                                @csrf        

                                <div class="row mb-3">

                                    <label for="email" class="col-md-4 col-form-label text-md-end text-white">{{ __('Email Address') }}</label>        

                                    <div class="col-md-6">

                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>        

                                        @error('email')

                                            <span class="invalid-feedback" role="alert">

                                                <strong>{{ $message }}</strong>

                                            </span>

                                        @enderror

                                    </div>

                                </div>

        

                                <div class="row mb-3">

                                    <label for="password" class="col-md-4 col-form-label text-md-end text-white">{{ __('Password') }}</label>        

                                    <div class="col-md-6">

                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">        

                                        @error('password')

                                            <span class="invalid-feedback" role="alert">

                                                <strong>{{ $message }}</strong>

                                            </span>

                                        @enderror

                                    </div>

                                </div>  

                                <div class="row mb-0">

                                    <div class="col-md-8 offset-md-4" >

                                        <button type="submit" class="btn btn-primary">

                                            {{ __('Đăng nhập') }}

                                        </button>   

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>        

    </body>     

</html>