@extends('pages.layouts.layout')

@section('title','Not found')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/not_found.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_news">
    </div>
</section>
<section class="section_container">
    <div class="container">
        <div class="text-center">
            <h1 class="title_content text_logo">KHÔNG TÌM THẤY TRANG</h1>
        </div>
        <div class="text-center">
            <img class="not_found" src="assets/img/404.svg" alt="404">
        </div>
        <div class="text-center row">
            <div class="col-12">
                <div class="d-flex justify-content-center">
                    <a href="/" class="link_to">
                        <h2>trang chủ</h2>
                    </a>
                    <a href="/tin-tuc" class="link_to">
                        <h2>tin tức</h2>
                    </a>
                    <a href="/san-pham" class="link_to">
                        <h2>Sản phẩm</h2>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@include('pages.contents.container.downloadApp')

@endsection

