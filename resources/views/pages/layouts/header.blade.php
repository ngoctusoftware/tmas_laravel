<header class="menu_header d-flex align_item_center">
    <div class="container">
        <div class="d-flex space_bw align_item_center">
            <div class="mobile icons_24 align_item_center" id="menu_mobile">
                <img src="assets/img/icons/menu-white.svg" alt="menu">
            </div>
            <div class="box_logo_header">
                <a href="/"> <img src="assets/img/logo-tmas.svg" alt="logo"></a>
            </div>
            <div class="menu_search d-flex">
                <ul class="list_menu">
                    <li>
                        <a rel="noopener" class="menu_item mobile" href="/">
                            Trang chủ
                        </a>
                    </li>
                    <li>
                        <a rel="noopener" class="menu_item" href="/gioi-thieu">
                            Giới thiệu
                        </a>
                    </li>
                    <li>
                        <a rel="noopener" class="menu_item" href="/san-pham">
                            Sản phẩm
                        </a>
                    </li>
                    <li>
                        <a rel="noopener" class="menu_item" href="/tin-tuc">
                            Tin tức
                        </a>
                    </li>
                    <li>
                        <a rel="noopener" class="menu_item" href="/tuyen-dung">
                            Tuyển dụng
                        </a>
                    </li>
                    <li>
                        <a rel="noopener" class="menu_item" href="{{route('pages.contact.index')}}">
                            Liên hệ
                        </a>
                    </li>
                    <li>
                        <a rel="noopener" class="menu_item" href="/dai-ly">
                            Đại lý
                        </a>
                    </li>
                    <li>
                        <div class="icon_24 close_button mobile" id="close_menu">
                            <img src="assets/img/icons/close-white.svg" alt="search btn">
                        </div>
                    </li>
                </ul>
                <div class="button_search icons_24" id="button_search">
                    <img class="w-100" src="assets/img/icons/search-white.svg" alt="btn">
                </div>
            </div>
        </div>
        <div class="search_form">
            <div class="box_input">
                <div class="icon_24 close_button" id="close_search">
                    <img src="assets/img/icons/close-white.svg" alt="search btn">
                </div>
                <div class="d-flex search_box">
                    <input type="text" id="search_input">
                    <div class="icons_32" id="button_search_action">
                        <img class="w-100" src="assets/img/icons/search-white.svg" alt="btn">
                    </div>
                </div>
                <div class="tag_search ">
                    <span class="tag_item" data="3">Màn hình android</span>
                    <span class="tag_item" data="6">Android box</span>
                    <span class="tag_item" data="4">Màn hình liền cam</span>
                    <span class="tag_item" data="14">Đề nổ từ xa</span>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="offset-lg-3 col-lg-6 col-12">
                            <div class="result_search row" id="result_search">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>