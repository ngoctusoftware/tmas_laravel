<footer class="section">
    <div class="container">
        <div class="row space_md">
            <div class="col-lg-4 col-4">
                <img class="w-100" src="assets/img/logo-tmas.svg" alt="Tmas việt nam">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="row space_md">
                    <div class="col-lg-4 col-sm-12">
                        <div class="box_name_company space_lg">
                            <h2>CÔNG TY TNHH THƯƠNG MẠI VÀ XUẤT NHẬP KHẨU THIÊN MINH</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-6 box_social space_lg">
                                <h3 class="space_md">Social</h3>
                                <div class="box_item_social d-flex">
                                    <a class="item_social_footer youtube" target="_blank" href="https://www.youtube.com/channel/UCxmEcB0yGAJ3DV8pJoRPiuQ">
                                        <img class="w-100" src="assets/img/footer/youtube.svg" alt="youtube tmas">
                                    </a>
                                    <a class="item_social_footer tiktok" target="_blank" href="https://www.tiktok.com/@tiktok.tmasvietnam">
                                        <img class="w-100" src="assets/img/footer/tiktok.svg" alt="tiktok tmas">
                                    </a>
                                    <a class="item_social_footer facebook" target="_blank" href="https://www.facebook.com/tmas.vn">
                                        <img class="w-100" src="assets/img/footer/facebook.svg" alt="facebook tmas">
                                    </a>
                                </div>
                            </div>
                            <div class=" col-lg-12 col-6 box_app space_lg">
                                <h3 class="space_md">APP TMAS</h3>
                                <div class="box_item_app d-flex">
                                    <a class="item_app_footer" href="/download-android" target="_blank">
                                        <img class="w-100" src="assets/img/footer/android.svg" alt="google play ">
                                    </a>
                                    <a class="item_app_footer" href="/download-ios" target="_blank">
                                        <img class="w-100" src="assets/img/footer/apple.svg" alt="apple store">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <div class="box_required space_lg">
                            <h3 class="space_md">Chính sách & hướng dẫn</h3>
                            <div class="row">
                                <div class="col-12 space_sm">
                                    <a class="link_footer" href="">Chính sách TMAS</a>
                                </div>
                                <div class="col-12 space_sm">
                                    <a class="link_footer" href="">Tải về catalog</a>
                                </div>
                                <div class="col-12 space_sm">
                                    <a class="link_footer" href="">Cẩm nang cho đại lý</a>
                                </div>
                                <div class="col-12 space_sm">
                                    <a href="/bao-hanh-dien-tu" class="link_footer">Bảo hành điện tử</a>
                                </div>
                                <div class="col-12 space_sm">
                                    <a href="/hinh-nen" class="link_footer">Thư viện hình nền Launcher</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-12">
                        <div class="box_contact space_lg">
                            <h3 class="space_md">Địa chỉ & liên hệ</h3>
                            <p class="space_sm">Chi nhánh Hà Nội: Số 88 Khúc Thừa Dụ, Dịch Vọng, Cầu Giấy, Hà Nội</p>
                            <p class="space_sm">Chi nhánh TP.HCM: Địa chỉ: Số 2/1 Hồng Hà, Phường 2, Quận Tân Bình, Tp.HCM</p>
                            <a href="tel:1900998859" class="space_sm">Hotline: 1900 99 88 59</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        Copyright 2023 © TMAS
    </div>
</footer>