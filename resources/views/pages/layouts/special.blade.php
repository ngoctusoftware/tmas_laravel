@push ('after-scripts')
<script src="assets/js/special.js"></script>
@endpush
<div class="popup_application">
    <div class="modal_tmas">
        <p class="title_app space_sm">Tải ngay ứng dụng TMAS</p>
        <p class="hint_app space_sm">Tặng ngay 3 tháng bảo hành điện tử khi khách hàng tải và đăng ký trên TMAS APP, Quý khách vui lòng tải app và gửi thông tin về fanpage để được tăng thời gian bảo hành</p>
        <p class="hint_app space_md">Đối với khách hàng đã mua sản phẩm nhưng chưa được kích hoạt, vui lòng gửi thông tin về fanpage để TMAS cập nhật thời gian bảo hành điện tử lên hệ thống</p>
        <div class="d-flex space_bw">
            <a class="btn_app button_download_app" href=""><button>Download</button></a> 
            <button class="btn_app close_popup_app"><p>Cancel</p></button>
        </div>
    </div>
</div>
