@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="TMAS, tmas việt nam">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/gioi-thieu">
<meta property="og:description" content="TMAS Việt Nam">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam">
@endpush

@section('title','Đại lý')

@push ('after-styles')
<style>
	section {
		margin-top: 120px;
	}

	#map {
		height: 800px;
		width: 100%;
	}
</style>
@endpush

@push ('after-scripts')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9H8BNiRcnAefZzwTPnBlQdRHMl9Z6VbM&callback=initMap" async defer></script>
<script>
	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: 21.0312873,
				lng: 105.7907115
			},
			zoom: 20
		});
		var marker = new google.maps.Marker({
			position: {
				lat: 21.0312873,
				lng: 105.7907115
			},
			map: map,
			title: "Marker",
		});

	}
</script>
@endpush

@section('content')
<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6"></div>
			<div class="col-lg-6">
				<div id="map"></div>
			</div>
		</div>
	</div>
</section>
@endsection