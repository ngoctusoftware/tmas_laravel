@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/huong-dan-dai-ly">
<meta property="og:description" content="Hướng dẫn các ứng dụng phát triển bởi TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn, tin tức">
@endpush
@section('title','Hướng dẫn đại lý')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/guide.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_news">
    </div>
</section>
@include('pages.layouts.breadcrum')

<section class="section_list_new section_container">
    <div class="container">
        <div class="row title_content space_md">
            <h1 class="text_logo text-center">Hướng dẫn đại lý</h1>
        </div>
        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-sm-12">
                <div class="guide_component space_md">
                    <div class="d-flex">
                        <div class="guide_app_image text-center space_md">
                            <img src="assets/img/guide/tmas-store/iconstore.png" alt="tmas store">
                        </div>
                        <div class="guide_title text_logo space_md">
                            <h2 class="space_md">tmas store</h2>
                            <p class="guide_des">Ứng dụng TMAS store là kho ứng dụng được phát triển bởi đội ngũ R&D của TMAS. Bạn có thể tải xuống và cài đặt những ứng dụng hữu ích được phát triển riêng cho màn Android và các ứng dụng chọn lọc khác trên CH play</p>
                        </div>
                    </div>
                    <div>
                        <div class="guide_start">
                            <h3 class=" space_md">Hướng dẫn sử dụng</h3>
                            <img src="assets/img/guide/tmas-store/trangchu.png" alt="">
                            <p class="guide_des space_md">Màn hình trang chủ của ứng dụng</p>
                        </div>
                        <div class="guide_start">
                            <p>Để sử dụng app, bạn chỉ cần tải file apk về thiết bị android và đồng ý cài đặt</p>
                            <p>Để tải ứng dụng do đội ngũ TMAS phát triển, bạn chỉ cần chọn ứng dụng và nhấn nút tải về.</p>
                            <img src="assets/img/guide/tmas-store/launcher.png" alt="">
                        </div>
                       
                    </div>
                </div>
                <div class="guide_component space_md">
                    <div class="d-flex">
                        <div class="guide_app_image text-center space_md">
                            <img src="assets/img/guide/tmas-launcher/iconlauncher.png" alt="tmas launcher">
                        </div>
                        <div class="guide_title text_logo space_md">
                            <h2 class="space_md">tmas launcher</h2>
                            <p class="guide_des">Ứng dụng TMAS launcher là ứng dụng giao diện được phát triển riêng cho màn hình android Safe View.
                                TMAS launcher có thể kết nối với các sản phẩm trong hệ sinh thái của TMAS
                            </p>
                        </div>
                    </div>
                    <div>
                        
                        <div class="guide_start">
                            <h3 class=" space_md">Hướng dẫn sử dụng</h3>
                            <img src="assets/img/guide/tmas-launcher/trangchu.png" alt="">
                            <p class="guide_des space_md">Màn hình giao diện của launcher</p>
                            <img src="assets/img/guide/tmas-launcher/launcher.png" alt="">
                            <p class="guide_des space_md">TMAS launcher có thể nhận diện được thời tiết, cài đặt được biển số xe và hiển thị áp suất lốp</p>
                            <img src="assets/img/guide/tmas-launcher/caidat.jpg" alt="">
                            <p class="guide_des space_md">Để vào màn cài đặt chung, bạn cần ấn giữ vào mô hình xe trên màn hiển thị</p>
                            <img src="assets/img/guide/tmas-launcher/caidatasl.jpg" alt="">
                            <p class="guide_des space_md">Để hiển thị áp suất lốp, bạn cần vào cài đặt chế độ hiển thị riêng cho áp suất lốp</p>
                            <img src="assets/img/guide/tmas-launcher/caidatasl.jpg" alt="">
                            <p class="guide_des space_md">Để cài đặt biển số xe, bạn cần vào mục cài đặt biển số và nhập biển số xe của bạn</p>
                        </div>
                        <div class="guide_start">
                            <p>Để sử dụng app, bạn chỉ cần tải file apk về thiết bị android và đồng ý cài đặt</p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="section_product_related section_container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12 space_md">
                <h3 class="title_related text_logo space_md_sm text-center">Sản phẩm nổi bật</h3>
            </div>
            @foreach($listProductsRelated->toArray() as $item)
            <div class="col-xl-3 col-lg-6 col-sm-12 col-12 product_item_related">
                <div class="image_product_related">
                    <a href="/san-pham/{{$item->id}}/{{$item->slug}}">
                        <img src="{{$item->image}}" alt="{{$item->slug ? $item->slug : $item->name}}">
                    </a>
                </div>
                <div class="news_item_box_content_related">
                    <div class="news_item_title_related">
                        <a href="/san-pham/{{$item->id}}/{{$item->slug}}">
                            <h3>{{$item->name}}</h3>
                        </a>
                    </div>
                    <div class="news_item_description_related">
                        <p>{{$item->description}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@endsection