@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/huong-dan-dai-ly">
<meta property="og:description" content="Hướng dẫn các ứng dụng phát triển bởi TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn, tin tức">
@endpush
@section('title','Hướng dẫn bảo hành điện tử')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/guide.css">
@endpush
@push ('after-scripts')
<script src="assets/js/guide.js"></script>
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_warranty">
        <div class="title_content">
            <h1 class="title text_white text-center">Bảo hành điện tử TMAS</h1>
            <p class="text_white text-center">Bảo vệ quyền lợi người tiêu dùng TMAS</p>
        </div>
    </div>
</section>
@include('pages.layouts.breadcrum')

@include('pages.contents.container.findproductcar')

<section class="section_list_new section_container">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="menu_warranly">
                    <a href="{{$_SERVER['REQUEST_URI']}}#tai-ung-dung">Hướng dẫn tải ứng dụng và đăng nhập</a>
                    <a href="{{$_SERVER['REQUEST_URI']}}#kich-hoat-bao-hanh">Hướng dẫn kích hoạt bảo hành</a>
                    <a href="{{$_SERVER['REQUEST_URI']}}#kiem-tra-bao-hanh-dai-ly">Hướng dẫn kiểm tra bảo hành đại lý</a>
                    <a href="{{$_SERVER['REQUEST_URI']}}#kiem-tra-bao-hanh-nguoi-tieu-dung">Hướng dẫn kiểm tra bảo hành người dùng</a>
                </div>
            </div>
            <div class="col-lg-8 col-12 warranly_active_col">
                <div class="box_warranly warranly_active space_lg" id="tai-ung-dung">
                    <div class="title_content">
                        <h2 class="text_logo">Hướng dẫn tải ứng dụng và đăng nhập</h2>
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 1: Tải ứng dụng</p>
                        <p class="space_sm">Truy cập vào đây để tải ứng dụng</p>
                        <div class="box_item_app d-flex space_sm ">
                            <a target="_blank" class="item_app_footer" href="/download-android">
                                <img class="w-100" src="assets/img/footer/android.svg" alt="google play ">
                            </a>
                            <a target="_blank" class="item_app_footer" href="/download-ios">
                                <img class="w-100" src="assets/img/footer/apple.svg" alt="apple store">
                            </a>
                        </div>
                        <p class="space_sm">Hoặc quét mã QR dưới đây để tải ứng dụng TMAS.</p>
                        <div class="d-flex space_sm">
                            <div>
                                <img class="qr_img" src="assets/img/QR/qr_android.svg" alt="android">
                                <p class="text-center">Android</p>
                            </div>
                            <div>
                                <img class="qr_img" src="assets/img/QR/qr_ios.svg" alt="ios">
                                <p class="text-center">IOS</p>
                            </div>
                        </div>
                    </div>
                    <div class="step_box">
                        <p class="step space_sm">Bước 2: Đăng nhập tài khoản đối tác</p>
                        <p>Đăng nhập vào ứng dụng bằng tài khoản đã được TMAS cung cấp riêng cho Đại lý / Trung tâm nội thất</p>
                        <p>Nếu chưa có tài khoản đại lý, bạn cần liên hệ với bộ phận chăm sóc khách hàng của TMAS để nhận tài khoản</p>
                    </div>
                </div>
                <div class="box_warranly warranly_active space_lg" id='kich-hoat-bao-hanh'>
                    <div class="title_content">
                        <h2 class="text_logo">Hướng dẫn kích hoạt bảo hành</h2>
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 1: Truy cập tính năng tạo hoá đơn</p>
                        <p>Lựa chọn menu Admin, chọn tạo hoá đơn cho khách hàng</p>
                        <img class="img_guide" src="assets/img/warranly/trang-admin-ban-le.jpg" alt="trang-chu-dai-ly">
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 2: Tạo hoá đơn điện tử cho khách hàng</p>
                        <p>Nhập thông tin số điện thoại khách hàng (Bắt buộc)</p>
                        <img class="img_guide" src="assets/img/warranly/tao-thong-tin-hoa-don.jpg" alt="tao-thong-tin-hoa-don">
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 3: Quét mã QR trên vỏ hộp sản phẩm để kích hoạt bảo hành </p>
                        <p>Quét mã xong sản phẩm sẽ tự động kích hoạt bảo hành điện tử trên hệ thống của TMAS.</p>
                        <img class="img_guide" src="assets/img/warranly/tao-thong-tin-hoa-don.jpg" alt="kích hoạt thành công">
                    </div>
                </div>

                <div class="box_warranly warranly_check space_lg" id="kiem-tra-bao-hanh-dai-ly">
                    <div class="title_content">
                        <h2 class="text_logo">Hướng dẫn kiểm tra bảo hành</h2>
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 1: Truy cập tính năng kiểm tra bảo hành</p>
                        <p>Lựa chọn menu Admin, chọn kiểm tra bảo hành cho khách hàng</p>
                        <img class="img_guide" src="assets/img/warranly/trang-admin-kiem-tra-bao-hanh.jpg" alt="trang-chu-dai-ly">
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 2: Quét mã QR trên vỏ hộp sản phẩm để kiểm tra bảo hành</p>
                        <p>Quét mã xong, thời gian bảo hành của sản phẩm sẽ được hiển thị chi tiết.</p>
                        <p>Bạn có thể liên hệ với nhân viên của TMAS để có thể được hỗ trợ kiểm bảo hành chính hãng nhanh chóng.</p>
                    </div>
                </div>


                <div class="box_warranly warranly_check space_lg" id="kiem-tra-bao-hanh-nguoi-tieu-dung">
                    <div class="title_content">
                        <h2 class="text_logo">Hướng dẫn kiểm tra bảo hành</h2>
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 1: Quý khách vui lòng đăng nhập và truy cập menu sản phẩm đã mua</p>
                        <p>Lựa chọn menu sản phẩm đã mua</p>
                        <img class="img_guide" src="assets/img/warranly/trang-admin-kiem-tra-bao-hanh.jpg" alt="trang-chu-dai-ly">
                    </div>
                    <div class="step_box space_md">
                        <p class="step space_sm">Bước 2: Truy cập vào sản phẩm Quý khách muốn xem chi tiết bảo hành</p>
                        <p>Quý khách có thể xem thời hạn bảo hành, nếu Quý khách gặp vấn đề về sản phẩm, hãy mang trực tiếp sản phẩm và mở app,
                            lấy mã QR code của TMAS để được hỗ trợ bảo hành.</p>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

@include('pages.contents.container.productrelated')

@endsection