<svg id="17417" viewBox="0 0 1660 1635" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g transform="matrix(1,0,0,1,0,0)">
        <g id="17417" opacity="1" style="mix-blend-mode:normal">
            <g>
                <defs>
                    <clipPath id="17417_clipPath" x="-50%" y="-50%" width="200%" height="200%">
                        <path d="M0,1635v-1635h1660v1635z" fill="white" clip-rule="nonzero"></path>
                    </clipPath>
                </defs>
                <g clip-path="url(#17417_clipPath)">
                    <g transform="matrix(1,0,0,1,903.21,0.0001)">
                        <g id="17523" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <g transform="matrix(1,0,0,1,0,57.609899999999996)">
                                        <g id="17524" opacity="0.949999988079071" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko1ur21414806737474734" x1="193.76000471740969" y1="400.26001316727803" x2="193.76000471740969" y2="83.68001515907623" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(209, 215, 224)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(235, 235, 255)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17524_fill_path" d="M48.22998,0h291.06c26.62,0 48.22998,21.60999 48.22998,48.23v252.55003c0,26.62 -21.60998,48.22998 -48.22998,48.22998h-291.06c-26.62,0 -48.22998,-21.60998 -48.22998,-48.22998v-252.55003c0,-26.62 21.60998,-48.23 48.22998,-48.23z" fill-rule="nonzero" fill="url(#lfuko1ur21414806737474734)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,63.309999999999945,120.7599)">
                                        <g id="17525" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17525_fill_path" d="M222.55994,0.00999v5.57h32.77002v30.76001h5.57007v-36.32001h-38.34009zM0,36.32999h5.57001v-30.75999h32.76996v-5.57h-38.33997v36.32zM255.32996,195.03998v30.77002h-32.77002v5.57001h38.34009v-36.34003zM5.57001,195.03998h-5.57001v36.34003h38.33997v-5.57001h-32.76996z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,70.75990000000002,0)">
                                        <g id="17526" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g transform="matrix(1,0,0,1,193.8391999999999,173.9299)">
                                                        <g id="17527" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17527_fill_path" d="M52.14076,119.37h-44.62c-0.62,-3.84 -1.1699,-7.69999 -1.6499,-11.51999v-0.03c-2.95,-23.27 -3.69006,-44.50999 -3.69006,-44.50999c0,0 -13.03001,-59.64001 24.60999,-63.31001c0,0 10.08005,3.16 10.68005,7.3c2.27,15.94 7.45994,50.4 7.68994,58.56c0.11,4.21 4.07999,32.67 6.98999,53.52z" fill-rule="nonzero" fill="rgb(50, 54, 87)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,4.490099999999984,137.4108)">
                                                        <g id="17528" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17528_fill_path" d="M217.25995,37.76905c-0.55,16.34 -1.52002,33.85001 -2.90002,52.59001c0,0 -1.8799,30.55997 -2.5199,65.52998h-158.63c-3.69,-17.22 -7.81002,-32.46997 -8.33002,-34.37997c0,-0.02 -0.02002,-0.05003 -0.02002,-0.06003c0,-0.02 -0.00002,-0.04999 -0.02002,-0.04999v-0.07999c0,0 -3.55997,17.17998 -6.40997,34.57998h-38.42999c4.76,-38.2 16.38998,-122.89999 22.53998,-127.30999c5.24,-3.75 38.10003,-16.91999 55.16003,-23.39999c7.88,-3.01 13.87999,-5.10001 15.10999,-5.18001c3.9,-0.24 51.98999,4.37 51.98999,4.37c0,0 6.88993,2.36001 13.92993,5.60001c12.04,5.52 30.77009,14.14 44.22009,20.55c4.35,2.07 9.78993,4.87 14.29993,7.25z" fill-rule="nonzero" fill="rgb(216, 213, 224)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,97.55009999999993,89.52759999999999)">
                                                        <g id="17529" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17529_fill_path" d="M0,47.79225c3.53,32.4 39.51001,32.39999 39.51001,32.39999c0,0 14.88992,-13.03 14.41992,-22.67c-0.01,-0.21 -0.01993,-0.43 -0.04993,-0.63c-0.22,-1.78 -2.23006,-3.97999 -2.44006,-7.53999c-0.03,-0.48 -0.03993,-0.98 -0.04993,-1.47c-0.01,-0.48 -0.01001,-0.96 -0.01001,-1.44c0.01,-2.46 0.14993,-4.89 0.29993,-6.58c0.05,-0.59 0.10001,-1.1 0.14001,-1.5c0.06,-0.6 0.09998,-0.94 0.09998,-0.94c0,0 -5.79992,-6.11 -13.16992,-13.39c-12.88,-12.73 -30.59,-29.05 -30.62,-22.56c0,0.34 0.05002,0.76 0.15002,1.23c3.11,14.61 -8.28003,45.09001 -8.28003,45.09001z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,116.55009999999993,156.38989999999998)">
                                                        <g id="17530" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17530_fill_path" d="M5.54004,0l14.97998,13.33c0,0 -17.29002,-1.15 -20.52002,-4.03z" fill-rule="nonzero" fill="rgb(180, 177, 186)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,157.05009999999993,144.7399)">
                                                        <g id="17531" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17531_fill_path" d="M50.39001,23.18999c-9.09,-4.32 -20.62999,-9.67999 -30.85999,-14.39999c-4.87,-2.27 -9.47,-4.36999 -13.37,-6.14999l-6.16003,-2.64c0.34,0.83 11.55002,27.8 20.02002,86.95c2.17,15.14 5.55,37.10001 8.87,61.60001h31.57996c1.73,-34.79 2.58997,-64.92001 2.58997,-64.92001c1.41,-19.06 2.22,-36.88999 2.75,-53.48999c-4.61,-1.96 -10.57992,-4.66001 -15.41992,-6.96001z" fill-rule="nonzero" fill="rgb(50, 54, 87)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,0,142.5999)">
                                                        <g id="17532" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17532_fill_path" d="M123.92004,150.69998h-68.77002c-1.44,-12.07 -2.99004,-22.66998 -4.42004,-27.59998c0,0 -2.98998,12.24998 -5.72998,27.59998h-45c4.77,-42.26 14.53004,-122.08999 20.35004,-126.26999c4.43,-3.17 37.27994,-14.82 53.37994,-21.16c2.95,-1.16 5.81007,-2.25999 8.45007,-3.26999c3.41,3.8 39.65999,70.85001 42.48999,125.35001c0.32,6.26 -0.03001,15.15998 -0.76001,25.34998z" fill-rule="nonzero" fill="rgb(50, 54, 87)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,111.23000000000002,156.3799)">
                                                        <g id="17533" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17533_fill_path" d="M42.20007,2.36c0,0 1.27997,18.4 -14.28003,18.83c-15.56,0.43 -27.92004,-14.52 -27.92004,-14.52l25.83008,6.66l15.71997,-13.33l0.64001,2.34999z" fill-rule="nonzero" fill="rgb(216, 213, 224)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,87.69010000000003,131.2499)">
                                                        <g id="17534" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17534_fill_path" d="M11.93994,0.00999c0,0 14.75007,18.93001 22.45007,25.13l-12.62,21.18001l-21.77002,-37.04001l11.93994,-9.28z" fill-rule="nonzero" fill="rgb(234, 230, 242)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,137.06009999999992,138.88989999999998)">
                                                        <g id="17535" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17535_fill_path" d="M13.31995,20.98l-13.31995,9.85001c0,0 11.69993,-21.34 11.92993,-30.83z" fill-rule="nonzero" fill="rgb(180, 177, 186)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,148.99,138.8799)">
                                                        <g id="17536" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17536_fill_path" d="M0,0l8.55005,6.04999l9.53003,28.42001l-16.69006,-13.48001z" fill-rule="nonzero" fill="rgb(234, 230, 242)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,105.35359999999991,12.3864)">
                                                        <g id="17537" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17537_fill_path" d="M86.00641,31.65347c0,0 9.39001,22.86 3.26001,25.61c-5.07,2.28 3.07007,24.59 4.07007,28.21c0.99,3.61 -9.28002,5.08 -11.65002,7.09c-2.36,2.01 -11.55,28.81 -19.88,28.77c-8.33,-0.04 -44.13001,-16.87 -53.14001,-28.45c-9.01,-11.58 -12.31996,-59.97 -3.45996,-79.3c8.86,-19.33 64.18993,-21.88 80.79993,18.07z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,95.73980000000006,0)">
                                                        <g id="17538" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17538_fill_path" d="M97.45027,48.96984c0,0 14.36994,-22.11 -5.44006,-41.71c-19.81,-19.6 -62.90001,6.62 -67.51001,7.3c-4.62,0.69 -31.67993,6.47 -22.67993,47.65c8.01,36.67 8.78003,37.63 8.78003,37.63c0,0 10.74991,-19.38999 15.77991,-22.28999c7.86,-4.54 23.73999,-1.58 23.73999,-1.58c0,0 -5.4999,-21.06 6.7301,-22.68c12.23,-1.62 29.00998,-10.07 40.60999,-4.32z" fill-rule="nonzero" fill="rgb(14, 45, 61)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,116.36850000000004,52.1798)">
                                                        <g id="17539" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17539_fill_path" d="M18.0115,10.85006c-0.76,-2.89 -2.90002,-15.19 -13.89002,-9.25c-10.99,5.94 3.28004,30.2 5.04004,30.67c1.76,0.48 15.24998,2.9 8.84998,-21.42z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,115.43010000000004,106.9099)">
                                                        <g id="17540" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17540_fill_path" d="M0,0c0.25,0.62 8.42001,20.48 33.51001,30.5c-0.01,-0.48 -0.01001,-0.96 -0.01001,-1.44c0.01,-2.46 0.14993,-4.89 0.29993,-6.58c0.05,-0.59 0.10001,-1.1 0.14001,-1.5c0,0 -26.94993,-12.67 -33.92993,-20.98z" fill-rule="nonzero" fill="rgb(167, 120, 96)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g transform="matrix(1,0,0,1,938.17,377.9619)">
                        <g id="17418" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <g transform="matrix(1,0,0,1,0,57.90809999999999)">
                                        <g id="17419" opacity="0.949999988079071" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko1yt7178033603202287" x1="360.940045705224" y1="971.7299668278491" x2="360.940045705224" y2="203.1499759518666" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(209, 215, 224)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(235, 235, 255)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17419_fill_path" d="M69.11005,0h583.65991c38.1684,0 69.11011,30.94162 69.11011,69.11002v709.10995c0,38.1684 -30.94171,69.10999 -69.11011,69.10999h-583.65991c-38.1684,0 -69.11005,-30.94159 -69.11005,-69.10999v-709.10995c0,-38.1684 30.94165,-69.11002 69.11005,-69.11002z" fill-rule="nonzero" fill="url(#lfuko1yt7178033603202287)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,109.93999999999994,654.9481000000001)">
                                        <g id="17420" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g transform="matrix(1,0,0,1,0,0)">
                                                        <g id="17421" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17421_fill_path" d="M0,0v0v20.52991v0z" fill-rule="nonzero" fill="rgb(150, 191, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,0,98.55989999999997)">
                                                        <g id="17422" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17422_fill_path" d="M0,0v0v20.53003v0z" fill-rule="nonzero" fill="rgb(150, 191, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,0,49.2799)">
                                                        <g id="17423" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17423_fill_path" d="M0,0v0v20.53003v0z" fill-rule="nonzero" fill="rgb(150, 191, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,115.66999999999996,142.4081)">
                                        <g id="17424" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17424_fill_path" d="M414.58008,0v10.37h61.04993v57.29999h10.37v-67.66998zM0,67.66998h10.37v-57.29999h61.05005v-10.37h-71.42004zM475.63,363.32001v57.32001h-61.04993v10.37h71.41992v-67.69zM10.37,363.32001h-10.37v67.69h71.42004v-10.37h-61.05005z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,508.2900000000001,653.4281000000001)">
                                        <g id="17425" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g transform="matrix(1,0,0,1,0,0)">
                                                        <g id="17426" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17426_fill_path" d="M61.06006,122.12c-33.67,0 -61.06006,-27.39006 -61.06006,-61.06006c0,-33.67 27.39006,-61.05994 61.06006,-61.05994c33.67,0 61.05994,27.38994 61.05994,61.05994c0,33.67 -27.38994,61.06006 -61.05994,61.06006zM61.06006,11.82996c-27.15,0 -49.23999,22.08999 -49.23999,49.23999c0,27.15 22.08999,49.23999 49.23999,49.23999c27.15,0 49.23999,-22.08999 49.23999,-49.23999c0,-27.15 -22.08999,-49.23999 -49.23999,-49.23999z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,29.430000000000064,41.209899999999834)">
                                                        <g id="17427" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17427_fill_path" d="M27.73999,43.88l-27.73999,-27.73999l8.35999,-8.34998l19.38,19.38l27.17004,-27.17004l8.34998,8.35999z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,132.68999999999994,0)">
                                        <g id="17428" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g transform="matrix(1,0,0,1,98.54000000000019,0)">
                                                        <g id="17429" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17429_fill_path" d="M118.97998,0.18812c0,0 56.53001,-5.92 76.89001,48.66001c20.37,54.59 39.53995,72.46001 41.94995,101.14001c9.71,115.99 63.88001,89.74998 48.64001,156.60999c-15.23,66.86 -25.78003,76.97003 -25.78003,76.97003l-129.5,-25.39001l-131.17993,0.41998c0,0 51.32002,-25.62997 39.90002,-80.21997c-11.43,-54.59 -39.90007,-144.06002 -23.82007,-205.00002c16.08,-60.94 69.12003,-68.54999 102.90002,-73.20999z" fill-rule="nonzero" fill="rgb(14, 45, 61)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,0,283.36490000000003)">
                                                        <g id="17430" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17430_fill_path" d="M443.67004,191.48314h-443.67004c21.42,-54.94 42.45007,-106.17001 48.45007,-112.45001c12.74,-13.29 75.44995,-54.89001 128.07996,-71.51001c52.6,-16.6 82.52002,-0.54999 82.52002,-0.54999c0,0 97.65995,27.39001 139.19995,72.26001c11.59,12.52 29.40004,61.6 45.42004,112.25z" fill-rule="nonzero" fill="rgb(235, 71, 71)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,172.894,222.02509999999995)">
                                                        <g id="17431" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17431_fill_path" d="M83.67608,4.92295c0,0 -4.55005,56.43002 6.31995,73.33002c10.87,16.91 -9.29997,39.85 -50.83997,32c-41.54,-7.85 -39.13,-31.39996 -39.13,-31.39996c0,0 11.62998,-34.82 8.22998,-62.69c-3.4,-27.87 75.41004,-11.24005 75.41004,-11.24005z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,156.66000000000008,284.11619999999994)">
                                                        <g id="17432" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17432_fill_path" d="M126.91997,14.16186c0,0 -37.46989,84.75998 -86.00989,103.04998c0,0 -51.7301,-41.09998 -38.8501,-104.04998c0,0 65.63999,-30.27 124.85999,1z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,135.78330000000005,31.504500000000007)">
                                                        <g id="17433" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17433_fill_path" d="M89.19662,0.60358c0,0 74.86002,10.80001 63.90002,121.32001c-10.96,110.51 -77.02995,101.73996 -81.44995,101.64996c-4.41,-0.09 -66.28001,2.30003 -71.64001,-132.54998c0,0 -2.50007,-99.72 89.17993,-90.41z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,128.35400000000004,14.020999999999958)">
                                                        <g id="17434" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17434_fill_path" d="M88.05599,58.13711c0,0 34.97997,36.77002 51.22997,55.39002c16.25,18.62 18.12,54.58997 18.12,54.58997c0,0 27.37004,-33.42999 15.92004,-72.35999c-11.44,-38.93 -38.53007,-114.68 -107.08007,-91.4c-68.55,23.27 -71.08993,125.68 -63.04993,176.45999c0,0 30.77,-112.1 84.87,-122.68z" fill-rule="nonzero" fill="rgb(14, 45, 61)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,187.6300000000001,241.1381)">
                                                        <g id="17435" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17435_fill_path" d="M0,9.28003c0,0 32.00996,16.02997 64.45996,-9.28003c0,0 -12.44994,20.71003 -34.18994,21.84003c-21.74,1.13 -30.27002,-12.56 -30.27002,-12.56z" fill-rule="nonzero" fill="rgb(167, 120, 96)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g transform="matrix(1,0,0,1,323.94,157.54)">
                        <g id="17436" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <g transform="matrix(1,0,0,1,0,95.40299999999999)">
                                        <g id="17437" opacity="0.949999988079071" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko22b2307551162526431" x1="301.4000131893151" y1="946.5300701923832" x2="260.2100098305272" y2="199.9800427857195" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(209, 215, 224)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(235, 235, 255)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17437_fill_path" d="M58.66998,0h410.12c32.40255,0 58.67004,26.26744 58.67004,58.66998v410.12c0,32.40255 -26.2675,58.67004 -58.67004,58.67004h-410.12c-32.40255,0 -58.66998,-26.2675 -58.66998,-58.67004v-410.12c0,-32.40255 26.26744,-58.66998 58.66998,-58.66998z" fill-rule="nonzero" fill="url(#lfuko22b2307551162526431)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,141.63,502.7729999999999)">
                                        <g id="17438" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17438_fill_path" d="M0,0h244.20001v12.06h-244.20001z" fill-rule="nonzero" fill="rgb(150, 191, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,176.22000000000003,540.783)">
                                        <g id="17439" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17439_fill_path" d="M0,0h175.03v12.06h-175.03z" fill-rule="nonzero" fill="rgb(150, 191, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,243.2,443.83299999999997)">
                                        <g id="17440" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17440_fill_path" d="M0,0h41.04999v7.59998h-41.04999z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,89.49000000000001,150.18300000000002)">
                                        <g id="17441" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17441_fill_path" d="M297.27002,0.01001v7.42999h43.77997v41.07999h7.42999v-48.51999h-51.20996zM0,48.53h7.42999v-41.07999h43.78003v-7.43002h-51.21002v48.52002zM341.03998,260.51999v41.10004h-43.77997v7.42999h51.21002v-48.53003zM7.42999,260.51999h-7.42999v48.53003h51.21002v-7.42999h-43.78003z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,110.35000000000002,0)">
                                        <g id="17442" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g transform="matrix(1,0,0,1,97.48939999999999,0.31270000000000664)">
                                                        <g id="17443" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17443_fill_path" d="M64.79063,4.65029c0,0 -25.85999,-16.59001 -49.17999,12.89999c-23.32,29.49 -14.93,57.84001 -10.06,84.32001c4.83,26.26 28.33998,86.33999 18.34998,112.80999c-9.99,26.47 2.78003,40.25 2.78003,40.25c0,0 80.18997,20.53001 70.83997,-100.07999c-9.35,-120.61 -32.73999,-150.20001 -32.73999,-150.20001z" fill-rule="nonzero" fill="rgb(22, 51, 66)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,207.4078,231.703)">
                                                        <g id="17444" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17444_fill_path" d="M3.70221,40.59c0,0 23.47997,62.21999 45.65997,120.20999h60.16999c-9.2,-28.39 -18.51999,-58.64999 -19.54999,-64.20999c-1.96,-10.8 -2.85999,-42.80998 -15.17999,-65.25998c-12.34,-22.45 -41.71003,-31.33002 -41.71003,-31.33002c-48.47,2.19 -29.38995,40.60001 -29.38995,40.60001z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,48.58909999999997,213.6426)">
                                                        <g id="17445" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17445_fill_path" d="M11.67089,119.40039c0,0 9.36999,30.50999 18.42999,59.45999h196.33001c0.83,-2.54 1.85002,-5.22 3.08002,-8.06c14.37,-33.28 21.35999,-61.61999 14.37,-80.51999c-6.99,-18.9 -46.81999,-70.65999 -57.10999,-72.70999c-10.27,-2.05 -39.84003,-16.01999 -39.84003,-16.01999c-19.33,-2.88 -46.8,-1.45002 -56.26,1.84998c-9.44,3.28 -62.07,8.62002 -81.37,23.61002c-22.23,17.27 2.37,92.39999 2.37,92.39999z" fill-rule="nonzero" fill="rgb(237, 218, 211)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,204.12079999999997,204.66299999999998)">
                                                        <g id="17446" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17446_fill_path" d="M0.85917,76.45999l17.97998,-21.05002l-15.60998,-55.40997c0,0 -5.23999,30.85999 -2.37,76.45999z" fill-rule="nonzero" fill="rgb(214, 197, 190)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,139.28000000000003,140.556)">
                                                        <g id="17447" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17447_fill_path" d="M70.37,40.49697c0,0 -5.89998,54.26999 -4.66998,71.92999c0,0 -30.74001,30.80001 -65.70001,-35.95999c0,0 14.71001,-27.52001 3.64001,-65.01001c-11.07,-37.49 66.72998,29.03 66.72998,29.03z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,169.59999999999997,252.98299999999998)">
                                                        <g id="17448" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17448_fill_path" d="M35.38,0v28.14001l-35.38,-26.42999z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,124.81,213.633)">
                                                        <g id="17449" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17449_fill_path" d="M80.17004,67.48999l-14.69,-18.17999l-49.48004,-49.31l-16,15.41998z" fill-rule="nonzero" fill="rgb(214, 197, 190)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,39.639999999999986,218.35299999999998)">
                                                        <g id="17450" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17450_fill_path" d="M0,64.89999c0,0 8.48001,31.44997 31.42001,109.24997h102.17001c3.17,-40.93 4.92997,-73.53996 4.21997,-83.39996c-1.99,-27.74 -43.52002,-90.75 -43.52002,-90.75c0,0 -25.86996,3.11999 -53.64996,9.14999c-31.69,6.87 -40.64001,55.75 -40.64001,55.75z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,217.46999999999997,224.91299999999998)">
                                                        <g id="17451" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17451_fill_path" d="M0,0c25.31,30.29 34.29998,96.83997 35.72998,167.58997h22.72003c26.36,-55.15 19.29999,-80.16998 19.29999,-80.16998c-13.77,-47.14 -54.72003,-80.63998 -54.72003,-80.63998z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,207.33999999999997,204.66299999999998)">
                                                        <g id="17452" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17452_fill_path" d="M15.60999,55.40997l22.65002,6.26001l-38.26001,-61.66998z" fill-rule="nonzero" fill="rgb(250, 230, 222)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,127.33999999999997,213.633)">
                                                        <g id="17453" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17453_fill_path" d="M62.95001,49.31l-20.69,27.12l-42.26001,-70.83002l13.46997,-5.59998z" fill-rule="nonzero" fill="rgb(250, 230, 222)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,151.00999999999993,169.453)">
                                                        <g id="17454" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17454_fill_path" d="M57.41998,23.54001l-1.08997,10.69c0,0 -24.36002,4.76999 -56.33002,-34.23001c0,0 22.65998,27.64001 57.41998,23.54001z" fill-rule="nonzero" fill="rgb(167, 120, 96)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,123.6737,4.164799999999985)">
                                                        <g id="17455" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17455_fill_path" d="M78.55632,189.61823c9.62,-0.35 36.48999,-3.50002 49.73999,-65.39001c16.19,-75.61 -19.59003,-114.82999 -50.22003,-122.09999c-30.64,-7.27 -58.88996,3.37999 -74.57996,42.89999c0,0 -11.47005,30.81001 7.13995,85.71001c18.61,54.89 57.61004,59.26 67.92004,58.88z" fill-rule="nonzero" fill="rgb(238, 169, 135)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,115.0976,0)">
                                                        <g id="17456" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17456_fill_path" d="M62.55238,55.33303c0,0 41.14002,2.56001 79.39002,60.01001c0,0 17.89998,-69.13001 -28.89002,-100.68001c-46.79,-31.55 -79.56003,-4.42 -90.97003,9.33c0,0 -16.36995,3.76 -16.88995,34.83c-0.52,31.06 -21.40001,77.93999 24.42999,100.16999c0,0 -4.37002,-35.46 -0.33002,-43.57c4.04,-8.11 18.04001,-2.63999 33.26001,-60.09999z" fill-rule="nonzero" fill="rgb(22, 51, 66)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,134.70710000000003,96.46799999999999)">
                                                        <g id="17457" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17457_fill_path" d="M11.93294,5.19506c-2.07,-5.38 -7.72997,-7.54 -10.83997,-1.78c-3.11,5.77 0.58996,28.11998 8.89996,30.56998c8.3,2.45 6.15,-17.88998 1.94,-28.78998z" fill-rule="nonzero" fill="rgb(238, 169, 135)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    <g transform="matrix(1,0,0,1,0,221.0941)">
                                                        <g id="17458" opacity="1" style="mix-blend-mode:normal">
                                                            <g>
                                                                <g>
                                                                    <path id="17458_fill_path" d="M0,171.40892h66.88998c7.03,-17.23 13.57,-32.49999 17.94,-43.47999c14.48,-36.45 72.13002,-104.98 35.40002,-127.84c0,0 -48.14002,-2.95 -72.27002,25.13c-24.15,28.1 -22.83,61.43999 -28.97,75.92999c-4.55,10.74 -13.42999,47.71999 -18.98999,70.25999z" fill-rule="nonzero" fill="rgb(69, 152, 230)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g transform="matrix(1,0,0,1,185.33,769.4789)">
                        <g id="17459" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <g transform="matrix(0.9999718382517224,-0.007504845332926968,0.007504845332926968,0.9999718382517224,489.37289999999996,296.2565000000001)">
                                        <g id="17460" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17460_fill_path" d="M0,0h204.26001v18.70996h-204.26001z" fill-rule="nonzero" fill="rgb(50, 54, 87)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(0.292538606935254,-0.9562537129090695,0.9562537129090695,0.292538606935254,674.9181,307.75990000000013)">
                                        <g id="17461" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17461_fill_path" d="M0,0h193.94995v17.76001h-193.94995z" fill-rule="nonzero" fill="rgb(50, 54, 87)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,1065.39,337.76110000000006)">
                                        <g id="17462" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17462_fill_path" d="M0,0h15.94006v463.39001h-15.94006z" fill-rule="nonzero" fill="rgb(112, 104, 101)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(-1,2.4492935982947064e-16,-2.4492935982947064e-16,-1,1170.96,337.76110000000006)">
                                        <g id="17463" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17463_fill_path" d="M0,0h700.5v27.92004h-700.5z" fill-rule="nonzero" fill="rgb(181, 144, 116)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(-1,2.4492935982947064e-16,-2.4492935982947064e-16,-1,567.39,801.1611000000001)">
                                        <g id="17464" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17464_fill_path" d="M0,0h15.93994v463.39001h-15.93994z" fill-rule="nonzero" fill="rgb(112, 104, 101)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,480.51,268.54319999999996)">
                                        <g id="17465" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17465_fill_path" d="M3.01996,7.20788c0,0 10.56,2.13004 15.62,2.29004c5.05,0.16 28.42002,-10.44005 35.90002,-9.43005c7.48,1.01 56.25002,21.72999 58.46002,23.23999c2.21,1.51 -1.04004,4.84009 -1.04004,4.84009l-111.95996,1.32996l3.02997,-22.27002z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,254.37800000000001,729.4711000000001)">
                                        <g id="17466" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17466_fill_path" d="M37.58199,17.70007l-7.18003,15.53992c0,0 -30.24999,-11.27991 -30.39999,-11.65991c-0.15,-0.38 8.33002,-21.58008 8.33002,-21.58008l29.25998,17.70007z" fill-rule="nonzero" fill="rgb(81, 82, 105)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,515.12,746.841)">
                                        <g id="17467" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17467_fill_path" d="M35.92999,0l8.37,24.46008c0,0 -33.20002,7.30993 -38.02002,6.92993c-0.43,-0.03 -6.27997,-27.69995 -6.27997,-27.69995l35.92999,-3.68005z" fill-rule="nonzero" fill="rgb(106, 107, 138)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,517.51,757.6972)">
                                        <g id="17468" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17468_fill_path" d="M0.17002,13.3938c0,0 23.20001,-6.29994 23.89001,-7.30994c0.69,-1 9.19997,-6.75002 13.58997,-6.02002c4.39,0.74 4.77998,5.98005 9.16998,7.05005c4.38,1.07 38.29004,11.90993 42.98004,14.67993c4.7,2.77 27.13,-5.38996 31.44,0.42004c4.32,5.81 8.50996,21.07998 -26.73004,26.35998c-35.24,5.28 -72.96996,21.01 -81.39996,18.87c-8.44,-2.14 -14.21999,-44.65994 -12.92999,-54.05994z" fill-rule="nonzero" fill="rgb(81, 88, 143)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,228.465,746.9011000000002)">
                                        <g id="17469" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17469_fill_path" d="M23.52505,0.01001c0,0 21.5,9.17996 22.63,8.82996c1.13,-0.35 11.08999,0.39998 13.98999,3.59998c2.9,3.21 0.03999,7.45003 2.73999,10.91003c2.7,3.46 18.03001,23.75005 22.51001,28.43005c3.67,3.83 23.79,7.41002 30.25,14.15002c4.87,5.09 -0.84999,25.40995 -35.09,15.69995c-33.34,-9.46 -74.62001,-25.85996 -79.76001,-32.57996c-5.14,-6.72 16.07,-42.65005 22.72,-49.05005z" fill-rule="nonzero" fill="rgb(81, 88, 143)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,130.95629999999997,392.0924000000001)">
                                        <g id="17470" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17470_fill_path" d="M143.08371,49.9287c0,0 94.56998,42.05005 114.60999,56.43005c20.04,14.38 63.92,40.85997 65.38,47.33997c1.46,6.48 10.15003,0.16004 -5.40997,32.54004c-15.56,32.38 -89.14003,110.28987 -96.03003,118.75987c-7.19,8.83 -58.22998,61.75 -58.22998,61.75l-38.38001,-28.90991c0,0 73.79999,-108.51997 84.47999,-124.47997c10.68,-15.96 15.49004,-18.31999 9.17004,-25.37c-6.32,-7.05 -31.55003,-6.94011 -161.43003,-60.74011c-12.98,-5.38 -49.18999,-17.82989 -53.95999,-35.25989c-8.62,-31.49 -1.23001,-71.41009 33.67999,-88.96008c34.91,-17.55 106.13001,46.90003 106.13001,46.90003z" fill-rule="nonzero" fill="rgb(52, 57, 92)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,256.74929999999995,124.86110000000008)">
                                        <g id="17471" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17471_fill_path" d="M62.26067,12.71997c-0.83,0.73 -1.72998,2.33998 -2.66998,4.53998c-1.36,3.16 -2.8,7.57005 -4.19,12.43005c-4.29,15 -8.18002,34.25 -8.18002,34.25c0,0 -54.12998,-45.10005 -46.47998,-47.93005c7.65,-2.83 13.31,-16.00995 13.31,-16.00995c0,0 53.90999,7.65997 48.20999,12.71997z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,178.1,293.36109999999996)">
                                        <g id="17472" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17472_fill_path" d="M137.54001,0c0,0 -43.92999,83.99999 -45.82999,115.48999c-1.9,31.49 -91.71002,-9.90991 -91.71002,-9.90991z" fill-rule="nonzero" fill="rgb(237, 220, 197)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,177.2637,400.7211000000001)">
                                        <g id="17473" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17473_fill_path" d="M92.71626,6.12l1.01001,7.83008l1.57001,10.32996c0,0 -0.00002,0 -0.02002,0c-1.35,-0.09 -87.69998,-5.50992 -93.94998,-3.91992c-6.3,1.61 11.88998,-20.36011 11.88998,-20.36011l79.51001,6.12z" fill-rule="nonzero" fill="rgb(50, 54, 87)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,127.70919999999998,420.3307000000001)">
                                        <g id="17474" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17474_fill_path" d="M144.84079,4.67041c0,0 97.70999,22.98996 121.85999,30.70996c24.16,7.72 65.73002,21.31005 76.90002,27.68005c5.93,3.39 14.98997,8.45001 25.83997,43.76001c12.3,40.03 40.38,152.58999 46.75,170.12c4,11.01 15.51001,54.32996 15.51001,54.32996l-49.92999,8.66003c0,0 -59.45997,-119.71 -64.27997,-151.62c-8.12,-53.71 -28.94004,-54.00001 -37.35004,-58.89001c-8.41,-4.89 -48.91998,4.29005 -193.01999,-7.19995c-14.4,-1.15 -53.76999,-1.86002 -63.95999,-17.40002c-18.41,-28.08 -36.99002,-59.96995 -8.40002,-88.19995c28.6,-28.23 130.08001,-11.95007 130.08001,-11.95007z" fill-rule="nonzero" fill="rgb(73, 79, 128)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,248.29759999999996,16.755500000000097)">
                                        <g id="17475" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17475_fill_path" d="M89.80244,7.75558c0,0 16.91,29.20002 13.06,37.15002c-3.85,7.95 17.66,27.20998 14.13,31.28998c-3.53,4.08 -10.81,9.02002 -10.69,10.21002c0.12,1.19 -6.24999,39.24998 -14.54999,40.09998c-8.3,0.85 -65.03001,-2.08997 -69.73001,-19.83997c-4.7,-17.74 -18.53999,-56.61002 -21.79999,-63.83002c-3.26,-7.22 29.43999,-62.97002 89.57999,-35.08002z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,234.84939999999997,0)">
                                        <g id="17476" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17476_fill_path" d="M33.31063,18.8711c-4.72,3.5 -8.53999,7.22001 -11.58999,11.01001c-3.72,1.75 -25.66001,12.65005 -21.10001,24.42005c6.14,15.85 30.94,58.29999 33.03,62.73999c0.28,0.59 0.28002,-0.53003 0.08002,-2.91003c1.53,-14.99 19.98001,-31.78998 19.98001,-31.78998l9.84,-4.45001c-5.14,-9.55 -3.11002,-18.47998 -3.11002,-18.47998c11.14,-11.94 48.16999,-24.81 48.16999,-24.81c-5.28,-52.83 -50.48997,-34.16 -75.30997,-15.75z" fill-rule="nonzero" fill="rgb(64, 52, 41)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,270.29089999999997,67.71030000000007)">
                                        <g id="17477" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17477_fill_path" d="M14.61908,2.21079c-2.62,-4 -16.42998,-3.34 -14.41998,8.44c2.01,11.78 18.95999,21.23998 22.64999,15.97998c3.69,-5.25 -5.43999,-20.16999 -8.23999,-24.42999z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,129.52,140.87110000000007)">
                                        <g id="17478" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17478_fill_path" d="M127.97,0c0,0 -68.56,39.95002 -84.03,109.71002c-15.47,69.76 -43.45,192.26996 -43.94,206.76996c0,0 92.78,-44.64999 143.03,-32.35999c0,0 31.64001,-113.68003 48.29001,-140.53003c16.64,-26.85 -8.09003,-89.96997 -8.09003,-89.96997c0,0 -23.65,-37.88 -55.25,-53.62z" fill-rule="nonzero" fill="rgb(92, 154, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,288.12,136.48110000000008)">
                                        <g id="17479" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17479_fill_path" d="M28.22,5.63995c-1.36,3.16 -2.8,7.57005 -4.19,12.43005c-8.1,-3.37 -23.56,-17.63001 -24.03,-18.07001c6.45,2.5 28.22,5.63995 28.22,5.63995z" fill-rule="nonzero" fill="rgb(167, 120, 96)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,224.8926,154.56130000000007)">
                                        <g id="17480" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17480_fill_path" d="M59.29742,22.64976c0,0 7.24002,8.30001 17.33002,25.82001c11.44,19.86 20.71998,39.48996 23.19998,43.89996c5.05,8.97 22.85999,18.20006 30.80999,21.62006c7.31,3.14 133.44006,5.80992 134.98006,5.66992c1.55,-0.14 -1.83002,38.31007 -1.83002,38.31007c0,0 -174.11003,4.98003 -181.71003,-1.96997c-7.6,-6.94 -29.57,-33.17007 -38.81,-46.19007c-8.79,-12.4 -22.26998,-32.74996 -29.41998,-41.51996c-7.99,-9.8 -24.77,-43.35002 -3.44,-60.08002c27.25,-21.38 42.31,4.63 48.87,14.44z" fill-rule="nonzero" fill="rgb(92, 154, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,247.35,234.40110000000004)">
                                        <g id="17481" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17481_fill_path" d="M0.01001,0c0,0 13.43,40.87996 25.19,58.95996c11.76,18.08 29.59,36.03003 29.59,36.03003l6.51999,-19.06995c0,0 -49.61,-59.61004 -61.31,-75.92004z" fill-rule="nonzero" fill="rgb(64, 108, 179)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,28.069999999999993,749.841)">
                                        <g id="17482" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17482_fill_path" d="M0,66.52002l4.16,11.72009l175.95999,-54.52002l-8.44,-23.72009z" fill-rule="nonzero" fill="rgb(145, 135, 131)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,194.22,573.9111000000001)">
                                        <g id="17483" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17483_fill_path" d="M0,0h20v191.29993h-20z" fill-rule="nonzero" fill="rgb(145, 135, 131)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,145.23999999999998,563.7511000000001)">
                                        <g id="17484" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17484_fill_path" d="M0,0h117.95999v11.27002h-117.95999z" fill-rule="nonzero" fill="rgb(94, 88, 84)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,201.79999999999998,748.3511)">
                                        <g id="17485" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17485_fill_path" d="M180.12,68.01001l-4.15997,11.72009l-175.96002,-54.52002l7.73999,-25.21008z" fill-rule="nonzero" fill="rgb(145, 135, 131)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,201.79999999999998,748.3511)">
                                        <g id="17486" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17486_fill_path" d="M56.14999,78.47009v13.63l-56.14999,-66.89001l7.73999,-25.21008z" fill-rule="nonzero" fill="rgb(145, 135, 131)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,365.49,816.2110000000001)">
                                        <g id="17487" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17487_fill_path" d="M32.88,16.44006c0,9.08 -7.36,16.43994 -16.44,16.43994c-9.08,0 -16.44,-7.35994 -16.44,-16.43994c0,-9.08 7.36,-16.44006 16.44,-16.44006c9.08,0 16.44,7.36006 16.44,16.44006z" fill-rule="nonzero" fill="rgb(191, 178, 172)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,226.47,816.2110000000001)">
                                        <g id="17488" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17488_fill_path" d="M32.88,16.44006c0,9.08 -7.36,16.43994 -16.44,16.43994c-9.08,0 -16.44,-7.35994 -16.44,-16.43994c0,-9.08 7.36,-16.44006 16.44,-16.44006c9.08,0 16.44,7.36006 16.44,16.44006z" fill-rule="nonzero" fill="rgb(191, 178, 172)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,11.629999999999995,816.2110000000001)">
                                        <g id="17489" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17489_fill_path" d="M32.87999,16.44006c0,9.08 -7.36,16.43994 -16.44,16.43994c-9.08,0 -16.43999,-7.35994 -16.43999,-16.43994c0,-9.08 7.35999,-16.44006 16.43999,-16.44006c9.08,0 16.44,7.36006 16.44,16.44006z" fill-rule="nonzero" fill="rgb(191, 178, 172)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,65.26999999999998,521.0511)">
                                        <g id="17490" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17490_fill_path" d="M21.07999,0h237.28c11.64216,0 21.07999,9.43779 21.07999,21.07996v0.54004c0,11.64216 -9.43782,21.07996 -21.07999,21.07996h-237.28c-11.64216,0 -21.07999,-9.43779 -21.07999,-21.07996v-0.54004c0,-11.64216 9.43782,-21.07996 21.07999,-21.07996z" fill-rule="nonzero" fill="rgb(226, 210, 202)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,0,224.24110000000007)">
                                        <g id="17491" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17491_fill_path" d="M36.5,0h83.77c20.15,0 36.5,16.36 36.5,36.5v193.90002c0,20.15 -16.36,36.5 -36.5,36.5h-83.77c-20.15,0 -36.5,-16.36 -36.5,-36.5v-193.90002c0,-20.15 16.36,-36.5 36.5,-36.5z" fill-rule="nonzero" fill="rgb(226, 210, 202)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,58.08709999999999,394.63710000000003)">
                                        <g id="17492" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17492_fill_path" d="M100.46293,158.48396c-28.41,0 -51.45,-9.53999 -68.5,-28.35999c-38.52,-42.53 -31.60001,-117.29996 -31.29001,-120.45996c0.59,-5.89 5.84001,-10.19999 11.73001,-9.60999c5.89,0.58 10.18999,5.82997 9.60999,11.71997c-0.08,0.85 -6.21999,68.62 25.88,104c12.99,14.31 30.17999,21.27002 52.55999,21.27002c5.92,0 10.72,4.79997 10.72,10.71997c0,5.92 -4.8,10.71997 -10.72,10.71997z" fill-rule="nonzero" fill="rgb(226, 210, 202)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g transform="matrix(1,0,0,1,752.9471,640.3223)">
                        <g id="17493" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <g transform="matrix(1,0,0,1,61.02499999999998,0)">
                                        <g id="17494" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17494_fill_path" d="M84.87788,7.23775c0,0 33.01004,16.15001 24.85004,60.08001c-8.16,43.93 -11.77001,49.85001 -2.64001,67.01001c9.13,17.16 19.9,90.38996 11.69,104.26996c-8.22,13.88 -101.70998,-60.98999 -114.66998,-123.79999c-12.96,-62.82 8.22001,-90.74997 15.70001,-101.15997c7.49,-10.41 35.92996,-21.18001 65.07996,-6.39001z" fill-rule="nonzero" fill="rgb(14, 45, 61)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,155.05290000000002,165.81769999999995)">
                                        <g id="17495" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17495_fill_path" d="M0,0c0,0 16.79999,3.42 26.23999,17.31c6.56,9.66 7.10004,31.12998 8.04004,35.16998c0.95,4.04 13.47998,49.78998 13.47998,49.78998l-35,23.46002z" fill-rule="nonzero" fill="rgb(92, 154, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,240.0335,899.3389999999999)">
                                        <g id="17496" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17496_fill_path" d="M44.33933,48.75884c0,0 22.84009,30.74999 19.71009,35.61999c-3.13,4.87 -27.15003,5.57 -41.78003,0c-14.62,-5.57 -14.97002,-41.61009 -21.58002,-57.09009c-2.77,-6.48 3.15999,-25.62 11.48999,-27c8.33,-1.39 17.39001,2.69006 17.39001,2.69006c0,0 -8.64004,21.81004 14.76996,45.79004z" fill-rule="nonzero" fill="rgb(29, 59, 84)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,246.92290000000003,898.8978)">
                                        <g id="17497" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17497_fill_path" d="M22.83002,3.16997c0,0 4.81,13.08997 6.56,25.59998c1.46,10.37 16.26996,32.39004 15.57996,35.17004c-0.7,2.79 -19.49996,5.31 -27.14996,-1.87c-7.66,-7.18 -12.59002,-28.06005 -14.27002,-40.55005c-1.57,-11.63 -3.54999,-17.38 -3.54999,-17.38c0,0 15.53003,-8.62997 22.84003,-0.96997z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,40.383500000000026,899.3546999999999)">
                                        <g id="17498" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17498_fill_path" d="M45.16935,49.25311c0,0 22.02001,30.23998 18.88001,35.10998c-3.13,4.87 -27.15003,5.57 -41.78003,0c-14.62,-5.57 -14.96996,-41.61009 -21.57996,-57.09009c-2.77,-6.48 3.15999,-25.62 11.48999,-27c8.33,-1.39 18.09997,2.9801 18.09997,2.9801c0,0 -8.52999,22.03 14.88,46z" fill-rule="nonzero" fill="rgb(29, 59, 84)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,50.70670000000007,898.2164)">
                                        <g id="17499" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17499_fill_path" d="M19.95617,4.39136c0,0 1.74003,14.27 4.53003,24.37c2.78,10.1 17.75,33.06998 17.06,35.85998c-0.7,2.79 -19.50002,5.31 -27.15002,-1.87c-7.66,-7.18 -13.57996,-27.97005 -14.26996,-40.55005c-0.7,-12.58 1.73999,-20.79004 1.73999,-20.79004c0,0 10.78998,-4.6699 18.09998,2.9801z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,17.371600000000058,335.49619999999993)">
                                        <g id="17500" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17500_fill_path" d="M163.3213,66.48151c0,0 19.59998,91.1799 24.09998,114.1399c4.5,22.96 17.11002,134.15003 18.46002,153.05003c1.35,18.91 20.44999,90.14002 19.54999,96.90002c-0.9,6.75 28.31995,142.39001 28.31995,142.39001c0,0 -9.97994,8.02998 -20.49994,0.72998c-5.41,-3.76 -64.10999,-126.99 -64.10999,-160.75c0,-46.67 -34.51002,-107.59003 -38.96002,-118.03003c-41.37,-96.99 -33.21999,-126.64994 -38.61999,-132.94994c-5.4,-6.3 -5.87002,3.88998 -9.02002,17.84998c-3.15,13.95 -9.90001,134.60001 -13.95001,149.90001c-4.05,15.3 -0.90997,67.06993 -3.15997,93.17993c-2.25,26.11 -12.13001,144.21008 -12.13001,144.21008c0,0 -7.65001,11.98994 -18.39001,0.30994c-2.31,-2.51 -18.53999,-131.56 -18.98999,-162.62c-0.45,-31.06 -0.92999,-73.89996 -1.79999,-87.32996c-8.34,-128.71001 -28.83999,-147.10006 3.45001,-238.19006c60.45,-170.56 145.76001,-12.79993 145.76001,-12.79993z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,11.25279999999998,376.48969999999997)">
                                        <g id="17501" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17501_fill_path" d="M37.81015,0.00799c0,0 -55.83999,78.14996 -31.79999,244.39996c0,0 132.44998,18.01003 203.40997,-20.83997c0,0 -29.33997,-171.40995 -39.96997,-198.07995c-10.63,-26.67 -131.63,-25.48004 -131.63,-25.48004z" fill-rule="nonzero" fill="rgb(20, 25, 84)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,240.10289999999998,923.9177)">
                                        <g id="17502" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17502_fill_path" d="M0,0v32.19995h4.17999v-24.87988z" fill-rule="nonzero" fill="rgb(29, 59, 84)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,40.46289999999999,923.9177)">
                                        <g id="17503" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17503_fill_path" d="M0,0v32.19995h4.18005v-24.87988z" fill-rule="nonzero" fill="rgb(29, 59, 84)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,21.845000000000027,153.31999999999994)">
                                        <g id="17504" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17504_fill_path" d="M102.15797,1.09773c0,0 20.56998,9.71001 27.71998,11.14001c7.14,1.43 34.86003,37.43001 39.72003,50.57001c4.86,13.14 0,32.86 -10,56c-10,23.14 -0.57001,30.86 -5.14001,54c-4.57,23.14 -8.57002,72.14995 -7.71002,84.99995c0.86,12.86 -115.56996,15.29008 -142.13996,-8.14992c0,0 26.70995,-40.00002 30.13995,-45.71002c3.43,-5.71 0.56999,-36.86005 -2.57001,-44.86005c-3.14,-8 -24.06,-76.03998 -24.06,-76.03998c0,0 -17.10996,-52.25001 -1.64996,-64.26001c13.43,-10.43 50.02998,-14.14999 56.59998,-16.42999c6.57,-2.29 25.68999,-3.28003 39.11999,-1.28003z" fill-rule="nonzero" fill="rgb(237, 218, 211)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,129.96400000000006,147.09769999999992)">
                                        <g id="17505" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17505_fill_path" d="M0.60893,53.17999l12.5,-14.63995l-10.86005,-38.54004c0,0 -3.64996,21.45999 -1.64996,53.17999z" fill-rule="nonzero" fill="rgb(214, 197, 190)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,84.89290000000005,102.51109999999994)">
                                        <g id="17506" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17506_fill_path" d="M48.94,28.16665c0,0 -0.56004,5.15998 -1.23004,12.28998c-0.29,3.16 -0.59996,6.71 -0.89996,10.37c-0.84,10.39 -1.52,21.64005 -1.12,27.36005c0,0 -21.38,21.43 -45.69,-25c0,0 10.22997,-19.14003 2.52997,-45.22003c-7.7,-26.07 46.41003,20.19 46.41003,20.19z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,105.97289999999998,180.70769999999993)">
                                        <g id="17507" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17507_fill_path" d="M24.60004,0v19.56995l-24.60004,-18.38z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,74.8229,153.33769999999993)">
                                        <g id="17508" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17508_fill_path" d="M55.75,46.94l-10.21002,-12.63995l-34.40997,-34.30005l-11.13,10.72003z" fill-rule="nonzero" fill="rgb(214, 197, 190)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,11.232899999999972,156.6177)">
                                        <g id="17509" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17509_fill_path" d="M69.95001,0.01001c0,0 28.87,43.81998 30.25,63.10999c1.38,19.29 -10.72999,163.55999 -20.42999,209.92999c-9.7,46.38 -79.77002,-5.22998 -79.77002,-5.22998c0,0 47.09001,-76.22 29.01001,-137.25c-18.08,-61.03 -24.64001,-85.42999 -24.64001,-85.42999c0,0 6.22001,-33.99002 28.26001,-38.77002c19.32,-4.19 37.32001,-6.37 37.32001,-6.37z" fill-rule="nonzero" fill="rgb(92, 154, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,139.28290000000004,161.17769999999996)">
                                        <g id="17510" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17510_fill_path" d="M54.06,60.81c0,0 5.21004,18.42997 -15.07996,59.15997c0,0 0.31995,52.13004 0.88995,68.42004c0.57,16.29 12.09003,74.84998 12.09003,74.84998l-27.72003,14.76001c-4.04,2.37 -9.04998,-0.90995 -8.47998,-5.56995c5.68,-46.87 24.16999,-224.65005 -15.76001,-272.43005l16.02002,4.71997c0,0 28.46998,23.30002 38.03998,56.08002z" fill-rule="nonzero" fill="rgb(92, 154, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,132.22289999999998,147.09769999999992)">
                                        <g id="17511" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17511_fill_path" d="M10.86005,38.54004l15.75,4.34998l-26.61005,-42.89001z" fill-rule="nonzero" fill="rgb(250, 230, 222)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,76.43290000000002,151.45769999999993)">
                                        <g id="17512" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17512_fill_path" d="M43.92999,36.17999l-14.39001,18.84998l-29.53998,-49.22998l10.23999,-5.79999z" fill-rule="nonzero" fill="rgb(250, 230, 222)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(0.7841269092655715,-0.6206005077065456,0.6206005077065456,0.7841269092655715,141.17470000000003,263.9703999999999)">
                                        <g id="17513" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17513_fill_path" d="M0,0h5.21002v61.59998h-5.21002z" fill-rule="nonzero" fill="rgb(235, 71, 71)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,132.79129999999998,294.70859999999993)">
                                        <g id="17514" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17514_fill_path" d="M6.0316,12.02903c0,0 3.64999,0.07001 6.04999,-1.73999c4.15,-3.13 5.64002,-10.82001 12.96002,-10.26001c7.32,0.56 10.70994,23.50997 8.87994,27.84997c-1.83,4.34 -11.98995,6.52002 -27.00995,3.52002c-15.02,-2.99 -0.88,-19.36999 -0.88,-19.36999z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,152.5367,286.43039999999996)">
                                        <g id="17515" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17515_fill_path" d="M2.78618,8.45725c0,0 10.51997,-3.59002 22.65997,-6.21002c10.65,-2.3 23.38,-3.06002 24.62,-1.21002c1.38,2.06 -10.37,3.83005 -20.81,6.80005c-9.61,2.73 -7.32001,3.72998 -7.32001,3.72998c0,0 -0.62997,-0.32004 10.34003,-1.35004c11.79,-1.1 23.94001,-1.07997 25.14001,1.28003c0.58,1.15 0.17999,1.92 -4.07001,3c2.55,-0.12 2.91001,0.54001 3.01001,1.57001c0.5,5.31 -16.70003,9.53997 -23.53003,11.21997c5.25,-1.26 9.22002,-1.63996 7.33002,1.92004c-2.16,4.07 -26.71,9.00001 -35.37,11.57001c-9.45,2.81 -1.98999,-32.32001 -1.98999,-32.32001z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,0,163.36860000000001)">
                                        <g id="17516" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17516_fill_path" d="M47.9329,0.40907c-7.24,-2.34 -28.47,5.19002 -32.06,26.96002c-3.59,21.78 -5.03,45.94998 -8.37,57.66998c-3.35,11.73 -11.89002,63.67999 -4.71002,67.98999c7.18,4.31 20.77003,8.24005 39.84003,8.24005c19.07,0 96.04999,3.42999 96.04999,3.42999l4.78998,-27.12c0,0 -28.99998,-1.50002 -44.02998,-3.77002c-13.37,-2.03 -31.90999,-7.71999 -53.73999,-10.10999c0,0 4.62998,-32.18003 7.97998,-41.28003c3.35,-9.09 26.8,-71.47 -5.75,-82z" fill-rule="nonzero" fill="rgb(92, 154, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,145.52290000000005,211.20769999999993)">
                                        <g id="17517" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17517_fill_path" d="M0,137.31995l76.92004,-15.25995l57.04004,-122.06l-82.10004,8.62994z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,224.9176,274.4358)">
                                        <g id="17518" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17518_fill_path" d="M7.11531,38.12195c0,0 4.22003,-3.91002 7.97003,-9.39002c3.76,-5.48 16.03998,-26.40997 14.91998,-27.71997c-1.12,-1.31 -19.36999,-1.76003 -22.67999,1.21997c-3.31,2.98 3.26001,4.14001 3.26001,4.14001c0,0 -9.98001,1.02997 -9.26001,3.83997c0.71,2.82 6.71997,2.04004 6.71997,2.04004c0,0 -8.03,1.54001 -6.81,4.01001c1.22,2.47 5.08002,2.44995 5.08002,2.44995c0,0 -7.11999,0.89003 -6.23999,3.59003c0.88,2.7 12.14001,5.24999 10.63001,7.48999c-1.5,2.24 -3.59003,8.33002 -3.59003,8.33002z" fill-rule="nonzero" fill="rgb(238, 168, 134)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,77.88509999999997,10.707699999999932)">
                                        <g id="17519" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17519_fill_path" d="M63.22784,1.4c0,0 47.92997,15.06003 21.90997,97.78003c-26.02,82.72 -91.47995,-9.59004 -84.63995,-59.98004c6.85,-50.4 62.71997,-37.79999 62.71997,-37.79999z" fill-rule="nonzero" fill="rgb(238, 169, 135)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,74.10230000000001,4.0482999999999265)">
                                        <g id="17520" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17520_fill_path" d="M93.82061,56.3194c0,0 -23.81999,23.36998 -63.67999,26.28998c0,0 -15.32,17.96001 -16.5,25.01001c0,0 -35.55997,-60.61996 8.15002,-96.95996c43.71,-36.34 88.08996,30.67997 72.01996,45.64997z" fill-rule="nonzero" fill="rgb(14, 45, 61)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,88.10360000000003,71.75259999999992)">
                                        <g id="17521" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17521_fill_path" d="M16.62926,16.55509c-0.31,-6.02 -4.02002,-19.72002 -12.96002,-15.89002c-8.95,3.84 0.55004,31.73002 6.48004,30.02002c0,0 7.20998,0.10999 6.47998,-14.13z" fill-rule="nonzero" fill="rgb(238, 169, 135)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,99.61289999999997,119.30769999999995)">
                                        <g id="17522" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17522_fill_path" d="M32.97998,23.65997c-0.29,3.16 -0.59996,6.71 -0.89996,10.37c-27.71,-13.68 -31.96002,-33.40997 -32.08002,-34.02997c19.74,25.51 32.98999,23.65002 32.98999,23.65002z" fill-rule="nonzero" fill="rgb(167, 120, 96)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g transform="matrix(1,0,0,1,0,987.7078)">
                        <g id="17541" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <g transform="matrix(1,0,0,1,105.25,246.24220000000003)">
                                        <g id="17542" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2ko4519644509198102" x1="0" y1="141.37005126076156" x2="20.170008533062802" y2="141.37005126076156" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17542_fill_path" d="M18.09,282.7301l-1.39,-0.07007c0.03,-0.51 2.68,-51.56998 1.94,-111.60999c-0.68,-55.38 -4.47,-129.91997 -18.64,-170.58997l1.31,-0.46008c14.24,40.85 18.04,115.56004 18.72,171.04004c0.74,60.08 -1.92,111.18006 -1.94,111.69006z" fill-rule="nonzero" fill="url(#lfuko2ko4519644509198102)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,105.23,101.67220000000009)">
                                        <g id="17543" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2l25523506988857709" x1="1.7599255386357983e-14" y1="213.64996117816347" x2="39.63000000000002" y2="213.64996117816347" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17543_fill_path" d="M38.24,427.30005c-0.17,-3.28 -16.98,-328.97001 -38.24,-427.01001l1.35999,-0.29004c21.29,98.15 38.11,423.95999 38.27,427.23999l-1.39,0.06995z" fill-rule="nonzero" fill="url(#lfuko2l25523506988857709)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,128.1074,191.7722)">
                                        <g id="17544" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2lh5222720176347655" x1="0.002630766964620418" y1="174.0300367998698" x2="8.942599766376558" y2="174.0300367998698" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17544_fill_path" d="M7.55263,348.07007c-0.15,-2.89 -14.5,-290.07007 -3.25,-348.07007l1.37,0.26001c-11.21,57.83 3.13,344.83998 3.28,347.72998l-1.39,0.07007z" fill-rule="nonzero" fill="url(#lfuko2lh5222720176347655)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,133.33,297.8721999999999)">
                                        <g id="17545" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2lv8240030857483627" x1="0" y1="117.81999307155633" x2="14.53" y2="117.81999307155633" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17545_fill_path" d="M1.39,235.64001h-1.39c0,-1.61 0.15,-162.31001 13.16,-235.64001l1.37,0.23999c-12.99,73.21 -13.14,233.78002 -13.14,235.40002z" fill-rule="nonzero" fill="url(#lfuko2lv8240030857483627)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,141.14,171.0838)">
                                        <g id="17546" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2m97123522684024779" x1="-4.7259973712243664e-14" y1="87.7383992109786" x2="106.41999999999996" y2="87.7383992109786" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17546_fill_path" d="M106.42,63.87843c0,0 -14.35,-97.75003 -50.22,-51.41003c-24.76,32 -40.92999,77.16005 -48.56999,102.05005c-3.35,10.91 -5.36,22.17995 -6.03,33.56995l-1.60001,27.40002c77.82,-203.12 106.42,-111.62 106.42,-111.62z" fill-rule="nonzero" fill="url(#lfuko2m97123522684024779)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,128.9276,55.2722)">
                                        <g id="17547" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2mo6014163655376732" x1="0.002443522748778951" y1="85.36996590799087" x2="137.73242524907081" y2="85.36996590799087" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17547_fill_path" d="M136.76245,62.50997c0,0 14.35999,-93.47994 -67.13001,-51.80994c-78.08,39.94 -69.33999,160.02991 -69.33999,160.02991c4.68,-20.03 13.13,-73.54997 41.02,-108.21997c73.91,-91.86 95.43999,0 95.43999,0z" fill-rule="nonzero" fill="url(#lfuko2mo6014163655376732)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,135.53,415.7739)">
                                        <g id="17548" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2n29130284500392138" x1="0" y1="23.31829616441898" x2="48.99" y2="23.31829616441898" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17548_fill_path" d="M48.99001,1.36829c-46.47,-9.16 -48.99001,30.67005 -48.99001,30.67005v14.59998c3.93,-25.16 48.99001,-45.27002 48.99001,-45.27002z" fill-rule="nonzero" fill="url(#lfuko2n29130284500392138)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,0,0)">
                                        <g id="17549" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2ng23716249052477067" x1="0" y1="105.91226139631004" x2="120.48" y2="105.91226139631004" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17549_fill_path" d="M69.94,15.14222c-45.43,-34.07 -69.94,0 -69.94,0c0,0 87.33,4.24007 120.48,196.69007c0,0 -5.11,-162.62007 -50.54,-196.69007z" fill-rule="nonzero" fill="url(#lfuko2ng23716249052477067)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,32.32,152.6221999999999)">
                                        <g id="17550" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2nu4842468895166909" x1="4.85722573273506e-15" y1="85.77" x2="87.489997993937" y2="85.77" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17550_fill_path" d="M0,0c0,0 72.84,94.42004 87.5,171.54004l-1.61,-18.03003c0,0 -2.89,-136.96 -85.89,-153.5z" fill-rule="nonzero" fill="url(#lfuko2nu4842468895166909)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,119.81,324.16219999999987)">
                                        <g id="17551" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <path id="17551_fill_path" d="M0,0l2.51,28.13c0.27,-8.95 -0.66,-18.41 -2.51,-28.13z" fill-rule="nonzero" fill="rgb(218, 229, 232)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,63.4,358.2522)">
                                        <g id="17552" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2pu2510371760236303" x1="0" y1="36.1700501839688" x2="61.3" y2="36.1700501839688" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(107, 158, 52)"></stop>
                                                                <stop offset="14.000000059604645%" stop-opacity="1" stop-color="rgb(97, 147, 60)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(31, 74, 110)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17552_fill_path" d="M0,0c0,0 57.45,59.77008 61.3,72.33008c0,0 -23.05,-68.74008 -61.3,-72.33008z" fill-rule="nonzero" fill="url(#lfuko2pu2510371760236303)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,64.93,521.4021999999999)">
                                        <g id="17553" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2q925904723604014723" x1="0" y1="62.75999961140127" x2="136.8" y2="62.75999961140127" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(209, 215, 224)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(235, 235, 255)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17553_fill_path" d="M116.03001,125.53003l20.76998,-125.53003h-136.79999l20.78,125.53003z" fill-rule="nonzero" fill="url(#lfuko2q925904723604014723)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,52.08,487.7422)">
                                        <g id="17554" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2ql6873329783717441" x1="81.25000191345686" y1="-36.610000915017274" x2="81.25000191345686" y2="236.5300055693285" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(209, 215, 224)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(235, 235, 255)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17554_fill_path" d="M0,0h162.50999v33.65002h-162.50999z" fill-rule="nonzero" fill="url(#lfuko2ql6873329783717441)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    <g transform="matrix(1,0,0,1,64.93,521.4021999999999)">
                                        <g id="17555" opacity="1" style="mix-blend-mode:normal">
                                            <g>
                                                <g>
                                                    <g style="mix-blend-mode:normal">
                                                        <defs>
                                                            <linearGradient id="lfuko2qx7256656661992578" x1="63.1699967047927" y1="-30.289999655794222" x2="103.76999630314873" y2="211.6100061041595" gradientUnits="userSpaceOnUse">
                                                                <stop offset="0%" stop-opacity="1" stop-color="rgb(209, 215, 224)"></stop>
                                                                <stop offset="100%" stop-opacity="1" stop-color="rgb(235, 235, 255)"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                        <path id="17555_fill_path" d="M136.79999,0h-136.79999l20.77,125.53003h23.39c0,0 -22.88,-97.44006 88.15,-98.44006l4.48001,-27.08997z" fill-rule="nonzero" fill="url(#lfuko2qx7256656661992578)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                    <g transform="matrix(1,0,0,1,937.87,862.99)">
                        <g id="17556" opacity="1" style="mix-blend-mode:normal">
                            <g>
                                <g>
                                    <defs>
                                        <mask id="17557_mask" style="mask-type:alpha" x="-50%" y="-50%" width="200%" height="200%">
                                            <g transform="matrix(1,0,0,1,0,0)">
                                                <g id="17557" opacity="1" style="mix-blend-mode:normal">
                                                    <g>
                                                        <g>
                                                            <g transform="matrix(1,0,0,1,0,0)">
                                                                <g id="17558" opacity="1" style="mix-blend-mode:normal">
                                                                    <g>
                                                                        <g>
                                                                            <path id="17558_fill_path" d="M15.08002,5.85999l66.72998,-5.85999l-15.08997,37.21002l-66.72003,5.85999z" fill-rule="nonzero" fill="rgb(0, 0, 0)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </mask>
                                    </defs>
                                    <g mask="url(#17557_mask)">
                                        <g transform="matrix(1,0,0,1,0.6699999999999591,0.30999999999994543)">
                                            <g id="17559" opacity="1" style="mix-blend-mode:normal">
                                                <g>
                                                    <g>
                                                        <g transform="matrix(1,0,0,1,0,0)">
                                                            <g id="17560" opacity="1" style="mix-blend-mode:normal">
                                                                <g>
                                                                    <g>
                                                                        <g transform="matrix(1,0,0,1,36.97000000000003,12.940000000000055)">
                                                                            <g id="17561" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17561_fill_path" d="M6.71997,5.85999l1.12,-0.08997l0.39001,-0.03003c0.37,-0.17 0.72999,-0.30999 1.10999,-0.48999c1.99,-0.93 4.01,-1.98998 5.94,-2.66998c1.4,-0.5 2.73004,-0.76998 4.04004,-0.97998l1.01996,-1.60004l-12.13,1c-1.18,0.1 -2.54999,0.82998 -3.85999,1.97998c-1,0.88 -1.97999,2.00004 -2.79999,3.29004v0.02002l-1.54999,2.40997c2.05,-0.55 4.14997,-1.26 6.27997,-2.12l0.45001,-0.71002z" fill-rule="nonzero" fill="rgb(20, 79, 146)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,25.030000000000086,33.15000000000009)">
                                                                            <g id="17562" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17562_fill_path" d="M0,7.20996l1.71002,-0.13995c2.09,-0.17 4.76999,-2.29003 6.67999,-5.28003l1.13995,-1.78998c-2.02,0.66 -3.99999,1.19 -5.92999,1.56z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,28.629999999999995,14.530000000000086)">
                                                                            <g id="17563" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17563_fill_path" d="M22.88,7.47998l0.72003,-1.13l1.88,-2.95001l2.16998,-3.39996c-1.31,0.21 -2.64003,0.48998 -4.03003,0.97998c-1.93,0.69 -3.95,1.73998 -5.94,2.66998c-0.37,0.18 -0.73999,0.32999 -1.10999,0.48999l3.71002,-0.31l-1.48999,2.33002l-0.39001,0.62l-5.22003,0.42999l1.42004,-2.22998c-2.14,0.87 -4.23003,1.57 -6.28003,2.12l-0.33997,0.53998l-3.18005,5l-0.96997,1.52002l-3.83002,6.02002c1.93,-0.37 3.90999,-0.9 5.92999,-1.56l2.11005,-3.32001l1.95996,-3.08002l5.22003,-0.42999l-2.69,4.22003c0,0 0.09001,-0.04 0.14001,-0.06c1.99,-0.93 4.01,-1.99004 5.94,-2.67004c0.24,-0.09 0.46996,-0.12996 0.70996,-0.20996l1.09003,-1.72003l2.45996,-3.87z" fill-rule="nonzero" fill="rgb(42, 92, 158)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,35.450000000000045,27.600000000000023)">
                                                                            <g id="17564" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17564_fill_path" d="M5.85004,2.87c0,0 -0.09001,0.04 -0.14001,0.06l-1.32001,2.07996l-4.39001,6.89001l1.71002,-0.14001c1.91,-0.16 4.28996,-1.93997 6.14996,-4.52997c0.18,-0.25 0.36003,-0.50001 0.53003,-0.76001l4.10999,-6.46997c-0.24,0.08 -0.46996,0.11996 -0.70996,0.20996c-1.93,0.67 -3.94005,1.73003 -5.93005,2.66003z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,26.15000000000009,0.009999999999990905)">
                                                                            <g id="17565" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17565_fill_path" d="M0,10.77002l46.04999,-3.81c2.76,-0.23 6.28,-3.01002 8.81,-6.96002l-46.04999,3.81c-2.77,0.23 -6.28,3.01002 -8.81,6.96002z" fill-rule="nonzero" fill="rgb(20, 79, 146)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,50.139999999999986,14.13250000000005)">
                                                                            <g id="17566" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17566_fill_path" d="M15.60004,10.36749l3.18994,-5l-10.21997,0.85004l-0.19,0.01996l1.88,-2.95001l3.79999,-0.31l3.13,-0.25995c0.46,-0.04 0.94,-0.18001 1.44,-0.39001c-0.33,-0.09 -0.65997,-0.17999 -0.96997,-0.29999c-1.15,-0.44 -2.17002,-1.08004 -3.33002,-1.48004c-1.8,-0.62 -3.91,-0.63998 -6.12,-0.41998c-1.12,0.92 -2.22001,2.15003 -3.14001,3.59003l-1.88,2.94995l-0.79999,1.25l-2.39001,3.74005l10.41998,-0.86005l-1.25995,1.98999c0.06,0.02 0.11998,0.02004 0.16998,0.04004c1.16,0.4 2.19002,1.03998 3.33002,1.47998c0.12,0.05 0.24994,0.06999 0.37994,0.10999l1.90002,-3l0.66998,-1.04999z" fill-rule="nonzero" fill="rgb(42, 92, 158)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,58.35000000000002,11.56000000000006)">
                                                                            <g id="17567" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17567_fill_path" d="M6.12,3.10999c1.17,0.4 2.19002,1.04004 3.33002,1.48004c0.31,0.12 0.63997,0.20999 0.96997,0.29999c1.78,-0.76 3.73998,-2.54001 5.22998,-4.89001l-3.10999,0.26001l-9.01001,0.75c-1.08,0.09 -2.31997,0.71 -3.52997,1.69c2.21,-0.22 4.32,-0.20002 6.12,0.41998z" fill-rule="nonzero" fill="rgb(20, 79, 146)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,41.74000000000001,26.920000000000073)">
                                                                            <g id="17568" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17568_fill_path" d="M17.71997,0.04004c-0.05,-0.02 -0.11998,-0.03004 -0.16998,-0.04004l-2.28003,3.60004c-1,1.56 -2.38998,2.67001 -3.47998,2.76001l-5.10999,0.41998c-1.06,0.09 -2.27002,0.68 -3.46002,1.63c-1.15,0.92 -2.27997,2.17997 -3.21997,3.65997l12.12994,-1c2.09,-0.17 4.76005,-2.28997 6.68005,-5.27997l2.62994,-4.14001c-0.12,-0.04 -0.25994,-0.06999 -0.37994,-0.10999c-1.14,-0.44 -2.17003,-1.07998 -3.34003,-1.47998z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,0,31.200000000000045)">
                                                                            <g id="17569" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17569_fill_path" d="M0.88,9.84003l-0.88,1.37994l1.70001,-0.13995c0.85,-0.07 1.78999,-0.47005 2.73999,-1.11005c1.39,-0.93 2.8,-2.39999 3.94,-4.17999l2.38,-3.73999c-1.27,-0.57 -2.47999,-1.24999 -3.60999,-2.04999l-6.27002,9.83002z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,11.81000000000006,4.080000000000041)">
                                                                            <g id="17570" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17570_fill_path" d="M10.03003,11.78998l3.19,-5l0.42999,-0.66998c1,-1.51 3.64998,-5.18 6.34998,-6.12l-2.56,0.21002l-0.32001,0.02997l-8.27997,0.69c-2.77,0.23 -6.29003,3.00002 -8.84003,6.96002l8.03003,-0.66003l-3.19,5l-2.47003,3.88c1.22,0.67 2.50004,1.22998 3.85004,1.66998l3.81,-5.97998z" fill-rule="nonzero" fill="rgb(20, 79, 146)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,7.150000000000091,20.180000000000064)">
                                                                            <g id="17571" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17571_fill_path" d="M10.88,1.67004c-1.35,-0.44 -2.64004,-1.00004 -3.85004,-1.67004l-4.83997,7.59003l-2.19,3.45001c1.13,0.8 2.33999,1.47999 3.60999,2.04999l4.47003,-7.03003z" fill-rule="nonzero" fill="rgb(42, 92, 158)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,18.75999999999999,34.90000000000009)">
                                                                            <g id="17572" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17572_fill_path" d="M3.48004,0.51001l-3.48004,5.46997l1.71002,-0.14001c2.09,-0.17 4.76999,-2.28998 6.67999,-5.28998l0.34998,-0.54999c-1.8,0.3 -3.55995,0.48001 -5.25995,0.51001z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,18.930000000000064,14.330000000000041)">
                                                                            <g id="17573" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17573_fill_path" d="M5.90002,6.23999l0.14001,-0.22998l1.04999,-0.09003l-1.77002,2.79004c1.59,0.12 3.22999,0.12 4.92999,0l2.04004,-3.21002l1.01001,-0.08002l-2.03003,3.17999c1.8,-0.19 3.64999,-0.50997 5.54999,-0.96997l4.14001,-6.51001l0.72003,-1.12l-1.71002,0.14001l-4.5,0.37l-1.71002,0.14996l-4.52997,0.37l-1.5,0.12006l-0.20001,0.01996c-2.09,0.17 -4.74997,2.28002 -6.65997,5.27002l-0.87006,1.35999c1.4,0.4 2.87001,0.67002 4.39001,0.83002l1.52002,-2.39001z" fill-rule="nonzero" fill="rgb(20, 79, 146)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,12.550000000000068,35.00999999999999)">
                                                                            <g id="17574" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17574_fill_path" d="M3.20996,1.34003l-3.20996,5.03998l1.69995,-0.14001c2.09,-0.17 4.76999,-2.28998 6.67999,-5.28998l0.35004,-0.54999c-1.61,-0.01 -3.16998,-0.15002 -4.66998,-0.40002l-0.86005,1.34003z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,16.610000000000014,23.020000000000095)">
                                                                            <g id="17575" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17575_fill_path" d="M12.56,0.01001c-1.7,0.13 -3.33999,0.12 -4.92999,0l-7.63,11.96997c1.5,0.25 3.05998,0.39002 4.66998,0.40002l2.64001,-4.14001l5.25,-8.23999z" fill-rule="nonzero" fill="rgb(42, 92, 158)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,22.230000000000018,21.950000000000045)">
                                                                            <g id="17576" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17576_fill_path" d="M13.51001,0c-1.9,0.46 -3.75005,0.77997 -5.55005,0.96997l-4.88,7.65002l-3.07996,4.84003c1.7,-0.04 3.45001,-0.22001 5.26001,-0.51001l3.90997,-6.14001l4.34003,-6.79999z" fill-rule="nonzero" fill="rgb(42, 92, 158)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,11.590000000000032,22.129999999999995)">
                                                                            <g id="17577" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17577_fill_path" d="M7.32001,0l-2.16998,3.41003l-5.15002,8.08997c1.31,0.53 2.70001,0.93998 4.14001,1.22998l7.57996,-11.89996c-1.52,-0.16 -2.98995,-0.44002 -4.38995,-0.83002z" fill-rule="nonzero" fill="rgb(42, 92, 158)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,6.310000000000059,33.629999999999995)">
                                                                            <g id="17578" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17578_fill_path" d="M0.80005,7.02002l-0.80005,1.26001l1.70001,-0.14001c1.86,-0.15 4.17001,-1.84001 6.01001,-4.32001c0.23,-0.31 0.45998,-0.62997 0.66998,-0.96997l1.03003,-1.62006c-1.44,-0.29 -2.82001,-0.70998 -4.14001,-1.22998z" fill-rule="nonzero" fill="rgb(64, 129, 192)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,56.280000000000086,0)">
                                                                            <g id="17579" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17579_fill_path" d="M0,8.28003l15.91998,-1.32001c2.76,-0.23 6.28,-3.01002 8.81,-6.96002l-15.91998,1.32001c-2.76,0.23 -6.28,3.01002 -8.81,6.96002z" fill-rule="nonzero" fill="rgb(233, 41, 42)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,61.66000000000008,2.6800000000000637)">
                                                                            <g id="17580" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17580_fill_path" d="M2.47998,0.03003l-2.17999,3.41998l0.54999,-3.28998l-0.26001,0.01996l-0.58997,3.67004l0.26996,-0.02002l2.46002,-3.83002l-0.25,0.02002z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,63.10000000000002,2.650000000000091)">
                                                                            <g id="17581" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17581_fill_path" d="M1.52997,0.01996l-1.52997,3.75l0.23999,-0.01996l1.52997,-3.75z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,63.85000000000002,2.4700000000000273)">
                                                                            <g id="17582" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17582_fill_path" d="M0.38,3.56l0.59998,-1.48004l1.40002,-0.12l0.12,-0.29999l-1.40002,0.12l0.54999,-1.35999l1.49005,-0.12l0.12,-0.29999l-1.73004,0.13995l-1.52997,3.75l1.76001,-0.13995l0.12,-0.30005l-1.51001,0.12006z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,66.56000000000006,2.2900000000000773)">
                                                                            <g id="17583" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17583_fill_path" d="M0.65002,0.15997l-0.12,0.29999l0.87,-0.07001l-1.40002,3.45001l0.24005,-0.02002l1.39996,-3.44995l0.87,-0.07001l0.12,-0.29999z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,67.92000000000007,2.1100000000000136)">
                                                                            <g id="17584" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17584_fill_path" d="M3.25995,0.02002l-1.28998,3.16998l-0.16998,-3.04999l-0.27002,0.02002l-1.52997,3.75l0.22998,-0.02002l1.28998,-3.16998l0.17999,3.04999l0.27002,-0.02002l1.53003,-3.75z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,70.12,1.9700000000000273)">
                                                                            <g id="17585" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17585_fill_path" d="M2.80005,0.01001l-0.27002,0.01996l-2.53003,3.83002l0.25,-0.02002l0.78003,-1.19l1.14001,-0.09998l-0.17004,1.14001l0.26001,-0.02002l0.54004,-3.66998zM1.22003,2.34998l1.28998,-1.97998l-0.31,1.89996l-0.98999,0.08002z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                        <g transform="matrix(1,0,0,1,72.59000000000003,1.6800000000000637)">
                                                                            <g id="17586" opacity="1" style="mix-blend-mode:normal">
                                                                                <g>
                                                                                    <g>
                                                                                        <path id="17586_fill_path" d="M3.62,0.03003l-2.20001,3.35999l0.47003,-3.21997l-0.36005,0.02997l-1.52997,3.75l0.22998,-0.02002l1.32001,-3.23999h0.01001l-0.46002,3.17004l0.22003,-0.02002l2.17999,-3.31h0.01001l-1.32001,3.23999l0.22998,-0.02002l1.53003,-3.75l-0.36005,0.03003z" fill-rule="nonzero" fill="rgb(255, 255, 255)" fill-opacity="1" style="mix-blend-mode:NORMAL"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
    <animateTransform href="#17523" attributeName="transform" type="translate" values="0 0;-129 0;0 -0.0001" dur="10s" repeatCount="indefinite" calcMode="spline" keyTimes="0;0.5;1" keySplines="0.5 0.35 0.15 1;0.5 0.35 0.15 1" additive="sum" fill="freeze"></animateTransform>
    <animateTransform href="#17418" attributeName="transform" type="translate" values="0 0;13 -108;0 -0.0019000000000346517" dur="10s" repeatCount="indefinite" calcMode="spline" keyTimes="0;0.5;1" keySplines="0.5 0.35 0.15 1;0.5 0.35 0.15 1" additive="sum" fill="freeze"></animateTransform>
    <animate href="#17421_fill_path" attributeName="d" values="M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0h3.0019v20.52991h-3.0019z;M0,0h9.5649v20.52991h-9.5649z;M0,0h16.9992v20.52991h-16.9992z;M0,0h25.5002v20.52991h-25.5002z;M0,0h35.3336v20.52991h-35.3336z;M0,0h46.8686v20.52991h-46.8686z;M0,0h60.61821v20.52991h-60.61821z;M0,0h77.25641v20.52991h-77.25641z;M0,0h97.44591v20.52991h-97.44591z;M0,0h120.97321v20.52991h-120.97321z;M0,0h145.27151v20.52991h-145.27151z;M0,0h166.64192v20.52991h-166.64192z;M0,0h183.65942v20.52991h-183.65942z;M0,0h196.89152v20.52991h-196.89152z;M0,0h207.27622v20.52991h-207.27622z;M0,0h215.56002v20.52991h-215.56002z;M0,0h222.26592v20.52991h-222.26592z;M0,0h227.75602v20.52991h-227.75602z;M0,0h232.28512v20.52991h-232.28512z;M0,0h236.03732v20.52991h-236.03732z;M0,0h239.14912v20.52991h-239.14912z;M0,0h241.72422v20.52991h-241.72422z;M0,0h243.84303v20.52991h-243.84303z;M0,0h245.56903v20.52991h-245.56903z;M0,0h246.95313v20.52991h-246.95313z;M0,0h248.03703v20.52991h-248.03703z;M0,0h248.85503v20.52991h-248.85503z;M0,0h249.43573v20.52991h-249.43573z;M0,0h249.80353v20.52991h-249.80353z;M0,0h249.97893v20.52991h-249.97893z;M0,0h249.61313v20.52991h-249.61313z;M0,0h248.83043v20.52991h-248.83043z;M0,0h248.03583v20.52991h-248.03583z;M0,0h247.22903v20.52991h-247.22903z;M0,0h246.40963v20.52991h-246.40963z;M0,0h245.57733v20.52991h-245.57733z;M0,0h244.73203v20.52991h-244.73203z;M0,0h243.87323v20.52991h-243.87323z;M0,0h243.00062v20.52991h-243.00062z;M0,0h242.11392v20.52991h-242.11392z;M0,0h241.21272v20.52991h-241.21272z;M0,0h240.29682v20.52991h-240.29682z;M0,0h239.36562v20.52991h-239.36562z;M0,0h238.41882v20.52991h-238.41882z;M0,0h237.45592v20.52991h-237.45592z;M0,0h236.47672v20.52991h-236.47672z;M0,0h235.48062v20.52991h-235.48062z;M0,0h234.46712v20.52991h-234.46712z;M0,0h233.43582v20.52991h-233.43582z;M0,0h232.38622v20.52991h-232.38622z;M0,0h231.31782v20.52991h-231.31782z;M0,0h230.23002v20.52991h-230.23002z;M0,0h229.12222v20.52991h-229.12222z;M0,0h227.99402v20.52991h-227.99402z;M0,0h226.84462v20.52991h-226.84462z;M0,0h225.67342v20.52991h-225.67342z;M0,0h224.47972v20.52991h-224.47972z;M0,0h223.26302v20.52991h-223.26302z;M0,0h222.02232v20.52991h-222.02232z;M0,0h220.75712v20.52991h-220.75712z;M0,0h219.46642v20.52991h-219.46642z;M0,0h218.14942v20.52991h-218.14942z;M0,0h216.80542v20.52991h-216.80542z;M0,0h215.43332v20.52991h-215.43332z;M0,0h214.03232v20.52991h-214.03232z;M0,0h212.60122v20.52991h-212.60122z;M0,0h211.13922v20.52991h-211.13922z;M0,0h209.64512v20.52991h-209.64512z;M0,0h208.11782v20.52991h-208.11782z;M0,0h206.55612v20.52991h-206.55612z;M0,0h204.95882v20.52991h-204.95882z;M0,0h203.32452v20.52991h-203.32452z;M0,0h201.65202v20.52991h-201.65202z;M0,0h199.93982v20.52991h-199.93982z;M0,0h198.18652v20.52991h-198.18652z;M0,0h196.39062v20.52991h-196.39062z;M0,0h194.55042v20.52991h-194.55042z;M0,0h192.66452v20.52991h-192.66452z;M0,0h190.73122v20.52991h-190.73122z;M0,0h188.74862v20.52991h-188.74862z;M0,0h186.71522v20.52991h-186.71522z;M0,0h184.62912v20.52991h-184.62912z;M0,0h182.48852v20.52991h-182.48852z;M0,0h180.29172v20.52991h-180.29172z;M0,0h178.03702v20.52991h-178.03702z;M0,0h175.72272v20.52991h-175.72272z;M0,0h173.34722v20.52991h-173.34722z;M0,0h170.90902v20.52991h-170.90902z;M0,0h168.40682v20.52991h-168.40682z;M0,0h165.83962v20.52991h-165.83962z;M0,0h163.20682v20.52991h-163.20682z;M0,0h160.50782v20.52991h-160.50782z;M0,0h157.74292v20.52991h-157.74292z;M0,0h154.91282v20.52991h-154.91282z;M0,0h152.01882v20.52991h-152.01882z;M0,0h149.06302v20.52991h-149.06302z;M0,0h146.04861v20.52991h-146.04861z;M0,0h142.97931v20.52991h-142.97931z;M0,0h139.86031v20.52991h-139.86031z;M0,0h136.69751v20.52991h-136.69751z;M0,0h133.49801v20.52991h-133.49801z;M0,0h130.27001v20.52991h-130.27001z;M0,0h127.02231v20.52991h-127.02231z;M0,0h123.76451v20.52991h-123.76451z;M0,0h120.50691v20.52991h-120.50691z;M0,0h117.25971v20.52991h-117.25971z;M0,0h114.03321v20.52991h-114.03321z;M0,0h110.83741v20.52991h-110.83741z;M0,0h107.68161v20.52991h-107.68161z;M0,0h104.57441v20.52991h-104.57441z;M0,0h101.52331v20.52991h-101.52331z;M0,0h98.53471v20.52991h-98.53471z;M0,0h95.61421v20.52991h-95.61421z;M0,0h92.76571v20.52991h-92.76571z;M0,0h89.99251v20.52991h-89.99251z;M0,0h87.29671v20.52991h-87.29671z;M0,0h84.67941v20.52991h-84.67941z;M0,0h82.14111v20.52991h-82.14111z;M0,0h79.68161v20.52991h-79.68161z;M0,0h77.29991v20.52991h-77.29991z;M0,0h74.99491v20.52991h-74.99491z;M0,0h72.76501v20.52991h-72.76501z;M0,0h70.60821v20.52991h-70.60821z;M0,0h68.52251v20.52991h-68.52251z;M0,0h66.50561v20.52991h-66.50561z;M0,0h64.55531v20.52991h-64.55531z;M0,0h62.66931v20.52991h-62.66931z;M0,0h60.84511v20.52991h-60.84511z;M0,0h59.08041v20.52991h-59.08041z;M0,0h57.37301v20.52991h-57.37301z;M0,0h55.72051v20.52991h-55.72051z;M0,0h54.12091v20.52991h-54.12091z;M0,0h52.57201v20.52991h-52.57201z;M0,0h51.07181v20.52991h-51.07181z;M0,0h49.61831v20.52991h-49.61831z;M0,0h48.2097v20.52991h-48.2097z;M0,0h46.8441v20.52991h-46.8441z;M0,0h45.5198v20.52991h-45.5198z;M0,0h44.2353v20.52991h-44.2353z;M0,0h42.989v20.52991h-42.989z;M0,0h41.7793v20.52991h-41.7793z;M0,0h40.6049v20.52991h-40.6049z;M0,0h39.4644v20.52991h-39.4644z;M0,0h38.3565v20.52991h-38.3565z;M0,0h37.2801v20.52991h-37.2801z;M0,0h36.234v20.52991h-36.234z;M0,0h35.217v20.52991h-35.217z;M0,0h34.2281v20.52991h-34.2281z;M0,0h33.2664v20.52991h-33.2664z;M0,0h32.3308v20.52991h-32.3308z;M0,0h31.4205v20.52991h-31.4205z;M0,0h30.5345v20.52991h-30.5345z;M0,0h29.6722v20.52991h-29.6722z;M0,0h28.8326v20.52991h-28.8326z;M0,0h28.0151v20.52991h-28.0151z;M0,0h27.2189v20.52991h-27.2189z;M0,0h26.4434v20.52991h-26.4434z;M0,0h25.6878v20.52991h-25.6878z;M0,0h24.9517v20.52991h-24.9517z;M0,0h24.2343v20.52991h-24.2343z;M0,0h23.5351v20.52991h-23.5351z;M0,0h22.8536v20.52991h-22.8536z;M0,0h22.1892v20.52991h-22.1892z;M0,0h21.5415v20.52991h-21.5415z;M0,0h20.91v20.52991h-20.91z;M0,0h20.2941v20.52991h-20.2941z;M0,0h19.6936v20.52991h-19.6936z;M0,0h19.1078v20.52991h-19.1078z;M0,0h18.5365v20.52991h-18.5365z;M0,0h17.9793v20.52991h-17.9793z;M0,0h17.4358v20.52991h-17.4358z;M0,0h16.9055v20.52991h-16.9055z;M0,0h16.3882v20.52991h-16.3882z;M0,0h15.8836v20.52991h-15.8836z;M0,0h15.3913v20.52991h-15.3913z;M0,0h14.911v20.52991h-14.911z;M0,0h14.4424v20.52991h-14.4424z;M0,0h13.9853v20.52991h-13.9853z;M0,0h13.5393v20.52991h-13.5393z;M0,0h13.1042v20.52991h-13.1042z;M0,0h12.6797v20.52991h-12.6797z;M0,0h12.2656v20.52991h-12.2656z;M0,0h11.8616v20.52991h-11.8616z;M0,0h11.4675v20.52991h-11.4675z;M0,0h11.0832v20.52991h-11.0832z;M0,0h10.7083v20.52991h-10.7083z;M0,0h10.3427v20.52991h-10.3427z;M0,0h9.9861v20.52991h-9.9861z;M0,0h9.6384v20.52991h-9.6384z;M0,0h9.2993v20.52991h-9.2993z;M0,0h8.9688v20.52991h-8.9688z;M0,0h8.6466v20.52991h-8.6466z;M0,0h8.3325v20.52991h-8.3325z;M0,0h8.0264v20.52991h-8.0264z;M0,0h7.7281v20.52991h-7.7281z;M0,0h7.4375v20.52991h-7.4375z;M0,0h7.1543v20.52991h-7.1543z;M0,0h6.8786v20.52991h-6.8786z;M0,0h6.61v20.52991h-6.61z;M0,0h6.3485v20.52991h-6.3485z;M0,0h6.094v20.52991h-6.094z;M0,0h5.8463v20.52991h-5.8463z;M0,0h5.6052v20.52991h-5.6052z;M0,0h5.3708v20.52991h-5.3708z;M0,0h5.1427v20.52991h-5.1427z;M0,0h4.921v20.52991h-4.921z;M0,0h4.7055v20.52991h-4.7055z;M0,0h4.4961v20.52991h-4.4961z;M0,0h4.2927v20.52991h-4.2927z;M0,0h4.0952v20.52991h-4.0952z;M0,0h3.9034v20.52991h-3.9034z;M0,0h3.7173v20.52991h-3.7173z;M0,0h3.5369v20.52991h-3.5369z;M0,0h3.3619v20.52991h-3.3619z;M0,0h3.1923v20.52991h-3.1923z;M0,0h3.028v20.52991h-3.028z;M0,0h2.869v20.52991h-2.869z;M0,0h2.7151v20.52991h-2.7151z;M0,0h2.5662v20.52991h-2.5662z;M0,0h2.4223v20.52991h-2.4223z;M0,0h2.2833v20.52991h-2.2833z;M0,0h2.1492v20.52991h-2.1492z;M0,0h2.0197v20.52991h-2.0197z;M0,0h1.895v20.52991h-1.895z;M0,0h1.7748v20.52991h-1.7748z;M0,0h1.6592v20.52991h-1.6592z;M0,0h1.548v20.52991h-1.548z;M0,0h1.4412v20.52991h-1.4412z;M0,0h1.3388v20.52991h-1.3388z;M0,0h1.2406v20.52991h-1.2406z;M0,0h1.1466v20.52991h-1.1466z;M0,0h1.0567v20.52991h-1.0567z;M0,0h0.9709v20.52991h-0.9709z;M0,0h0.8891v20.52991h-0.8891z;M0,0h0.8113v20.52991h-0.8113z;M0,0h0.7375v20.52991h-0.7375z;M0,0h0.6674v20.52991h-0.6674z;M0,0h0.6012v20.52991h-0.6012z;M0,0h0.5387v20.52991h-0.5387z;M0,0h0.4799v20.52991h-0.4799z;M0,0h0.4247v20.52991h-0.4247z;M0,0h0.3731v20.52991h-0.3731z;M0,0h0.3251v20.52991h-0.3251z;M0,0h0.2806v20.52991h-0.2806z;M0,0h0.2395v20.52991h-0.2395z;M0,0h0.2019v20.52991h-0.2019z;M0,0h0.1676v20.52991h-0.1676z;M0,0h0.1366v20.52991h-0.1366z;M0,0h0.1089v20.52991h-0.1089z;M0,0h0.0844v20.52991h-0.0844z;M0,0h0.0631v20.52991h-0.0631z;M0,0h0.045v20.52991h-0.045z;M0,0h0.03v20.52991h-0.03z;M0,0h0.0181v20.52991h-0.0181z;M0,0h0.0092v20.52991h-0.0092z;M0,0h0.0033v20.52991h-0.0033z;M0,0h0.0004v20.52991h-0.0004z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z;M0,0v0v20.52991v0z" dur="10s" repeatCount="indefinite" keyTimes="0;0.002;0.004;0.006;0.008;0.01;0.012;0.014;0.016;0.018;0.02;0.022;0.024;0.026;0.028;0.03;0.032;0.034;0.036;0.038;0.04;0.042;0.044;0.046;0.048;0.05;0.052;0.054;0.056;0.058;0.06;0.062;0.064;0.066;0.068;0.07;0.072;0.074;0.076;0.078;0.08;0.082;0.084;0.086;0.088;0.09;0.092;0.094;0.096;0.098;0.1;0.102;0.104;0.106;0.108;0.11;0.112;0.114;0.116;0.118;0.12;0.122;0.124;0.126;0.128;0.13;0.132;0.134;0.136;0.138;0.14;0.142;0.144;0.146;0.148;0.15;0.152;0.154;0.156;0.158;0.16;0.162;0.164;0.166;0.168;0.17;0.172;0.174;0.176;0.178;0.18;0.182;0.184;0.186;0.188;0.19;0.192;0.194;0.196;0.198;0.2;0.202;0.204;0.206;0.208;0.21;0.212;0.214;0.216;0.218;0.22;0.222;0.224;0.226;0.228;0.23;0.232;0.234;0.236;0.238;0.24;0.242;0.244;0.246;0.248;0.25;0.252;0.254;0.256;0.258;0.26;0.262;0.264;0.266;0.268;0.27;0.272;0.274;0.276;0.278;0.28;0.282;0.284;0.286;0.288;0.29;0.292;0.294;0.296;0.298;0.3;0.302;0.304;0.306;0.308;0.31;0.312;0.314;0.316;0.318;0.32;0.322;0.324;0.326;0.328;0.33;0.332;0.334;0.336;0.338;0.34;0.342;0.344;0.346;0.348;0.35;0.352;0.354;0.356;0.358;0.36;0.362;0.364;0.366;0.368;0.37;0.372;0.374;0.376;0.378;0.38;0.382;0.384;0.386;0.388;0.39;0.392;0.394;0.396;0.398;0.4;0.402;0.404;0.406;0.408;0.41;0.412;0.414;0.416;0.418;0.42;0.422;0.424;0.426;0.428;0.43;0.432;0.434;0.436;0.438;0.44;0.442;0.444;0.446;0.448;0.45;0.452;0.454;0.456;0.458;0.46;0.462;0.464;0.466;0.468;0.47;0.472;0.474;0.476;0.478;0.48;0.482;0.484;0.486;0.488;0.49;0.492;0.494;0.496;0.498;0.5;0.502;0.504;0.506;0.508;0.51;0.512;0.514;0.516;0.518;0.52;0.522;0.524;0.526;0.528;0.53;0.532;0.534;0.536;0.538;0.54;0.542;0.544;0.546;0.548;0.55;0.552;0.554;0.556;0.558;0.56;0.562;0.564;0.566;0.568;0.57;0.572;0.574;0.576;0.578;0.58;0.582;0.584;0.586;0.588;0.59;0.592;0.594;0.596;0.598;0.6;0.602;0.604;0.606;0.608;0.61;0.612;0.614;0.616;0.618;0.62;0.622;0.624;0.626;0.628;0.63;0.632;0.634;0.636;0.638;0.64;0.642;0.644;0.646;0.648;0.65;0.652;0.654;0.656;0.658;0.66;0.662;0.664;0.666;0.668;0.67;0.672;0.674;0.676;0.678;0.68;0.682;0.684;0.686;0.688;0.69;0.692;0.694;0.696;0.698;0.7;0.702;0.704;0.706;0.708;0.71;0.712;0.714;0.716;0.718;0.72;0.722;0.724;0.726;0.728;0.73;0.732;0.734;0.736;0.738;0.74;0.742;0.744;0.746;0.748;0.75;0.752;0.754;0.756;0.758;0.76;0.762;0.764;0.766;0.768;0.77;0.772;0.774;0.776;0.778;0.78;0.782;0.784;0.786;0.788;0.79;0.792;0.794;0.796;0.798;0.8;0.802;0.804;0.806;0.808;0.81;0.812;0.814;0.816;0.818;0.82;0.822;0.824;0.826;0.828;0.83;0.832;0.834;0.836;0.838;0.84;0.842;0.844;0.846;0.848;0.85;0.852;0.854;0.856;0.858;0.86;0.862;0.864;0.866;0.868;0.87;0.872;0.874;0.876;0.878;0.88;0.882;0.884;0.886;0.888;0.89;0.892;0.894;0.896;0.898;0.9;0.902;0.904;0.906;0.908;0.91;0.912;0.914;0.916;0.918;0.92;0.922;0.924;0.926;0.928;0.93;0.932;0.934;0.936;0.938;0.94;0.942;0.944;0.946;0.948;0.95;0.952;0.954;0.956;0.958;0.96;0.962;0.964;0.966;0.968;0.97;0.972;0.974;0.976;0.978;0.98;0.982;0.984;0.986;0.988;0.99;0.992;0.994;0.996;0.998;1" fill="freeze"></animate>
    <animate href="#17422_fill_path" attributeName="d" values="M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0h4.546v20.53003h-4.546z;M0,0h9.6248v20.53003h-9.6248z;M0,0h15.3388v20.53003h-15.3388z;M0,0h21.8224v20.53003h-21.8224z;M0,0h29.2556v20.53003h-29.2556z;M0,0h37.8821v20.53003h-37.8821z;M0,0h48.03251v20.53003h-48.03251z;M0,0h60.13231v20.53003h-60.13231z;M0,0h74.61821v20.53003h-74.61821z;M0,0h91.52501v20.53003h-91.52501z;M0,0h109.62221v20.53003h-109.62221z;M0,0h126.49372v20.53003h-126.49372z;M0,0h140.54552v20.53003h-140.54552z;M0,0h151.73052v20.53003h-151.73052z;M0,0h160.61222v20.53003h-160.61222z;M0,0h167.75022v20.53003h-167.75022z;M0,0h173.56732v20.53003h-173.56732z;M0,0h178.36472v20.53003h-178.36472z;M0,0h182.35732v20.53003h-182.35732z;M0,0h185.70102v20.53003h-185.70102z;M0,0h188.51142v20.53003h-188.51142z;M0,0h190.87622v20.53003h-190.87622z;M0,0h192.86312v20.53003h-192.86312z;M0,0h194.52562v20.53003h-194.52562z;M0,0h195.90652v20.53003h-195.90652z;M0,0h197.04082v20.53003h-197.04082z;M0,0h197.95693v20.53003h-197.95693z;M0,0h198.67893v20.53003h-198.67893z;M0,0h199.22693v20.53003h-199.22693z;M0,0h199.61793v20.53003h-199.61793z;M0,0h199.86663v20.53003h-199.86663z;M0,0h199.98563v20.53003h-199.98563z;M0,0h199.57363v20.53003h-199.57363z;M0,0h198.70723v20.53003h-198.70723z;M0,0h197.82263v20.53003h-197.82263z;M0,0h196.91892v20.53003h-196.91892z;M0,0h195.99572v20.53003h-195.99572z;M0,0h195.05232v20.53003h-195.05232z;M0,0h194.08802v20.53003h-194.08802z;M0,0h193.10212v20.53003h-193.10212z;M0,0h192.09392v20.53003h-192.09392z;M0,0h191.06252v20.53003h-191.06252z;M0,0h190.00712v20.53003h-190.00712z;M0,0h188.92692v20.53003h-188.92692z;M0,0h187.82092v20.53003h-187.82092z;M0,0h186.68812v20.53003h-186.68812z;M0,0h185.52752v20.53003h-185.52752z;M0,0h184.33812v20.53003h-184.33812z;M0,0h183.11862v20.53003h-183.11862z;M0,0h181.86792v20.53003h-181.86792z;M0,0h180.58452v20.53003h-180.58452z;M0,0h179.26732v20.53003h-179.26732z;M0,0h177.91462v20.53003h-177.91462z;M0,0h176.52512v20.53003h-176.52512z;M0,0h175.09692v20.53003h-175.09692z;M0,0h173.62832v20.53003h-173.62832z;M0,0h172.11762v20.53003h-172.11762z;M0,0h170.56272v20.53003h-170.56272z;M0,0h168.96152v20.53003h-168.96152z;M0,0h167.31182v20.53003h-167.31182z;M0,0h165.61122v20.53003h-165.61122z;M0,0h163.85722v20.53003h-163.85722z;M0,0h162.04722v20.53003h-162.04722z;M0,0h160.17822v20.53003h-160.17822z;M0,0h158.24732v20.53003h-158.24732z;M0,0h156.25142v20.53003h-156.25142z;M0,0h154.18712v20.53003h-154.18712z;M0,0h152.05112v20.53003h-152.05112z;M0,0h149.83972v20.53003h-149.83972z;M0,0h147.54942v20.53003h-147.54942z;M0,0h145.17642v20.53003h-145.17642z;M0,0h142.71702v20.53003h-142.71702z;M0,0h140.16782v20.53003h-140.16782z;M0,0h137.52532v20.53003h-137.52532z;M0,0h134.78692v20.53003h-134.78692z;M0,0h131.95012v20.53003h-131.95012z;M0,0h129.01352v20.53003h-129.01352z;M0,0h125.97712v20.53003h-125.97712z;M0,0h122.84212v20.53003h-122.84212z;M0,0h119.61182v20.53003h-119.61182z;M0,0h116.29191v20.53003h-116.29191z;M0,0h112.89081v20.53003h-112.89081z;M0,0h109.41971v20.53003h-109.41971z;M0,0h105.89291v20.53003h-105.89291z;M0,0h102.32751v20.53003h-102.32751z;M0,0h98.74301v20.53003h-98.74301z;M0,0h95.16021v20.53003h-95.16021z;M0,0h91.60071v20.53003h-91.60071z;M0,0h88.08541v20.53003h-88.08541z;M0,0h84.63371v20.53003h-84.63371z;M0,0h81.26261v20.53003h-81.26261z;M0,0h77.98591v20.53003h-77.98591z;M0,0h74.81441v20.53003h-74.81441z;M0,0h71.75571v20.53003h-71.75571z;M0,0h68.81431v20.53003h-68.81431z;M0,0h65.99221v20.53003h-65.99221z;M0,0h63.28931v20.53003h-63.28931z;M0,0h60.70401v20.53003h-60.70401z;M0,0h58.23331v20.53003h-58.23331z;M0,0h55.87331v20.53003h-55.87331z;M0,0h53.61981v20.53003h-53.61981z;M0,0h51.46791v20.53003h-51.46791z;M0,0h49.41291v20.53003h-49.41291z;M0,0h47.44991v20.53003h-47.44991z;M0,0h45.57391v20.53003h-45.57391z;M0,0h43.78051v20.53003h-43.78051z;M0,0h42.06501v20.53003h-42.06501z;M0,0h40.42331v20.53003h-40.42331z;M0,0h38.8513v20.53003h-38.8513z;M0,0h37.3451v20.53003h-37.3451z;M0,0h35.9013v20.53003h-35.9013z;M0,0h34.5164v20.53003h-34.5164z;M0,0h33.1874v20.53003h-33.1874z;M0,0h31.9112v20.53003h-31.9112z;M0,0h30.6852v20.53003h-30.6852z;M0,0h29.5068v20.53003h-29.5068z;M0,0h28.3736v20.53003h-28.3736z;M0,0h27.2834v20.53003h-27.2834z;M0,0h26.234v20.53003h-26.234z;M0,0h25.2236v20.53003h-25.2236z;M0,0h24.2503v20.53003h-24.2503z;M0,0h23.3123v20.53003h-23.3123z;M0,0h22.4082v20.53003h-22.4082z;M0,0h21.5363v20.53003h-21.5363z;M0,0h20.6954v20.53003h-20.6954z;M0,0h19.884v20.53003h-19.884z;M0,0h19.1009v20.53003h-19.1009z;M0,0h18.345v20.53003h-18.345z;M0,0h17.6151v20.53003h-17.6151z;M0,0h16.9102v20.53003h-16.9102z;M0,0h16.2294v20.53003h-16.2294z;M0,0h15.5717v20.53003h-15.5717z;M0,0h14.9362v20.53003h-14.9362z;M0,0h14.3222v20.53003h-14.3222z;M0,0h13.7288v20.53003h-13.7288z;M0,0h13.1552v20.53003h-13.1552z;M0,0h12.6009v20.53003h-12.6009z;M0,0h12.0651v20.53003h-12.0651z;M0,0h11.5472v20.53003h-11.5472z;M0,0h11.0466v20.53003h-11.0466z;M0,0h10.5627v20.53003h-10.5627z;M0,0h10.095v20.53003h-10.095z;M0,0h9.643v20.53003h-9.643z;M0,0h9.2062v20.53003h-9.2062z;M0,0h8.784v20.53003h-8.784z;M0,0h8.3761v20.53003h-8.3761z;M0,0h7.982v20.53003h-7.982z;M0,0h7.6014v20.53003h-7.6014z;M0,0h7.2337v20.53003h-7.2337z;M0,0h6.8788v20.53003h-6.8788z;M0,0h6.5361v20.53003h-6.5361z;M0,0h6.2053v20.53003h-6.2053z;M0,0h5.8862v20.53003h-5.8862z;M0,0h5.5784v20.53003h-5.5784z;M0,0h5.2816v20.53003h-5.2816z;M0,0h4.9955v20.53003h-4.9955z;M0,0h4.7198v20.53003h-4.7198z;M0,0h4.4543v20.53003h-4.4543z;M0,0h4.1987v20.53003h-4.1987z;M0,0h3.9527v20.53003h-3.9527z;M0,0h3.7162v20.53003h-3.7162z;M0,0h3.4889v20.53003h-3.4889z;M0,0h3.2705v20.53003h-3.2705z;M0,0h3.0608v20.53003h-3.0608z;M0,0h2.8598v20.53003h-2.8598z;M0,0h2.667v20.53003h-2.667z;M0,0h2.4824v20.53003h-2.4824z;M0,0h2.3058v20.53003h-2.3058z;M0,0h2.137v20.53003h-2.137z;M0,0h1.9758v20.53003h-1.9758z;M0,0h1.822v20.53003h-1.822z;M0,0h1.6756v20.53003h-1.6756z;M0,0h1.5362v20.53003h-1.5362z;M0,0h1.4039v20.53003h-1.4039z;M0,0h1.2784v20.53003h-1.2784z;M0,0h1.1596v20.53003h-1.1596z;M0,0h1.0473v20.53003h-1.0473z;M0,0h0.9415v20.53003h-0.9415z;M0,0h0.842v20.53003h-0.842z;M0,0h0.7486v20.53003h-0.7486z;M0,0h0.6613v20.53003h-0.6613z;M0,0h0.5799v20.53003h-0.5799z;M0,0h0.5043v20.53003h-0.5043z;M0,0h0.4345v20.53003h-0.4345z;M0,0h0.3702v20.53003h-0.3702z;M0,0h0.3114v20.53003h-0.3114z;M0,0h0.2581v20.53003h-0.2581z;M0,0h0.21v20.53003h-0.21z;M0,0h0.1671v20.53003h-0.1671z;M0,0h0.1293v20.53003h-0.1293z;M0,0h0.0966v20.53003h-0.0966z;M0,0h0.0688v20.53003h-0.0688z;M0,0h0.0458v20.53003h-0.0458z;M0,0h0.0275v20.53003h-0.0275z;M0,0h0.014v20.53003h-0.014z;M0,0h0.005v20.53003h-0.005z;M0,0h0.0006v20.53003h-0.0006z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z" dur="10s" repeatCount="indefinite" keyTimes="0;0.002;0.004;0.006;0.008;0.01;0.012;0.014;0.016;0.018;0.02;0.022;0.024;0.026;0.028;0.03;0.032;0.034;0.036;0.038;0.04;0.042;0.044;0.046;0.048;0.05;0.052;0.054;0.056;0.058;0.06;0.062;0.064;0.066;0.068;0.07;0.072;0.074;0.076;0.078;0.08;0.082;0.084;0.086;0.088;0.09;0.092;0.094;0.096;0.098;0.1;0.102;0.104;0.106;0.108;0.11;0.112;0.114;0.116;0.118;0.12;0.122;0.124;0.126;0.128;0.13;0.132;0.134;0.136;0.138;0.14;0.142;0.144;0.146;0.148;0.15;0.152;0.154;0.156;0.158;0.16;0.162;0.164;0.166;0.168;0.17;0.172;0.174;0.176;0.178;0.18;0.182;0.184;0.186;0.188;0.19;0.192;0.194;0.196;0.198;0.2;0.202;0.204;0.206;0.208;0.21;0.212;0.214;0.216;0.218;0.22;0.222;0.224;0.226;0.228;0.23;0.232;0.234;0.236;0.238;0.24;0.242;0.244;0.246;0.248;0.25;0.252;0.254;0.256;0.258;0.26;0.262;0.264;0.266;0.268;0.27;0.272;0.274;0.276;0.278;0.28;0.282;0.284;0.286;0.288;0.29;0.292;0.294;0.296;0.298;0.3;0.302;0.304;0.306;0.308;0.31;0.312;0.314;0.316;0.318;0.32;0.322;0.324;0.326;0.328;0.33;0.332;0.334;0.336;0.338;0.34;0.342;0.344;0.346;0.348;0.35;0.352;0.354;0.356;0.358;0.36;0.362;0.364;0.366;0.368;0.37;0.372;0.374;0.376;0.378;0.38;0.382;0.384;0.386;0.388;0.39;0.392;0.394;0.396;0.398;0.4;0.402;0.404;0.406;0.408;0.41;0.412;0.414;0.416;0.418;0.42;0.422;0.424;0.426;0.428;0.43;0.432;0.434;0.436;0.438;0.44;0.442;0.444;0.446;0.448;0.45;0.452;0.454;0.456;0.458;0.46;0.462;0.464;0.466;0.468;0.47;0.472;0.474;0.476;0.478;0.48;0.482;0.484;0.486;0.488;0.49;0.492;0.494;0.496;0.498;0.5;0.502;0.504;0.506;0.508;0.51;0.512;0.514;0.516;0.518;0.52;0.522;0.524;0.526;0.528;0.53;0.532;0.534;0.536;0.538;0.54;0.542;0.544;0.546;0.548;0.55;0.552;0.554;0.556;0.558;0.56;0.562;0.564;0.566;0.568;0.57;0.572;0.574;0.576;0.578;0.58;0.582;0.584;0.586;0.588;0.59;0.592;0.594;0.596;0.598;0.6;0.602;0.604;0.606;0.608;0.61;0.612;0.614;0.616;0.618;0.62;0.622;0.624;0.626;0.628;0.63;0.632;0.634;0.636;0.638;0.64;0.642;0.644;0.646;0.648;0.65;0.652;0.654;0.656;0.658;0.66;0.662;0.664;0.666;0.668;0.67;0.672;0.674;0.676;0.678;0.68;0.682;0.684;0.686;0.688;0.69;0.692;0.694;0.696;0.698;0.7;0.702;0.704;0.706;0.708;0.71;0.712;0.714;0.716;0.718;0.72;0.722;0.724;0.726;0.728;0.73;0.732;0.734;0.736;0.738;0.74;0.742;0.744;0.746;0.748;0.75;0.752;0.754;0.756;0.758;0.76;0.762;0.764;0.766;0.768;0.77;0.772;0.774;0.776;0.778;0.78;0.782;0.784;0.786;0.788;0.79;0.792;0.794;0.796;0.798;0.8;0.802;0.804;0.806;0.808;0.81;0.812;0.814;0.816;0.818;0.82;0.822;0.824;0.826;0.828;0.83;0.832;0.834;0.836;0.838;0.84;0.842;0.844;0.846;0.848;0.85;0.852;0.854;0.856;0.858;0.86;0.862;0.864;0.866;0.868;0.87;0.872;0.874;0.876;0.878;0.88;0.882;0.884;0.886;0.888;0.89;0.892;0.894;0.896;0.898;0.9;0.902;0.904;0.906;0.908;0.91;0.912;0.914;0.916;0.918;0.92;0.922;0.924;0.926;0.928;0.93;0.932;0.934;0.936;0.938;0.94;0.942;0.944;0.946;0.948;0.95;0.952;0.954;0.956;0.958;0.96;0.962;0.964;0.966;0.968;0.97;0.972;0.974;0.976;0.978;0.98;0.982;0.984;0.986;0.988;0.99;0.992;0.994;0.996;0.998;1" fill="freeze"></animate>
    <animate href="#17423_fill_path" attributeName="d" values="M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0h8.6834v20.53003h-8.6834z;M0,0h18.5966v20.53003h-18.5966z;M0,0h30.03139v20.53003h-30.03139z;M0,0h43.39459v20.53003h-43.39459z;M0,0h59.26499v20.53003h-59.26499z;M0,0h78.47188v20.53003h-78.47188z;M0,0h102.13078v20.53003h-102.13078z;M0,0h131.25977v20.53003h-131.25977z;M0,0h164.87996v20.53003h-164.87996z;M0,0h197.78155v20.53003h-197.78155z;M0,0h224.87265v20.53003h-224.87265z;M0,0h245.59784v20.53003h-245.59784z;M0,0h261.41494v20.53003h-261.41494z;M0,0h273.70434v20.53003h-273.70434z;M0,0h283.43013v20.53003h-283.43013z;M0,0h291.23703v20.53003h-291.23703z;M0,0h297.56323v20.53003h-297.56323z;M0,0h302.71573v20.53003h-302.71573z;M0,0h306.91613v20.53003h-306.91613z;M0,0h310.32893v20.53003h-310.32893z;M0,0h313.07953v20.53003h-313.07953z;M0,0h315.26513v20.53003h-315.26513z;M0,0h316.96313v20.53003h-316.96313z;M0,0h318.23573v20.53003h-318.23573z;M0,0h319.13373v20.53003h-319.13373z;M0,0h319.69923v20.53003h-319.69923z;M0,0h319.96763v20.53003h-319.96763z;M0,0h319.42583v20.53003h-319.42583z;M0,0h318.26113v20.53003h-318.26113z;M0,0h317.07403v20.53003h-317.07403z;M0,0h315.86363v20.53003h-315.86363z;M0,0h314.62933v20.53003h-314.62933z;M0,0h313.37033v20.53003h-313.37033z;M0,0h312.08573v20.53003h-312.08573z;M0,0h310.77463v20.53003h-310.77463z;M0,0h309.43613v20.53003h-309.43613z;M0,0h308.06923v20.53003h-308.06923z;M0,0h306.67293v20.53003h-306.67293z;M0,0h305.24613v20.53003h-305.24613z;M0,0h303.78763v20.53003h-303.78763z;M0,0h302.29623v20.53003h-302.29623z;M0,0h300.77053v20.53003h-300.77053z;M0,0h299.20933v20.53003h-299.20933z;M0,0h297.61093v20.53003h-297.61093z;M0,0h295.97393v20.53003h-295.97393z;M0,0h294.29653v20.53003h-294.29653z;M0,0h292.57713v20.53003h-292.57713z;M0,0h290.81353v20.53003h-290.81353z;M0,0h289.00393v20.53003h-289.00393z;M0,0h287.14603v20.53003h-287.14603z;M0,0h285.23753v20.53003h-285.23753z;M0,0h283.27583v20.53003h-283.27583z;M0,0h281.25823v20.53003h-281.25823z;M0,0h279.18174v20.53003h-279.18174z;M0,0h277.04334v20.53003h-277.04334z;M0,0h274.83944v20.53003h-274.83944z;M0,0h272.56644v20.53003h-272.56644z;M0,0h270.22014v20.53003h-270.22014z;M0,0h267.79634v20.53003h-267.79634z;M0,0h265.29014v20.53003h-265.29014z;M0,0h262.69644v20.53003h-262.69644z;M0,0h260.00944v20.53003h-260.00944z;M0,0h257.22304v20.53003h-257.22304z;M0,0h254.33044v20.53003h-254.33044z;M0,0h251.32434v20.53003h-251.32434z;M0,0h248.19634v20.53003h-248.19634z;M0,0h244.93794v20.53003h-244.93794z;M0,0h241.53924v20.53003h-241.53924z;M0,0h237.98984v20.53003h-237.98984z;M0,0h234.27845v20.53003h-234.27845z;M0,0h230.39275v20.53003h-230.39275z;M0,0h226.32005v20.53003h-226.32005z;M0,0h222.04675v20.53003h-222.04675z;M0,0h217.55945v20.53003h-217.55945z;M0,0h212.84485v20.53003h-212.84485z;M0,0h207.89135v20.53003h-207.89135z;M0,0h202.68995v20.53003h-202.68995z;M0,0h197.23615v20.53003h-197.23615z;M0,0h191.53296v20.53003h-191.53296z;M0,0h185.59286v20.53003h-185.59286z;M0,0h179.44126v20.53003h-179.44126z;M0,0h173.11866v20.53003h-173.11866z;M0,0h166.68016v20.53003h-166.68016z;M0,0h160.19356v20.53003h-160.19356z;M0,0h153.73376v20.53003h-153.73376z;M0,0h147.37487v20.53003h-147.37487z;M0,0h141.18327v20.53003h-141.18327z;M0,0h135.21177v20.53003h-135.21177z;M0,0h129.49697v20.53003h-129.49697z;M0,0h124.06047v20.53003h-124.06047z;M0,0h118.90997v20.53003h-118.90997z;M0,0h114.04377v20.53003h-114.04377z;M0,0h109.45277v20.53003h-109.45277z;M0,0h105.12378v20.53003h-105.12378z;M0,0h101.04118v20.53003h-101.04118z;M0,0h97.18848v20.53003h-97.18848z;M0,0h93.54908v20.53003h-93.54908z;M0,0h90.10728v20.53003h-90.10728z;M0,0h86.84798v20.53003h-86.84798z;M0,0h83.75718v20.53003h-83.75718z;M0,0h80.82208v20.53003h-80.82208z;M0,0h78.03098v20.53003h-78.03098z;M0,0h75.37318v20.53003h-75.37318z;M0,0h72.83898v20.53003h-72.83898z;M0,0h70.41958v20.53003h-70.41958z;M0,0h68.10688v20.53003h-68.10688z;M0,0h65.89358v20.53003h-65.89358z;M0,0h63.77329v20.53003h-63.77329z;M0,0h61.73979v20.53003h-61.73979z;M0,0h59.78769v20.53003h-59.78769z;M0,0h57.91189v20.53003h-57.91189z;M0,0h56.10789v20.53003h-56.10789z;M0,0h54.37139v20.53003h-54.37139z;M0,0h52.69869v20.53003h-52.69869z;M0,0h51.08619v20.53003h-51.08619z;M0,0h49.53059v20.53003h-49.53059z;M0,0h48.02899v20.53003h-48.02899z;M0,0h46.57849v20.53003h-46.57849z;M0,0h45.17649v20.53003h-45.17649z;M0,0h43.82079v20.53003h-43.82079z;M0,0h42.50899v20.53003h-42.50899z;M0,0h41.23909v20.53003h-41.23909z;M0,0h40.00919v20.53003h-40.00919z;M0,0h38.81749v20.53003h-38.81749z;M0,0h37.66229v20.53003h-37.66229z;M0,0h36.54209v20.53003h-36.54209z;M0,0h35.45539v20.53003h-35.45539z;M0,0h34.40079v20.53003h-34.40079z;M0,0h33.37699v20.53003h-33.37699z;M0,0h32.38289v20.53003h-32.38289z;M0,0h31.41719v20.53003h-31.41719z;M0,0h30.47889v20.53003h-30.47889z;M0,0h29.56689v20.53003h-29.56689z;M0,0h28.68039v20.53003h-28.68039z;M0,0h27.81839v20.53003h-27.81839z;M0,0h26.97999v20.53003h-26.97999z;M0,0h26.16439v20.53003h-26.16439z;M0,0h25.37089v20.53003h-25.37089z;M0,0h24.59869v20.53003h-24.59869z;M0,0h23.84709v20.53003h-23.84709z;M0,0h23.11539v20.53003h-23.11539z;M0,0h22.40309v20.53003h-22.40309z;M0,0h21.70959v20.53003h-21.70959z;M0,0h21.0341v20.53003h-21.0341z;M0,0h20.3763v20.53003h-20.3763z;M0,0h19.7356v20.53003h-19.7356z;M0,0h19.1114v20.53003h-19.1114z;M0,0h18.5034v20.53003h-18.5034z;M0,0h17.911v20.53003h-17.911z;M0,0h17.3338v20.53003h-17.3338z;M0,0h16.7715v20.53003h-16.7715z;M0,0h16.2235v20.53003h-16.2235z;M0,0h15.6895v20.53003h-15.6895z;M0,0h15.1692v20.53003h-15.1692z;M0,0h14.6622v20.53003h-14.6622z;M0,0h14.1682v20.53003h-14.1682z;M0,0h13.6868v20.53003h-13.6868z;M0,0h13.2177v20.53003h-13.2177z;M0,0h12.7606v20.53003h-12.7606z;M0,0h12.3152v20.53003h-12.3152z;M0,0h11.8813v20.53003h-11.8813z;M0,0h11.4586v20.53003h-11.4586z;M0,0h11.0467v20.53003h-11.0467z;M0,0h10.6456v20.53003h-10.6456z;M0,0h10.2548v20.53003h-10.2548z;M0,0h9.8742v20.53003h-9.8742z;M0,0h9.5036v20.53003h-9.5036z;M0,0h9.1428v20.53003h-9.1428z;M0,0h8.7914v20.53003h-8.7914z;M0,0h8.4494v20.53003h-8.4494z;M0,0h8.1165v20.53003h-8.1165z;M0,0h7.7926v20.53003h-7.7926z;M0,0h7.4774v20.53003h-7.4774z;M0,0h7.1708v20.53003h-7.1708z;M0,0h6.8726v20.53003h-6.8726z;M0,0h6.5827v20.53003h-6.5827z;M0,0h6.3008v20.53003h-6.3008z;M0,0h6.0268v20.53003h-6.0268z;M0,0h5.7606v20.53003h-5.7606z;M0,0h5.502v20.53003h-5.502z;M0,0h5.2508v20.53003h-5.2508z;M0,0h5.007v20.53003h-5.007z;M0,0h4.7704v20.53003h-4.7704z;M0,0h4.5409v20.53003h-4.5409z;M0,0h4.3183v20.53003h-4.3183z;M0,0h4.1024v20.53003h-4.1024z;M0,0h3.8933v20.53003h-3.8933z;M0,0h3.6908v20.53003h-3.6908z;M0,0h3.4947v20.53003h-3.4947z;M0,0h3.3049v20.53003h-3.3049z;M0,0h3.1214v20.53003h-3.1214z;M0,0h2.944v20.53003h-2.944z;M0,0h2.7726v20.53003h-2.7726z;M0,0h2.6072v20.53003h-2.6072z;M0,0h2.4476v20.53003h-2.4476z;M0,0h2.2937v20.53003h-2.2937z;M0,0h2.1454v20.53003h-2.1454z;M0,0h2.0027v20.53003h-2.0027z;M0,0h1.8655v20.53003h-1.8655z;M0,0h1.7336v20.53003h-1.7336z;M0,0h1.607v20.53003h-1.607z;M0,0h1.4856v20.53003h-1.4856z;M0,0h1.3694v20.53003h-1.3694z;M0,0h1.2582v20.53003h-1.2582z;M0,0h1.1519v20.53003h-1.1519z;M0,0h1.0506v20.53003h-1.0506z;M0,0h0.9541v20.53003h-0.9541z;M0,0h0.8624v20.53003h-0.8624z;M0,0h0.7753v20.53003h-0.7753z;M0,0h0.6928v20.53003h-0.6928z;M0,0h0.6149v20.53003h-0.6149z;M0,0h0.5415v20.53003h-0.5415z;M0,0h0.4725v20.53003h-0.4725z;M0,0h0.4079v20.53003h-0.4079z;M0,0h0.3475v20.53003h-0.3475z;M0,0h0.2914v20.53003h-0.2914z;M0,0h0.2395v20.53003h-0.2395z;M0,0h0.1917v20.53003h-0.1917z;M0,0h0.1479v20.53003h-0.1479z;M0,0h0.1082v20.53003h-0.1082z;M0,0h0.0724v20.53003h-0.0724z;M0,0h0.0406v20.53003h-0.0406z;M0,0h0.0126v20.53003h-0.0126z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z;M0,0v0v20.53003v0z" dur="10s" repeatCount="indefinite" keyTimes="0;0.002;0.004;0.006;0.008;0.01;0.012;0.014;0.016;0.018;0.02;0.022;0.024;0.026;0.028;0.03;0.032;0.034;0.036;0.038;0.04;0.042;0.044;0.046;0.048;0.05;0.052;0.054;0.056;0.058;0.06;0.062;0.064;0.066;0.068;0.07;0.072;0.074;0.076;0.078;0.08;0.082;0.084;0.086;0.088;0.09;0.092;0.094;0.096;0.098;0.1;0.102;0.104;0.106;0.108;0.11;0.112;0.114;0.116;0.118;0.12;0.122;0.124;0.126;0.128;0.13;0.132;0.134;0.136;0.138;0.14;0.142;0.144;0.146;0.148;0.15;0.152;0.154;0.156;0.158;0.16;0.162;0.164;0.166;0.168;0.17;0.172;0.174;0.176;0.178;0.18;0.182;0.184;0.186;0.188;0.19;0.192;0.194;0.196;0.198;0.2;0.202;0.204;0.206;0.208;0.21;0.212;0.214;0.216;0.218;0.22;0.222;0.224;0.226;0.228;0.23;0.232;0.234;0.236;0.238;0.24;0.242;0.244;0.246;0.248;0.25;0.252;0.254;0.256;0.258;0.26;0.262;0.264;0.266;0.268;0.27;0.272;0.274;0.276;0.278;0.28;0.282;0.284;0.286;0.288;0.29;0.292;0.294;0.296;0.298;0.3;0.302;0.304;0.306;0.308;0.31;0.312;0.314;0.316;0.318;0.32;0.322;0.324;0.326;0.328;0.33;0.332;0.334;0.336;0.338;0.34;0.342;0.344;0.346;0.348;0.35;0.352;0.354;0.356;0.358;0.36;0.362;0.364;0.366;0.368;0.37;0.372;0.374;0.376;0.378;0.38;0.382;0.384;0.386;0.388;0.39;0.392;0.394;0.396;0.398;0.4;0.402;0.404;0.406;0.408;0.41;0.412;0.414;0.416;0.418;0.42;0.422;0.424;0.426;0.428;0.43;0.432;0.434;0.436;0.438;0.44;0.442;0.444;0.446;0.448;0.45;0.452;0.454;0.456;0.458;0.46;0.462;0.464;0.466;0.468;0.47;0.472;0.474;0.476;0.478;0.48;0.482;0.484;0.486;0.488;0.49;0.492;0.494;0.496;0.498;0.5;0.502;0.504;0.506;0.508;0.51;0.512;0.514;0.516;0.518;0.52;0.522;0.524;0.526;0.528;0.53;0.532;0.534;0.536;0.538;0.54;0.542;0.544;0.546;0.548;0.55;0.552;0.554;0.556;0.558;0.56;0.562;0.564;0.566;0.568;0.57;0.572;0.574;0.576;0.578;0.58;0.582;0.584;0.586;0.588;0.59;0.592;0.594;0.596;0.598;0.6;0.602;0.604;0.606;0.608;0.61;0.612;0.614;0.616;0.618;0.62;0.622;0.624;0.626;0.628;0.63;0.632;0.634;0.636;0.638;0.64;0.642;0.644;0.646;0.648;0.65;0.652;0.654;0.656;0.658;0.66;0.662;0.664;0.666;0.668;0.67;0.672;0.674;0.676;0.678;0.68;0.682;0.684;0.686;0.688;0.69;0.692;0.694;0.696;0.698;0.7;0.702;0.704;0.706;0.708;0.71;0.712;0.714;0.716;0.718;0.72;0.722;0.724;0.726;0.728;0.73;0.732;0.734;0.736;0.738;0.74;0.742;0.744;0.746;0.748;0.75;0.752;0.754;0.756;0.758;0.76;0.762;0.764;0.766;0.768;0.77;0.772;0.774;0.776;0.778;0.78;0.782;0.784;0.786;0.788;0.79;0.792;0.794;0.796;0.798;0.8;0.802;0.804;0.806;0.808;0.81;0.812;0.814;0.816;0.818;0.82;0.822;0.824;0.826;0.828;0.83;0.832;0.834;0.836;0.838;0.84;0.842;0.844;0.846;0.848;0.85;0.852;0.854;0.856;0.858;0.86;0.862;0.864;0.866;0.868;0.87;0.872;0.874;0.876;0.878;0.88;0.882;0.884;0.886;0.888;0.89;0.892;0.894;0.896;0.898;0.9;0.902;0.904;0.906;0.908;0.91;0.912;0.914;0.916;0.918;0.92;0.922;0.924;0.926;0.928;0.93;0.932;0.934;0.936;0.938;0.94;0.942;0.944;0.946;0.948;0.95;0.952;0.954;0.956;0.958;0.96;0.962;0.964;0.966;0.968;0.97;0.972;0.974;0.976;0.978;0.98;0.982;0.984;0.986;0.988;0.99;0.992;0.994;0.996;0.998;1" fill="freeze"></animate>
    <animateTransform href="#17436" attributeName="transform" type="translate" values="0 0;77 -0.002999999999985903;0 -0.002999999999985903" dur="10s" repeatCount="indefinite" calcMode="spline" keyTimes="0;0.5;1" keySplines="0.5 0.35 0.15 1;0.5 0.35 0.15 1" additive="sum" fill="freeze"></animateTransform>
</svg>