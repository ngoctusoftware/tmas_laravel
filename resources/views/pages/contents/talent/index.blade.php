@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, công nghệ, tuyển dụng, sửa chữa, nội thất, lắp đặt">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/tuyen-dung">
<meta property="og:description" content="TMAS tuyển dụng">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/banner_child_page/tin-tuc.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, công nghệ, tuyển dụng, sửa chữa, nội thất, lắp đặt">
@endpush
@section('title','Tuyển Dụng')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/talent.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image_talent">
        <div class="title_content">
            <h1 class="title text_white text-center">Tuyển dụng</h1>
            <p class="text_white text-center">TMAS tuyển dụng</p>
        </div>
    </div>
</section>
@include('pages.layouts.breadcrum')
<section class="section_talent section_container">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 desktop">
                @include('pages.contents.dynamicbg.recuit')
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="row box_talent">
                    @foreach($listPost as $index=>$item)
                    <div class="col-12 card_talent">
                        <a href="/tuyen-dung/{{$item->id}}/{{$item->slug}}">
                            <div class="width_100 d-flex">
                                <div class="image_talent">
                                    <img class="{{$index == 0 ? 'image_news_first' : 'image_news'}}" src="/{{$item->featured_image}}" alt="{{$item->slug ? $item->slug : $item->name}}">
                                </div>
                                <div class="content_talent">
                                    <h3 class="block_spacing">
                                        {{$item->name}}
                                    </h3>
                                    <p class="content_talent_description block_spacing">
                                        {{$item->description}}
                                    </p>
                                    <p class="time_left_talent block_spacing">
                                        Ngày hết hạn: {{date("d/m/Y",strtotime($item->published_at))}}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center pagination_box">
                    {{ $listPost->links() }}
                </div>
            </div>
        </div>
    </div>
</section>

@endsection