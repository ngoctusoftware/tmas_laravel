@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="{{$dataDetail->meta_description}}">
<meta property="og:title" content="{{$dataDetail->meta_title}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{$dataDetail->meta_og_url}}">
<meta property="og:description" content="{{$dataDetail->name}}">
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/{{$dataDetail->featured_image}}">
<meta name="keywords" content="{{$dataDetail->meta_keywords}}">
@endpush
@section('title','Chi tiết tuyển dụng')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/talent.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_talent">
        <div class="title_content">
            <h1 class="title text_white text-center">Tuyển dụng</h1>
            <p class="text_white text-center">TMAS tuyển dụng</p>
        </div>
    </div>
</section>
@include('pages.layouts.breadcrum')
<section class="section_talent_detail section_container">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-12 detail_component_new">
                <div class="detail_new_box_content">
                    <h2 class="title title_new text_logo space_md">{{$dataDetail->name}}</h2>
                    <div class="body_content_detail space_md">
                        {!! $dataDetail->content !!}
                    </div>
                    <p>Ngày hết hạn: {{date("d/m/Y",strtotime($dataDetail->published_at))}}</p>
                </div>
            </div>
            <div class="col-lg-6 desktop">
                <img class="w-100" src="assets/img/banner_child_page/bg_detail_talent.svg" alt="tmas tuyen dung">
            </div>
        </div>
    </div>
</section>
@include('pages.contents.container.newrelated')

@endsection