@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="TMAS, tmas việt nam">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/gioi-thieu">
<meta property="og:description" content="TMAS Việt Nam">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam">
@endpush
@section('title','Giới thiệu')
@push ('after-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.css">
<link rel="stylesheet" href="assets/styles/intro.css">
<link rel="stylesheet" href="assets/styles/scroll_full_page.css">
@endpush
@push ('after-scripts')
<script src="assets/lib/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/vendors/jquery.easings.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/vendors/scrolloverflow.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.min.js"></script>
<script src="assets/js/intro.js"></script>
@endpush

@section('content')
<section class="section section_intro section_slogan">
    <div class="slide" data-anchor="slogan">
        <div class="box_view_image">
            <img src="assets\img\homepage\banner/banner_slogan.svg" alt="slogan">
        </div>
    </div>
    <div class="slide" id="thu-ngo" data-anchor="thu-ngo">
        <div>
            <div class="title_content">
                <h2 class="space_lg">Thư ngỏ</h2>
            </div>
            <div class="mess_invite space_md">
                <p>Công ty TNHH Thương Mại Và Xuất Nhập Khẩu
                    Thiên Minh (TMAS Co.,Ltd) xin gửi tới Quý đối
                    tác lời chào trân trọng và hợp tác thành công</p>
                <p>Chúng tôi cũng xin gửi lời cảm ơn sâu sắc tới
                    Quý đối tác đã tin tưởng và đồng hành cùng
                    chúng tôi trong thời gian qua. Sự ủng hộ đó
                    chính là động lực lớn lao để Công ty TNHH
                    TNHH Thương Mại Và Xuất Nhập Khẩu Thiên
                    Minh tiếp tục nỗ lực và khẳng định vị thế của
                    mình trên thị trường</p>
                <p>Ngày 19/09/2018, Công ty TNHH Thương mại
                    và XNK Thiên Minh được thành lập và được
                    khách hàng biết đến với thương hiệu TMAS
                    Việt Nam. Trải qua gần 5 năm hình thành và
                    phát triển, hiện tại TMAS Việt Nam đã trở
                    thành một trong những Hãng công nghệ cung
                    cấp các giải pháp hỗ trợ lái xe an toàn, thông
                    minh, tiện ích hàng đầu Việt Nam.</p>
                <p>Với mục tiêu phát triển doanh nghiệp “Chuyên
                    nghiệp – Hiệu quả - Phát triển bền vững”,
                    trong đó yếu tố cốt lõi là nhân tố con người và
                    phương thức kết nối các lợi thế cạnh tranh
                    giữa công ty và quý đối tác, chúng tôi cam kết
                    mang tới khách hàng những giá pháp ưu việt
                    nhằm hiện thực hóa các phương án đầu tư với
                    hiệu quả cao nhất.</p>
                <p>Kính chúc Quý đối tác phát triển, thịnh vượng
                    và mong có cơ hội hợp tác thành công trong
                    thời gian tới.</p>
                <p>Một lần nữa, xin chân thành cảm ơn quý đối
                    tác đã tin tưởng và lựa chọn sản phẩm của
                    TMAS Việt Nam. Chúng tôi hy vọng sẽ có cơ
                    hội được phục vụ Quý Khách hàng và Quý Đối
                    Tác nhiều hơn trong thời gian tới.</p>
                <p>Trân trọng!</p>
            </div>
            <div class="quote">
                <p>"Một doanh nghiệp công nghệ lớn không phải là một doanh nghiệp có thể kiếm được
                    hàng triệu đô mà phải là một doanh nghiệp mang giá trị cho hàng triệu người"</p>
            </div>
        </div>
    </div>
    <div class="slide" id="info" data-anchor="thong-tin-cong-ty">
        <div class="content_info">
            <div class="title_content space_lg">
                <h2 class="space_md">Thông tin công ty</h2>
                <h3 class="">CÔNG TY TNHH THƯƠNG MẠI VÀ XUẤT NHẬP KHẨU THIÊN MINH được thành
                    lập theo giấy đăng ký kinh doanh số 0102312978 do Sở kế hoạch và Đầu tư
                    thành phố Hà Nội cấp vào ngày 19 tháng 9 năm 2018</h3>
            </div>
            <div class="info">
                <ul>
                    <li>
                        <p>Tên công ty: CÔNG TY TNHH THƯƠNG MẠI VÀ XUẤT NHẬP KHẨU THIÊN MINH</p>
                    </li>
                    <li>
                        <p>Tên quốc tế: THIEN MINH TRADING AND IMPORT - EXPORT COMPANY LIMITED</p>
                    </li>
                    <li>
                        <p>Tên viết tắt: THIEN MINH TRADE CO.,LTD</p>
                    </li>
                    <li>
                        <p>Trụ sở chính: Số 88 Khúc Thừa Dụ, Dịch Vọng, Cầu Giấy, Hà Nội</p>
                    </li>
                    <li>
                        <p>Mã số thuế: 0102312978</p>
                    </li>
                    <li>
                        <p>Đại diện pháp luật: Ông Trần Thế Minh - Chức danh: Tổng Giám đốc</p>
                    </li>
                    <li>
                        <p>Hotline: 032 790 9399 - 036 310 9558</p>
                    </li>
                    <li>
                        <p>Website: www.tmas.vn</p>
                    </li>
                    <li>
                        <p>Email: marketing@thien-minh.com</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="section section_intro section_mission">
    <div class="flex_box">
        <div class="card_mission" id="vision">
            <div class="icon_card">
                <img src="assets\img\intro\UI\tam-nhin.svg" alt="tầm nhìn">
            </div>
            <p class="title_card">Tầm nhìn</p>
            <p class="des_card">Trở thành công ty cung cấp các giải pháp công nghệ hỗ trợ lái xe an toàn, thông minh và tiện ích số 1 Việt Nam và khu vực.</p>
        </div>
        <div class="card_mission" id="mission">
            <div class="icon_card">
                <img src="assets\img\intro\UI\su-menh.svg" alt="sứ mệnh">
            </div>
            <p class="title_card">Sứ mệnh</p>
            <p class="des_card">TMAS tập trung vào nghiên cứu sản xuất và phát triển các công nghệ mới để giúp xe hơi vận hành một cách thông minh và an toàn hơn.</p>
            <p class="des_card">Với tư duy của người tiên phong, luôn hướng về mục tiêu, TMAS
                cam kết luôn giữ gìn và phát huy giá trị cốt lõi của doanh nghiệp,
                giúp đất nước ngày càng phát triển, người tiêu dùng tiếp xúc với
                công nghệ tiên tiến và mang đến những giá trị tốt đẹp nhất!</p>
        </div>
        <div class="card_mission" id="value">
            <div class="icon_card">
                <img src="assets\img\intro\UI\gia-tri-cot-loi.svg" alt="giá trị cốt lõi">
            </div>
            <p class="title_card">Giá trị cốt lõi</p>
            <p class="des_card">Một doanh nghiệp công nghệ lớn không phải là một doanh nghiệp có thể kiếm được hàng triệu đô mà phải là một doanh nghiệp mang giá trị cho hàng triệu người.</p>
        </div>
    </div>
</section>

<section class="section section_intro section_number">
    <div class="flex_box">
        <div class="card_mission" id="dai-ly">
            <div class="icon_card">
                <img src="assets/img/icon_aboutus/dai-ly.svg" alt="Đại lý">
            </div>
            <p class="title_card_bold">700+</p>
            <p class="des_card">Đại lý</p>
        </div>
        <div class="card_mission" id="tinh-thanh">
            <div class="icon_card">
                <img src="assets/img/icon_aboutus/tinh-thanh.svg" alt="tỉnh thành">
            </div>
            <p class="title_card_bold">63</p>
            <p class="des_card">Tỉnh thành trên cả nước
            </p>
        </div>
        <div class="card_mission" id="khach-hang">
            <div class="icon_card">
                <img src="assets/img/icon_aboutus/khach-hang.svg" alt="Khách hàng">
            </div>
            <p class="title_card_bold">1.500.000</p>
            <p class="des_card">Khách hàng tin dùng</p>
        </div>
    </div>
</section>
<section class="section section_intro section_achievement">
    <div class="box_text">
        <p class="text-center">GIẢI THƯỞNG SMART CITY</p>
        <p class="text-center">GIẢI PHÁP GIAO THÔNG THÔNG MINH</p>
    </div>
    <div class="box_image_cup">
        <img src="assets\img\intro\CUP.png" alt="mission, vision, value">
    </div>
</section>
<section class="section section_intro section_industry">
    <div class="container">
        <div class="row">
            <div class="box_content col-lg-6 col-12">
                <div class="title_content">
                    <h2 class="space_lg">Lĩnh vực hoạt động</h2>
                    <p class="space_sm">TMAS là thương hiệu tiên phong tại Việt Nam trong
                        lĩnh vực sản xuất, nghiên cứu phát triển và cung
                        cấp các sản phẩm công nghệ hỗ trợ lái xe an
                        toàn, thông minh, tiện ích cho ô tô.</p>
                    <p class="space_sm">Sau gần 5 năm hình thành và phát triển, đến
                        nay TMAS hoạt động với trụ sở chính tại Hà
                        Nội và văn phòng đại diện miền Nam tại
                        Tp.Hồ Chí Minh cùng mạng lưới phát triển
                        kênh phân phối toàn quốc, tại khắp 63
                        tỉnh, thành phố với hơn 700 đối tác là
                        các Đại lý xe mới, Trung tâm lắp đặt
                        phụ kiện chính Hãng trên toàn quốc.</p>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <img class="w-100" src="assets\img\intro\1.png" alt="industry">
            </div>
        </div>
    </div>
</section>
<section class="section section_intro section_history">
    <div class="slide" id="hanh-trinh" data-anchor="hanh-trinh">
        <div class="container hanh_trinh">
            <h2>Dấu ấn trong hành trình của chúng tôi</h2>
        </div>
    </div>
    <div class="slide" id="hanh-trinh" data-anchor="2018">
        <div class="container hanh_trinh">
            <div class="box_year">
                <h3>2018</h3>
                <p>Tháng 09/2018: Công ty TNHH
                    Thương Mại Và Xuất Nhập Khẩu Thiên
                    Minh được thành lập và đi vào hoạt
                    động với trụ sở chính tại Hà Nội</p>
                <p>Khai trương văn phòng đại diện miền
                    Nam tại Tp. Hồ Chí Minh</p>
            </div>
        </div>
    </div>
    <div class="slide" id="hanh-trinh" data-anchor="2018">
        <div class="container hanh_trinh">
            <div class="box_year">
                <h3>2019</h3>
                <p>Trở thành nhà cung cấp độc quyền sản phẩm Cảnh báo điểm mù BSM 01 và
                    BSM-01(M) thương hiệu XFD</p>
                <p>Công ty TNHH Thương Mại Và Xuất Nhập Khẩu Thiên Minh & Mobileye An Intel
                    Company trở thành đối tác chiến lược toàn diện và cung cấp độc quyền Hệ thống
                    cảnh báo va chạm sớm Mobileye630</p>
                <p>Đặt nền móng hợp tác cung cấp và lắp đặt các sản phẩm của tới các Hãng, Đại lý xe
                    mới trên toàn quốc: Lexus; Toyota; Hyundai; Ford; Mitsubishi; VinFast; Mercedes…</p>
                <p>Hợp tác cung cấp và lắp đặt các sản phẩm với với Công Ty Cổ Phần Ô Tô Thành An
                    sở hữu chuỗi 10 Đại lý xe Hyundai.</p>
                <p>Hợp tác cung cấp và lắp đặt các sản phẩm với Công Ty Cổ Phần Dịch Vụ Tổng Hợp
                    Sài Gòn (SAVICO) sở hữu chuỗi hơn 80 Đại lý xe Toyota, Ford, Mitsubishi</p>
                <p>Hợp tác cùng hệ thống chuỗi nâng cấp xe Auto365 Việt Nam với hơn 70 Showroom
                    trên toàn quốc</p>
                <p>Ra mắt sản phẩm Camera 360 2D và 3D ghi hình toàn cảnh xe ô tô</p>
            </div>
        </div>
    </div>
    <div class="slide" id="hanh-trinh" data-anchor="2020">
        <div class="container hanh_trinh">
            <div class="box_year">
                <h3>2020</h3>
                <p>Trở thành nhà cung cấp độc quyền sản phẩm Camera 360
                    ghi hình toàn cảnh xe ô tô thương hiệu KEAKKY với dòng sản
                    phẩm Camera 360 Safeview LD900H và Safeview LD980H.</p>
                <p>Trở thành nhà cung cấp độc quyền sản phẩm Cửa Hít Tự
                    Động và Cốp Điện Thông Minh thương hiệu Dr. Door.</p>
                <p>Ra mắt sản phẩm Đề Nổ Từ Xa Digital CarKey, Cảm Biến Áp
                    Suất Lốp SAFETIRE</p>
                <p>Trở thành nhà cung cấp độc quyền sản phẩm Cốp Điện
                    Thông Minh, Cảm Biến Đá Cốp Đa Điểm, Bậc Dẫm Điện
                    Thông Minh thương hiệu AOD</p>
            </div>
        </div>
    </div>
    <div class="slide" id="hanh-trinh" data-anchor="2021">
        <div class="container hanh_trinh">
            <div class="box_year">
                <h3>2021</h3>
                <p>Ra mắt sản phẩm Màn hình Android / Màn
                    hình Android liền Camera 360 Safeview.</p>
                <p>TMAS hợp tác cùng Kiki Zalo phát triển
                    công nghệ AI – ra lệnh giọng nói điều khiển
                    tích hợp trên các sản phẩm Android.</p>
                <p>TMAS hợp tác cùng công ty Công ty Cổ
                    Phần Ứng Dụng Bản Đồ Việt (Vietmap).</p>
                <p>Ra mắt sản phẩm Carplay Android Box thế
                    hệ đầu tiên – SMARTVIEW.</p>
            </div>
        </div>
    </div>
    <div class="slide" id="hanh-trinh" data-anchor="2022">
        <div class="container hanh_trinh">
            <div class="box_year">
                <h3>2022</h3>
                <p>Ra mắt Carplay Android Box thế hệ tiếp theo – SAFEVIEW 6125</p>
                <p>Ra mắt sản phẩm Màn hình Android Safeview S Premium 12’3Inch</p>
                <p>Ra mắt giao diện Android độc quyền SAFEVIEW SIGNATURE</p>
                <p>Ra mắt phiên bản Camera 360 Safeview thế hệ mới S SERIES – SAFEVIEW S500 </p>
                <p>Ra mắt Carplay Android Box thế hệ mới – SAFEVIEW SA-8</p>
            </div>
        </div>
    </div>
    <div class="slide" id="hanh-trinh" data-anchor="2023">
        <div class="container hanh_trinh">
            <div class="box_year">
                <h3>2023</h3>
                <p>Hiện tại 2023 TMAS có hơn 700 đối tác
                    là các Đại lý xe mới, Trung tâm lắp đặt
                    phụ kiện chính hãng trên toàn quốc.</p>
            </div>
        </div>
    </div>
</section>
<section class="section section_intro section_about_us">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="title_content">
                    <h2 class="space_lg">Đội ngũ & con người TMAS</h2>
                    <p class="space_sm">TMAS coi nguồn nhân lực là tài sản quý giá nhất, là chìa khóa cho sự thành công và phát triển trong tương lai của Công ty</p>
                    <p class="space_sm">Hiện tại, TMAS có gần 100 nhân sự cùng đầy đủ các phòng ban chuyên trách đang làm việc tại 2 văn phòng chính ở thành phố Hồ Chí Minh & Hà Nội, Việt Nam.</p>
                    <p class="space_sm">Nhân lực tại Công ty có kinh nghiệm trong lĩnh vực nội thất ô tô, công nghệ thông tin, truyền thông quảng cáo.</p>
                    <p class="space_sm"> Chúng tôi nỗ lực để đặt sự phát triển con người lên trên các lợi ích kinh doanh.</p>
                    <p class="space_sm"> TMAS rất trân trọng những đóng góp của nhân viên vào sự phát triển chung của công ty, luôn luôn lắng nghe và hỗ trợ để nhân viên có thể phát huy mọi tiềm năng,
                        góp phần đưa TMAS phát triển hơn nữa.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection