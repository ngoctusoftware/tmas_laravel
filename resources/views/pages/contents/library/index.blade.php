@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/huong-dan-dai-ly">
<meta property="og:description" content="Hướng dẫn các ứng dụng phát triển bởi TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn, thư viện hình nền tmas">
@endpush
@section('title','Hướng dẫn đại lý')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/library.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_library">
    </div>
</section>
@include('pages.layouts.breadcrum')

<section class="section_list_new section_container">
    <div class="container">
        <div class="row title_content space_md">
            <h1 class="text_logo text-center">Thư viện hình nền Launcher</h1>
        </div>
        <div class="row">
            <div class="col-12">
                <h2>12 Con giáp</h2>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/chuot.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/trau.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/ho.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/rong.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/ran.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/ngua.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/de.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/khi.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/cho.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/quy-dau.jpg" alt="hình nền">
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <img class="w-100" src="assets/img/lib/lon.jpg" alt="hình nền">
            </div>
           
        </div>
    </div>
</section>
@include('pages.contents.container.downloadApp')

@endsection