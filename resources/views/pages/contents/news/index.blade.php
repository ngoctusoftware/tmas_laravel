@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/tin-tuc">
<meta property="og:description" content="Tin tức về TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/banner_child_page/tin-tuc.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn, tin tức">
@endpush
@section('title','Tin tức')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/news.css">
@endpush
@push ('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js"></script>
<script src="assets/js/news.js"></script>
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_news center_title_banner">
        <div class="title_content">
            <h1 class="title text_white text-center">tin tức</h1>
            <p class="text_white text-center">Theo dõi TMAS để cập nhật thông tin mới nhất về công nghệ</p>
        </div>
    </div>
</section>
@include('pages.layouts.breadcrum')
<section class="section_list_new section_container">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-12 box_related">
                <div class="row">
                    <div class="group_type_new">
                        <p class="type_new_item active">Sự kiện</p>
                        <p class="type_new_item">Công nghệ</p>
                        <p class="type_new_item">Cẩm nang</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="box_new_filter">
                </div>
                <div class="pagination_post"></div>
            </div>
            <div class="col-lg-3 col-sm-12 box_related">
                <div class="row">
                    @include('pages.contents.container.featureproduct')
                </div>
            </div>
        </div>
    </div>
</section>
@include('pages.contents.container.productrelated')
@endsection