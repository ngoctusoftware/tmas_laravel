@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="{{$dataDetail->meta_description}}">
<meta property="og:title" content="{{$dataDetail->meta_title}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{$dataDetail->meta_og_url}}">
<meta property="og:description" content="{{$dataDetail->name}}">
<meta property="og:image" content="https://{{$_SERVER['SERVER_NAME']}}/{{$dataDetail->featured_image}}">
<meta name="keywords" content="{{$dataDetail->meta_keywords}}">
@endpush
@section('title', $dataDetail->name)
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/news.css">
@endpush
@push ('after-scripts')
<script src="assets/js/news.js"></script>
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image banner_image_news center_title_banner">
        <div class="title_content">
            <h1 class="title text_white text-center">tin tức</h1>
            <p class="text_white text-center">Theo dõi TMAS để cập nhật thông tin mới nhất về công nghệ</p>
        </div>
    </div>
</section>
@include('pages.layouts.breadcrum')
<section class="section_new_detail section_container">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-12">
                @include('pages.contents.container.featureproduct')
            </div>
            <div class="col-lg-6 col-sm-12 detail_component_new">
                <div class="detail_new_box_content">
                    <div class="text-center space_md">
                        <p class="title_link_type  {{$dataDetail->type == 'Sự kiện' ? 'green_text' : 'blue_text'}}">{{$dataDetail->type}}</p>
                    </div>
                    <h2 class="title title_new text_logo space_md  text-center">{{$dataDetail->name}}</h2>
                    @php
                    $date = new DateTime($dataDetail->published_at);
                    @endphp
                    <p class="text_date text-center space_md">{{$date->format('d/m/Y')}}</p>
                    <img src="/{{$dataDetail->featured_image}}" class="space_md" alt="{{$dataDetail->name}}">
                    <p class="description_detail_page">{{$dataDetail->description}}</p>
                    <div class="body_content_detail">
                        {!! $dataDetail->content !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                @include('pages.contents.container.featurenew')
            </div>
        </div>
    </div>
</section>
@include('pages.contents.container.productrelated')

@endsection