@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="tmas liên hệ" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/lien-he">
<meta property="og:description" content="Liên hệ với TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn, liên hệ">
@endpush
@section('title','TMAS Việt Nam')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/contact.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image_contact">
        <div class="title_content">
            <h1 class="title text_white text-center">Liên hệ</h1>
            <p class="text_white text-center">Theo dõi TMAS để cập nhật thông tin mới nhất về công nghệ</p>
        </div>
    </div>
</section>
@include('pages.layouts.breadcrum')
<section class="section_contact section_container">
    @include('pages.contents.contact.components.form')
</section>

@endsection