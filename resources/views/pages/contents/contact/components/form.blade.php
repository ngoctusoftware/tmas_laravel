<div class="container">
    <div class="row">
        <div class="col-md-6 desktop">
            <div class="row w-100">
                <img src="assets/img/homepage/img_contact.svg" class="width_100" alt="contact">
            </div>
        </div>
        <div class="col-lg-6 col-md-12 px-2">
            <div class="title_content space_lg">
                <h2 class="title text_white">CHUYÊN GIA TƯ VẤN</h2>
                <p class="text_white">Bất cứ thắc mắc gì cần giải đáp, hãy để chuyên gia TMAS tư vấn</p>
            </div>
            <div class="contact-wrap w-100 p-md-3 py-4">
                <form method="POST" id="contactForm" name="contactForm" class="contactForm" action="{{route('pages.contact.save')}}">
                    @csrf
                    <div class="row pt-2">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <input type="text" class="form-control" required name="cus_name" id="cus_name" placeholder="Họ và tên">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <input type="text" class="form-control" name="cus_phone" id="cus_phone" required placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                                <input type="text" class="form-control" name="cus_address" id="cus_address" required placeholder="Địa chỉ">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                                <textarea name="content" class="form-control" id="content" cols="30" rows="4" required placeholder="Nội dung"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 text-right ">
                            <div class="form-group  center_mobile">
                                <input type="submit" required value="Gửi liên hệ" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>