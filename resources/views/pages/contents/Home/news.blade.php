<section class="section section_container section_news home_page">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 feature_news">
                <h2 class="title_new_page text_dark space_lg">Tin tức TMAS Việt Nam</h2>
                <div class="card_new">
                    <div class="image_new space_sm">
                        <p class="feature_type">{{$dataNews['dataNewsFeature'][0]->type}}</p>
                        <img class="w-100" src="/{{$dataNews['dataNewsFeature'][0]->featured_image}}" alt="{{$dataNews['dataNewsFeature'][0]->name}}">
                    </div>
                    <div class="content_new">
                        <div class="link_new space_sm">
                            <a href="/tin-tuc/{{$dataNews['dataNewsFeature'][0]->id}}/{{$dataNews['dataNewsFeature'][0]->slug}}">
                                <h2>{{$dataNews['dataNewsFeature'][0]->name}}</h2>
                            </a>
                        </div>
                        <div class="des_new space_sm">
                            <p>{{$dataNews['dataNewsFeature'][0]->description}}</p>
                        </div>
                        <div class="time_publish">
                            @php
                            $date = new DateTime($dataNews['dataNewsFeature'][0]->published_at);
                            @endphp
                            <p class="text-end">{{$date->format('d/m/Y')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 list_new">
                @foreach($dataNews['dataNewsRelated'] as $item)
                <div class="card_new_sm d-flex">
                    <div class="content_new">
                        <div class="d-flex space_bw">
                            <div class="type_new space_sm">
                                <p class="text-start tag_post"><span>{{$item->type}}</span></p>
                            </div>
                            <div class="time_publish space_sm">
                                @php
                                $date = new DateTime($item->published_at);
                                @endphp
                                <p class="text-end">{{$date->format('d/m/Y')}}</p>
                            </div>
                        </div>
                        <div class="link_new space_sm">
                            <a href="/tin-tuc/{{$item->id}}/{{$item->slug}}">
                                <h2>{{$item->name}}</h2>
                            </a>
                        </div>
                        <div class="des_new space_sm">
                            <p>{{$item->description}}</p>
                        </div>

                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</section>