<section class="section_container section_video_product home_page">
    <div class="container">
        <div class="title_content row">
            <div class="col-12">
                <h2 class="title text_logo">videos sản phẩm</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <iframe class="block_spacing" src="https://www.youtube.com/embed/k1G87_dzQeM" title="CỐP ĐIỆN DR.DOOR: ĐẲNG CẤP – HIỆN ĐẠI – SANG TRỌNG  DÀNH RIÊNG CHO XE KIA SPORTAGE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6 col-sm-12">
                <iframe class="block_spacing" src="https://www.youtube.com/embed/LpIvlAObA4M" title="[ NEWS ] Giải mã áp suất lốp SAFETIRE - Option hoàn hảo dành riêng TOYOTA COROLLA CROSS" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6 col-sm-12">
                <iframe class="block_spacing" src="https://www.youtube.com/embed/2Up-ER1le1M" title="Đề nổ từ xa Digital CarKey xe Mazda tiện lợi – nhanh gọn – thông minh" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6 col-sm-12">
                <iframe class="block_spacing" src="https://www.youtube.com/embed/nwYb2BjsBIw" title="Siêu phẩm  CAMERA 360 Safeview LUX Vinfast cao cấp nhất hiện nay" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>