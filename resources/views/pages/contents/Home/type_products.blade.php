<section class="section section_container section_type_product home_page">
    <div class="container">
        <div class="row">
            @foreach($listCategory as $key => $item)
            <div class="{{$key > 3 ? 'col-lg-4' : 'col-lg-6'}} col-12 space_sm">
                <a href="/san-pham/?danh-muc={{$item->slug}}">
                    <div class="card_cat">
                        <p class="title text_white">{{$item->name}}</p>

                    </div>
                </a>
            </div>
            @endforeach

        </div>
        <div class="text_white col-12">
            <div class="title_content text-center space_lg">
            </div>
        </div>
    </div>
</section>