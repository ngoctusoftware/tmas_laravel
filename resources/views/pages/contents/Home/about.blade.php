<section class="section section_container section_about_us home_page">
    <div class="container_slide slide">
        <div class="container">
            <div class="row">
                <div class="title_content text-center">
                    <h2 class="title text_white">Về chúng tôi</h2>
                </div>
            </div>
            <div class="row space_lg">
                <div class="col-lg-4 col-12">
                    <div class="card_about">
                        <div class="card_head">
                            <div class="card_title">
                                <h3 class="number_count space_md">700+</h3>
                                <div class="card_image">
                                    <img class="w-100" src="assets/img/icon_aboutus/dai-ly-white.svg" id="dai-ly" alt="Đại lý">
                                </div>
                            </div>
                            <h3>Đại lý phân phối</h3>
                        </div>
                        <div class="card_des">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card_about">
                        <div class="card_head">
                            <div class="card_title">
                                <h3 class="number_count space_md">63</h3>
                                <div class="card_image">
                                    <img class="w-100" src="assets/img/icon_aboutus/tinh-thanh-white.svg" id="tinh-thanh" alt="Tỉnh thành">
                                </div>
                            </div>
                            <h3>Tỉnh thành trên cả nước</h3>
                        </div>
                        <div class="card_des">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card_about">
                        <div class="card_head">
                            <div class="card_title">
                                <h3 class="number_count space_md">1.5M+</h3>
                                <div class="card_image">
                                    <img class="w-100" src="assets/img/icon_aboutus/khach-hang-white.svg" id="khach-hang" alt="Khách hàng tin tưởng sử dụng">
                                </div>
                            </div>
                            <h3>Khách hàng tin dùng</h3>
                        </div>
                        <div class="card_des">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-12">
                    <div class="card_about">
                        <div class="card_head">
                            <div class="card_title space_md d-flex justify-content-center">
                                <div class="card_image">
                                    <img class="w-100" src="assets/img/icon_aboutus/star-white.svg" id="star" alt="Giải thưởng">
                                </div>
                            </div>
                            <h3 class="text-center">Giải thưởng Smart City Awards</h3>
                        </div>
                        <div class="card_des">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card_about">
                        <div class="card_head">
                            <div class="card_title space_md d-flex justify-content-center">
                                <div class="card_image">
                                    <img class="w-100" src="assets/img/icon_aboutus/certificate-white.svg" id="certificate" alt="Chứng chỉ quốc tế">
                                </div>
                            </div>
                            <h3 class="text-center">Chứng chỉ quốc tế</h3>
                        </div>
                        <div class="card_des">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="card_about">
                        <div class="card_head">
                            <div class="card_title space_md d-flex justify-content-center">
                                <div class="card_image">
                                    <img class="w-100" src="assets/img/icon_aboutus/cup-white.svg" id="tien-phong" alt="Tiên phong công nghệ">
                                </div>
                            </div>
                            <h3 class="text-center">Tiên phong công nghệ</h3>
                        </div>
                        <div class="card_des">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_slide slide">
            <div class="container">
                <div class="row">
                    <div class="title_content text-center space_xl">
                        <h2 class="title text_white">TMAS</h2>
                    </div>
                    <div class="col-lg-4 col-12 space_md">
                        <div class="card_about card_about_m">
                            <div class="card_head">
                                <div class="card_title space_md d-flex justify-content-center">
                                    <div class="card_image">
                                        <img class="w-100" src="assets/img/icon_aboutus/nhan-vien-white.svg" id="nhan-vien" alt="Đội ngũ chuyên nghiệp">
                                    </div>
                                </div>
                                <h3 class="text-center">Đội ngũ chuyên nghiệp</h3>
                            </div>
                            <div class="card_des">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12 space_md">
                        <div class="card_about card_about_m">
                            <div class="card_head">
                                <div class="card_title space_md d-flex justify-content-center">
                                    <div class="card_image">
                                        <img class="w-100" src="assets/img/icon_aboutus/so-mot-white.svg" id="so-mot" alt="Số 1 công nghệ lái xe an toàn">
                                    </div>
                                </div>
                                <h3 class="text-center">Công nghệ hàng đầu</h3>
                            </div>
                            <div class="card_des">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12 space_md">
                        <div class="card_about card_about_m">
                            <div class="card_head">
                                <div class="card_title space_md d-flex justify-content-center">
                                    <div class="card_image">
                                        <img class="w-100" src="assets/img/icon_aboutus/bao-hanh-white.svg" id="bao-hanh" alt="Bảo hành điện tử">
                                    </div>
                                </div>
                                <h3 class="text-center">Bảo hành chính hãng</h3>
                            </div>
                            <div class="card_des">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>