<section class="section section_banner">
    <div class="slide banner_homepage" id="banner_slide_search_product" data-anchor="banner_slide_search_product">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="content_banner_left">
                        <div>
                            <div class="box_title_banner">
                                <p class="title_banner">TMAS thay đổi slogan</p>
                            </div>
                            <div class="box_description_banner">
                                <p class="description_banner">Với sứ mệnh dẫn đầu công nghệ</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="content_banner_right">
                        <div class="box_view_image">
                            <img src="assets\img\homepage\banner/banner_slogan.svg" alt="slogan">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slide banner_homepage" id="banner_slide_find_product_by_model" data-anchor="banner_slide_find_product_by_model">
        <a href="/tin-tuc/386/nang-tam-tien-ich-nhan-doi-trai-nghiem-chuong-trinh-khuyen-mai-tri-an-hap-dan-chua-tung-co"></a>
    </div>
    <div class="slide banner_homepage" id="banner_slide_launcher" data-anchor="banner_slide_launcher_v3">
        <div class="slide_launcher">
            <div class="box_title_banner">
                <p class="title_banner text-center">Giao diện SIGNATURE V3 mới</p>
            </div>
            <div class="box_title_banner">
                <p class="title_banner text-center">Tự do tuỳ biến - tạo phong cách riêng!</p>
            </div>
        </div>
    </div>

    <div>
        <!-- <div id="hotspot-example" class="cloudimage-360" data-folder="https://alkvuowvrr.cloudimg.io/_my_/" data-filename-x="car360ThienMinh%20({index}).png" data-amount-x="49" data-spin-reverse data-responsive="scaleflex" data-hotspots>
        </div> -->
        <div id="hotspot-example" class="cloudimage-360" data-folder="https://alkvuowvrr.cloudimg.io/_car_/" data-filename-x="car_{index}.png" data-amount-x="49" data-spin-reverse data-responsive="scaleflex" data-hotspots>
        </div>

    </div>
    <h3 class="close" id="close_360">close</h3>
</section>


<!-- <section class="section_banner">
    <div class="banner_image">
        <img src="assets/img/banner.png" alt="banner">
    </div>
    
</section> -->

