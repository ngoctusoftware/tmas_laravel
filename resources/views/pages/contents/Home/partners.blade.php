<section class="section section_container section_partner home_page">
    <div class="title_content text-center space_lg">
        <h2 class="title text_dark">Đối tác chiến lược</h2>
        <!-- <p class="text_dark">TMAS luôn làm việc cùng những công ty đi đầu trong lĩnh vực an toàn xe hơi</p> -->
    </div>
    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-4">
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/xfd.svg" alt="xfd">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/360.png" alt="360">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/mobileye.svg" alt="mobileye">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/tiremagic.svg" alt="tire">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/dr.door.svg" alt="door">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/aod.svg" alt="aod">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/cartizan.svg" alt="aod">
                </div>
                <div class="partner_image slide">
                    <img class="w-100" src="assets/img/partners/digitalcarkey.svg" alt="aod">
                </div>
            </div>
        </div>
</section>