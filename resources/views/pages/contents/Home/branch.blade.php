<section class="section_container section_address home_page">
    <div class="container">
        <div class="title_content row">
            <div class="col-12">
                <h2 class="title text_logo">Đại lý & showroom</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 tablet_hide">
                <div id="viewDiv"></div>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="text-center block_spacing big_cat" id="dai-ly">Đại lý toàn quốc</h2>
                    </div>
                    <div class="col-lg-6">
                        <h2 class="text-center block_spacing big_cat" id="ttnt">Trung tâm nội thất</h2>
                    </div>
                </div>
                <div class="address_contain">
                    <div class="active title_address" id="mienbac">
                        <h3 class="title_address_text text_white">Miền bắc</h3>
                    </div>
                    <div class="title_address"id="mientrung">
                        <h3 class="title_address_text text_white">Miền trung</h3>
                    </div>
                    <div class="title_address"id="miennam">
                        <h3 class="title_address_text text_white">Miền nam</h3>
                    </div>
                </div>
                <div class="showing_store">
                    <div class="row address_store">
                        <div class="col-12 d-flex justify-content-center">
                            <div class="address_box_selected">
                                <h3 id="address_selected">Miền bắc</h3>
                            </div>
                        </div>
                        <div id="fill_list_address"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>