
@extends('pages.layouts.layout')

@section('title','TMAS Việt Nam')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/home-page.css">
<link rel="stylesheet" type="text/css" href="assets/lib/slick.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.css">
<link rel="stylesheet" href="assets/styles/scroll_full_page.css">
@endpush
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, màn hình android, màn liền cam cho ô tô, đề nổ từ xa, cốp điện">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn">
<meta property="og:description" content="TMAS - CÔNG NGHỆ TIẾN TỚI TƯƠNG LAI">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/public/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, màn hình android, màn liền cam cho ô tô, đề nổ từ xa, cốp điện">
@endpush

@section('content')
@include('pages.contents.home.banner')
@include('pages.contents.home.about')
@include('pages.contents.home.type_products', ['listCategory' => $listCategory])
@include('pages.contents.home.news', ['dataNews' => $dataNews])
@include('pages.contents.home.contact')
@include('pages.contents.home.partners')
@endsection
@push ('after-scripts')
<script src="assets/lib/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/vendors/jquery.easings.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/vendors/scrolloverflow.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.min.js"></script>
<script type="text/javascript" src="assets/lib/slick1.5.8.js"></script>
<script src="assets/js/scroll_full_page.js"></script>
<script src="assets/js/home_page.js"></script>

@endpush
