<section class="section section_container section_qa home_page">
    <div class="title_content text-center space_lg">
        <h2 class="title text_white">Hỏi đáp cùng TMAS</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 ">
                <div class="box_img_qa w-100">
                    <img class="w-100" src="assets/img/homepage/qa.png" alt="Q&A">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="list_qa">
                    <div class="card_qa">
                        <div class="border_qa space_sm">
                            <p>Trở thành đại lý của TMAS, cần đáp ứng những yêu cầu gì?</p>
                        </div>
                        <div class="answer space_sm">
                            <p>Để trở thành đối tác chiến lược của tmas, Quý khách cần có kinh doanh các mặt hàng </p>
                        </div>
                    </div>
                    <div class="card_qa ">
                        <div class="border_qa space_sm">
                            <p>Chính cách dành cho đại lý như thế nào?</p>
                        </div>
                        <div class="answer space_sm">
                            <p>Danh sách các </p>
                        </div>
                    </div>
                    <div class="card_qa ">
                        <div class="border_qa space_sm">
                            <p>Danh sách các đại lý, trung tâm nội thất của TMAS</p>
                        </div>
                        <div class="answer space_sm">
                            <p>Danh sách các </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>