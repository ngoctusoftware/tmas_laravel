@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/san-pham">
<meta property="og:description" content="Sản phẩm của TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
@endpush
@section('title','Sản phẩm')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/products.css">
@endpush
@push ('after-scripts')
<script src="assets/js/descat.js"></script>
<script src="assets/js/products.js"></script>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
@endpush
@section('content')
@include('pages.contents.container.findproductcar')
@include('pages.contents.branch.branch')
@include('pages.layouts.breadcrum')
<section class="section_container section_type_product">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="box_category desktop">
                    <p id="0" class="cat_id active">Tất cả sản phẩm</p>
                    @foreach($listCategory as $key => $item)
                    <p id="{{$item->id}}" class="cat_id " data="{{$item->slug}}">{{$item->name}}</p>
                    @endforeach
                </div>
                <div class="box_category space_lg mobile">
                    <select class="select_box_find_car" name="select_cat" id="category_select">
                        <option value="0" class="cat_id">Tất cả sản phẩm</option>
                        @foreach($listCategory as $key => $item)
                        <option value="{{$item->id}}" class="cat_id " data="{{$item->slug}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-8 col-12">
                <div id="loading"></div>
                <div class="box_product">

                </div>
                <div class="content_category">

                </div>
            </div>
        </div>
    </div>
</section>
@include('pages.contents.container.desCategories')
@include('pages.contents.container.newrelated')
@endsection