@extends('pages.layouts.layout')

@section('title','Tin tức')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/products.css">
@endpush
@section('content')
<section class="section_banner_child_page">
    <div class="banner_image">
        <img src="assets/img/banner.png" alt="banner">
    </div>
</section>
<section class="section_new_detail section_container">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-6 col-sm-12">
                <div class="detail_new_box_content">
                    <h2 class="title title_new text_logo">{{$dataDetail->name}}</h2>
                    <div class="body_content_detail">
                        {!! $dataDetail->content !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12">
                @foreach($listPostRelated->toArray() as $item)
                <div class="col-12 news_item_related">
                    <div class="image_news_related">
                        <a href="tin-tuc/{{$item->id}}/{{$item->slug}}">
                            <img src="{{$item->featured_image}}" alt="{{$item->slug ? $item->slug : $item->name}}">
                        </a>
                    </div>
                    <div class="news_item_box_content_related">
                        <div class="news_item_title_related">
                            <a href="tin-tuc/{{$item->id}}/{{$item->slug}}">
                                <h3>{{$item->name}}</h3>
                            </a>
                        </div>
                        <div class="news_item_description_related">
                            <p>{{$item->description}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection