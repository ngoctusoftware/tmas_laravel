@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="{{$detailProducts->meta_description}}">
<meta property="og:title" content="{{$detailProducts->meta_title}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{$detailProducts->meta_og_url}}">
<meta property="og:description" content="{{$detailProducts->meta_description}}">
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/{{$detailProducts->image}}">
<meta name="keywords" content="{{$detailProducts->meta_keywords}}">
@endpush
@section('title', $detailProducts->name)
@push ('after-styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.css">
<link rel="stylesheet" href="assets/styles/product-detail.css">
@endpush
@push ('after-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/vendors/jquery.easings.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/vendors/scrolloverflow.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.min.js"></script>
<script src="assets/js/productDetail.js"></script>
@endpush
@section('content')
@php
$str = $detailProducts->images;
$position = strlen($detailProducts->images) - 2;
if ($detailProducts->images[strlen($detailProducts->images) - 2] == ',') {
$str = substr($str, 0, $position) . substr($str, $position + 1);
}
$detailProducts->images = json_decode($str);
@endphp
<!-- <section class="section section_banner_child_page">
    <div class="banner_image_product">
        <div class="title_content">
            <h1 class="title text_white text-center">{{$detailProducts->name}}</h1>
        </div>
    </div>
</section> -->
<section class="section section_product_detail section_container section_banner_child_page">
    <div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-12 space_sm_m">
                    <div class="box_image_product">
                        <div class="main_image">
                            <img src="" class="d-block w-100" alt="image product">
                        </div>
                        <div class="item_image custom-scroll">
                            @foreach($detailProducts->images as $key => $item )
                            @php
                            if (env('APP_ENV') == 'local') {
                            $item = str_replace('/public', "", $item);
                            }
                            @endphp
                            <div class=" slide-item {{$key == 0 ? 'active' : ''}}">
                                <img src="{{$item}}" class="d-block w-100" alt="image product">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 ">
                    <div class="box_des_product">
                        <h2 class="title name_product space_sm text_dark space_sm">{{$detailProducts->name}}</h2>
                        <p class="text_dark space_sm des_prod">{{$detailProducts->description}}</p>
                        <div class="detail_product_option d-flex space_sm">
                            <div class="option_type">
                                <label for="phan-loai">Phân loại sản phẩm</label>
                                <select name="phan-loai" id="phan-loai">
                                    @foreach(json_decode($detailProducts->optionName) as $key => $item )
                                    <option class="option option_{{$key}}" value="{{$key}}">{{$item->optionName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <a class="link_to_detail" href="{{$_SERVER['REQUEST_URI']}}#thong-so">Xem mô tả sản phẩm</a>
                            <a class="link_to_detail" href="{{$_SERVER['REQUEST_URI']}}#thong-so/thong-so-ki-thuat">Xem thông số kỹ thuật</a>
                            <a class="link_to_detail" href="{{$_SERVER['REQUEST_URI']}}#thong-so/dac-diem-noi-bat">Xem đặc điểm nổi bật</a>
                            <a class="link_to_detail" href="{{$_SERVER['REQUEST_URI']}}#thong-so/dong-xe">Xem các dòng xe có thể lắp đặt</a>
                        </div>
                        <div class="d-flex price_warranty">
                            <div class="d-flex flex_item_end">
                                @foreach(json_decode($detailProducts->warranty) as $key => $item )
                                <p class="warranty warranty_{{$key}} {{$key == 0 ? 'active': ''}}">Bảo hành: {{$item->warranty}} tháng</p>
                                @endforeach
                            </div>
                            <div class="detail_product_price space_sm">
                                @foreach(json_decode($detailProducts->price) as $key => $item )
                                <p class="price price_{{$key}} {{$key == 0 ? 'active': ''}}">{{number_format($item->price, 0, ',', '.')}} <span>đ</span></p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('pages.layouts.breadcrum')
</section>
<section class="section section_container">
    <div class="slide" data-anchor="mo-ta-san-pham">
        <div class="container">
            <div class="row">
                <div class="title_des">
                    <h2 class="text_dark">Mô tả sản phẩm</h2>
                </div>
                <div class="body_content_detail">
                    {!! $detailProducts->contents !!}
                </div>
            </div>
        </div>
    </div>
    <div class="slide" data-anchor="thong-so-ki-thuat">
        <div class="container">
            <div class="row">
                <div class="title_des">
                    <h2 class="text_dark">Thông số kỹ thuật</h2>
                </div>
                <div class="body_content_detail">
                    {!! $detailProducts->contents !!}
                </div>
            </div>
        </div>
    </div>
    <div class="slide" data-anchor="dac-diem-noi-bat">
        <div class="container">
            <div class="row">
                <div class="title_des">
                    <h2 class="text_dark">Đặc điểm nổi bật</h2>
                </div>
                <div class="body_content_detail">
                    {!! $detailProducts->contents !!}
                </div>
            </div>
        </div>
    </div>
    <div class="slide" id="dong-xe" data-anchor="dong-xe">
        <div class="container">
            <div class="row">
                <div class="title_des">
                    <h2 class="text_dark">Dòng xe lắp đặt</h2>
                </div>
                <div class="car_model_product row">
                    
                </div>
            </div>
        </div>
        <div class="pop_model_select">
            <div class="card_head">
                <p class="name_company"></p>
                <p class="close">Đóng</p>
            </div>
            <div class="pop_model_select_body"></div>
        </div>
    </div>
</section>
@include('pages.contents.container.productrelated')

@endsection