@extends('pages.layouts.layout')
@push ('after-meta')
<meta name="author" content="TMAS">
<meta name="description" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
<meta property="og:title" content="TMAS" />
<meta property="og:type" content="website" />
<meta property="og:url" content="tmas.vn/san-pham">
<meta property="og:description" content="Sản phẩm của TMAS">
<meta property="og:site_name" content="TMAS" />
<meta property="og:image" content="{{$_SERVER['SERVER_NAME']}}/assets/img/meta_og_image/homepage.png">
<meta name="keywords" content="tmas, TMAS, tmas việt nam, cảm biến, xe hơi, thông minh, an toàn">
@endpush
@section('title','Sản phẩm')
@push ('after-styles')
<link rel="stylesheet" href="assets/styles/products.css">
@endpush
@push ('after-scripts')
<script src="assets/js/products.js"></script>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
@endpush
@section('content')
@include('pages.contents.container.findproductcar')
@include('pages.layouts.breadcrum')
<section class="section_container section_type_product">
   
</section>
@include('pages.contents.container.newrelated')
@endsection