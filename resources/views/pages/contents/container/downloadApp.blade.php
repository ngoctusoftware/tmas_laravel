@push ('after-styles')
<link rel="stylesheet" href="assets/styles/download-app.css">
@endpush

@push ('after-scripts')
@endpush

<section class="section_download_app">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 center_vertical">
                <div class="">
                    <div class="d-flex box_logo_tmas">
                        <div class="">
                            <div class="box_logo_app">
                                <img class="w-100" src="assets/img/download_app/logo-app.svg" alt="download app tmas">
                            </div>
                            <p class="dowload_text">Tải ngay ứng dụng TMAS</p>
                        </div>
                    </div>

                    <div class="box_item_app d-flex">
                        <a class="item_app_footer" href="/download-android" target="_blank">
                            <img class="w-100" src="assets/img/footer/android.svg" alt="google play ">
                        </a>
                        <a class="item_app_footer" href="/download-ios" target="_blank">
                            <img class="w-100" src="assets/img/footer/apple.svg" alt="apple store">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 center_vertical">
                <div class="download_app_box_mar desktop">
                    <p>- Bảo hành điện tử</p>
                    <p>- Theo dõi thông tin sản phẩm</p>
                    <p>- Nhận nhiều ưu đãi từ TMAS</p>
                    <p>- Cập nhật các thông tin mới nhất về công nghệ an toàn xe </p>
                </div>
            </div>
        </div>
    </div>
</section>