@push ('after-styles')
<link rel="stylesheet" href="assets/styles/find-product-car.css">
@endpush
@push ('after-scripts')
<script src="assets/js/find-product-car.js"></script>
@endpush
<section class="section_search_car_product">
    <div class="container">
        <h2 class="text_white space_lg text-center">TÌM KIẾM SẢN PHẨM THEO DÒNG XE</h2>
        <div class="row" id="filter">
            <div class="col-md-4 col-6 space_md">
                <select class="select_box_find_car" id="select_company">

                </select>
            </div>
            <div class="col-md-4 col-6 space_md">
                <select class="select_box_find_car" id="select_model">

                </select>
            </div>
            <div class="col-md-4 col-12">
                <button class="btn_find w-100" id="button_search_product_by_car" data-bs-toggle="modal" data-bs-target="#modal_result">Tìm kiếm sản phẩm</button>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="modal_result" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_result_label"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="" id="result_product">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng cửa sổ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>