@push ('after-styles')
<link rel="stylesheet" href="assets/styles/new-related.css">
@endpush

@push ('after-scripts')
<script src="assets/js/newRelated.js"></script>
@endpush

<section class="section_container section_related">
    <div class="container">
        <div class="title_content text-center space_lg">
            <h2 class="title text_white">Bài viết gần đây</h2>
            <p class="text_white">Theo dõi TMAS để cập nhật thông tin mới nhất về công nghệ và sản phẩm</p>
        </div>
        <div class="list_new_related row space_lg" id="new_related"></div>
        <div class="d-flex w-100 justify-content-center">
            <a class="btn_view_more" href="/tin-tuc">Xem thêm +</a>
        </div>
    </div>
</section>