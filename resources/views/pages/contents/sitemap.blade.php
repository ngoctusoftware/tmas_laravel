<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($arrayMenu as $item)
        <url>
            <loc>{{ url('/') }}/{{ $item }}</loc>
            <lastmod>2022-09-01 09:00:00</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @foreach ($siteMapPost as $item)
        <url>
            <loc>{{ url('/') }}/tin-tuc/{{ $item->id }}/{{ $item->slug }}</loc>
            <lastmod>{{ $item->created_at }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
    @foreach ($siteMapProduct as $item)
        <url>
            <loc>{{ url('/') }}/san-pham/{{ $item->id }}/{{ $item->slug }}</loc>
            <lastmod>{{ $item->created_at }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>